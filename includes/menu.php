<div class="contenitore-topbar">
    <div class="container">
        
            <div class="col-md-6 topbar-sx">
                 <?php echo easymenu(5, 'class="nav navbar-nav"'); ?>
            </div>
            
            <div class="col-md-6 topbar-dx">
            	<div class="col-md-2 contenitore-area-riservata">
                    <a class="icona-social" href=""><i class="fa fa-user-o" aria-hidden="true"></i></a>
                </div>
            	<div class="col-md-2 contenitore-social">
                    <a class="icona-social" href=""><i class="fa fa-vimeo" aria-hidden="true"></i></a>
                    <a class="icona-social" href=""><i class="fa fa-instagram" aria-hidden="true"></i></a>
                    <a class="icona-social" href=""><i class="fa fa-twitter" aria-hidden="true"></i></a>
                    <a class="icona-social" href=""><i class="fa fa-facebook" aria-hidden="true"></i></a>
                </div>
            </div>
        
    </div>
</div>

<div class="clear"></div>

<div class="container contenitore-menu">
    <div class="row">
        
        <nav class="navbar navbar-default">
          <div class="container-fluid no-padding">
          
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header no-margin no-padding">
              <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#esplodi-menu" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
              </button>
              <a id="logo" data-rjs="3" class="navbar-brand" href="/">
              </a>
            </div>
            
            <div class="collapse navbar-collapse no-padding" id="esplodi-menu">
                
                <?php echo easymenu(1, 'class="nav navbar-nav menu-principale"'); ?>
                
            </div><!-- /.navbar-collapse -->
          </div><!-- /.container-fluid -->
        </nav>
    
    </div>
</div>