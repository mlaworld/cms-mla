<?php
/**
 * Created da Giuseppe Alessandro De Blasio.
 * User: Giuseppe Alessandro De Blasio
 * Date: 27/04/17
 * Time: 16:54
 */
include '../admin/includes/init.php';

$eventi = Eventi::view_front_end_ajax();


$contaEventi = Eventi::conta_eventi();

if($contaEventi > 0 AND !empty($eventi)):

?>


 <?php foreach ($eventi as $evento): ?>
    <div class="singolo-evento" id="accordion" role="tablist" aria-multiselectable="false">
        <a class="singolo-evento-collapse" role="button" data-toggle="collapse" data-parent="#accordion" href="#evento<?php echo $evento->id; ?>" aria-expanded="false" aria-controls="evento<?php echo $evento->id ?>">
            <div class="giorno-cal-evento">
                <img src="images/cal-sing-evento.gif" alt="">
                <span class="num-cal-evento"><?php echo ritorna_giorno($evento->data_inizio); ?></span>
            </div>
            <span class="citta-evento"><?php echo $evento->localita; ?></span>
            <span class="data-evento"><?php $old_date_timestamp = strtotime($evento->data_inizio); $nuovaData = date('l d F', $old_date_timestamp); echo data_eventi_home($nuovaData); ?></span>
            <span class="slash-evento">/ </span>
            <span class="tipo-evento"><?php $cat_evento = categoriaEventi::seleziona_per_id($evento->id_cat_eventi); echo $cat_evento->titolo_categoria;  ?></span>
            <a href="<?php echo $evento->link_easy; ?>" target="_blank" class="iscriviti-evento">Iscriviti</a>
        </a>
        <div id="evento<?php echo $evento->id; ?>" class="panel-collapse collapse singolo-evento-contenuto" role="tabpanel" aria-labelledby="headingTwo">
            <div class="panel-body">
                <p><?php echo $evento->breve_descrizione; ?></p>
            </div>
        </div>
    </div>
<?php endforeach; ?>

    <?php else: ?>
    <p id="empity-event" data-value="0">Nessun Altro Evento</p>

<?php endif; ?>




