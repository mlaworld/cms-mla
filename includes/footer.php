<div class="clear"></div>
    
    <footer>
    	<div class="container container-footer">

            <div class="col-xs-4 col-md-2 colonna-footer-admin">
            
            	<div class="no-padding">
                	<a href="#" class="admin-footer">Area riservata</a>
                </div>
                
                <div class="no-padding select-footer-lingua">
                	<div class="container-select-lingua">
                        <select class="select-lingua" name="" id="">
                            <option value="">Italiano</option>
                            <option value="">Inglese</option>
                        </select>
                        <span class="arrow-select">
                            <img src="images/select-arrow.png" alt="" />
                        </span>
                    </div>
                </div>
                
            </div>
            
            <div class="col-xs-4 col-md-2 colonna-footer">
            	<span class="titolo-colonna-footer">Move Language Ahead</span>
                <ul>
                	<li><a class="link-footer" href="#">Chi siamo</a></li>
                	<li><a class="link-footer" href="#">Contatti</a></li>
                	<li><a class="link-footer" href="#">Lavora con noi</a></li>
                	<li><a class="link-footer" href="#">Condizioni Generali</a></li>
                	<li><a class="link-footer" href="#">Termini e Condizioni</a></li>
                	<li><a class="link-footer" href="#">Privacy e Cookies</a></li>
                	<li><a class="link-footer" href="#">Partnership</a></li>
                	<li><a class="link-footer" href="#">Mappa del sito</a></li>
                </ul>
            </div>
            
            <div class="col-xs-4 col-md-2 colonna-footer">
            	<span class="titolo-colonna-footer">College più richiesti</span>
                <ul>
                	<li><a class="link-footer" href="#">Brunel University Uxbridge</a></li>
                	<li><a class="link-footer" href="#">Heriot Watt University</a></li>
                	<li><a class="link-footer" href="#">Dublin City University</a></li>
                    <li><a class="link-footer" href="#">Griffith College</a></li>
                	<li><a class="link-footer" href="#">Southbank University</a></li>
                	<li><a class="link-footer" href="#">New York Ramapo</a></li>
                	<li><a class="link-footer" href="#">Pitzer College</a></li>
                    <li><a class="link-footer" href="#">Nova Southeastern</a></li>
                </ul>
            </div>  
            
            <div class="col-xs-4 col-md-2 colonna-footer">
            	<span class="titolo-colonna-footer">Paesi più richiesti</span>
                <ul>
                	<li><a class="link-footer" href="#">Inghilterra</a></li>
                	<li><a class="link-footer" href="#">Scozia</a></li>
                	<li><a class="link-footer" href="#">Irlanda</a></li>
                	<li><a class="link-footer" href="#">Malta</a></li>
                	<li><a class="link-footer" href="#">Stati Uniti</a></li>
                	<li><a class="link-footer" href="#">Francia</a></li>
                	<li><a class="link-footer" href="#">Spagna</a></li>
                    <li><a class="link-footer" href="#">Germania</a></li>
                </ul>
            </div>  
            
            <div class="col-xs-4 col-md-2 colonna-footer">
            	<span class="titolo-colonna-footer">Città più richieste</span>
                <ul>
                	<li><a class="link-footer" href="#">Londra</a></li>
                	<li><a class="link-footer" href="#">Edimburgo</a></li>
                	<li><a class="link-footer" href="#">Dublino</a></li>
                    <li><a class="link-footer" href="#">Worcester</a></li>
                    <li><a class="link-footer" href="#">Leeds</a></li>
                	<li><a class="link-footer" href="#">New York</a></li>
                	<li><a class="link-footer" href="#">Miami</a></li>
                	<li><a class="link-footer" href="#">Los Angeles</a></li>
                </ul>
            </div>   
            
            <div class="col-xs-4 col-md-2 colonna-footer">
            	<span class="titolo-colonna-footer">Seguici su</span>
                <ul>
                	<li><a class="link-footer" href="#">Facebook</a></li>
                    <li><a class="link-footer" href="#">Twitter</a></li>
                    <li><a class="link-footer" href="#">Instagram</a></li>
                    <li><a class="link-footer" href="#">Vimeo</a></li>
                    <li><a class="link-footer" href="#">Blog</a></li>
                    <li><a class="link-footer" href="#">Mlascuola</a></li>
                </ul>
            </div>
            
            <div class="clear"></div>
            
            <div class="col-md-12 copyright">
            	<img src="images/logo_footer.png" alt="" />
                <p>è un marchio di The Golden Globe srl dal 1976, tutti i diritti riservati, P.I. 01557500632, autorizzazione regionale ADV cat. A n.5564, IATA n.38/20506-4, capitale sociale euro 100.000.00</p>
            </div>
            
        </div>   
<!--    	<script async src="js/jquery-2.2.4.min.js"></script>-->
        <script src="js/custom.js"></script>
		<script src="js/bootstrap.min.js"></script>
        <script src="js/swiper.js"></script>
        <script src="js/swiper.jquery.js"></script>
        <script src="js/jquery.swipebox.js"></script>
        <script src="js/validetta.min.js"></script>
    </footer>