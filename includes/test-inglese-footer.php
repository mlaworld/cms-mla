<div class="clear"></div>
     
     <section>
     	<div class="container container-elementi-home">
        
        	<hr>
            <div class="col-md-3 colonna-home-sx">
            	<h4 class="titolo-home-sx">Fai il tuo test gratuito</h4>
            </div>
            
            <div class="col-md-9 colonna-home-dx">
            	<img class="img-test-inglese" src="images/test_inglese.jpg" alt="test gratuito inglese" />
                <span class="titolo-test-inglese">Verificate online il vostro livello linguistico in pochi minuti!</span>
                <a class="link-test-inglese" href="#">Fai il test</a>
            </div>
        
        </div>
     </section>