<?php include 'admin/includes/init.php'; ?>
<?php include 'admin/menu/easymenu.php'; ?>
<!DOCTYPE HTML>
<html lang="it">

<head>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="content-language" content="it">
    <BASE href="http://cms.mlaworld.com">
    <script type="text/javascript" src="js/jquery-2.2.4.min.js"></script>
    <link rel="stylesheet" href="css/bootstrap.css">
    <link href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700" rel="stylesheet">
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/swiper.css">
    <link rel="stylesheet" href="css/font-awesome.css">
    <link rel="stylesheet" href="css/swipebox.css">
    <link rel="stylesheet" href="css/jquery.fancybox.min.css">
    <link rel="stylesheet" href="css/validetta.min.css">

    <noscript>
        <style type="text/css">
            .pagecontainer {display:none;}
        </style>
        <div class="noscriptmsg">
            <meta http-equiv="refresh" content="0;url=no-javascript">
        </div>
    </noscript>