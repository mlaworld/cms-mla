<div class="clear"></div>
     
     <section>
     	<div class="container container-elementi-home">
        
        	<hr>
            <div class="col-md-3 colonna-home-sx">
            	<h4 class="titolo-home-sx">Scopri gli incontri<br>informativi</h4>
            </div>
            
            <div class="col-md-9 colonna-home-dx no-padding-bottom">
            	<span class="titolo-evento">MLA vi incontra! Incontrateci nella vostra città durante le nostre giornate informative.</span>

                <div id="event-ajax" style="display: none"></div>
                <div id="event-list">
                <?php foreach ($eventi as $evento): ?>
                        <div class="singolo-evento" id="accordion" role="tablist" aria-multiselectable="false">
                            <a class="singolo-evento-collapse" role="button" data-toggle="collapse"
                               data-parent="#accordion" href="#evento<?php echo $evento->id; ?>" aria-expanded="false"
                               aria-controls="evento1<?php echo $evento->id ?>">
                                <div class="giorno-cal-evento">
                                    <img src="images/cal-sing-evento.gif" alt="">
                                    <span
                                        class="num-cal-evento"><?php echo ritorna_giorno($evento->data_inizio); ?></span>
                                </div>
                                <span class="citta-evento"><?php echo $evento->localita; ?></span>
                                <span class="data-evento"><?php $old_date_timestamp = strtotime($evento->data_inizio);
                                    $nuovaData = date('l d F', $old_date_timestamp);
                                    echo data_eventi_home($nuovaData); ?></span>
                                <span class="slash-evento">/ </span>
                                <span
                                    class="tipo-evento"><?php $cat_evento = categoriaEventi::seleziona_per_id($evento->id_cat_eventi);
                                    echo $cat_evento->titolo_categoria; ?></span>
                                <a href="<?php echo $evento->link_easy; ?>" target="_blank" class="iscriviti-evento">Iscriviti</a>
                            </a>
                            <div id="evento<?php echo $evento->id; ?>"
                                 class="panel-collapse collapse singolo-evento-contenuto" role="tabpanel"
                                 aria-labelledby="headingTwo">
                                <div class="panel-body">
                                    <p><?php echo $evento->breve_descrizione; ?></p>
                                </div>
                            </div>
                        </div>
                <?php endforeach; ?>
                    <div id="event-no-list"></div>
                </div>
                <a id="clickEventi" class="visualizza-calendario-eventi">Visualizza tutto il calendario</a>
                <a id="clickEventiNasc" class="visualizza-calendario-eventi" style="display: none;">Nascondi Eventi</a>
            </div>
        
        </div>
     </section>

<script>
//    $("#clickEventi").click(function() {
//        $('#event-no-list').show();
//        $('#clickEventiNasc').show();
//        $(this).hide();
//
//    });
//
//    $('#clickEventiNasc').on('click', function () {
//            $('#clickEventiNasc').hide();
//            $('#event-no-list').hide();
//            $("#clickEventi").show();
//    });
//
//    if($('#empity-event').length ==1){
//        console.log('true');
//    }
//
//    $(document).ready(function(){
//
//
//        $("#clickEventi").one('click', function() {
//
//            $.ajax({
//                url : 'includes/eventi-ajax.php',
//                type: 'GET',
//                dataType: 'html',
//                success: function(data){
//
//                    $('#clickEventi').addClass('allEvent');
//
//
//                        $('#event-no-list').html(data);
//
//                        $('#clickEventi').hide();
//
//                    if ($("#empity-event")[0]){
//
//                        $(".visualizza-calendario-eventi").hide();
//
//                    }
//
//                }
//            });
//        });
//    });
</script>