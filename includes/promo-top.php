<?php
/**
 * Created da Giuseppe Alessandro De Blasio.
 * User: Giuseppe Alessandro De Blasio
 * Date: 06/04/17
 * Time: 10:55
 */
?>
<style>
    .banner-promo {
        bottom: 0;
        left: 0;
        margin: 0 auto;
        min-width: 980px;
        height: 40px;
        background-color: lightgrey;
    }

</style>

<div class="banner-promo text-center">

</div>
<div id="change" class="banner-open" data-name="show"><i id="toggleIco" class="fa fa-sort-desc"></i></div>

<script>
    $(document).ready(function(){
        $("#change").click(function () {
            if ($(this).attr("data-name") == 'show') {
                $(".banner-promo").animate({
                    height: '700px'
                }).show();
                $('.banner-open').attr("data-name", "hide");
                $('#toggleIco').addClass('fa-close');
                $('#toggleIco').removeClass('fa-sort-desc');
            } else {
                $(".banner-promo").animate({
                    height: '40px'
                }).show();
                $('.banner-open').attr("data-name", "show");
                $('#toggleIco').removeClass('fa-close');
                $('#toggleIco').addClass('fa-sort-desc');
            }
        });
    });
</script>