<?php $feeds = simplexml_load_file('cache/blog.xml.cache');?>
<div class="clear"></div>
<section>
    <div class="container container-blog">
        <span class="titolo-section">Ultime dal <strong>blog</strong></span>

        <div class="swiper-container-4">
            <div class="swiper-wrapper">

                <?php foreach ($feeds->channel->item as $feed): ?>
                    <?php
                    $titolo       = (string) $feed->title;
                    $descrizione  = $feed->description;
                    $link         = (string) $feed->link;
                    $posizione = strpos($descrizione, '[&#8230;]');
                    ?>
                    <div class="swiper-slide box-blog" onclick="location.href='<?php echo $link; ?>'">
                        <div class="contenitore-singolo-feed">
                            <h4><?php echo $titolo; ?></h4>
                            <p class="more-blog"><?php echo leggi_di_piu_feed($descrizione, $posizione, $link); ?></p>
                        </div>
                    </div>
                <?php endforeach; ?>
            </div>
        </div>


    </div>
</section>