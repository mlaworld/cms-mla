<?php
/**
 * Created da Giuseppe Alessandro De Blasio.
 * User: Giuseppe Alessandro De Blasio
 * Date: 22/09/17
 * Time: 10:38
 */
include '../admin/includes/init.php';

$errori = array();

//if(isset($_POST['invioContatti'])){

    if(verifica_campi_string($_POST['nome'])){
        $nome = ucwords($_POST['nome']);
    } else{
        $errori[] = "Nome non inserito correttamente";
    }

    if(verifica_campi_string($_POST['cognome'])){
        $cognome = ucwords($_POST['cognome']);
    } else{
        $errori[] = "Cognome non inserito correttamente";
    }

    if(verifica_data($_POST['dataNascita'], 'Y-m-d')){
        $dataNascita = $_POST['dataNascita'];
    } else {
        $errori[] = "Data non inserita correttamente";
    }

    if (!empty($_POST['indirizzo'])){
        $indirizzo = $_POST['indirizzo'];
    } else{
        $errori[] = "Inserire l'indirizzo";
    }


    if(verifica_campi_string($_POST['citta'])){
        $citta = $_POST['citta'];
    } else{
        $errori[] = "Città non inserita correttamente";
    }

    if(verifica_email($_POST['email'])){
        $email = $_POST['email'];
    } else{
        $errori[] = "Email non corretta";
    }

//    if(verifica_campi_numero( $_POST['telefono'])){
    $telefono = $_POST['telefono'];
//    } else{
//        $errori[] = "Numero di telefono errato";
//    }

    if(verifica_campi_numero( $_POST['cellulare'])){
        $cellulare = $_POST['cellulare'];
    } else{
        $errori[] = "Numero di cellulare errato";
    }

    if(!empty($_POST['messaggio'])){
        $messaggio = $_POST['messaggio'];
    } else {
        $errori[] = "Il corpo del messaggio non può essere vuoto";
    }

    if(!isset($_POST['termini_condizioni'])){
        $errori[] = "Si prega di accettare i termini e le condizioni!";
    }

    if(empty($errori)){

        $messageClient =
            "<html xmlns:v=\"urn:schemas-microsoft-com:vml\"
      xmlns:o=\"urn:schemas-microsoft-com:office:office\"
      xmlns:w=\"urn:schemas-microsoft-com:office:word\"
      xmlns:m=\"http://schemas.microsoft.com/office/2004/12/omml\"
      xmlns=\"http://www.w3.org/TR/REC-html40\">

<head>
   <meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\">
    <meta name=ProgId content=Word.Document>
    <meta name=Generator content=\"Microsoft Word 15\">
    <meta name=Originator content=\"Microsoft Word 15\">
    <link rel=File-List href=\"MLA%20Contatti_file/filelist.xml\">
    <link rel=Edit-Time-Data href=\"MLA%20Contatti_file/editdata.mso\">
    <!--[if !mso]>
    <style>
        v\: * {
            behavior: url(#default#VML);
        }

        o\: * {
            behavior: url(#default#VML);
        }

        w\: * {
            behavior: url(#default#VML);
        }

        .shape {
            behavior: url(#default#VML);
        }
    </style>
    <![endif]-->
    <title>Demystifying Email Design</title>
    <link rel=themeData href=\"MLA%20Contatti_file/themedata.thmx\">
    <link rel=colorSchemeMapping href=\"MLA%20Contatti_file/colorschememapping.xml\">
    <style>
        <!--
        /* Font Definitions */
        @font-face {
            font-family: \"Cambria Math\";
            panose-1: 2 4 5 3 5 4 6 3 2 4;
            mso-font-charset: 1;
            mso-generic-font-family: roman;
            mso-font-pitch: variable;
            mso-font-signature: 0 0 0 0 0 0;
        }

        @font-face {
            font-family: Calibri;
            panose-1: 2 15 5 2 2 2 4 3 2 4;
            mso-font-charset: 0;
            mso-generic-font-family: swiss;
            mso-font-pitch: variable;
            mso-font-signature: -536859905 -1073732485 9 0 511 0;
        }

        /* Style Definitions */
        p.MsoNormal, li.MsoNormal, div.MsoNormal {
            mso-style-unhide: no;
            mso-style-qformat: yes;
            mso-style-parent: \"\";
            margin: 0cm;
            margin-bottom: .0001pt;
            mso-pagination: widow-orphan;
            font-size: 12.0pt;
            font-family: \"Times New Roman\", serif;
            mso-fareast-font-family: Calibri;
            mso-fareast-theme-font: minor-latin;
            color: windowtext;
        }

        a:link, span.MsoHyperlink {
            mso-style-noshow: yes;
            mso-style-priority: 99;
            color: #3399CC;
            mso-text-animation: none;
            font-weight: normal;
            font-style: normal;
            text-decoration: none;
            text-underline: none;
            text-decoration: none;
            text-line-through: none;
        }

        a:visited, span.MsoHyperlinkFollowed {
            mso-style-noshow: yes;
            mso-style-priority: 99;
            color: #3399CC;
            mso-text-animation: none;
            font-weight: normal;
            font-style: normal;
            text-decoration: none;
            text-underline: none;
            text-decoration: none;
            text-line-through: none;
        }

        p {
            mso-style-noshow: yes;
            mso-style-priority: 99;
            margin-top: 0cm;
            margin-right: 0cm;
            margin-bottom: 12.0pt;
            margin-left: 0cm;
            mso-pagination: widow-orphan;
            font-size: 9.0pt;
            font-family: \"Arial\", sans-serif;
            mso-fareast-font-family: Calibri;
            mso-fareast-theme-font: minor-latin;
            color: #434242;
        }

        p.msonormal0, li.msonormal0, div.msonormal0 {
            mso-style-name: msonormal;
            mso-style-unhide: no;
            margin-top: 0cm;
            margin-right: 0cm;
            margin-bottom: 12.0pt;
            margin-left: 0cm;
            mso-pagination: widow-orphan;
            font-size: 9.0pt;
            font-family: \"Arial\", sans-serif;
            mso-fareast-font-family: Calibri;
            mso-fareast-theme-font: minor-latin;
            color: #434242;
        }

        p.table-bordered, li.table-bordered, div.table-bordered {
            mso-style-name: table-bordered;
            mso-style-unhide: no;
            margin-top: 7.5pt;
            margin-right: 0cm;
            margin-bottom: 12.0pt;
            margin-left: 0cm;
            mso-pagination: widow-orphan;
            font-size: 9.0pt;
            font-family: \"Arial\", sans-serif;
            mso-fareast-font-family: Calibri;
            mso-fareast-theme-font: minor-latin;
            color: #434242;
        }

        .MsoChpDefault {
            mso-style-type: export-only;
            mso-default-props: yes;
            font-size: 10.0pt;
            mso-ansi-font-size: 10.0pt;
            mso-bidi-font-size: 10.0pt;
        }

        @page WordSection1 {
            size: 612.0pt 792.0pt;
            margin: 70.85pt 2.0cm 2.0cm 2.0cm;
            mso-header-margin: 36.0pt;
            mso-footer-margin: 36.0pt;
            mso-paper-source: 0;
        }

        div.WordSection1 {
            page: WordSection1;
        }

        -->
    </style>
    <!--[if gte mso 10]>
    <style>
        /* Style Definitions */
        table.MsoNormalTable {
            mso-style-name: \"Tabella normale\";
            mso-tstyle-rowband-size: 0;
            mso-tstyle-colband-size: 0;
            mso-style-noshow: yes;
            mso-style-priority: 99;
            mso-style-parent: \"\";
            mso-padding-alt: 0cm 5.4pt 0cm 5.4pt;
            mso-para-margin: 0cm;
            mso-para-margin-bottom: .0001pt;
            mso-pagination: widow-orphan;
            font-size: 10.0pt;
            font-family: \"Times New Roman\", serif;
        }
    </style>
    <![endif]--><!--[if gte mso 9]>
    <xml>
        <o:shapedefaults v:ext=\"edit\" spidmax=\"1026\"/>
    </xml><![endif]--><!--[if gte mso 9]>
    <xml>
        <o:shapelayout v:ext=\"edit\">
            <o:idmap v:ext=\"edit\" data=\"1\"/>
        </o:shapelayout>
    </xml><![endif]-->
</head>

<body bgcolor=\"#F9FAFA\" lang=IT link=\"#3399CC\" vlink=\"#3399CC\"
      style='tab-interval:35.4pt'>

<div class=WordSection1>


    <table class=MsoNormalTable border=0 cellspacing=0 cellpadding=0 width=\"100%\"
           style='width:100.0%;border-collapse:collapse;mso-yfti-tbllook:1184;mso-padding-alt:
 0cm 0cm 0cm 0cm'>
        <tr style='mso-yfti-irow:0;mso-yfti-firstrow:yes;mso-yfti-lastrow:yes'>
            <td style='padding:15.0pt 0cm 22.5pt 0cm'>
                <div align=center>
                    <table class=MsoNormalTable border=0 cellspacing=0 cellpadding=0 width=680
                           style='width:255.0pt;border-collapse:collapse;mso-yfti-tbllook:1184;
   mso-padding-alt:0cm 0cm 0cm 0cm'>
                        <tr style='mso-yfti-irow:0;mso-yfti-firstrow:yes'>
                            <td style='padding:0cm 0cm 7.5pt 0cm'>
                                <div align=center>
                                    <table class=MsoNormalTable border=0 cellspacing=0 cellpadding=0
                                           width=\"100%\" style='width:100.0%;border-collapse:collapse;mso-yfti-tbllook:
     1184;mso-padding-alt:0cm 0cm 0cm 0cm'>
                                        <tr style='mso-yfti-irow:0;mso-yfti-firstrow:yes;mso-yfti-lastrow:yes'>
                                            <td valign=bottom style='padding:0cm 0cm 0cm 0cm'>
                                                <p class=MsoNormal><span style='font-size:9.0pt;font-family:\"Arial\",sans-serif;
      mso-fareast-font-family:\"Times New Roman\";color:#434242'><a
                                                        href=\"http://www.mlaworld.com\"><img border=0 width=100 height=26
                                                                                            id=\"_x0000_i1025\"
                                                                                            src=\"http://www.mlaworld.com/services/EmailTemplates/images/logo_MLA_email.png\"
                                                                                            alt=\"logo_MLA\"></a><o:p></o:p></span>
                                                </p>
                                            </td>
                                            <td valign=bottom style='padding:0cm 0cm 0cm 0cm'>
                                                <p class=MsoNormal align=right style='text-align:right'><span
                                                        style='font-size:9.0pt;font-family:\"Arial\",sans-serif;mso-fareast-font-family:
      \"Times New Roman\";color:#434242'><a href=\"http://www.mlaworld.com/blog\"
                                          target=\"_blank\"><span style='font-size:7.5pt;color:#707070'>Blog</span> </a><o:p></o:p></span>
                                                </p>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </td>
                        </tr>
                        <tr style='mso-yfti-irow:1'>
                            <td style='border:solid #CCCCCC 1.0pt;mso-border-alt:solid #CCCCCC .75pt;
    background:white;padding:30.0pt 22.5pt 30.0pt 22.5pt'>
                                <p>Gent.Le  %nome% %cognome%,<br>
                                    Ti ringraziamo per averci contattato, ti risponderemo al più presto. <br>
                                    <br>
                                    Cordiali saluti<br>
                                    MLA Team
                                    <u1:p></u1:p>
                                </p>
                                <p>&nbsp;</p>
                            </td>
                        </tr>
                        <tr style='mso-yfti-irow:2;mso-yfti-lastrow:yes'>
                            <td style='padding:0cm 0cm 0cm 0cm'><span \">
                                <table class=MsoNormalTable border=0 cellspacing=0 cellpadding=0
                                       width=\"100%\" style='width:100.0%;border-collapse:collapse;mso-yfti-tbllook:
     1184;mso-padding-alt:0cm 0cm 0cm 0cm'>
                                    <tr style='mso-yfti-irow:0;mso-yfti-firstrow:yes'>
                                        <td style='padding:11.25pt 0cm 0cm 0cm'>
                                            <p class=MsoNormal><strong><span style='font-size:9.0pt;font-family:\"Arial\",sans-serif;
      mso-fareast-font-family:\"Times New Roman\";color:#434242'>Move Language
      Ahead</span></span></strong><span style='font-size:9.0pt;font-family:
      \"Arial\",sans-serif;mso-fareast-font-family:\"Times New Roman\";color:#434242'>
      <br>
      <br>
      </span><strong><span style='font-size:8.0pt;font-family:\"Arial\",sans-serif;
      mso-fareast-font-family:\"Times New Roman\";color:#707070'>Milano</span></strong><span
                                                    style='font-size:8.0pt;font-family:\"Arial\",sans-serif;mso-fareast-font-family:
      \"Times New Roman\";color:#707070'> | Via Vincenzo Monti, 34 | 20123<br>
      <strong><span style='font-family:\"Arial\",sans-serif'>t</span></strong><a
                                                        href=\"skype:+39024695479?call\">+39 024695479</a><strong><span
                                                        style='font-family:\"Arial\",sans-serif'>&nbsp; &nbsp; f</span></strong>
      +39 024695489<br>
      <br>
      <strong><span style='font-family:\"Arial\",sans-serif'>Napoli</span></strong>
      | Corso Vittorio Emanuele, 114 | 80121<br>
      <strong><span style='font-family:\"Arial\",sans-serif'>t</span></strong><a
                                                        href=\"skype:+390817614900?call\">+39 0817614900</a><strong><span
                                                        style='font-family:\"Arial\",sans-serif'>&nbsp; &nbsp; f</span></strong> +39
      0817614165 </span><span style='font-size:9.0pt;font-family:\"Arial\",sans-serif;
      mso-fareast-font-family:\"Times New Roman\";color:#434242'><o:p></o:p></span></p>
                                        </td>
                                        <td rowspan=2 valign=bottom style='padding:0cm 0cm 0cm 0cm'><span
                                                style='align=right'>
      <p class=MsoNormal align=right style='text-align:right'><strong><span
              style='font-size:7.0pt;font-family:\"Arial\",sans-serif;mso-fareast-font-family:
      \"Times New Roman\";color:#707070'>MLA è certificata con </span></strong><b><span
              style='font-size:7.0pt;font-family:\"Arial\",sans-serif;mso-fareast-font-family:
      \"Times New Roman\";color:#707070'><br>
      <strong><span style='font-family:\"Arial\",sans-serif'>ISO 9001:2008 e UNI
      EN 14804:2005 </span></span></strong></span></b><span style='font-size:
      9.0pt;font-family:\"Arial\",sans-serif;mso-fareast-font-family:\"Times New Roman\";
      color:#434242'><o:p></o:p></span></p>
                                        </td>
                                    </tr>
                                    <tr style='mso-yfti-irow:1;mso-yfti-lastrow:yes'>
                                        <td style='padding:0cm 0cm 0cm 0cm'>
                                            <p class=MsoNormal><strong><span style='font-size:8.0pt;font-family:\"Arial\",sans-serif;
      mso-fareast-font-family:\"Times New Roman\";color:#707070'><a
                                                    href=\"http://easy.mlaworld.com\"><span style='font-weight:normal'>easy.mlaworld.com</span></a></span></strong><span
                                                    style='font-size:8.0pt;font-family:\"Arial\",sans-serif;mso-fareast-font-family:
      \"Times New Roman\";color:#707070'>&nbsp;|&nbsp;<strong><span
                                                    style='font-family:\"Arial\",sans-serif'><a
                                                    href=\"mailto:vacanzestudio@mlaworld.com\"><span
                                                    style='font-weight:normal'>vacanzestudio@mlaworld.com</span></a></span></strong></span><span
                                                    style='font-size:9.0pt;font-family:\"Arial\",sans-serif;mso-fareast-font-family:
      \"Times New Roman\";color:#434242'> <o:p></o:p></span></p>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </div>
            </td>
        </tr>
    </table>

    <p class=MsoNormal><span style='mso-fareast-font-family:\"Times New Roman\"'><o:p>&nbsp;</o:p></span></p>

</div>

</body>

</html>";

        $messageStaff =
            "<html xmlns:v=\"urn:schemas-microsoft-com:vml\"
      xmlns:o=\"urn:schemas-microsoft-com:office:office\"
      xmlns:w=\"urn:schemas-microsoft-com:office:word\"
      xmlns:m=\"http://schemas.microsoft.com/office/2004/12/omml\"
      xmlns=\"http://www.w3.org/TR/REC-html40\">

<head>
   <meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\">
    <meta name=ProgId content=Word.Document>
    <meta name=Generator content=\"Microsoft Word 15\">
    <meta name=Originator content=\"Microsoft Word 15\">
    <link rel=File-List href=\"MLA%20Contatti_file/filelist.xml\">
    <link rel=Edit-Time-Data href=\"MLA%20Contatti_file/editdata.mso\">
    <!--[if !mso]>
    <style>
        v\: * {
            behavior: url(#default#VML);
        }

        o\: * {
            behavior: url(#default#VML);
        }

        w\: * {
            behavior: url(#default#VML);
        }

        .shape {
            behavior: url(#default#VML);
        }
    </style>
    <![endif]-->
    <title>Demystifying Email Design</title>
    <link rel=themeData href=\"MLA%20Contatti_file/themedata.thmx\">
    <link rel=colorSchemeMapping href=\"MLA%20Contatti_file/colorschememapping.xml\">
    <style>
        <!--
        /* Font Definitions */
        @font-face {
            font-family: \"Cambria Math\";
            panose-1: 2 4 5 3 5 4 6 3 2 4;
            mso-font-charset: 1;
            mso-generic-font-family: roman;
            mso-font-pitch: variable;
            mso-font-signature: 0 0 0 0 0 0;
        }

        @font-face {
            font-family: Calibri;
            panose-1: 2 15 5 2 2 2 4 3 2 4;
            mso-font-charset: 0;
            mso-generic-font-family: swiss;
            mso-font-pitch: variable;
            mso-font-signature: -536859905 -1073732485 9 0 511 0;
        }

        /* Style Definitions */
        p.MsoNormal, li.MsoNormal, div.MsoNormal {
            mso-style-unhide: no;
            mso-style-qformat: yes;
            mso-style-parent: \"\";
            margin: 0cm;
            margin-bottom: .0001pt;
            mso-pagination: widow-orphan;
            font-size: 12.0pt;
            font-family: \"Times New Roman\", serif;
            mso-fareast-font-family: Calibri;
            mso-fareast-theme-font: minor-latin;
            color: windowtext;
        }

        a:link, span.MsoHyperlink {
            mso-style-noshow: yes;
            mso-style-priority: 99;
            color: #3399CC;
            mso-text-animation: none;
            font-weight: normal;
            font-style: normal;
            text-decoration: none;
            text-underline: none;
            text-decoration: none;
            text-line-through: none;
        }

        a:visited, span.MsoHyperlinkFollowed {
            mso-style-noshow: yes;
            mso-style-priority: 99;
            color: #3399CC;
            mso-text-animation: none;
            font-weight: normal;
            font-style: normal;
            text-decoration: none;
            text-underline: none;
            text-decoration: none;
            text-line-through: none;
        }

        p {
            mso-style-noshow: yes;
            mso-style-priority: 99;
            margin-top: 0cm;
            margin-right: 0cm;
            margin-bottom: 12.0pt;
            margin-left: 0cm;
            mso-pagination: widow-orphan;
            font-size: 9.0pt;
            font-family: \"Arial\", sans-serif;
            mso-fareast-font-family: Calibri;
            mso-fareast-theme-font: minor-latin;
            color: #434242;
        }

        p.msonormal0, li.msonormal0, div.msonormal0 {
            mso-style-name: msonormal;
            mso-style-unhide: no;
            margin-top: 0cm;
            margin-right: 0cm;
            margin-bottom: 12.0pt;
            margin-left: 0cm;
            mso-pagination: widow-orphan;
            font-size: 9.0pt;
            font-family: \"Arial\", sans-serif;
            mso-fareast-font-family: Calibri;
            mso-fareast-theme-font: minor-latin;
            color: #434242;
        }

        p.table-bordered, li.table-bordered, div.table-bordered {
            mso-style-name: table-bordered;
            mso-style-unhide: no;
            margin-top: 7.5pt;
            margin-right: 0cm;
            margin-bottom: 12.0pt;
            margin-left: 0cm;
            mso-pagination: widow-orphan;
            font-size: 9.0pt;
            font-family: \"Arial\", sans-serif;
            mso-fareast-font-family: Calibri;
            mso-fareast-theme-font: minor-latin;
            color: #434242;
        }

        .MsoChpDefault {
            mso-style-type: export-only;
            mso-default-props: yes;
            font-size: 10.0pt;
            mso-ansi-font-size: 10.0pt;
            mso-bidi-font-size: 10.0pt;
        }

        @page WordSection1 {
            size: 612.0pt 792.0pt;
            margin: 70.85pt 2.0cm 2.0cm 2.0cm;
            mso-header-margin: 36.0pt;
            mso-footer-margin: 36.0pt;
            mso-paper-source: 0;
        }

        div.WordSection1 {
            page: WordSection1;
        }

        -->
    </style>
    <!--[if gte mso 10]>
    <style>
        /* Style Definitions */
        table.MsoNormalTable {
            mso-style-name: \"Tabella normale\";
            mso-tstyle-rowband-size: 0;
            mso-tstyle-colband-size: 0;
            mso-style-noshow: yes;
            mso-style-priority: 99;
            mso-style-parent: \"\";
            mso-padding-alt: 0cm 5.4pt 0cm 5.4pt;
            mso-para-margin: 0cm;
            mso-para-margin-bottom: .0001pt;
            mso-pagination: widow-orphan;
            font-size: 10.0pt;
            font-family: \"Times New Roman\", serif;
        }
    </style>
    <![endif]--><!--[if gte mso 9]>
    <xml>
        <o:shapedefaults v:ext=\"edit\" spidmax=\"1026\"/>
    </xml><![endif]--><!--[if gte mso 9]>
    <xml>
        <o:shapelayout v:ext=\"edit\">
            <o:idmap v:ext=\"edit\" data=\"1\"/>
        </o:shapelayout>
    </xml><![endif]-->
</head>

<body bgcolor=\"#F9FAFA\" lang=IT link=\"#3399CC\" vlink=\"#3399CC\"
      style='tab-interval:35.4pt'>

<div class=WordSection1>


    <table class=MsoNormalTable border=0 cellspacing=0 cellpadding=0 width=\"100%\"
           style='width:100.0%;border-collapse:collapse;mso-yfti-tbllook:1184;mso-padding-alt:
 0cm 0cm 0cm 0cm'>
        <tr style='mso-yfti-irow:0;mso-yfti-firstrow:yes;mso-yfti-lastrow:yes'>
            <td style='padding:15.0pt 0cm 22.5pt 0cm'>
                <div align=center>
                    <table class=MsoNormalTable border=0 cellspacing=0 cellpadding=0 width=680
                           style='width:255.0pt;border-collapse:collapse;mso-yfti-tbllook:1184;
   mso-padding-alt:0cm 0cm 0cm 0cm'>
                        <tr style='mso-yfti-irow:0;mso-yfti-firstrow:yes'>
                            <td style='padding:0cm 0cm 7.5pt 0cm'>
                                <div align=center>
                                    <table class=MsoNormalTable border=0 cellspacing=0 cellpadding=0
                                           width=\"100%\" style='width:100.0%;border-collapse:collapse;mso-yfti-tbllook:
     1184;mso-padding-alt:0cm 0cm 0cm 0cm'>
                                        <tr style='mso-yfti-irow:0;mso-yfti-firstrow:yes;mso-yfti-lastrow:yes'>
                                            <td valign=bottom style='padding:0cm 0cm 0cm 0cm'>
                                                <p class=MsoNormal><span style='font-size:9.0pt;font-family:\"Arial\",sans-serif;
      mso-fareast-font-family:\"Times New Roman\";color:#434242'><a
                                                        href=\"http://www.mlaworld.com\"><img border=0 width=100 height=26
                                                                                            id=\"_x0000_i1025\"
                                                                                            src=\"http://www.mlaworld.com/services/EmailTemplates/images/logo_MLA_email.png\"
                                                                                            alt=\"logo_MLA\"></a><o:p></o:p></span>
                                                </p>
                                            </td>
                                            <td valign=bottom style='padding:0cm 0cm 0cm 0cm'>
                                                <p class=MsoNormal align=right style='text-align:right'><span
                                                        style='font-size:9.0pt;font-family:\"Arial\",sans-serif;mso-fareast-font-family:
      \"Times New Roman\";color:#434242'><a href=\"http://www.mlaworld.com/blog\"
                                          target=\"_blank\"><span style='font-size:7.5pt;color:#707070'>Blog</span> </a><o:p></o:p></span>
                                                </p>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </td>
                        </tr>
                        <tr style='mso-yfti-irow:1'>
                            <td style='border:solid #CCCCCC 1.0pt;mso-border-alt:solid #CCCCCC .75pt;
    background:white;padding:30.0pt 22.5pt 30.0pt 22.5pt'>
                                <p>Ciao,</p>
                                <p>Abbiamo ricevuto una nuova richiesta di contatto tramite il form Contatti.</p> 
                                <p>Di seguito puoi leggere i dettagli:</p>
                                <p>Nome: %nome%</p>
                                <p>Cognome: %cognome%</p>
                                <p>Data di Nascita: %dataNascita%</p>
                                <p>Indirizzo: %indirizzo%</p>
                                <p>Città: %citta%</p>
                                <p>Email: %email%</p>
                                <p>Telefono: %telefono%</p>
                                <p>Cellulare: %cellulare%</p>
                                <p>Messaggio: %messaggio%</p>
                                    <br>
                                    <br>
                                    Cordiali saluti<br>
                                    MLA Team
                                    <u1:p></u1:p>
                                </p>
                                <p>&nbsp;</p>
                            </td>
                        </tr>
                        <tr style='mso-yfti-irow:2;mso-yfti-lastrow:yes'>
                            <td style='padding:0cm 0cm 0cm 0cm'><span \">
                                <table class=MsoNormalTable border=0 cellspacing=0 cellpadding=0
                                       width=\"100%\" style='width:100.0%;border-collapse:collapse;mso-yfti-tbllook:
     1184;mso-padding-alt:0cm 0cm 0cm 0cm'>
                                    <tr style='mso-yfti-irow:0;mso-yfti-firstrow:yes'>
                                        <td style='padding:11.25pt 0cm 0cm 0cm'>
                                            <p class=MsoNormal><strong><span style='font-size:9.0pt;font-family:\"Arial\",sans-serif;
      mso-fareast-font-family:\"Times New Roman\";color:#434242'>Move Language
      Ahead</span></span></strong><span style='font-size:9.0pt;font-family:
      \"Arial\",sans-serif;mso-fareast-font-family:\"Times New Roman\";color:#434242'>
      <br>
      <br>
      </span><strong><span style='font-size:8.0pt;font-family:\"Arial\",sans-serif;
      mso-fareast-font-family:\"Times New Roman\";color:#707070'>Milano</span></strong><span
                                                    style='font-size:8.0pt;font-family:\"Arial\",sans-serif;mso-fareast-font-family:
      \"Times New Roman\";color:#707070'> | Via Vincenzo Monti, 34 | 20123<br>
      <strong><span style='font-family:\"Arial\",sans-serif'>t</span></strong><a
                                                        href=\"skype:+39024695479?call\">+39 024695479</a><strong><span
                                                        style='font-family:\"Arial\",sans-serif'>&nbsp; &nbsp; f</span></strong>
      +39 024695489<br>
      <br>
      <strong><span style='font-family:\"Arial\",sans-serif'>Napoli</span></strong>
      | Corso Vittorio Emanuele, 114 | 80121<br>
      <strong><span style='font-family:\"Arial\",sans-serif'>t</span></strong><a
                                                        href=\"skype:+390817614900?call\">+39 0817614900</a><strong><span
                                                        style='font-family:\"Arial\",sans-serif'>&nbsp; &nbsp; f</span></strong> +39
      0817614165 </span><span style='font-size:9.0pt;font-family:\"Arial\",sans-serif;
      mso-fareast-font-family:\"Times New Roman\";color:#434242'><o:p></o:p></span></p>
                                        </td>
                                        <td rowspan=2 valign=bottom style='padding:0cm 0cm 0cm 0cm'><span
                                                style='align=right'>
      <p class=MsoNormal align=right style='text-align:right'><strong><span
              style='font-size:7.0pt;font-family:\"Arial\",sans-serif;mso-fareast-font-family:
      \"Times New Roman\";color:#707070'>MLA è certificata con </span></strong><b><span
              style='font-size:7.0pt;font-family:\"Arial\",sans-serif;mso-fareast-font-family:
      \"Times New Roman\";color:#707070'><br>
      <strong><span style='font-family:\"Arial\",sans-serif'>ISO 9001:2008 e UNI
      EN 14804:2005 </span></span></strong></span></b><span style='font-size:
      9.0pt;font-family:\"Arial\",sans-serif;mso-fareast-font-family:\"Times New Roman\";
      color:#434242'><o:p></o:p></span></p>
                                        </td>
                                    </tr>
                                    <tr style='mso-yfti-irow:1;mso-yfti-lastrow:yes'>
                                        <td style='padding:0cm 0cm 0cm 0cm'>
                                            <p class=MsoNormal><strong><span style='font-size:8.0pt;font-family:\"Arial\",sans-serif;
      mso-fareast-font-family:\"Times New Roman\";color:#707070'><a
                                                    href=\"http://easy.mlaworld.com\"><span style='font-weight:normal'>easy.mlaworld.com</span></a></span></strong><span
                                                    style='font-size:8.0pt;font-family:\"Arial\",sans-serif;mso-fareast-font-family:
      \"Times New Roman\";color:#707070'>&nbsp;|&nbsp;<strong><span
                                                    style='font-family:\"Arial\",sans-serif'><a
                                                    href=\"mailto:vacanzestudio@mlaworld.com\"><span
                                                    style='font-weight:normal'>vacanzestudio@mlaworld.com</span></a></span></strong></span><span
                                                    style='font-size:9.0pt;font-family:\"Arial\",sans-serif;mso-fareast-font-family:
      \"Times New Roman\";color:#434242'> <o:p></o:p></span></p>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </div>
            </td>
        </tr>
    </table>

    <p class=MsoNormal><span style='mso-fareast-font-family:\"Times New Roman\"'><o:p>&nbsp;</o:p></span></p>

</div>

</body>

</html>";

//        echo "NESSUN ERRORE EMAIL INVIATA";
        require '../admin/includes/phpmailer/PHPMailerAutoload.php';
//        $message = file_get_contents('http://cms.mlaworld.com/admin/templates/contatti.html');
        // MESSAGGIO ALLO STAFF
        $messageStaff = str_replace('%nome%', ucwords($_POST['nome']), $messageStaff);
        $messageStaff = str_replace('%cognome%', ucwords($_POST['cognome']), $messageStaff);
        $messageStaff = str_replace('%dataNascita%', data_it($_POST['dataNascita']), $messageStaff);
        $messageStaff = str_replace('%indirizzo%', ucwords($_POST['indirizzo']), $messageStaff);
        $messageStaff = str_replace('%citta%', ucwords($_POST['citta']), $messageStaff);
        $messageStaff = str_replace('%email%', $_POST['email'], $messageStaff);
        $messageStaff = str_replace('%telefono%', $_POST['telefono'], $messageStaff);
        $messageStaff = str_replace('%cellulare%', $_POST['cellulare'], $messageStaff);
        $messageStaff = str_replace('%messaggio%', $_POST['messaggio'], $messageStaff);
        $emailObjStaff = new Mailer();
        $emailObjStaff->CharSet = 'UTF-8';
        $emailObjStaff->MsgHTML($messageStaff);
        $emailObjStaff->sendMail('noreply@mlaworld.com', 'g.deblasio@mlaworld.com', 'Ci hanno contattato!', $messageStaff);

        // MESSAGGIO AL CLIENTE
        $messageClient = str_replace('%nome%', ucwords($_POST['nome']), $messageClient);
        $messageClient = str_replace('%cognome%', ucwords($_POST['cognome']), $messageClient);
        $emailObjClient = new Mailer();
        $emailObjClient->CharSet = 'UTF-8';
        $emailObjClient->MsgHTML($messageClient);
        $emailObjClient->sendMail('noreply@mlaworld.com', $_POST['email'], 'MLA - Grazie per averci contattato!', $messageClient);

    }

//}
