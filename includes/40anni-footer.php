<div class="clear"></div>
     
     <section>
     	<div class="container container-anni">
        
        	 <span class="titolo-section">Da <strong>40 anni</strong> al tuo servizio</span>
        
             <div class="swiper-container-3">
                <div class="swiper-wrapper">
                    
                    <div class="swiper-slide box-anni">
                        <img src="images/qualita.jpg" alt="" />
                    </div> 
                    
                    <div class="swiper-slide box-anni">
                        <img src="images/prezzo-garantito.jpg" alt="" />
                    </div>
                    
                    <div class="swiper-slide box-anni">
                        <img src="images/servizio.jpg" alt="" />
                    </div>
                                          
                </div>
            </div>
            
            <a href="#" class="maggiori-info">Maggiori informazioni</a>
        
        </div>
     </section>