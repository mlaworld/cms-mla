<?php
/**
 * Created da Giuseppe Alessandro De Blasio.
 * User: Giuseppe Alessandro De Blasio
 * Date: 21/06/17
 * Time: 12:43
 */

include 'includes/header.php';
include 'includes/menu.php';


if(empty($_GET['eta'])){$_GET['eta'] = "";}
if(empty($_GET['lingua'])){$_GET['lingua'] = "";}
if(empty($_GET['destinazione'])){$_GET['destinazione'] = "";}

?>

<style>
    .loader {
        margin: 100px auto;
        font-size: 3px;
        left: 8px;
        width: 1em;
        height: 1em;
        border-radius: 50%;
        position: relative;
        text-indent: -9999em;
        -webkit-animation: load5 1.1s infinite ease;
        animation: load5 1.1s infinite ease;
        -webkit-transform: translateZ(0);
        -ms-transform: translateZ(0);
        transform: translateZ(0);
    }
    @-webkit-keyframes load5 {
        0%,
        100% {
            box-shadow: 0em -2.6em 0em 0em #000000, 1.8em -1.8em 0 0em rgba(0,0,0, 0.2), 2.5em 0em 0 0em rgba(0,0,0, 0.2), 1.75em 1.75em 0 0em rgba(0,0,0, 0.2), 0em 2.5em 0 0em rgba(0,0,0, 0.2), -1.8em 1.8em 0 0em rgba(0,0,0, 0.2), -2.6em 0em 0 0em rgba(0,0,0, 0.5), -1.8em -1.8em 0 0em rgba(0,0,0, 0.7);
        }
        12.5% {
            box-shadow: 0em -2.6em 0em 0em rgba(0,0,0, 0.7), 1.8em -1.8em 0 0em #000000, 2.5em 0em 0 0em rgba(0,0,0, 0.2), 1.75em 1.75em 0 0em rgba(0,0,0, 0.2), 0em 2.5em 0 0em rgba(0,0,0, 0.2), -1.8em 1.8em 0 0em rgba(0,0,0, 0.2), -2.6em 0em 0 0em rgba(0,0,0, 0.2), -1.8em -1.8em 0 0em rgba(0,0,0, 0.5);
        }
        25% {
            box-shadow: 0em -2.6em 0em 0em rgba(0,0,0, 0.5), 1.8em -1.8em 0 0em rgba(0,0,0, 0.7), 2.5em 0em 0 0em #000000, 1.75em 1.75em 0 0em rgba(0,0,0, 0.2), 0em 2.5em 0 0em rgba(0,0,0, 0.2), -1.8em 1.8em 0 0em rgba(0,0,0, 0.2), -2.6em 0em 0 0em rgba(0,0,0, 0.2), -1.8em -1.8em 0 0em rgba(0,0,0, 0.2);
        }
        37.5% {
            box-shadow: 0em -2.6em 0em 0em rgba(0,0,0, 0.2), 1.8em -1.8em 0 0em rgba(0,0,0, 0.5), 2.5em 0em 0 0em rgba(0,0,0, 0.7), 1.75em 1.75em 0 0em #000000, 0em 2.5em 0 0em rgba(0,0,0, 0.2), -1.8em 1.8em 0 0em rgba(0,0,0, 0.2), -2.6em 0em 0 0em rgba(0,0,0, 0.2), -1.8em -1.8em 0 0em rgba(0,0,0, 0.2);
        }
        50% {
            box-shadow: 0em -2.6em 0em 0em rgba(0,0,0, 0.2), 1.8em -1.8em 0 0em rgba(0,0,0, 0.2), 2.5em 0em 0 0em rgba(0,0,0, 0.5), 1.75em 1.75em 0 0em rgba(0,0,0, 0.7), 0em 2.5em 0 0em #000000, -1.8em 1.8em 0 0em rgba(0,0,0, 0.2), -2.6em 0em 0 0em rgba(0,0,0, 0.2), -1.8em -1.8em 0 0em rgba(0,0,0, 0.2);
        }
        62.5% {
            box-shadow: 0em -2.6em 0em 0em rgba(0,0,0, 0.2), 1.8em -1.8em 0 0em rgba(0,0,0, 0.2), 2.5em 0em 0 0em rgba(0,0,0, 0.2), 1.75em 1.75em 0 0em rgba(0,0,0, 0.5), 0em 2.5em 0 0em rgba(0,0,0, 0.7), -1.8em 1.8em 0 0em #000000, -2.6em 0em 0 0em rgba(0,0,0, 0.2), -1.8em -1.8em 0 0em rgba(0,0,0, 0.2);
        }
        75% {
            box-shadow: 0em -2.6em 0em 0em rgba(0,0,0, 0.2), 1.8em -1.8em 0 0em rgba(0,0,0, 0.2), 2.5em 0em 0 0em rgba(0,0,0, 0.2), 1.75em 1.75em 0 0em rgba(0,0,0, 0.2), 0em 2.5em 0 0em rgba(0,0,0, 0.5), -1.8em 1.8em 0 0em rgba(0,0,0, 0.7), -2.6em 0em 0 0em #000000, -1.8em -1.8em 0 0em rgba(0,0,0, 0.2);
        }
        87.5% {
            box-shadow: 0em -2.6em 0em 0em rgba(0,0,0, 0.2), 1.8em -1.8em 0 0em rgba(0,0,0, 0.2), 2.5em 0em 0 0em rgba(0,0,0, 0.2), 1.75em 1.75em 0 0em rgba(0,0,0, 0.2), 0em 2.5em 0 0em rgba(0,0,0, 0.2), -1.8em 1.8em 0 0em rgba(0,0,0, 0.5), -2.6em 0em 0 0em rgba(0,0,0, 0.7), -1.8em -1.8em 0 0em #000000;
        }
    }
    @keyframes load5 {
        0%,
        100% {
            box-shadow: 0em -2.6em 0em 0em #000000, 1.8em -1.8em 0 0em rgba(0,0,0, 0.2), 2.5em 0em 0 0em rgba(0,0,0, 0.2), 1.75em 1.75em 0 0em rgba(0,0,0, 0.2), 0em 2.5em 0 0em rgba(0,0,0, 0.2), -1.8em 1.8em 0 0em rgba(0,0,0, 0.2), -2.6em 0em 0 0em rgba(0,0,0, 0.5), -1.8em -1.8em 0 0em rgba(0,0,0, 0.7);
        }
        12.5% {
            box-shadow: 0em -2.6em 0em 0em rgba(0,0,0, 0.7), 1.8em -1.8em 0 0em #000000, 2.5em 0em 0 0em rgba(0,0,0, 0.2), 1.75em 1.75em 0 0em rgba(0,0,0, 0.2), 0em 2.5em 0 0em rgba(0,0,0, 0.2), -1.8em 1.8em 0 0em rgba(0,0,0, 0.2), -2.6em 0em 0 0em rgba(0,0,0, 0.2), -1.8em -1.8em 0 0em rgba(0,0,0, 0.5);
        }
        25% {
            box-shadow: 0em -2.6em 0em 0em rgba(0,0,0, 0.5), 1.8em -1.8em 0 0em rgba(0,0,0, 0.7), 2.5em 0em 0 0em #000000, 1.75em 1.75em 0 0em rgba(0,0,0, 0.2), 0em 2.5em 0 0em rgba(0,0,0, 0.2), -1.8em 1.8em 0 0em rgba(0,0,0, 0.2), -2.6em 0em 0 0em rgba(0,0,0, 0.2), -1.8em -1.8em 0 0em rgba(0,0,0, 0.2);
        }
        37.5% {
            box-shadow: 0em -2.6em 0em 0em rgba(0,0,0, 0.2), 1.8em -1.8em 0 0em rgba(0,0,0, 0.5), 2.5em 0em 0 0em rgba(0,0,0, 0.7), 1.75em 1.75em 0 0em #000000, 0em 2.5em 0 0em rgba(0,0,0, 0.2), -1.8em 1.8em 0 0em rgba(0,0,0, 0.2), -2.6em 0em 0 0em rgba(0,0,0, 0.2), -1.8em -1.8em 0 0em rgba(0,0,0, 0.2);
        }
        50% {
            box-shadow: 0em -2.6em 0em 0em rgba(0,0,0, 0.2), 1.8em -1.8em 0 0em rgba(0,0,0, 0.2), 2.5em 0em 0 0em rgba(0,0,0, 0.5), 1.75em 1.75em 0 0em rgba(0,0,0, 0.7), 0em 2.5em 0 0em #000000, -1.8em 1.8em 0 0em rgba(0,0,0, 0.2), -2.6em 0em 0 0em rgba(0,0,0, 0.2), -1.8em -1.8em 0 0em rgba(0,0,0, 0.2);
        }
        62.5% {
            box-shadow: 0em -2.6em 0em 0em rgba(0,0,0, 0.2), 1.8em -1.8em 0 0em rgba(0,0,0, 0.2), 2.5em 0em 0 0em rgba(0,0,0, 0.2), 1.75em 1.75em 0 0em rgba(0,0,0, 0.5), 0em 2.5em 0 0em rgba(0,0,0, 0.7), -1.8em 1.8em 0 0em #000000, -2.6em 0em 0 0em rgba(0,0,0, 0.2), -1.8em -1.8em 0 0em rgba(0,0,0, 0.2);
        }
        75% {
            box-shadow: 0em -2.6em 0em 0em rgba(0,0,0, 0.2), 1.8em -1.8em 0 0em rgba(0,0,0, 0.2), 2.5em 0em 0 0em rgba(0,0,0, 0.2), 1.75em 1.75em 0 0em rgba(0,0,0, 0.2), 0em 2.5em 0 0em rgba(0,0,0, 0.5), -1.8em 1.8em 0 0em rgba(0,0,0, 0.7), -2.6em 0em 0 0em #000000, -1.8em -1.8em 0 0em rgba(0,0,0, 0.2);
        }
        87.5% {
            box-shadow: 0em -2.6em 0em 0em rgba(0,0,0, 0.2), 1.8em -1.8em 0 0em rgba(0,0,0, 0.2), 2.5em 0em 0 0em rgba(0,0,0, 0.2), 1.75em 1.75em 0 0em rgba(0,0,0, 0.2), 0em 2.5em 0 0em rgba(0,0,0, 0.2), -1.8em 1.8em 0 0em rgba(0,0,0, 0.5), -2.6em 0em 0 0em rgba(0,0,0, 0.7), -1.8em -1.8em 0 0em #000000;
        }
    }

</style>

<br><br><br>
<section>
    <div class="container">
        <div class="row">
            <div class="col-md-4">
                <div class="jumbotron">
                    <form action="" id="listForm" method="GET">
                        <div class="form-group">
                            <select name="etaT" id="etaT" class="lista">
                                <option value="">--</option>
<!--                                <option value="test">test</option>-->
<!--                                <option value="test2">test2</option>-->
<!--                                <option value="test3">test3</option>-->
                                <option value="8" <?php echo ($_GET['eta'] == 8)? "selected=selected": "" ?>>8</option>
                                <option value="9" <?php echo ($_GET['eta'] == 9)? "selected=selected": "" ?>>9</option>
                                <option value="10" <?php echo ($_GET['eta'] == 10)? "selected=selected": "" ?>>10</option>
                                <option value="11" <?php echo ($_GET['eta'] == 11)? "selected=selected": "" ?>>11</option>
                                <option value="12" <?php echo ($_GET['eta'] == 12)? "selected=selected": "" ?>>12</option>
                                <option value="13" <?php echo ($_GET['eta'] == 13)? "selected=selected": "" ?>>13</option>
                                <option value="14" <?php echo ($_GET['eta'] == 14)? "selected=selected": "" ?>>14</option>
                                <option value="15" <?php echo ($_GET['eta'] == 15)? "selected=selected": "" ?>>15</option>
                                <option value="16" <?php echo ($_GET['eta'] == 16)? "selected=selected": "" ?>>16</option>
                                <option value="17" <?php echo ($_GET['eta'] == 17)? "selected=selected": "" ?>>17</option>
                                <option value="18" <?php echo ($_GET['eta'] == 18)? "selected=selected": "" ?>>18</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <select name="linguaT" id="linguaT" class="lista">
                                <option value="">--</option>
                                <option value="italiano" <?php echo ($_GET['lingua'] == "italiano")? "selected=selected": "" ?> >Italiano</option>
                                <option value="inglese" <?php echo ($_GET['lingua'] == "inglese")? "selected=selected": "" ?> >Inglese</option>
                                <option value="francese" <?php echo ($_GET['lingua'] == "francese")? "selected=selected": "" ?> >Francese</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <select name="destinazioneT" id="destinazioneT" class="lista"> <!-- onchange="listaCentri()"  -->
                                <option value="">--</option>
                                <option value="inghilterra" <?php echo ($_GET['destinazione'] == "Inghilterra")? "selected=selected": "" ?>>Inghilterra</option>
                                <option value="scozia" <?php echo ($_GET['destinazione'] == "Scozia")? "selected=selected": "" ?>>Scozia</option>
                                <option value="usa" <?php echo ($_GET['destinazione'] == "USA")? "selected=selected": "" ?>>USA</option>
                            </select>
                        </div>

                        <hr style="background-color: #0d0d0d; border: solid 1px">

                        <div class="form-group">
                                <div class="checkbox">
                                    <label>
                                        <input name="college" id="college" type="checkbox" value="" ><span>College</span> <i id="numCol"></i>
                                    </label>
                                </div>
                        </div>
                        <div class="form-group">
                                <div class="checkbox">
                                    <label>
                                        <input name="famiglia" id="famiglia" type="checkbox" value="" ><span>Famiglia</span> <i id="numFam"></i>
                                    </label>
                                </div>
                        </div>
                        <div class="form-group">
                            <div class="checkbox">
                                <label>
                                    <input name="hotel" id="hotel" type="checkbox" value="" ><span>Hotel</span> <i id="numHot"></i>
                                </label>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="checkbox">
                                <label>
                                    <input name="residence" id="residence" type="checkbox" value="" ><span>Residance</span> <i id="numRes"></i>
                                </label>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="checkbox">
                                <label>
                                    <input name="studio" id="studio" type="checkbox" value="" ><span>Studio</span> <i id="numStu"></i>
                                </label>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="checkbox">
                                <label>
                                    <input name="appartamento" id="appartamento" type="checkbox" value="" ><span>Appartamento</span> <i id="numApp"></i>
                                </label>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="checkbox">
                                <label>
                                    <input name="monolocale" id="monolocale" type="checkbox" value="" ><span>Monolocale</span> <i id="numMon"></i>
                                </label>
                            </div>
                        </div>

                        <hr style="background-color: #0d0d0d; border: solid 1px">

                        <div class="form-group">
                            <div class="checkbox">
                                <label>
                                    <input name="finoMille" id="finoMille" type="checkbox" value="1000" ><span>Fino a 1000,00</span> <i id="numFinMil"></i>
                                </label>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="checkbox">
                                <label>
                                    <input name="finoDue" id="finoDue" type="checkbox" value="2000" ><span>Da 1000,00 a 2000,00</span> <i id="minDiDue"></i>
                                </label>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="checkbox">
                                <label>
                                    <input name="magDue" id="magDue" type="checkbox" value="2000" ><span>Da 1000,00 a 2000,00</span> <i id="magDiDue"></i>
                                </label>
                            </div>
                        </div>

                        <hr style="background-color: #0d0d0d; border: solid 1px">

                        <div class="form-group">
                            <div class="checkbox">
                                <label>
                                    <input name="bagPriv" id="bagPriv" type="checkbox" value="" ><span>Bagno Privato</span> <i id="numBagPriv"></i>
                                </label>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="checkbox">
                                <label>
                                    <input name="medicoIta" id="medicoIta" type="checkbox" value="" ><span>Medico Italiano</span> <i id="numMedicoIta"></i>
                                </label>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="checkbox">
                                <label>
                                    <input name="escuEsc" id="escuEsc" type="checkbox" value="" ><span>Escursioni Esclusive</span> <i id="numEscuEsc"></i>
                                </label>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="checkbox">
                                <label>
                                    <input name="triColEx" id="triColEx" type="checkbox" value="" ><span>Trinity College Examination</span> <i id="numTriColEx"></i>
                                </label>
                            </div>
                        </div>
<!--                        Filter by price interval: <b>€ 10</b>-->
<!--                        <input id="ex2" type="text" class="span2" value="" data-slider-min="10" data-slider-max="1000" data-slider-step="5" data-slider-value="[250,450]"/> <b>€ 1000</b>-->
                        <hr style="background-color: #0d0d0d; border: solid 1px">

                        <div class="form-group">
                            <div class="checkbox">
                                <label>
                                    <input name="footAc" id="footAc" type="checkbox" value="" ><span>Football Academy</span> <i id="numFootAc"></i>
                                </label>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="checkbox">
                                <label>
                                    <input name="danceAc" id="danceAc" type="checkbox" value="" ><span>Dance Academy</span> <i id="numDanceAc"></i>
                                </label>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="checkbox">
                                <label>
                                    <input name="baskAc" id="baskAc" type="checkbox" value="" ><span>Basket Academy</span> <i id="numBaskAc"></i>
                                </label>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="checkbox">
                                <label>
                                    <input name="theaAc" id="theaAc" type="checkbox" value="" ><span>Theatre Academy</span> <i id="numTheaAc"></i>
                                </label>
                            </div>
                        </div>

                        <input type='reset' value='Reset' name='reset' onclick="return resetForm(this.form);">
                    </form>
                </div>

            </div>
            <div class="col-md-8 testView" id="testView">
                <table id="testTable">
<!--                    <thead>-->
<!--                    <tr>-->
<!--                        <th>Subscriber ID</th>-->
<!--                        <th>Install Location</th>-->
<!--                        <th>Subscriber Name</th>-->
<!--                        <th>some data</th>-->
<!--                    </tr>-->
<!--                    </thead>-->
                </table>

            </div>


        </div>
    </div>
</section>
<?php include 'includes/footer.php'; ?>
<!--<script src="js/bootpag.js"></script>-->
<!--<script src="js/pagination.js"></script>-->
<!--<script src="js/jquery.easyPaginate.js"></script>-->
<script src="js/spin.min.js"></script>
<script src="js/datatables/datatables.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-slider/9.8.1/bootstrap-slider.js"></script>

<script>
    function spinnerTimeDestroy(){
        $('#testView').removeClass('csspinner');

    }
    setTimeout(spinnerTimeDestroy, 900);

    function resetForm(form) {

        window.location.href = "http://cms.mlaworld.com/lista-centri";
//        location.reload();


        // clearing inputs
        var inputs = form.getElementsByTagName('input');
        for (var i = 0; i<inputs.length; i++) {
            switch (inputs[i].type) {
                case 'hidden':
                case 'text':
                    inputs[i].value = '';
                    break;
                case 'radio':
                case 'checkbox':
                    inputs[i].checked = false;
            }
        }

        // clearing selects
        var selects = form.getElementsByTagName('select');
        for (var i = 0; i<selects.length; i++)
            selects[i].selectedIndex = 0;

        // clearing textarea
        var text= form.getElementsByTagName('textarea');
        for (var i = 0; i<text.length; i++)
            text[i].innerHTML= '';

        return false;


    }

    // Variabili per il count degli elementi
//    var countFam = 0;
//    var countColl = 0;
//    var countHot = 0;
//    var countRes = 0;
//    var countStu = 0;
//    var countApp= 0;
//    var countMon= 0;
//
//    var countMinMill = 0;
//    var countMinDue = 0;
//    var countMagDue = 0;
//
//    var countBagno = 0;
//    var countMedico = 0;
//    var countEscursioni = 0;
//    var countTrinity = 0;
//
//    var countFootAc = 0;
//    var countDanceAc = 0;
//    var countBaskAc= 0;
//    var countTheaAc = 0;




    function addClass(){

//        $('#numCol').addClass('fa fa-spinner fa-spin');
        $('#numCol').addClass('loader');
//        $('#numFam').addClass('fa fa-spinner fa-spin');
        $('#numFam').addClass('loader');
//        $('#numHot').addClass('fa fa-spinner fa-spin');
        $('#numHot').addClass('loader');
//        $('#numRes').addClass('fa fa-spinner fa-spin');
        $('#numRes').addClass('loader');
//        $('#numStu').addClass('fa fa-spinner fa-spin');
        $('#numStu').addClass('loader');
//        $('#numApp').addClass('fa fa-spinner fa-spin');
        $('#numApp').addClass('loader');
//        $('#numMon').addClass('fa fa-spinner fa-spin');
        $('#numMon').addClass('loader');

//        $('#numFinMil').addClass('fa fa-spinner fa-spin');
        $('#numFinMil').addClass('loader');
//        $('#minDiDue').addClass('fa fa-spinner fa-spin');
        $('#minDiDue').addClass('loader');
//        $('#magDiDue').addClass('fa fa-spinner fa-spin');
        $('#magDiDue').addClass('loader');

//        $('#numBagPriv').addClass('fa fa-spinner fa-spin');
        $('#numBagPriv').addClass('loader');
//        $('#numMedicoIta').addClass('fa fa-spinner fa-spin');
        $('#numMedicoIta').addClass('loader');
//        $('#numEscuEsc').addClass('fa fa-spinner fa-spin');
        $('#numEscuEsc').addClass('loader');
//        $('#numTriColEx').addClass('fa fa-spinner fa-spin');
        $('#numTriColEx').addClass('loader');

//        $('#numFootAc').addClass('fa fa-spinner fa-spin');
        $('#numFootAc').addClass('loader');
//        $('#numDanceAc').addClass('fa fa-spinner fa-spin');
        $('#numDanceAc').addClass('loader');
//        $('#numBaskAc').addClass('fa fa-spinner fa-spin');
        $('#numBaskAc').addClass('loader');
//        $('#numTheaAc').addClass('fa fa-spinner fa-spin');
        $('#numTheaAc').addClass('loader');
    }

    function removeClass(){

//        $('#numCol').addClass('fa fa-spinner fa-spin');
        $('#numCol').removeClass('loader');
//        $('#numFam').addClass('fa fa-spinner fa-spin');
        $('#numFam').removeClass('loader');
//        $('#numHot').addClass('fa fa-spinner fa-spin');
        $('#numHot').removeClass('loader');
//        $('#numRes').addClass('fa fa-spinner fa-spin');
        $('#numRes').removeClass('loader');
//        $('#numStu').addClass('fa fa-spinner fa-spin');
        $('#numStu').removeClass('loader');
//        $('#numApp').addClass('fa fa-spinner fa-spin');
        $('#numApp').removeClass('loader');
//        $('#numMon').addClass('fa fa-spinner fa-spin');
        $('#numMon').removeClass('loader');

//        $('#numFinMil').addClass('fa fa-spinner fa-spin');
        $('#numFinMil').removeClass('loader');
//        $('#minDiDue').addClass('fa fa-spinner fa-spin');
        $('#minDiDue').removeClass('loader');
//        $('#magDiDue').addClass('fa fa-spinner fa-spin');
        $('#magDiDue').removeClass('loader');

//        $('#numBagPriv').addClass('fa fa-spinner fa-spin');
        $('#numBagPriv').removeClass('loader');
//        $('#numMedicoIta').addClass('fa fa-spinner fa-spin');
        $('#numMedicoIta').removeClass('loader');
//        $('#numEscuEsc').addClass('fa fa-spinner fa-spin');
        $('#numEscuEsc').removeClass('loader');
//        $('#numTriColEx').addClass('fa fa-spinner fa-spin');
        $('#numTriColEx').removeClass('loader');

//        $('#numFootAc').addClass('fa fa-spinner fa-spin');
        $('#numFootAc').removeClass('loader');
//        $('#numDanceAc').addClass('fa fa-spinner fa-spin');
        $('#numDanceAc').removeClass('loader');
//        $('#numBaskAc').addClass('fa fa-spinner fa-spin');
        $('#numBaskAc').removeClass('loader');
//        $('#numTheaAc').addClass('fa fa-spinner fa-spin');
        $('#numTheaAc').removeClass('loader');
    }

//    function objectData(dataJson){
//
//        Object.keys(dataJson).forEach(function(k){
////                        console.log(k + ' - ' + dataJson[k].Famiglia);
//            if(dataJson[k].Famiglia == 1){countFam++}
//            if(dataJson[k].College == 1){countColl++}
//            if(dataJson[k].Hotel == 1){countHot++}
//            if(dataJson[k].Residence == 1){countRes++}
//            if(dataJson[k].Studio == 1){countStu++}
//            if(dataJson[k].Appartamento == 1){countApp++}
//            if(dataJson[k].Monolocale == 1){countMon++}
//
//            if(dataJson[k].PrezziDa <= 1000){countMinMill++}
//            if(dataJson[k].PrezziDa > 1000 && dataJson[k].PrezziDa <=2000 ){countMinDue++}
//            if(dataJson[k].PrezziDa > 2000 ){countMagDue++}
//
//            if(dataJson[k].BagnoPrivato == 1){countBagno++}
//            if(dataJson[k].MedicoItaliano == 1){countMedico++}
//            if(dataJson[k].Escursioni == 1){countEscursioni++}
//            if(dataJson[k].Trinity == 1){countTrinity++}
//
//            if(dataJson[k].FootballAcademy == 1){countFootAc++}
//            if(dataJson[k].DanceAcademy == 1){countDanceAc++}
//            if(dataJson[k].BasketAcademy == 1){countBaskAc++}
//            if(dataJson[k].TheatreAcademy == 1){countTheaAc++}
//        });

//            if(countColl > 0 ){ $('#college').attr('disabled', false); $('#college').next('span').css('textDecoration', 'none');} else {  $('#college').next('span').css('textDecoration', 'line-through'); $('#college').attr('disabled', true)}
//            if(countFam > 0 ){ $('#famiglia').attr('disabled', false); $('#famiglia').next('span').css('textDecoration', 'none');} else {  $('#famiglia').next('span').css('textDecoration', 'line-through'); $('#famiglia').attr('disabled', true)}
//            if(countHot > 0 ){ $('#hotel').attr('disabled', false); $('#hotel').next('span').css('textDecoration', 'none');} else {  $('#hotel').next('span').css('textDecoration', 'line-through'); $('#hotel').attr('disabled', true)}
//            if(countRes > 0 ){ $('#residence').attr('disabled', false); $('#residence').next('span').css('textDecoration', 'none');} else {  $('#residence').next('span').css('textDecoration', 'line-through'); $('#residence').attr('disabled', true)}
//            if(countStu > 0 ){ $('#studio').attr('disabled', false); $('#studio').next('span').css('textDecoration', 'none');} else {  $('#studio').next('span').css('textDecoration', 'line-through'); $('#studio').attr('disabled', true)}
//            if(countApp > 0 ){ $('#appartamento').attr('disabled', false); $('#appartamento').next('span').css('textDecoration', 'none');} else {  $('#appartamento').next('span').css('textDecoration', 'line-through'); $('#appartamento').attr('disabled', true)}
//            if(countMon > 0 ){ $('#monolocale').attr('disabled', false); $('#monolocale').next('span').css('textDecoration', 'none');} else {  $('#monolocale').next('span').css('textDecoration', 'line-through'); $('#monolocale').attr('disabled', true)}
//
//            if(countMinMill > 0 ){ $('#finoMille').attr('disabled', false); $('#finoMille').next('span').css('textDecoration', 'none');} else {  $('#finoMille').next('span').css('textDecoration', 'line-through'); $('#finoMille').attr('disabled', true)}
//            if(countMinDue > 0 ){ $('#finoDue').attr('disabled', false); $('#finoDue').next('span').css('textDecoration', 'none');} else {  $('#finoDue').next('span').css('textDecoration', 'line-through'); $('#finoDue').attr('disabled', true)}
//            if(countMagDue > 0 ){ $('#magDue').attr('disabled', false); $('#magDue').next('span').css('textDecoration', 'none');} else {  $('#magDue').next('span').css('textDecoration', 'line-through'); $('#magDue').attr('disabled', true)}
//
//            if(countBagno > 0 ){ $('#bagPriv').attr('disabled', false); $('#bagPriv').next('span').css('textDecoration', 'none');} else {  $('#bagPriv').next('span').css('textDecoration', 'line-through'); $('#bagPriv').attr('disabled', true)}
//            if(countMedico > 0 ){ $('#medicoIta').attr('disabled', false); $('#medicoIta').next('span').css('textDecoration', 'none');} else {  $('#medicoIta').next('span').css('textDecoration', 'line-through'); $('#medicoIta').attr('disabled', true)}
//            if(countEscursioni > 0 ){ $('#escuEsc').attr('disabled', false); $('#escuEsc').next('span').css('textDecoration', 'none');} else {  $('#escuEsc').next('span').css('textDecoration', 'line-through'); $('#escuEsc').attr('disabled', true)}
//            if(countTrinity > 0 ){ $('#triColEx').attr('disabled', false); $('#triColEx').next('span').css('textDecoration', 'none');} else {  $('#triColEx').next('span').css('textDecoration', 'line-through'); $('#triColEx').attr('disabled', true)}
//
//            if(countFootAc > 0 ){ $('#footAc').attr('disabled', false); $('#footAc').next('span').css('textDecoration', 'none');} else {  $('#footAc').next('span').css('textDecoration', 'line-through'); $('#footAc').attr('disabled', true)}
//            if(countDanceAc > 0 ){$('#danceAc').attr('disabled', false); $('#danceAc').next('span').css('textDecoration', 'none');} else {  $('#danceAc').next('span').css('textDecoration', 'line-through'); $('#danceAc').attr('disabled', true)}
//            if(countBaskAc > 0 ){ $('#baskAc').attr('disabled', false); $('#baskAc').next('span').css('textDecoration', 'none');} else {  $('#baskAc').next('span').css('textDecoration', 'line-through'); $('#baskAc').attr('disabled', true)}
//            if(countTheaAc > 0 ){ $('#theaAc').attr('disabled', false); $('#theaAc').next('span').css('textDecoration', 'none');} else {  $('#theaAc').next('span').css('textDecoration', 'line-through'); $('#theaAc').attr('disabled', true)}
//
//
//            $('#numFam').text("(" + countFam + ")");
//            $('#numCol').text("(" + countColl + ")");
//            $('#numHot').text("(" + countHot + ")");
//            $('#numRes').text("(" + countRes + ")");
//            $('#numStu').text("(" + countStu + ")");
//            $('#numApp').text("(" + countApp + ")");
//            $('#numMon').text("(" + countMon + ")");
//
//            $('#numFinMil').text("(" + countMinMill + ")");
//            $('#minDiDue').text("(" + countMinDue + ")");
//            $('#magDiDue').text("(" + countMagDue + ")");
//
//            $('#numBagPriv').text("(" + countBagno + ")");
//            $('#numMedicoIta').text("(" + countMedico + ")");
//            $('#numEscuEsc').text("(" + countEscursioni + ")");
//            $('#numTriColEx').text("(" + countTrinity + ")");
//
//            $('#numFootAc').text("(" + countFootAc + ")");
//            $('#numDanceAc').text("(" + countDanceAc + ")");
//            $('#numBaskAc').text("(" + countBaskAc + ")");
//            $('#numTheaAc').text("(" + countTheaAc + ")");
//    }

//    function countAction(){
//        if(countColl > 0 ){ $('#college').attr('disabled', false); $('#college').next('span').css('textDecoration', 'none');} else {  $('#college').next('span').css('textDecoration', 'line-through'); $('#college').attr('disabled', true)}
//        if(countFam > 0 ){ $('#famiglia').attr('disabled', false); $('#famiglia').next('span').css('textDecoration', 'none');} else {  $('#famiglia').next('span').css('textDecoration', 'line-through'); $('#famiglia').attr('disabled', true)}
//        if(countHot > 0 ){ $('#hotel').attr('disabled', false); $('#hotel').next('span').css('textDecoration', 'none');} else {  $('#hotel').next('span').css('textDecoration', 'line-through'); $('#hotel').attr('disabled', true)}
//        if(countRes > 0 ){ $('#residence').attr('disabled', false); $('#residence').next('span').css('textDecoration', 'none');} else {  $('#residence').next('span').css('textDecoration', 'line-through'); $('#residence').attr('disabled', true)}
//        if(countStu > 0 ){ $('#studio').attr('disabled', false); $('#studio').next('span').css('textDecoration', 'none');} else {  $('#studio').next('span').css('textDecoration', 'line-through'); $('#studio').attr('disabled', true)}
//        if(countApp > 0 ){ $('#appartamento').attr('disabled', false); $('#appartamento').next('span').css('textDecoration', 'none');} else {  $('#appartamento').next('span').css('textDecoration', 'line-through'); $('#appartamento').attr('disabled', true)}
//        if(countMon > 0 ){ $('#monolocale').attr('disabled', false); $('#monolocale').next('span').css('textDecoration', 'none');} else {  $('#monolocale').next('span').css('textDecoration', 'line-through'); $('#monolocale').attr('disabled', true)}
//
//        if(countMinMill > 0 ){ $('#finoMille').attr('disabled', false); $('#finoMille').next('span').css('textDecoration', 'none');} else {  $('#finoMille').next('span').css('textDecoration', 'line-through'); $('#finoMille').attr('disabled', true)}
//        if(countMinDue > 0 ){ $('#finoDue').attr('disabled', false); $('#finoDue').next('span').css('textDecoration', 'none');} else {  $('#finoDue').next('span').css('textDecoration', 'line-through'); $('#finoDue').attr('disabled', true)}
//        if(countMagDue > 0 ){ $('#magDue').attr('disabled', false); $('#magDue').next('span').css('textDecoration', 'none');} else {  $('#magDue').next('span').css('textDecoration', 'line-through'); $('#magDue').attr('disabled', true)}
//
//        if(countBagno > 0 ){ $('#bagPriv').attr('disabled', false); $('#bagPriv').next('span').css('textDecoration', 'none');} else {  $('#bagPriv').next('span').css('textDecoration', 'line-through'); $('#bagPriv').attr('disabled', true)}
//        if(countMedico > 0 ){ $('#medicoIta').attr('disabled', false); $('#medicoIta').next('span').css('textDecoration', 'none');} else {  $('#medicoIta').next('span').css('textDecoration', 'line-through'); $('#medicoIta').attr('disabled', true)}
//        if(countEscursioni > 0 ){ $('#escuEsc').attr('disabled', false); $('#escuEsc').next('span').css('textDecoration', 'none');} else {  $('#escuEsc').next('span').css('textDecoration', 'line-through'); $('#escuEsc').attr('disabled', true)}
//        if(countTrinity > 0 ){ $('#triColEx').attr('disabled', false); $('#triColEx').next('span').css('textDecoration', 'none');} else {  $('#triColEx').next('span').css('textDecoration', 'line-through'); $('#triColEx').attr('disabled', true)}
//
//        if(countFootAc > 0 ){ $('#footAc').attr('disabled', false); $('#footAc').next('span').css('textDecoration', 'none');} else {  $('#footAc').next('span').css('textDecoration', 'line-through'); $('#footAc').attr('disabled', true)}
//        if(countDanceAc > 0 ){$('#danceAc').attr('disabled', false); $('#danceAc').next('span').css('textDecoration', 'none');} else {  $('#danceAc').next('span').css('textDecoration', 'line-through'); $('#danceAc').attr('disabled', true)}
//        if(countBaskAc > 0 ){ $('#baskAc').attr('disabled', false); $('#baskAc').next('span').css('textDecoration', 'none');} else {  $('#baskAc').next('span').css('textDecoration', 'line-through'); $('#baskAc').attr('disabled', true)}
//        if(countTheaAc > 0 ){ $('#theaAc').attr('disabled', false); $('#theaAc').next('span').css('textDecoration', 'none');} else {  $('#theaAc').next('span').css('textDecoration', 'line-through'); $('#theaAc').attr('disabled', true)}
//
//
//        $('#numFam').text("(" + countFam + ")");
//        $('#numCol').text("(" + countColl + ")");
//        $('#numHot').text("(" + countHot + ")");
//        $('#numRes').text("(" + countRes + ")");
//        $('#numStu').text("(" + countStu + ")");
//        $('#numApp').text("(" + countApp + ")");
//        $('#numMon').text("(" + countMon + ")");
//
//        $('#numFinMil').text("(" + countMinMill + ")");
//        $('#minDiDue').text("(" + countMinDue + ")");
//        $('#magDiDue').text("(" + countMagDue + ")");
//
//        $('#numBagPriv').text("(" + countBagno + ")");
//        $('#numMedicoIta').text("(" + countMedico + ")");
//        $('#numEscuEsc').text("(" + countEscursioni + ")");
//        $('#numTriColEx').text("(" + countTrinity + ")");
//
//        $('#numFootAc').text("(" + countFootAc + ")");
//        $('#numDanceAc').text("(" + countDanceAc + ")");
//        $('#numBaskAc').text("(" + countBaskAc + ")");
//        $('#numTheaAc').text("(" + countTheaAc + ")");
//    }



    $("#ex2").slider({});

    $(window).load(function(){

        addClass();

        $('#testView').addClass('csspinner');

        var url_string = document.location.href;
//        console.log(url_string);
        var url = new URL(url_string);
//        console.log(url.search);
        if(url.search !== ""){
//            console.log("non è vuota");


            var eta = url.searchParams.get("eta");
            if(eta === 0 || typeof lingua === undefined){
                eta = "";
            }
            var lingua = url.searchParams.get("lingua");
            if(lingua === 0 || typeof lingua === undefined ){
                lingua = "";
            }
            var destinazione = url.searchParams.get("destinazione");
            if(destinazione === 0 || typeof lingua === undefined){
                destinazione = "";
            }
//            console.log(eta);
//            console.log(lingua);
//            console.log(destinazione);
//        if(eta !== null || lingua !== null || destinazione !== null ){
            $.ajax({
                url: 'includes/form-centri-lista.php',
                method: 'GET',
                cache: true,
                data: {etaT: eta, linguaT:lingua, destinazioneT:destinazione},
                success: function(data) {

                    var jsonResp = data;
//                    console.log(jsonResp);

                    var dataJson = $.parseJSON(data);

                    var countFam = 0;
                    var countColl = 0;
                    var countHot = 0;
                    var countRes = 0;
                    var countStu = 0;
                    var countApp= 0;
                    var countMon= 0;

                    var countMinMill = 0;
                    var countMinDue = 0;
                    var countMagDue = 0;

                    var countBagno = 0;
                    var countMedico = 0;
                    var countEscursioni = 0;
                    var countTrinity = 0;

                    var countFootAc = 0;
                    var countDanceAc = 0;
                    var countBaskAc= 0;
                    var countTheaAc = 0;



                    removeClass();

                    Object.keys(dataJson).forEach(function(k){
            //                        console.log(k + ' - ' + dataJson[k].Famiglia);
                        if(dataJson[k].Famiglia == 1){countFam++}
                        if(dataJson[k].College == 1){countColl++}
                        if(dataJson[k].Hotel == 1){countHot++}
                        if(dataJson[k].Residence == 1){countRes++}
                        if(dataJson[k].Studio == 1){countStu++}
                        if(dataJson[k].Appartamento == 1){countApp++}
                        if(dataJson[k].Monolocale == 1){countMon++}

                        if(dataJson[k].PrezziDa <= 1000){countMinMill++}
                        if(dataJson[k].PrezziDa > 1000 && dataJson[k].PrezziDa <=2000 ){countMinDue++}
                        if(dataJson[k].PrezziDa > 2000 ){countMagDue++}

                        if(dataJson[k].BagnoPrivato == 1){countBagno++}
                        if(dataJson[k].MedicoItaliano == 1){countMedico++}
                        if(dataJson[k].Escursioni == 1){countEscursioni++}
                        if(dataJson[k].Trinity == 1){countTrinity++}

                        if(dataJson[k].FootballAcademy == 1){countFootAc++}
                        if(dataJson[k].DanceAcademy == 1){countDanceAc++}
                        if(dataJson[k].BasketAcademy == 1){countBaskAc++}
                        if(dataJson[k].TheatreAcademy == 1){countTheaAc++}
                    });


//                    countAction();




//                    var lungData = dataJson.length;

//                    console.log(dataJson);

//                    pageSize = 1;
//
//                    var pageCount =  dataJson.length / pageSize;
//
//                    for(var u = 0 ; u<pageCount;u++){
//
//                        $("#pagin").append('<li><a href="#">'+(u+1)+'</a></li> ');
//                    }
//                    $("#pagin li").first().find("a").addClass("current");
//                    showPage = function(page) {
//                        $(".line-content").hide();
//                        $(".line-content").each(function(n) {
//                            if (n >= pageSize * (page - 1) && n < pageSize * page)
//                                $(this).show();
//                        });
//                    };
//
//                    showPage(1);
//
//                    $("#pagin li a").click(function() {
//                        $("#pagin li a").removeClass("current");
//                        $(this).addClass("current");
//                        showPage(parseInt($(this).text()))
//                    });

//                    testdata = [{"catUrl":"stage-linguistici","nazUrl":"usa","cittaUrl":"new-york","id":"167","id_catalogo":"5","Centro":"New York Ramapo & Boston","Foto":"e-boston.jpg","img_alt":"","img_title":"","Lingua":"francese","Sistemazione":"residence o famiglia","EtaMin":"10","EtaMax":"15","PrezziDa":"Prezzi da..","CentroUrl":"\/stage-linguistici\/usa\/new-york\/new-york-ramapo-boston","BreveDescrizione":"Test breve descrizione","DurataMin":"7 giorni","College":"1","Famiglia":"","Hotel":"","Residence":"1","GoldPlus":"1","FootballAcademy":"","DanceAcademy":"","BasketAcademy":"1","BagnoPrivato":"1","MedicoItaliano":"1","Escursioni":"1","Trinity":"","Studio":"1","Appartamento":"","Monolocale":"1","id_prezzo":"0","metaTitle":"","metaDescription":"","canonicalUrl":"","metaIndex":"index","urlfriendly":"new-york-ramapo-boston","stato":"pubblicato","lat":"0","lng":"0","NomeStruttura":"","IframeMaps":"","IframeAttrazioni":"","TheatreAcademy":"1","id_nazione":"41","id_categoria":"5","id_citta":"1"},{"catUrl":"stage-linguistici","nazUrl":"usa","cittaUrl":"new-york","id":"168","id_catalogo":"5","Centro":"New York Ramapo & Canada","Foto":"e-canada.jpg","img_alt":"","img_title":"","Lingua":"francese","Sistemazione":"college","EtaMin":"11","EtaMax":"15","PrezziDa":"Prezzi da..","CentroUrl":"\/stage-linguistici\/usa\/new-york\/new-york-ramapo-canada","BreveDescrizione":"Test breve descrizione","DurataMin":"15 giorni","College":"","Famiglia":"","Hotel":"","Residence":"","GoldPlus":"","FootballAcademy":"","DanceAcademy":"","BasketAcademy":"","BagnoPrivato":"1","MedicoItaliano":"","Escursioni":"1","Trinity":"","Studio":"1","Appartamento":"","Monolocale":"","id_prezzo":"0","metaTitle":"","metaDescription":"","canonicalUrl":"","metaIndex":"index","urlfriendly":"new-york-ramapo-canada","stato":"pubblicato","lat":"0","lng":"0","NomeStruttura":"","IframeMaps":"","IframeAttrazioni":"","TheatreAcademy":"","id_nazione":"41","id_categoria":"5","id_citta":"1"},{"catUrl":"stage-linguistici","nazUrl":"inghilterra","cittaUrl":"new-york","id":"170","id_catalogo":"6","Centro":"ultimo ins","Foto":"new-york-brooklyn-viaggio-studio-mla.jpg","img_alt":"","img_title":"","Lingua":"italiano","Sistemazione":"college o famiglia","EtaMin":"12","EtaMax":"17","PrezziDa":"Prezzi da..","CentroUrl":"\/stage-linguistici\/inghilterra\/new-york\/ultimo-ins","BreveDescrizione":"Test descrizione New York Ramapo & Washington D.C","DurataMin":"15 giorni","College":"1","Famiglia":"","Hotel":"","Residence":"","GoldPlus":"","FootballAcademy":"1","DanceAcademy":"","BasketAcademy":"","BagnoPrivato":"","MedicoItaliano":"","Escursioni":"","Trinity":"","Studio":"1","Appartamento":"","Monolocale":"","id_prezzo":"0","metaTitle":"","metaDescription":"","canonicalUrl":"","metaIndex":"index","urlfriendly":"ultimo-ins","stato":"bozza","lat":"0","lng":"0","NomeStruttura":"","IframeMaps":"","IframeAttrazioni":"","TheatreAcademy":"","id_nazione":"35","id_categoria":"5","id_citta":"1"},{"catUrl":"stage-linguistici","nazUrl":"usa","cittaUrl":"new-york","id":"173","id_catalogo":"5","Centro":"Loretto","Foto":"ny-top.jpg","img_alt":"","img_title":"","Lingua":"inglese","Sistemazione":"college","EtaMin":"11","EtaMax":"18","PrezziDa":"Prezzi da..","CentroUrl":"\/stage-linguistici\/usa\/new-york\/loretto","BreveDescrizione":"Test descrizione Loretto","DurataMin":"15 giorni","College":"1","Famiglia":"","Hotel":"","Residence":"","GoldPlus":"","FootballAcademy":"","DanceAcademy":"","BasketAcademy":"","BagnoPrivato":"","MedicoItaliano":"","Escursioni":"","Trinity":"","Studio":"1","Appartamento":"1","Monolocale":"","id_prezzo":"0","metaTitle":"","metaDescription":"","canonicalUrl":"","metaIndex":"index","urlfriendly":"loretto","stato":"pubblicato","lat":"0","lng":"0","NomeStruttura":"","IframeMaps":"","IframeAttrazioni":"","TheatreAcademy":"","id_nazione":"41","id_categoria":"5","id_citta":"1"},{"catUrl":"vacanze-studio","nazUrl":"inghilterra","cittaUrl":"san-diego","id":"171","id_catalogo":"2","Centro":"Londra Richmond","Foto":"new-york-ramapo-bostonviaggio-studio-mla.jpg","img_alt":"","img_title":"","Lingua":"italiano","Sistemazione":"hotel","EtaMin":"7","EtaMax":"17","PrezziDa":"1.1","CentroUrl":"http:\/\/test.it","BreveDescrizione":"Test descrizione Londra Richmond","DurataMin":"15 giorni","College":"1","Famiglia":"0","Hotel":"1","Residence":"0","GoldPlus":"0","FootballAcademy":"0","DanceAcademy":"0","BasketAcademy":"0","BagnoPrivato":"0","MedicoItaliano":"1","Escursioni":"0","Trinity":"0","Studio":"0","Appartamento":"1","Monolocale":"0","id_prezzo":"0","metaTitle":"","metaDescription":"","canonicalUrl":"","metaIndex":"","urlfriendly":"","stato":"pubblicato","lat":"0","lng":"0","NomeStruttura":"","IframeMaps":"","IframeAttrazioni":"","TheatreAcademy":"1","id_nazione":"35","id_categoria":"3","id_citta":"3"},{"catUrl":"stage-linguistici","nazUrl":"inghilterra","cittaUrl":"miami","id":"166","id_catalogo":"15","Centro":"New York Ramapo & Washington D.C","Foto":"ny-top.jpg","img_alt":"test","img_title":"gggggggg","Lingua":"italiano","Sistemazione":"famiglia","EtaMin":"8","EtaMax":"13","PrezziDa":"Prezzi da..","CentroUrl":"http:\/\/test.it","BreveDescrizione":"Test descrizione New York Ramapo & Washington D.C","DurataMin":"2 settimane","College":"1","Famiglia":"1","Hotel":"","Residence":"","GoldPlus":"","FootballAcademy":"1","DanceAcademy":"","BasketAcademy":"","BagnoPrivato":"1","MedicoItaliano":"","Escursioni":"1","Trinity":"","Studio":"","Appartamento":"1","Monolocale":"","id_prezzo":"0","metaTitle":"test meta title","metaDescription":"test descr","canonicalUrl":"","metaIndex":"index","urlfriendly":"new-york-ramapo-washington-dc","stato":"pubblicato","lat":"0","lng":"0","NomeStruttura":"","IframeMaps":"","IframeAttrazioni":"","TheatreAcademy":"","id_nazione":"35","id_categoria":"5","id_citta":"5"},{"catUrl":"stage-linguistici","nazUrl":"scozia","cittaUrl":"edimburgo","id":"172","id_catalogo":"5","Centro":"Heriot Watt","Foto":"heriot-watt.jpg","img_alt":"","img_title":"","Lingua":"inglese","Sistemazione":"famiglia o residence o appartamento","EtaMin":"17","EtaMax":"17","PrezziDa":"Prezzi da..","CentroUrl":"\/stage-linguistici\/scozia\/edimburgo\/heriot-watt","BreveDescrizione":"Test descrizione Heirott Watt","DurataMin":"8 giorni","College":"","Famiglia":"1","Hotel":"","Residence":"1","GoldPlus":"","FootballAcademy":"","DanceAcademy":"","BasketAcademy":"","BagnoPrivato":"","MedicoItaliano":"1","Escursioni":"","Trinity":"","Studio":"","Appartamento":"1","Monolocale":"","id_prezzo":"0","metaTitle":"","metaDescription":"","canonicalUrl":"","metaIndex":"index","urlfriendly":"heriot-watt","stato":"pubblicato","lat":"0","lng":"0","NomeStruttura":"","IframeMaps":"","IframeAttrazioni":"","TheatreAcademy":"","id_nazione":"36","id_categoria":"5","id_citta":"6"}]

//                    var naz = "";
                    $('#testTable').DataTable({
                        "aaData": dataJson,
                        "bFilter": false,
                        "bPaginate": true,
                        "bLengthChange": false,
//                    "fnCreatedRow" : function (row, dataJson, dataIndex) {
//                        console.log(row);
//                        var div = document.createElement("div");
//                        naz = $(div).html('<p style="background-color: red">' + dataJson.nazUrl + '</p>');
//                        $(row).children("td:last-child").append(div);
//                    },
                        "language": {
                            "emptyTable": "Nessun Centro Trovato",
                            "url": "js/datatables/traduzione/Italian.json"
                        },
                        "aoColumns": [{
//                        "mDataProp": "catUrl"
                            "mDataProp": "id",
                            "sClass":"nomeClasse",
                            "mRender":function(data, type, dato){
//                            var div = document.createElement("div");

//                            $(row).children("td:last-child").append(div);
//                            return dato.Lingua + dato.Sistemazione + dato.nazUrl;
//                            var html =  $(div).html(
//                                '<p style="background-color: red">' + dato.Lingua + '</p>' +
//                                '<p style="background-color: white">' + dato.Sistemazione + '</p>' +
//                                '<p style="background-color: green">' + dato.nazUrl + '</p>'
//                                );
                                return '<img src="images/centro/'+ dato.Foto +'" style="height:90px; width:90px">'
                                    + '<div style="background-color: yellow">' + dato.Centro + '</div>'
                                    + '<div style="background-color: green">' + dato.Sistemazione + '</div>'
                                    + '<div style="background-color: #4a1e7f; color: white">' + dato.EtaMin + ' - ' + dato.EtaMax + ' anni'+ '</div>'
                                    + '<div style="background-color: red">'+ dato.nazUrl + '</div>'
                                    + '<div style="background-color: #ffa400">'+ dato.Lingua + '</div>'
                                    + '<div style="background-color: blue; color: white">' + dato.PrezziDa + '</div>'
                                    + '<button type="submit" onclick="window.location.href=\'/vacanze-studio/inghilterra\'">Link</button>'
                                    ;
                            }
                        },
//                    {
//                        "mDataProp": "nazUrl"
//                    }
//                            {
//                            "mDataProp": "title"
//                        }, {
//                            "mDataProp": "pubdate"
//                        }, {
//                            "mDataProp": "url"
//                        }
                        ]
                    });

//                    $('#testView').removeClass('csspinner');
                    function spinnerTimeDestroy(){
                        $('#testView').removeClass('csspinner');

                    }
                    setTimeout(spinnerTimeDestroy, 900);

//                    var lung = dataJson.length;
//                    console.log(lung + " lunghezza del data");

//                    for(var i = 0; i < lung; i++ ){

//                        $('#testView').append($('<p class="remove">').html('CatUrl: '+dataJson[i].catUrl+'<br />BreveDescrizione: ' +dataJson[i].BreveDescrizione ));

//                    }
                }
            });
        } else {
            addClass();
//            console.log('è vota');
            $('#testView').addClass('csspinner');
            // Gestisco le disponibilità delle sistemazioni

//            $('#numCol').addClass('fa fa-spinner fa-spin');
//            $('#numFam').addClass('fa fa-spinner fa-spin');
//            $('#numHot').addClass('fa fa-spinner fa-spin');
//            $('#numRes').addClass('fa fa-spinner fa-spin');
//            $('#numStu').addClass('fa fa-spinner fa-spin');
//            $('#numApp').addClass('fa fa-spinner fa-spin');
//            $('#numMon').addClass('fa fa-spinner fa-spin');
//
//            $('#numFinMil').addClass('fa fa-spinner fa-spin');
//            $('#minDiDue').addClass('fa fa-spinner fa-spin');
//            $('#magDiDue').addClass('fa fa-spinner fa-spin');
//
//            $('#numBagPriv').addClass('fa fa-spinner fa-spin');
//            $('#numMedicoIta').addClass('fa fa-spinner fa-spin');
//            $('#numEscuEsc').addClass('fa fa-spinner fa-spin');
//            $('#numTriColEx').addClass('fa fa-spinner fa-spin');
//
//            $('#numFootAc').addClass('fa fa-spinner fa-spin');
//            $('#numDanceAc').addClass('fa fa-spinner fa-spin');
//            $('#numBaskAc').addClass('fa fa-spinner fa-spin');
//            $('#numTheaAc').addClass('fa fa-spinner fa-spin');


            var etaVal = $('#etaT').val();
            var linguaVal = $('#linguaT').val();
            var destinazioneVal = $('#destinazioneT').val();
            var collegeVal; if($('#college').is(':checked')){ collegeVal = 1}
            var famigliaVal; if($('#famiglia').is(':checked')){ famigliaVal = 1}
            var hotelVal; if($('#hotel').is(':checked')){ hotelVal = 1}
            var residenceVal; if($('#residence').is(':checked')){ residenceVal = 1}
            var studioVal; if($('#studio').is(':checked')){ studioVal = 1}
            var appartamentoVal; if($('#appartamento').is(':checked')){ appartamentoVal = 1}
            var monolocaleVal; if($('#monolocale').is(':checked')){ monolocaleVal = 1}
//            console.log(etaVal);
//            console.log(linguaVal);
//            console.log(destinazioneVal);
//            console.log(collegeVal);



//            $('#numCol').addClass('fa fa-spinner fa-spin');
//            $('#numFam').addClass('fa fa-spinner fa-spin');
//            $('#numHot').addClass('fa fa-spinner fa-spin');
//            $('#numRes').addClass('fa fa-spinner fa-spin');
//            $('#numStu').addClass('fa fa-spinner fa-spin');
//            $('#numApp').addClass('fa fa-spinner fa-spin');
//            $('#numMon').addClass('fa fa-spinner fa-spin');



            $.ajax({
                url: 'includes/form-centri-lista.php',
                method: 'GET',
                cache: true,
                data: {
                    etaT: etaVal,
                    linguaT:linguaVal,
                    destinazioneT:destinazioneVal,
                    college:collegeVal,
                    famiglia:famigliaVal,
                    hotel:hotelVal,
                    residence:residenceVal,
                    studio:studioVal,
                    appartamento:appartamentoVal,
                    monolocale:monolocaleVal,
                },
                success: function(data) {

                    var jsonResp = data;
//                    console.log(jsonResp);


                    var dataJson = $.parseJSON(data);
//                    console.log(dataJson);

                    var countFam = 0;
                    var countColl = 0;
                    var countHot = 0;
                    var countRes = 0;
                    var countStu = 0;
                    var countApp= 0;
                    var countMon= 0;

                    var countMinMill = 0;
                    var countMinDue = 0;
                    var countMagDue = 0;

                    var countBagno = 0;
                    var countMedico = 0;
                    var countEscursioni = 0;
                    var countTrinity = 0;

                    var countFootAc = 0;
                    var countDanceAc = 0;
                    var countBaskAc= 0;
                    var countTheaAc = 0;

                    Object.keys(dataJson).forEach(function(k){
//                        console.log(k + ' - ' + dataJson[k].Famiglia);
                        if(dataJson[k].Famiglia == 1){countFam++}
                        if(dataJson[k].College == 1){countColl++}
                        if(dataJson[k].Hotel == 1){countHot++}
                        if(dataJson[k].Residence == 1){countRes++}
                        if(dataJson[k].Studio == 1){countStu++}
                        if(dataJson[k].Appartamento == 1){countApp++}
                        if(dataJson[k].Monolocale == 1){countMon++}

                        if(dataJson[k].PrezziDa <= 1000){countMinMill++}
                        if(dataJson[k].PrezziDa > 1000 && dataJson[k].PrezziDa <=2000 ){countMinDue++}
                        if(dataJson[k].PrezziDa > 2000 ){countMagDue++}

                        if(dataJson[k].BagnoPrivato == 1){countBagno++}
                        if(dataJson[k].MedicoItaliano == 1){countMedico++}
                        if(dataJson[k].Escursioni == 1){countEscursioni++}
                        if(dataJson[k].Trinity == 1){countTrinity++}

                        if(dataJson[k].FootballAcademy == 1){countFootAc++}
                        if(dataJson[k].DanceAcademy == 1){countDanceAc++}
                        if(dataJson[k].BasketAcademy == 1){countBaskAc++}
                        if(dataJson[k].TheatreAcademy == 1){countTheaAc++}

                    });

//                    objectData(dataJson);



//                    $('#numCol').removeClass('fa fa-spinner fa-spin');
//                    $('#numFam').removeClass('fa fa-spinner fa-spin');
//                    $('#numHot').removeClass('fa fa-spinner fa-spin');
//                    $('#numRes').removeClass('fa fa-spinner fa-spin');
//                    $('#numStu').removeClass('fa fa-spinner fa-spin');
//                    $('#numApp').removeClass('fa fa-spinner fa-spin');
//                    $('#numMon').removeClass('fa fa-spinner fa-spin');
//
//                    $('#numFinMil').removeClass('fa fa-spinner fa-spin');
//                    $('#minDiDue').removeClass('fa fa-spinner fa-spin');
//                    $('#magDiDue').removeClass('fa fa-spinner fa-spin');
//
//                    $('#numBagPriv').removeClass('fa fa-spinner fa-spin');
//                    $('#numMedicoIta').removeClass('fa fa-spinner fa-spin');
//                    $('#numEscuEsc').removeClass('fa fa-spinner fa-spin');
//                    $('#numTriColEx').removeClass('fa fa-spinner fa-spin');
//
//                    $('#numFootAc').removeClass('fa fa-spinner fa-spin');
//                    $('#numDanceAc').removeClass('fa fa-spinner fa-spin');
//                    $('#numBaskAc').removeClass('fa fa-spinner fa-spin');
//                    $('#numTheaAc').removeClass('fa fa-spinner fa-spin');

                    removeClass();



                    if(countColl > 0 ){ $('#college').attr('disabled', false); $('#college').next('span').css('textDecoration', 'none');} else {  $('#college').next('span').css('textDecoration', 'line-through'); $('#college').attr('disabled', true)}
                    if(countFam > 0 ){ $('#famiglia').attr('disabled', false); $('#famiglia').next('span').css('textDecoration', 'none');} else {  $('#famiglia').next('span').css('textDecoration', 'line-through'); $('#famiglia').attr('disabled', true)}
                    if(countHot > 0 ){ $('#hotel').attr('disabled', false); $('#hotel').next('span').css('textDecoration', 'none');} else {  $('#hotel').next('span').css('textDecoration', 'line-through'); $('#hotel').attr('disabled', true)}
                    if(countRes > 0 ){ $('#residence').attr('disabled', false); $('#residence').next('span').css('textDecoration', 'none');} else {  $('#residence').next('span').css('textDecoration', 'line-through'); $('#residence').attr('disabled', true)}
                    if(countStu > 0 ){ $('#studio').attr('disabled', false); $('#studio').next('span').css('textDecoration', 'none');} else {  $('#studio').next('span').css('textDecoration', 'line-through'); $('#studio').attr('disabled', true)}
                    if(countApp > 0 ){ $('#appartamento').attr('disabled', false); $('#appartamento').next('span').css('textDecoration', 'none');} else {  $('#appartamento').next('span').css('textDecoration', 'line-through'); $('#appartamento').attr('disabled', true)}
                    if(countMon > 0 ){ $('#monolocale').attr('disabled', false); $('#monolocale').next('span').css('textDecoration', 'none');} else {  $('#monolocale').next('span').css('textDecoration', 'line-through'); $('#monolocale').attr('disabled', true)}

                    if(countMinMill > 0 ){ $('#finoMille').attr('disabled', false); $('#finoMille').next('span').css('textDecoration', 'none');} else {  $('#finoMille').next('span').css('textDecoration', 'line-through'); $('#finoMille').attr('disabled', true)}
                    if(countMinDue > 0 ){ $('#finoDue').attr('disabled', false); $('#finoDue').next('span').css('textDecoration', 'none');} else {  $('#finoDue').next('span').css('textDecoration', 'line-through'); $('#finoDue').attr('disabled', true)}
                    if(countMagDue > 0 ){ $('#magDue').attr('disabled', false); $('#magDue').next('span').css('textDecoration', 'none');} else {  $('#magDue').next('span').css('textDecoration', 'line-through'); $('#magDue').attr('disabled', true)}

                    if(countBagno > 0 ){ $('#bagPriv').attr('disabled', false); $('#bagPriv').next('span').css('textDecoration', 'none');} else {  $('#bagPriv').next('span').css('textDecoration', 'line-through'); $('#bagPriv').attr('disabled', true)}
                    if(countMedico > 0 ){ $('#medicoIta').attr('disabled', false); $('#medicoIta').next('span').css('textDecoration', 'none');} else {  $('#medicoIta').next('span').css('textDecoration', 'line-through'); $('#medicoIta').attr('disabled', true)}
                    if(countEscursioni > 0 ){ $('#escuEsc').attr('disabled', false); $('#escuEsc').next('span').css('textDecoration', 'none');} else {  $('#escuEsc').next('span').css('textDecoration', 'line-through'); $('#escuEsc').attr('disabled', true)}
                    if(countTrinity > 0 ){ $('#triColEx').attr('disabled', false); $('#triColEx').next('span').css('textDecoration', 'none');} else {  $('#triColEx').next('span').css('textDecoration', 'line-through'); $('#triColEx').attr('disabled', true)}

                    if(countFootAc > 0 ){ $('#footAc').attr('disabled', false); $('#footAc').next('span').css('textDecoration', 'none');} else {  $('#footAc').next('span').css('textDecoration', 'line-through'); $('#footAc').attr('disabled', true)}
                    if(countDanceAc > 0 ){$('#danceAc').attr('disabled', false); $('#danceAc').next('span').css('textDecoration', 'none');} else {  $('#danceAc').next('span').css('textDecoration', 'line-through'); $('#danceAc').attr('disabled', true)}
                    if(countBaskAc > 0 ){ $('#baskAc').attr('disabled', false); $('#baskAc').next('span').css('textDecoration', 'none');} else {  $('#baskAc').next('span').css('textDecoration', 'line-through'); $('#baskAc').attr('disabled', true)}
                    if(countTheaAc > 0 ){ $('#theaAc').attr('disabled', false); $('#theaAc').next('span').css('textDecoration', 'none');} else {  $('#theaAc').next('span').css('textDecoration', 'line-through'); $('#theaAc').attr('disabled', true)}


                    $('#numFam').text("(" + countFam + ")");
                    $('#numCol').text("(" + countColl + ")");
                    $('#numHot').text("(" + countHot + ")");
                    $('#numRes').text("(" + countRes + ")");
                    $('#numStu').text("(" + countStu + ")");
                    $('#numApp').text("(" + countApp + ")");
                    $('#numMon').text("(" + countMon + ")");

                    $('#numFinMil').text("(" + countMinMill + ")");
                    $('#minDiDue').text("(" + countMinDue + ")");
                    $('#magDiDue').text("(" + countMagDue + ")");

                    $('#numBagPriv').text("(" + countBagno + ")");
                    $('#numMedicoIta').text("(" + countMedico + ")");
                    $('#numEscuEsc').text("(" + countEscursioni + ")");
                    $('#numTriColEx').text("(" + countTrinity + ")");

                    $('#numFootAc').text("(" + countFootAc + ")");
                    $('#numDanceAc').text("(" + countDanceAc + ")");
                    $('#numBaskAc').text("(" + countBaskAc + ")");
                    $('#numTheaAc').text("(" + countTheaAc + ")");

//                    countAction();


//                    var lungData = dataJson.length;
//                    console.log(lungData);


//                    function compressArray(original) {
//
//                        var compressed = [];
//                        // make a copy of the input array
//                        var copy = original.slice(0);
//
//                        // first loop goes over every element
//                        for (var i = 0; i < original.length; i++) {
//
//                            var myCount = 0;
//                            // loop over every element in the copy and see if it's the same
//                            for (var w = 0; w < copy.length; w++) {
//                                if (original[i] == copy[w]) {
//                                    // increase amount of times duplicate is found
//                                    myCount++;
//                                    // sets item to undefined
//                                    delete copy[w];
//                                }
//                            }
//
//                            if (myCount > 0) {
//                                var a = new Object();
//                                a.value = original[i];
//                                a.count = myCount;
//                                compressed.push(a);
//                            }
//                        }
//
//                        return compressed;
//                    };
//
//                    var arrayOld = dataJson;
//                    var newArray = compressArray(arrayOld);
//                    console.log(newArray);

//                    var obj = {};
//
//                    function count(arr) {
//                        return arr.reduce((prev, curr) => (prev[curr] = ++prev[curr] || 1, prev), {})
//                    }



//                    var FamigliaArr = new Array();
//                    for(var l=0 ; l<=lungData; l++){
//                        var catUrlLen = dataJson[l];
//                        var Famiglia = dataJson[l].Famiglia;
//                        if(Famiglia === 1){
//                            var famigliaLen = Famiglia.length;
//                        }
//                        console.log(famigliaLen);
//                        obj[dataJson[l].Famiglia] = (obj[dataJson[l].Famiglia] || 0 ) +1;


//                        if(dataJson[l].Famiglia !==0){countFam++;}


//                        if(dataJson[l].Famiglia ==1){famArr.push();}
//                        famArr.push(dataJson[l].Famiglia);


//                        var concatExample = famArr.concat(dataJson[l].Famiglia);
//
//                        console.log(concatExample);

//                        console.log(countFam);

//                        console.log(dataJson[l].Famiglia);

//                        console.log(famLun + ' hbffffffffff');

//                        console.log(countFam);
//                        FamigliaArr = dataJson[l].Famiglia;
//                        if(dataJson[l].Famiglia === 1){
//                            FamigliaArr.push(+1);
//                        }
//                        console.log(FamigliaArr);
//                        console.log(obj.Famiglia);
//                        console.log(famLun + ' hbff');

//                    }




//                    var naz = "";
                    $('#testTable').DataTable({
                        "aaData": dataJson,
                        "bFilter": false,
                        "bPaginate": true,
                        "bLengthChange": false,
//                    "fnCreatedRow" : function (row, dataJson, dataIndex) {
//                        console.log(row);
//                        var div = document.createElement("div");
//                        naz = $(div).html('<p style="background-color: red">' + dataJson.nazUrl + '</p>');
//                        $(row).children("td:last-child").append(div);
//                    },
                        "language": {
                            "emptyTable": "Nessun Centro Trovato",
                            "url": "js/datatables/traduzione/Italian.json"
                        },
                        "aoColumns": [{
//                        "mDataProp": "catUrl"
                            "mDataProp": "id",
                            "sClass": "nomeClasse",
                            "mRender": function (data, type, dato) {
//                            var div = document.createElement("div");

//                            $(row).children("td:last-child").append(div);
//                            return dato.Lingua + dato.Sistemazione + dato.nazUrl;
//                            var html =  $(div).html(
//                                '<p style="background-color: red">' + dato.Lingua + '</p>' +
//                                '<p style="background-color: white">' + dato.Sistemazione + '</p>' +
//                                '<p style="background-color: green">' + dato.nazUrl + '</p>'
//                                );
                                return '<img src="images/centro/' + dato.Foto + '" style="height:90px; width:90px">'
                                    + '<div style="background-color: yellow">' + dato.Centro + '</div>'
                                    + '<div style="background-color: green">' + dato.Sistemazione + '</div>'
                                    + '<div style="background-color: #4a1e7f; color: white">' + dato.EtaMin + ' - ' + dato.EtaMax + ' anni'+ '</div>'
                                    + '<div style="background-color: red">' + dato.nazUrl + '</div>'
                                    + '<div style="background-color: #ffa400">'+ dato.Lingua + '</div>'
                                    + '<div style="background-color: blue; color: white">' + dato.PrezziDa + '</div>'
                                    + '<button type="submit" onclick="window.location.href=\'/vacanze-studio/inghilterra\'">Link</button>'
                                    ;
                            }
                        }]
                    });

//                    $('#testView').removeClass('csspinner');
                    function spinnerTimeDestroy(){
                        $('#testView').removeClass('csspinner');

                    }
                    setTimeout(spinnerTimeDestroy, 900);

//                    var lung = dataJson.length;
//                    console.log(lung + " lunghezza del data");

                }

            });
    }

    //    $(window).load(function(){
    //        $( "#listForm" ).trigger( "click" );
        });




</script>

<script>
//    $('.lista').on('change',function () {
//       console.log($(this).val());
//    });




    $('#etaT').on('change', function () {
//        $('#testView').remove();

    });

    $('#listForm').on('change', function () {
//        $('#testView').remove();
        $('#testView').addClass('csspinner');

        var opts = {
            lines: 7 // The number of lines to draw
            , length: 9 // The length of each line
            , width: 8 // The line thickness
            , radius: 9 // The radius of the inner circle
            , scale: 0.25 // Scales overall size of the spinner
            , corners: 1 // Corner roundness (0..1)
            , color: '#000' // #rgb or #rrggbb or array of colors
            , opacity: 0.4 // Opacity of the lines
            , rotate: 0 // The rotation offset
            , direction: 1 // 1: clockwise, -1: counterclockwise
            , speed: 1.5 // Rounds per second
            , trail: 60 // Afterglow percentage
            , fps: 20 // Frames per second when using setTimeout() as a fallback for CSS
            , zIndex: 2e9 // The z-index (defaults to 2000000000)
            , className: 'spinner' // The CSS class to assign to the spinner
            , top: '50%' // Top position relative to parent
            , left: '50%' // Left position relative to parent
            , shadow: true // Whether to render a shadow
            , hwaccel: false // Whether to use hardware acceleration
            , position: 'absolute' // Element positioning
        };


//        var target = document.getElementById('numCol');
//        var spinner = new Spinner(opts).spin(target);


//        $('#numCol').addClass('fa fa-spinner fa-spin');
//        $('#numFam').addClass('fa fa-spinner fa-spin');
//        $('#numHot').addClass('fa fa-spinner fa-spin');
//        $('#numRes').addClass('fa fa-spinner fa-spin');
//        $('#numStu').addClass('fa fa-spinner fa-spin');
//        $('#numApp').addClass('fa fa-spinner fa-spin');
//        $('#numMon').addClass('fa fa-spinner fa-spin');
//
//        $('#numFinMil').addClass('fa fa-spinner fa-spin');
//        $('#minDiDue').addClass('fa fa-spinner fa-spin');
//        $('#magDiDue').addClass('fa fa-spinner fa-spin');
//
//        $('#numBagPriv').addClass('fa fa-spinner fa-spin');
//        $('#numMedicoIta').addClass('fa fa-spinner fa-spin');
//        $('#numEscuEsc').addClass('fa fa-spinner fa-spin');
//        $('#numTriColEx').addClass('fa fa-spinner fa-spin');
//
//        $('#numFootAc').addClass('fa fa-spinner fa-spin');
//        $('#numDanceAc').addClass('fa fa-spinner fa-spin');
//        $('#numBaskAc').addClass('fa fa-spinner fa-spin');
//        $('#numTheaAc').addClass('fa fa-spinner fa-spin');


        addClass();



//        $('#current').remove();


//        $('.remove').remove();
//        $( "#testView" ).detach();
       var ser = $(this).serialize();
//       console.log(ser);

        $.ajax({
            url: 'includes/form-centri-lista.php',
            method: 'GET',
            cache: true,
//            data: {eta: eta, lingua:lingua, destinazione:destinazione },
            data: ser,
            success: function(data) {

                var dataJson = $.parseJSON(data);


                var countFam = 0;
                var countColl = 0;
                var countHot = 0;
                var countRes = 0;
                var countStu = 0;
                var countApp= 0;
                var countMon= 0;

                var countMinMill = 0;
                var countMinDue = 0;
                var countMagDue = 0;

                var countBagno = 0;
                var countMedico = 0;
                var countEscursioni = 0;
                var countTrinity = 0;

                var countFootAc = 0;
                var countDanceAc = 0;
                var countBaskAc= 0;
                var countTheaAc = 0;

                Object.keys(dataJson).forEach(function(k){
//                        console.log(k + ' - ' + dataJson[k].Famiglia);
                    if(dataJson[k].Famiglia == 1){countFam++}
                    if(dataJson[k].College == 1){countColl++}
                    if(dataJson[k].Hotel == 1){countHot++}
                    if(dataJson[k].Residence == 1){countRes++}
                    if(dataJson[k].Studio == 1){countStu++}
                    if(dataJson[k].Appartamento == 1){countApp++}
                    if(dataJson[k].Monolocale == 1){countMon++}

                    if(dataJson[k].PrezziDa <= 1000){countMinMill++}
                    if(dataJson[k].PrezziDa > 1000 && dataJson[k].PrezziDa <=2000 ){countMinDue++}
                    if(dataJson[k].PrezziDa > 2000 ){countMagDue++}

                    if(dataJson[k].BagnoPrivato == 1){countBagno++}
                    if(dataJson[k].MedicoItaliano == 1){countMedico++}
                    if(dataJson[k].Escursioni == 1){countEscursioni++}
                    if(dataJson[k].Trinity == 1){countTrinity++}

                    if(dataJson[k].FootballAcademy == 1){countFootAc++}
                    if(dataJson[k].DanceAcademy == 1){countDanceAc++}
                    if(dataJson[k].BasketAcademy == 1){countBaskAc++}
                    if(dataJson[k].TheatreAcademy == 1){countTheaAc++}


                });

//                objectData(dataJson);



//                $('#numCol').removeClass('fa fa-spinner fa-spin');
//                $('#numFam').removeClass('fa fa-spinner fa-spin');
//                $('#numHot').removeClass('fa fa-spinner fa-spin');
//                $('#numRes').removeClass('fa fa-spinner fa-spin');
//                $('#numStu').removeClass('fa fa-spinner fa-spin');
//                $('#numApp').removeClass('fa fa-spinner fa-spin');
//                $('#numMon').removeClass('fa fa-spinner fa-spin');
//
//                $('#numFinMil').removeClass('fa fa-spinner fa-spin');
//                $('#minDiDue').removeClass('fa fa-spinner fa-spin');
//                $('#magDiDue').removeClass('fa fa-spinner fa-spin');
//
//                $('#numBagPriv').removeClass('fa fa-spinner fa-spin');
//                $('#numMedicoIta').removeClass('fa fa-spinner fa-spin');
//                $('#numEscuEsc').removeClass('fa fa-spinner fa-spin');
//                $('#numTriColEx').removeClass('fa fa-spinner fa-spin');
//
//                $('#numFootAc').removeClass('fa fa-spinner fa-spin');
//                $('#numDanceAc').removeClass('fa fa-spinner fa-spin');
//                $('#numBaskAc').removeClass('fa fa-spinner fa-spin');
//                $('#numTheaAc').removeClass('fa fa-spinner fa-spin');

                removeClass();


//                console.log(countFam);
//                if(countFam > 0 ){console.log(true)} else { console.log(false)}

                if(countColl > 0 ){ $('#college').attr('disabled', false); $('#college').next('span').css('textDecoration', 'none');} else {  $('#college').next('span').css('textDecoration', 'line-through'); $('#college').attr('disabled', true)}
                if(countFam > 0 ){ $('#famiglia').attr('disabled', false); $('#famiglia').next('span').css('textDecoration', 'none');} else {  $('#famiglia').next('span').css('textDecoration', 'line-through'); $('#famiglia').attr('disabled', true)}
                if(countHot > 0 ){ $('#hotel').attr('disabled', false); $('#hotel').next('span').css('textDecoration', 'none');} else {  $('#hotel').next('span').css('textDecoration', 'line-through'); $('#hotel').attr('disabled', true)}
                if(countRes > 0 ){ $('#residence').attr('disabled', false); $('#residence').next('span').css('textDecoration', 'none');} else {  $('#residence').next('span').css('textDecoration', 'line-through'); $('#residence').attr('disabled', true)}
                if(countStu > 0 ){ $('#studio').attr('disabled', false); $('#studio').next('span').css('textDecoration', 'none');} else {  $('#studio').next('span').css('textDecoration', 'line-through'); $('#studio').attr('disabled', true)}
                if(countApp > 0 ){ $('#appartamento').attr('disabled', false); $('#appartamento').next('span').css('textDecoration', 'none');} else {  $('#appartamento').next('span').css('textDecoration', 'line-through'); $('#appartamento').attr('disabled', true)}
                if(countMon > 0 ){ $('#monolocale').attr('disabled', false); $('#monolocale').next('span').css('textDecoration', 'none');} else {  $('#monolocale').next('span').css('textDecoration', 'line-through'); $('#monolocale').attr('disabled', true)}

                if(countMinMill > 0 ){ $('#finoMille').attr('disabled', false); $('#finoMille').next('span').css('textDecoration', 'none');} else {  $('#finoMille').next('span').css('textDecoration', 'line-through'); $('#finoMille').attr('disabled', true)}
                if(countMinDue > 0 ){ $('#finoDue').attr('disabled', false); $('#finoDue').next('span').css('textDecoration', 'none');} else {  $('#finoDue').next('span').css('textDecoration', 'line-through'); $('#finoDue').attr('disabled', true)}
                if(countMagDue > 0 ){ $('#magDue').attr('disabled', false); $('#magDue').next('span').css('textDecoration', 'none');} else {  $('#magDue').next('span').css('textDecoration', 'line-through'); $('#magDue').attr('disabled', true)}

                if(countBagno > 0 ){ $('#bagPriv').attr('disabled', false); $('#bagPriv').next('span').css('textDecoration', 'none');} else {  $('#bagPriv').next('span').css('textDecoration', 'line-through'); $('#bagPriv').attr('disabled', true)}
                if(countMedico > 0 ){ $('#medicoIta').attr('disabled', false); $('#medicoIta').next('span').css('textDecoration', 'none');} else {  $('#medicoIta').next('span').css('textDecoration', 'line-through'); $('#medicoIta').attr('disabled', true)}
                if(countEscursioni > 0 ){ $('#escuEsc').attr('disabled', false); $('#escuEsc').next('span').css('textDecoration', 'none');} else {  $('#escuEsc').next('span').css('textDecoration', 'line-through'); $('#escuEsc').attr('disabled', true)}
                if(countTrinity > 0 ){ $('#triColEx').attr('disabled', false); $('#triColEx').next('span').css('textDecoration', 'none');} else {  $('#triColEx').next('span').css('textDecoration', 'line-through'); $('#triColEx').attr('disabled', true)}

                if(countFootAc > 0 ){ $('#footAc').attr('disabled', false); $('#footAc').next('span').css('textDecoration', 'none');} else {  $('#footAc').next('span').css('textDecoration', 'line-through'); $('#footAc').attr('disabled', true)}
                if(countDanceAc > 0 ){$('#danceAc').attr('disabled', false); $('#danceAc').next('span').css('textDecoration', 'none');} else {  $('#danceAc').next('span').css('textDecoration', 'line-through'); $('#danceAc').attr('disabled', true)}
                if(countBaskAc > 0 ){ $('#baskAc').attr('disabled', false); $('#baskAc').next('span').css('textDecoration', 'none');} else {  $('#baskAc').next('span').css('textDecoration', 'line-through'); $('#baskAc').attr('disabled', true)}
                if(countTheaAc > 0 ){ $('#theaAc').attr('disabled', false); $('#theaAc').next('span').css('textDecoration', 'none');} else {  $('#theaAc').next('span').css('textDecoration', 'line-through'); $('#theaAc').attr('disabled', true)}



                $('#numFam').text("(" + countFam + ")");
                $('#numCol').text("(" + countColl + ")");
                $('#numHot').text("(" + countHot + ")");
                $('#numRes').text("(" + countRes + ")");
                $('#numStu').text("(" + countStu + ")");
                $('#numApp').text("(" + countApp + ")");
                $('#numMon').text("(" + countMon + ")");

                $('#numFinMil').text("(" + countMinMill + ")");
                $('#minDiDue').text("(" + countMinDue + ")");
                $('#magDiDue').text("(" + countMagDue + ")");

                $('#numBagPriv').text("(" + countBagno + ")");
                $('#numMedicoIta').text("(" + countMedico + ")");
                $('#numEscuEsc').text("(" + countEscursioni + ")");
                $('#numTriColEx').text("(" + countTrinity + ")");

                $('#numFootAc').text("(" + countFootAc + ")");
                $('#numDanceAc').text("(" + countDanceAc + ")");
                $('#numBaskAc').text("(" + countBaskAc + ")");
                $('#numTheaAc').text("(" + countTheaAc + ")");

//                var jsonString = JSON.stringify(data);
//                console.log(jsonString);
//                console.log(dataJson);
//                $('#testView').removeClass('csspinner');
                function spinnerTimeDestroy(){
                    $('#testView').removeClass('csspinner');

                }
                setTimeout(spinnerTimeDestroy, 900);

//                countAction();


//                var lung = dataJson.length;
//                console.log(lung + " lunghezza del data");

//                for(var i = 0; i < lung; i++ ){

//                    console.log(dataJson[i].BreveDescrizione);



//                    $('.testView').append($('<p>').html('CatUrl: '+dataJson[i].catUrl+'<br />BreveDescrizione: ' +dataJson[i].BreveDescrizione ));
                    //BUONO
//                    $('#testView').append($('<div class="remove">')
//                        .html(
//                            'CatUrl: '+dataJson[i].catUrl+'<br />BreveDescrizione: ' +dataJson[i].BreveDescrizione
//                        )
//                    );

//                    $('.demo1').bootpag({
//                        total: 5
//                    }).on("page", function(event, num){
//                        $("#testView").html('CatUrl: '+dataJson[i].catUrl+'<br />BreveDescrizione: ' +dataJson[i].BreveDescrizione ); // or some ajax content loading...
//
//                        // ... after content load -> change total to 10
//                        $(this).bootpag({total: 5, maxVisible: 10});
//
//                    });

//                    return false;

//                    console.log($('#testView-'+i).html(dataJson[i].BreveDescrizione));
//                    $('.testView').append($('<p>').text(dataJson[i].catUrl + '<br>'));
//                    var desc = obj.BreveDescrizione;
//                    console.log(desc);
//                    var obj = dataJson[i];
//                    console.log(obj);
//                    $.each( obj, function( key, value ) {
////                        console.log( key + ": " + value );
//                        $('#testView').html(
//                      '<div class="col-md-8" style="background: lightblue">'
//                      + value +
//                      '</div>'
//                    );


//                }
                $('#testTable').DataTable({
                    "aaData": dataJson,
                    "bFilter": false,
                    "bPaginate": true,
                    "bLengthChange": false,
                    "language": {
                        "emptyTable": "Nessun Centro Trovato",
                        "url": "js/datatables/traduzione/Italian.json"
                    },
                    destroy: true,
                    "aoColumns": [{
                        "mDataProp": "id",
                        "sClass":"nomeClasse",
                        "mRender":function(data, type, dato){
//                            var div = document.createElement("div");

//                            $(row).children("td:last-child").append(div);
//                            return dato.Lingua + dato.Sistemazione + dato.nazUrl;
//                            var html =  $(div).html(
//                                '<p style="background-color: red">' + dato.Lingua + '</p>' +
//                                '<p style="background-color: white">' + dato.Sistemazione + '</p>' +
//                                '<p style="background-color: green">' + dato.nazUrl + '</p>'
//                                );
                            return '<img src="images/centro/'+ dato.Foto +'" style="height:90px; width:90px">'
                                + '<div style="background-color: yellow">' + dato.Centro + '</div>'
                                + '<div style="background-color: green">' + dato.Sistemazione + '</div>'
                                + '<div style="background-color: #4a1e7f; color: white">' + dato.EtaMin + ' - ' + dato.EtaMax + ' anni'+ '</div>'
                                + '<div style="background-color: red">'+ dato.nazUrl + '</div>'
                                + '<div style="background-color: #ffa400">'+ dato.Lingua + '</div>'
                                + '<div style="background-color: blue;  color: white">' + dato.PrezziDa + '</div>'
                                + '<button type="submit" onclick="window.location.href=\'/vacanze-studio/inghilterra\'">Link</button>'
                                ;
                        }
                    },

//                        {
//                        "mDataProp": "nazUrl"
//                    }
//                            {
//                            "mDataProp": "title"
//                        }, {
//                            "mDataProp": "pubdate"
//                        }, {
//                            "mDataProp": "url"
//                        }
                    ]
                });
            }
        });

    });

</script>





