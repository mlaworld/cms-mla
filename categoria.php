<?php
/**
 * Created da Giuseppe Alessandro De Blasio.
 * User: Giuseppe Alessandro De Blasio
 * Date: 09/05/17
 * Time: 15:33
 */

include 'includes/header.php';

global $databasePDO;
$databasePDO->query("SELECT * FROM view_categorie_page WHERE urlFriCat = :categoria ");
$databasePDO->bind(':categoria', $_GET['categoria']);
$contenutoObj = $databasePDO->resultClass('CategoriePage');
foreach ($contenutoObj AS $contenutoPagina){
    $contenutoPagina;
}
if($contenutoPagina == false):
    redirect('404');
endif;

?>


<title><?php echo $contenutoPagina->metaTitle; ?></title>
<meta name="description" content="<?php echo $contenutoPagina->metaDescription; ?>">
<?php echo (empty($contenutoPagina->canonicalUrl))? null : '<link rel="canonical" href="'.$contenutoPagina->canonicalUrl.'"/>' ?>
<?php echo ($contenutoPagina->metaRobots === 'index')? null :  '<meta name="robots" content="noindex,nofollow"' ?>

</head>

<body>

<header>

    <?php include 'includes/menu.php'; ?>

</header>

<section>
    <div class="contenitore-generale-interno">
        <div class="container">
            <div class="contenitore-menu-interno">

                <a class="brand-menu-interno" href="index.php">Move Language Ahead</a>
                <?php echo easymenu(2, 'class="nav navbar-nav menu-interno"'); ?>

            </div>
        </div>
    </div>
</section>

<section>

    <div class="contenitore-generale-interno">
        <div class="container">

            <div class="col-md-12 no-padding contenitore-breadcrumb">
                <ol class="breadcrumb lista-breadcrumb">
                    <li><a href="#">Home</a></li>
                    <li class="active"><?php echo $contenutoPagina->titolo_categoria; ?></li>
                </ol>
            </div>

            <div class="col-md-12 box-titolo-pagine">
                <?php echo $contenutoPagina->titolo; ?>
            </div>

        </div>
    </div>

</section>

<section>

    <div class="contenitore-generale-interno">
        <div class="container">
            <div class="collapse-catalogo">

                <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">

                    <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="heading1">
                            <h2 class="panel-title">
                                <a role="button" data-toggle="collapse" data-parent="#accordion1" href="#sogg1" aria-expanded="true" aria-controls="sogg1">
                                    <?php echo $contenutoPagina->titBox1; ?>
                                </a>
                            </h2>
                        </div>
                        <div id="sogg1" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="heading1">
                            <div class="panel-body no-padding">
                                <?php echo $contenutoPagina->box1; ?>
                            </div>
                        </div>
                    </div>

                    <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="heading2">
                            <h2 class="panel-title">
                                <a role="button" data-toggle="collapse" data-parent="#accordion2" href="#sogg2" aria-expanded="false" aria-controls="sogg2" class="collapsed">
                                    <?php echo $contenutoPagina->titBox2; ?>
                                </a>
                            </h2>
                        </div>
                        <div id="sogg2" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading2" style="height: 0px;">
                            <div class="panel-body no-padding">
                                <?php echo $contenutoPagina->box2; ?>
                            </div>
                        </div>
                    </div>

                    <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="heading3">
                            <h2 class="panel-title">
                                <a role="button" data-toggle="collapse" data-parent="#accordion3" href="#sogg3" aria-expanded="false" aria-controls="sogg3" class="collapsed">
                                    <?php echo $contenutoPagina->titBox3; ?>
                                </a>
                            </h2>
                        </div>
                        <div id="sogg3" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading3" style="height: 0px;">
                            <div class="panel-body no-padding">
                                <?php echo $contenutoPagina->box3; ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</section>

<?php

global $databasePDO;
$databasePDO->query("SELECT * FROM nazione_page WHERE id_categoria = :categoria ");
$databasePDO->bind(':categoria', $contenutoPagina->id);
$mostraNazioni = $databasePDO->resultClass('NazioniPage');
//var_dump($mostraNazioni);

//$conta = NazioniPage::conta_elementi_filtro('id_categoria', $contenutoPagina->id_cat);
?>
<section>
    <div class="contenitore-generale-interno">
        <div class="container contenitore-destinazioni">
            <span class="titolo-section">Le nostre destinazioni per <?php echo $contenutoPagina->titolo_categoria; ?></span>
            <div class="row row-destinazioni">
                <?php foreach ($mostraNazioni as $index => $mostraNazione): ?>
                    <?php if($index == 0 || $index == 5 || $index == 10 || $index == 15): ?>
                            <div class="col-sm-6 col-md-4-5 col-lg-1-5 singola-destinazione test">
                                <a href="<?php echo $mostraNazione->nazioneUrl; ?>">
                                    <img class="img-responsive" src="<?php echo $mostraNazione->percorso_immagine_e_plecholder_front(); ?>" alt="<?php echo $mostraNazione->img_alt; ?>" />
                                </a>
                                <a class="link-singola-destinazione" href="<?php echo $mostraNazione->nazioneUrl; ?>"><?php  $nomeNazione = Nazioni::seleziona_per_id($mostraNazione->id_nazione); echo $nomeNazione->nazione; ?></a>
                            </div>
                        <?php else: ?>
                        <div class="col-sm-6 col-md-4-5 col-lg-1-5 singola-destinazione">
                            <a href="<?php echo $mostraNazione->nazioneUrl; ?>">
                                <img class="img-responsive" src="<?php echo $mostraNazione->percorso_immagine_e_plecholder_front(); ?>" alt="<?php echo $mostraNazione->img_alt; ?>" />
                            </a>
                            <a class="link-singola-destinazione" href="<?php echo $mostraNazione->nazioneUrl; ?>"><?php  $nomeNazione = Nazioni::seleziona_per_id($mostraNazione->id_nazione); echo $nomeNazione->nazione; ?></a>
                        </div>
                <?php endif; ?>
                <?php endforeach; ?>
            </div>
        </div>
    </div>

</section>

<?php include 'includes/test-inglese-footer.php'; ?>
<?php include 'includes/cataloghi-footer.php'; ?>
<?php include 'includes/40anni-footer.php'; ?>
<?php include 'includes/blog-footer.php'; ?>
<?php include 'includes/footer.php'; ?>

</body>
</html>
