	<?php include 'includes/header.php'; ?>
    <?php

    $categoria = $_GET['categoria'];
    $nazione = $_GET['nazione'];
    $citta = $_GET['citta'];
    $centroUrl = $_GET['centro'];

//    $categoria = 'stage-linguistici';
//    $nazione = 'inghilterra';
//    $citta = 'miami';
    global $databasePDO;
    $databasePDO->query("SELECT * FROM view_centro_page WHERE catUrl= :categorie AND nazUrl = :nazione AND cittaUrl = :citta  AND urlfriendly = :centroUrl ");
    $databasePDO->bind(':categorie', $categoria);
    $databasePDO->bind(':nazione', $nazione);
    $databasePDO->bind(':citta', $citta);
    $databasePDO->bind(':centroUrl', $centroUrl);
    $centro = $databasePDO->resultClass('Centro');
    var_dump($centro);
    ($databasePDO->rowCount() !== 0)?: redirect('../404');


    ?>
<!--    --><?php //$feeds = simplexml_load_file('http://www.mlaworld.com/blog/feed'); ?>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.0.47/jquery.fancybox.min.css">
</head>

<body>

    <header>

        <?php include 'includes/menu.php'; ?>

    </header>

    <section>
        <div class="contenitore-generale-interno">
            <div class="container">
                <div class="contenitore-menu-interno">

                    <a class="brand-menu-interno" href="index.php">Move Language Ahead</a>
                    <?php echo easymenu(2, 'class="nav navbar-nav menu-interno"'); ?>

                </div>
            </div>
        </div>
    </section>

    <section>
    	<div class="container container-principale-centro">

        	<div class="col-md-12 col-xs-12 immagine-centro">
            	<img class="img-responsive" src="../../images/img-centro.jpg" alt="" />
            </div>

            <div class="clear"></div>

            <div class="col-md-12 no-padding contenuto-centro">

            	<div class="col-md-8 col-xs-6 no-padding box-titolo-centro-sx">
                    <div class="col-md-8 no-padding contenitore-breadcrumb">
                        <ol class="breadcrumb lista-breadcrumb">
                          <li><a href="#">Adulti</a></li>
                          <li><a href="#">Inghilterra</a></li>
                          <li><a href="#">Londra</a></li>
                          <li class="active">Brunel University</li>
                        </ol>
                    </div>

                    <div class="col-md-8 box-titolo-centro">
                        <h1 class="titolo-centro"><strong>London</strong><br>Brunel University</h1>
                        <span>12-18 anni</span>
                    </div>

                </div>

                <div class="col-md-4 col-xs-6 no-padding box-prezzo-centro-dx">
                    <div class="col-md-12 contenitore-prezzo-centro">
                        <div class="col-md-12 no-padding prezzo-centro">
                            <span>a partire da</span>
                            <p class="prezzo-partire">1050 €</p>
                        </div>
                    </div>
                    <div class="col-md-12 box-iscriviti-dx">
                        <div class="col-md-12 contenitore-prenota">
                        	<div class="col-md-6 no-padding">
                            	<a href="#" class="bottone-preventivo-centro">Preventivo gratuito</a>
                            </div>
                            <div class="col-md-6 no-padding">
                            	<a href="#" class="bottone-prenota-centro">Prenota</a>
                            </div>
                            <div class="clear"></div>
                            <a href="#" class="bottone-contatta-centro">Domande?  Contattaci!</a>
                        </div>
                    </div>
                </div>

            </div>

        </div>
    </section>

    <div class="clear"></div>
    <section>
        <div class="container contenitore-tab-centro">
            <div class="row">
                 <div class="col-md-12 no-padding">
                    <div class="card">

                        <ul class="nav nav-tabs tab-centro" role="tablist">
                            <li role="presentation" class="col-md-3 titolo-tab no-padding active"><a href="#corso" class="corso" aria-controls="home" role="tab" data-toggle="tab">Corsi e prezzi</a></li>
                            <li role="presentation" class="col-md-3 titolo-tab no-padding"><a href="#sistemazione" class="sistemazione" aria-controls="profile" role="tab" data-toggle="tab">Sistemazione</a></li>
                            <li role="presentation" class="col-md-3 titolo-tab no-padding"><a href="#messages" class="messages" aria-controls="messages" role="tab" data-toggle="tab">Tempo libero</a></li>
                            <li role="presentation" class="col-md-3 titolo-tab no-padding"><a href="#scuola" id="scuola1" class="scuola" aria-controls="settings" role="tab" data-toggle="tab">Scuola</a></li>
                        </ul>

                        <div class="col-md-12 contenitore-tab-corsi">

                            <!-- Tab panes -->
                            <div class="tab-content no-padding">

                                <div role="tabpanel" class="tab-pane active" id="corso">

                                     <div class="row">
                                        <div class="col-md-12 no-padding">
                                            <div class="collapse-centro">
                                                <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">

                                                    <div class="panel panel-default">
                                                        <div class="panel-heading" role="tab" id="corso-1">
                                                            <h4 class="panel-title">
                                                                <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#corso-centro1" aria-expanded="false" aria-controls="corso-centro1">
                                                                    <span class="titolo-centro-sx">
                                                                        <strong>Corso Standard</strong><br>Principiante a Avanzato<br>1-48 settimane/a
                                                                    </span>
                                                                    <span class="titolo-centro-dx">
                                                                        1 settimana/e<br><strong>a partire da 1890 €</strong>
                                                                    </span>
                                                                </a>
                                                            </h4>
                                                        </div>
                                                        <div id="corso-centro1" class="panel-collapse collapse corso-centro-contenuto" role="tabpanel" aria-labelledby="corso1">

                                                            <div class="panel-body">

                                                               <div class="col-md-2 no-padding centro-caratteristica">
                                                                    <span class="titolo-carat-corso">Lezioni</span>
                                                                    <p>20/45 minuti</p>
                                                               </div>

                                                               <div class="col-md-2 no-padding centro-caratteristica">
                                                                    <span class="titolo-carat-corso">Livello</span>
                                                                    <p>Principiante</p>
                                                               </div>

                                                               <div class="col-md-2 no-padding centro-caratteristica">
                                                                    <span class="titolo-carat-corso">Studenti/Classe</span>
                                                                    <p>Max 14 persone</p>
                                                               </div>

                                                               <div class="col-md-2 no-padding centro-caratteristica">
                                                                    <span class="titolo-carat-corso">Età minima</span>
                                                                    <p>16 anni</p>
                                                               </div>

                                                               <div class="col-md-2 no-padding centro-caratteristica">
                                                                    <span class="titolo-carat-corso">Durata del corso</span>
                                                                    <p>1/48 settimane/a</p>
                                                               </div>

                                                               <div class="col-md-2 no-padding centro-caratteristica">
                                                                    <span class="titolo-carat-corso">Orario</span>
                                                                    <p>09:00-12:15</p>
                                                               </div>

                                                            </div>

                                                            <div class="panel-body">
                                                                <div class="table-responsive">
                                                                  <table class="table table-condensed tabella-centro-prezzi">
                                                                    <thead>
                                                                        <tr>
                                                                            <th width="25%">Settimana/e</th>
                                                                            <th width="25%">Famiglia</th>
                                                                            <th width="25%">Senza<br>alloggio</th>
                                                                            <th width="25%">Student<br>House</th>
                                                                        </tr>
                                                                    </thead>
                                                                    <tbody>
                                                                        <tr>
                                                                            <td>1</td>
                                                                            <td>1582</td>
                                                                            <td>1841</td>
                                                                            <td>2841</td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>1</td>
                                                                            <td>1582</td>
                                                                            <td>1841</td>
                                                                            <td>2841</td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>1</td>
                                                                            <td>1582</td>
                                                                            <td>1841</td>
                                                                            <td>2841</td>
                                                                        </tr>
                                                                    </tbody>
                                                                  </table>
                                                                </div>
                                                            </div>

                                                            <div class="panel-group panel-body panel-body-interno" id="accordion-opzioni1">
                                                              <div class="panel panel-default">
                                                                <div class="panel-heading panel-interno">
                                                                  <h4 class="panel-title">
                                                                    <a class="pannello-interno collapsed" data-toggle="collapse" data-parent="#accordion-opzioni1" href="#opzioni1" aria-expanded="false">
                                                                        Opzioni e supplementi
                                                                    </a>
                                                                  </h4>
                                                                </div>
                                                                <div id="opzioni1" class="panel-collapse collapse">
                                                                  <div class="panel-body panel-body-opzioni">
                                                                    <div class="table-responsive">
                                                                      <table class="table table-condensed tabella-centro-supplementi">
                                                                        <thead>
                                                                            <tr>
                                                                                <th width="75%">Opzione</th>
                                                                                <th width="25%">Supplemento</th>
                                                                            </tr>
                                                                        </thead>
                                                                        <tbody>
                                                                            <tr>
                                                                                <td>Tassa d'esame Cambridge FCE (da pagare sul posto) (Prezzo su richiesta)</td>
                                                                                <td>70 €</td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>Tassa d'esame Cambridge CAE (da pagare sul posto) (Prezzo su richiesta)</td>
                                                                                <td>90 €</td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>Tassa d'esame Cambridge GPE (da pagare sul posto) (Prezzo su richiesta)</td>
                                                                                <td>120 €</td>
                                                                            </tr>
                                                                        </tbody>
                                                                      </table>
                                                                    </div>
                                                                </div>
                                                                </div>
                                                              </div>
                                                            </div>

                                                            <div class="panel-body">

                                                               <div class="col-md-3 no-padding centro-lezioni">
                                                                    <span class="titolo-carat-corso">Inizio del corso</span>
                                                                    <p>Ogni settimana, tutto l'anno</p>
                                                               </div>

                                                               <div class="col-md-3 no-padding centro-lezioni">
                                                                    <span class="titolo-carat-corso">Giorni festivi</span>
                                                                    <p>02.01 / 14.04 / 17.04 / 01.05<br>28.08 / 25.12 / 26.12.2017</p>
                                                               </div>

                                                               <div class="col-md-3 no-padding centro-lezioni">
                                                                    <span class="titolo-carat-corso">Vacanze</span>
                                                                    <p>25.12.2016 - 01.01.2017</p>
                                                               </div>

                                                               <div class="col-md-3 no-padding centro-lezioni">
                                                                    <a href="#" class="bottone-preventivo-centro">Preventivo gratuito</a>
                                                                    <a href="#" class="bottone-prenota-centro">Prenota</a>
                                                               </div>

                                                            </div>

                                                        </div>

                                                    </div>

                                                    <div class="panel panel-default">
                                                        <div class="panel-heading" role="tab" id="corso-2">
                                                            <h4 class="panel-title">
                                                                <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#corso-centro2" aria-expanded="false" aria-controls="corso-centro1">
                                                                    <span class="titolo-centro-sx">
                                                                        <strong>Corso Intensivo</strong><br>Principiante a Avanzato<br>1-24 settimane/a
                                                                    </span>
                                                                    <span class="titolo-centro-dx">
                                                                        5 settimana/e<br><strong>a partire da 2190 €</strong>
                                                                    </span>
                                                                </a>
                                                            </h4>
                                                        </div>
                                                        <div id="corso-centro2" class="panel-collapse collapse corso-centro-contenuto" role="tabpanel" aria-labelledby="corso2">

                                                            <div class="panel-body">

                                                               <div class="col-md-2 no-padding centro-caratteristica">
                                                                    <span class="titolo-carat-corso">Lezioni</span>
                                                                    <p>20/45 minuti</p>
                                                               </div>

                                                               <div class="col-md-2 no-padding centro-caratteristica">
                                                                    <span class="titolo-carat-corso">Livello</span>
                                                                    <p>Principiante</p>
                                                               </div>

                                                               <div class="col-md-2 no-padding centro-caratteristica">
                                                                    <span class="titolo-carat-corso">Studenti/Classe</span>
                                                                    <p>Max 14 persone</p>
                                                               </div>

                                                               <div class="col-md-2 no-padding centro-caratteristica">
                                                                    <span class="titolo-carat-corso">Età minima</span>
                                                                    <p>16 anni</p>
                                                               </div>

                                                               <div class="col-md-2 no-padding centro-caratteristica">
                                                                    <span class="titolo-carat-corso">Durata del corso</span>
                                                                    <p>1/48 settimane/a</p>
                                                               </div>

                                                               <div class="col-md-2 no-padding centro-caratteristica">
                                                                    <span class="titolo-carat-corso">Orario</span>
                                                                    <p>09:00-12:15</p>
                                                               </div>

                                                            </div>

                                                            <div class="panel-body">
                                                                <div class="table-responsive">
                                                                  <table class="table table-condensed tabella-centro-prezzi">
                                                                    <thead>
                                                                        <tr>
                                                                            <th width="25%">Settimana/e</th>
                                                                            <th width="25%">Famiglia</th>
                                                                            <th width="25%">Senza<br>alloggio</th>
                                                                            <th width="25%">Student<br>House</th>
                                                                        </tr>
                                                                    </thead>
                                                                    <tbody>
                                                                        <tr>
                                                                            <td>1</td>
                                                                            <td>1582</td>
                                                                            <td>1841</td>
                                                                            <td>2841</td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>1</td>
                                                                            <td>1582</td>
                                                                            <td>1841</td>
                                                                            <td>2841</td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>1</td>
                                                                            <td>1582</td>
                                                                            <td>1841</td>
                                                                            <td>2841</td>
                                                                        </tr>
                                                                    </tbody>
                                                                  </table>
                                                                </div>
                                                            </div>

                                                            <div class="panel-group panel-body panel-body-interno" id="accordion-opzioni1">
                                                              <div class="panel panel-default">
                                                                <div class="panel-heading panel-interno">
                                                                  <h4 class="panel-title">
                                                                    <a class="pannello-interno collapsed" data-toggle="collapse" data-parent="#accordion-opzioni2" href="#opzioni2" aria-expanded="false">
                                                                        Opzioni e supplementi
                                                                    </a>
                                                                  </h4>
                                                                </div>
                                                                <div id="opzioni2" class="panel-collapse collapse">
                                                                  <div class="panel-body panel-body-opzioni">
                                                                    <div class="table-responsive">
                                                                      <table class="table table-condensed tabella-centro-supplementi">
                                                                        <thead>
                                                                            <tr>
                                                                                <th width="75%">Opzione</th>
                                                                                <th width="25%">Supplemento</th>
                                                                            </tr>
                                                                        </thead>
                                                                        <tbody>
                                                                            <tr>
                                                                                <td>Tassa d'esame Cambridge FCE (da pagare sul posto) (Prezzo su richiesta)</td>
                                                                                <td>70 €</td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>Tassa d'esame Cambridge CAE (da pagare sul posto) (Prezzo su richiesta)</td>
                                                                                <td>90 €</td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>Tassa d'esame Cambridge GPE (da pagare sul posto) (Prezzo su richiesta)</td>
                                                                                <td>120 €</td>
                                                                            </tr>
                                                                        </tbody>
                                                                      </table>
                                                                    </div>
                                                                </div>
                                                                </div>
                                                              </div>
                                                            </div>

                                                            <div class="panel-body">

                                                               <div class="col-md-3 no-padding centro-lezioni">
                                                                    <span class="titolo-carat-corso">Inizio del corso</span>
                                                                    <p>Ogni settimana, tutto l'anno</p>
                                                               </div>

                                                               <div class="col-md-3 no-padding centro-lezioni">
                                                                    <span class="titolo-carat-corso">Giorni festivi</span>
                                                                    <p>02.01 / 14.04 / 17.04 / 01.05<br>28.08 / 25.12 / 26.12.2017</p>
                                                               </div>

                                                               <div class="col-md-3 no-padding centro-lezioni">
                                                                    <span class="titolo-carat-corso">Vacanze</span>
                                                                    <p>25.12.2016 - 01.01.2017</p>
                                                               </div>

                                                               <div class="col-md-3 no-padding centro-lezioni">
                                                                    <a href="#" class="bottone-preventivo-centro">Preventivo gratuito</a>
                                                                    <a href="#" class="bottone-prenota-centro">Prenota</a>
                                                               </div>

                                                            </div>

                                                        </div>

                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>

                                <div role="tabpanel" class="tab-pane" id="sistemazione">

                                    <div class="row">
                                        <div class="col-md-12 no-padding">
                                            <div class="collapse-centro sistemazione">
                                                <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">

                                                    <div class="panel panel-default">
                                                        <div class="panel-heading" role="tab" id="corso-1">
                                                            <h4 class="panel-title">
                                                                <a id="sistemazione-1" class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#sistemazione1" aria-expanded="false" aria-controls="sistemazione1">
                                                                    <span class="sistemazione-centro">
                                                                        Famiglia ospitante
                                                                    </span>

                                                                </a>
                                                            </h4>
                                                        </div>
                                                        <div id="sistemazione1" class="panel-collapse collapse corso-centro-contenuto" role="tabpanel" aria-labelledby="sistemazione1">

                                                            <div class="panel-body">

                                                                <div class="col-md-12 no-padding">
                                                                    <p class="no-margin">L'alloggio in famiglia ospitante è senza dubbio un'ottima opportunità per immergersi completamente nella lingua e nella cultura durante il vostro soggiorno linguistico in Inghilterra. Avrete così la possibilità di scoprire gli usi e costumi degli abitanti del posto e di mettere in pratica le vostre conoscenze d’inglese in tutti i momenti della vita quotidiana, insieme ai membri della famiglia ospitante. A circa 45 - 60 minuti dalla scuola, alloggerete in camera singola (è disponibile anche la soluzione in camera doppia) con trattamento di mezza pensione.
    NOTE: per gli studenti di età inferiore ai 18 anni è obbligatorio prenotare l’alloggio in famiglia ospitante, il trasferimento dall’aeroporto (in caso di arrivo tra le 20.00 e le 08.00) e fornire un modulo di autorizzazione compilato dai genitori.</p>
                                                                </div>

                                                            </div>

                                                            <div class="panel-group panel-body panel-body-interno" id="sistemazione-info1">
                                                              <div class="panel panel-default">

                                                                <div class="panel-heading panel-interno">
                                                                  <h4 class="panel-title">
                                                                    <a class="pannello-interno collapsed" data-toggle="collapse" data-parent="#sistemazione-info1" href="#info1" aria-expanded="false">
                                                                        Scheda dettagliata
                                                                    </a>
                                                                  </h4>
                                                                </div>

                                                                <div id="info1" class="panel-collapse collapse">
                                                                    <div class="panel-body panel-body-opzioni">

                                                                        <div class="col-md-6 no-padding-left">

                                                                            <div class="table-responsive">
                                                                              <table class="table table-condensed tabella-centro-supplementi">
                                                                                <thead>
                                                                                    <tr>
                                                                                        <th width="75%">TEMPO DI PERCORRENZA A PIEDI VERSO…</th>
                                                                                        <th width="25%"></th>
                                                                                    </tr>
                                                                                </thead>
                                                                                <tbody>
                                                                                    <tr>
                                                                                        <td>Supermercati</td>
                                                                                        <td>1 minuto/i</td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td>Bar</td>
                                                                                        <td>1 minuto/i</td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td>Ristoranti</td>
                                                                                        <td>5 minuto/i</td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td>Farmacie</td>
                                                                                        <td>10 minuto/i</td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td>Negozi</td>
                                                                                        <td>6 minuto/i</td>
                                                                                    </tr>
                                                                                </tbody>
                                                                              </table>
                                                                            </div>

                                                                        </div>

                                                                        <div class="col-md-6 no-padding">
                                                                            <div class="embed-responsive embed-responsive-16by9 embed-mappa-sistemazione">
                                                                                <iframe class="embed-responsive-item" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3018.603497525926!2d14.222153515760269!3d40.836672079318426!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x133b08e27a08453b%3A0xf7d62ba6b6eff533!2sCorso+Vittorio+Emanuele%2C+114%2C+80127+Napoli!5e0!3m2!1sit!2sit!4v1489677872782" width="100%" frameborder="0" style="border:0" allowfullscreen></iframe>
                                                                            </div>
                                                                        </div>

                                                                    </div>
                                                                </div>

                                                              </div>
                                                            </div>

                                                            <div class="panel-body panel-fotogallery-sistemazione">

                                                                <span class="titolo-fotogallery-sistemazione">Fotogallery</span>

                                                                <div class="container-slider-gallery-centro">

                                                                     <!-- Add Arrows -->
                                                                     <div class="swiper-button-next-5"></div>
                                                                     <div class="swiper-button-prev-5"></div>

                                                                     <div class="swiper-container-5">
                                                                        <div class="swiper-wrapper wrap-5">

                                                                            <div class="swiper-slide box-immagine-sistemazione" >
                                                                                <img src="images/prova-sistemazione.jpg" alt="" />
                                                                            </div>
                                                                            <div class="swiper-slide box-immagine-sistemazione" >
                                                                                <img src="images/prova-sistemazione2.jpg" alt="" />
                                                                            </div>
                                                                            <div class="swiper-slide box-immagine-sistemazione" >
                                                                                <img src="images/prova-sistemazione.jpg" alt="" />
                                                                            </div>
                                                                            <div class="swiper-slide box-immagine-sistemazione" >
                                                                                <img src="images/prova-sistemazione2.jpg" alt="" />
                                                                            </div>

                                                                        </div>
                                                                    </div>

                                                                </div>

                                                            </div>

                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>

                                <div role="tabpanel" class="tab-pane" id="messages">

                                    <div class="col-md-12 contenitore-centro-scuola no-padding">

                                        <div class="panel-body">
                                            <h3>Gestione del tempo libero</h3>
                                            <p>Londra offre innumerevoli possibilità per il tempo libero. La città è ricca di edifici storici, negozi, ristoranti e bar per uscire insieme agli amici. Durante il vostro viaggio linguistico in Inghilterra, potrete partecipare al programma per il tempo libero proposto dalla scuola di lingua EC, che vi permetterà di conoscere meglio gli altri studenti e di comunicare in inglese in un contesto autentico. Il vostro soggiorno si annuncia davvero memorabile!</p>
                                        </div>

                                   </div>

                                </div>

                                <div role="tabpanel" class="tab-pane" id="scuola">

                                   <div class="col-md-12 contenitore-centro-scuola no-padding">

                                        <div class="panel-body">
                                            <h3>La scuola di lingua</h3>
                                            <p>Costa del sud, sole, mare, relax…questa cittadina ha tutte le caratteristiche di una stazione balneare nella quale è piacevole riappropriarsi del tempo. Giovane, colta, moderna, attraente… Brighton ha tutti gli attributi di una grande!
        Il piccolo villaggio di pescatori si è trasformato negli anni in una stazione balneare di prima classe… un miracolo? No, solo una prova in più di come l’Inghilterra moderna abbia saputo reinventarsi con brio. Molto vicina alla City, Brighton è uno dei luoghi di villeggiatura preferiti dagli inglesi, in primo luogo dai londinesi: la sua reputazione è indiscutibile. La vostra vacanza studio diverrà un sapiente miscuglio di corsi d’inglese di qualità ed un’atmosfera di vacanza alla quale sarà difficile resistere. Dai famosi Lanes alla Churchill Square, passando per il Royal Theatre e le discoteche più alla moda, scoprirete ogni giorno di più ciò che rende unica questa cittadina. Un viaggio studio sotto il sole in terre inglesi ha molto per far sognare. "Londra sul mare" vi aspetta… Go for it!</p>
                                        </div>

                                   </div>

                                   <div class="col-md-12">

                                        <span class="titolo-fotogallery-scuola">Fotogallery</span>

                                        <div class="container-slider-gallery-scuola">

                                             <!-- Add Arrows -->
                                             <div class="swiper-button-next-6"></div>
                                             <div class="swiper-button-prev-6"></div>


                                             <div class="swiper-container-6">
                                                <div class="swiper-wrapper wrap-6">

                                                    <div class="swiper-slide box-immagine-sistemazione" >
                                                        <a href="images/prova-sistemazione.jpg" data-fancybox="sistemazione" >
                                                        <img src="images/prova-sistemazione.jpg"  alt="" />
                                                        </a>
                                                    </div>
                                                    <div class="swiper-slide box-immagine-sistemazione"  >
                                                        <a href="images/prova-sistemazione2.jpg" data-fancybox="sistemazione" >
                                                            <img src="images/prova-sistemazione2.jpg"  alt="" />
                                                        </a>
                                                    </div>
                                                    <div class="swiper-slide box-immagine-sistemazione"  >
                                                        <a href="images/prova-sistemazione.jpg" data-fancybox="sistemazione" >
                                                            <img src="images/prova-sistemazione.jpg"  alt="" />
                                                        </a>
                                                    </div>
                                                    <div class="swiper-slide box-immagine-sistemazione"  >
                                                        <a href="images/prova-sistemazione2.jpg" data-fancybox="sistemazione" >
                                                            <img src="images/prova-sistemazione2.jpg"  alt="" />
                                                        </a>
                                                    </div>

                                                </div>
                                            </div>

                                        </div>

                                   </div>

                                </div>

                            </div>

                        </div>

                    </div>
                </div>

            </div>
        </div>
    </section>

    <div style="height:400px;"></div>

	<?php include 'includes/test-inglese-footer.php'; ?>

    <?php include 'includes/cataloghi-footer.php'; ?>

    <?php include 'includes/40anni-footer.php'; ?>

    <?php include 'includes/blog-footer.php'; ?>

    <?php include 'includes/footer.php'; ?>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.0.47/jquery.fancybox.min.js"></script>


</body>
</html>