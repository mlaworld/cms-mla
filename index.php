<?php include 'includes/header.php'; ?>

<title>Home - MLA - Move Language Ahead </title>

</head>

<?php

$cataloghi = cataloghi::selezione_home_page();
$offrete = Promo::view_front_end();
$eventi = Eventi::view_front_end();
//$feeds = simplexml_load_file('cache/blog.xml.cache');

?>
<body>

    <header>
        <?php include 'includes/promo-top.php'; ?>
        <?php include 'includes/menu.php'; ?>
        
    </header>
    
    
    <section>
    	<div class="container">
    		<div class="row row-home-ricerca">
            
            	<h1 class="titolo-home">Vacanze Studio e <br>corsi di lingua all'estero</h1>
                <h2 class="sottotitolo-home">Cerca il soggiorno in base <br>alla tua età</h2>
        	
        		<form action="lista-centri" method="GET" id="form-mobile" class="form-inline form-homepage">
                
                  <div class="col-md-12 no-padding box-exit-ricerca">
                  	 <a class="" id="close-mobile" href=""><span class="glyphicon glyphicon-remove exit-form-ricerca" aria-hidden="true"></span></a>
                  </div>
                    
                  <div class="form-group col-xs-3 col-md-3 campo-ricerca-home no-padding">
                    <label class="ricerca-home-label" for="">Età</label>
                    <select  data-validetta="required,callback[eta]"  data-vd-message-required="Perpiacere scegliere un età!" class="form-control ricerca-home-input no-border-right bfh-number" name="eta" id="eta" placeholder="Seleziona la tua età">
                        <option value="" selected>Scegli l'età</option>
                        <option value="8" >8</option>
                        <option value="9" >9</option>
                        <option value="10" >10</option>
                        <option value="11" >11</option>
                        <option value="12" >12</option>
                        <option value="13" >13</option>
                        <option value="14" >14</option>
                        <option value="15" >15</option>
                        <option value="16" >16</option>
                        <option value="17" >17</option>
                        <option value="18" >18+</option>
                    </select>
                  </div>
                  
                  <div class="form-group col-xs-3 col-md-3 campo-ricerca-home no-padding">
                    <label class="ricerca-home-label" for="">Lingua</label>
                      <select class="form-control ricerca-home-input no-border-right" name="lingua" id="lingua" placeholder="Seleziona una lingua">

                      </select>
<!--                    <input type="text" class="" placeholder="Seleziona una lingua">-->
                  </div>
                  
                  <div class="form-group col-xs-6 col-md-6 campo-ricerca-home no-padding">
                    <label class="ricerca-home-label" for="">Destinazione</label>
<!--                    <input type="text" class="form-control ricerca-home-input" id="destinazione" placeholder="Seleziona una destinazione">-->
                      <select class="form-control ricerca-home-input no-border-right" name="destinazione" id="destinazione" placeholder="Seleziona una destinazione">

                      </select>
                    <button type="submit" class="btn btn-default ricerca-home-button">Cerca</button>
                  </div>

                  <div class="clear"></div>
                  
                  <div class="col-md-6 box-codice-viaggio">
                  	<div class="col-xs-6 col-md-6 codice-viaggio-sx">
                    	<span data-container="body" data-toggle="popover" data-placement="bottom" data-content="Vivamus sagittis lacus vel augue laoreet rutrum faucibus." class="glyphicon glyphicon-question-sign glyphicon-align-left punto punto-inter" aria-hidden="true"></span>
                        <a class="action-codice-viaggio" href="#">Hai un codice viaggio?<i class="fa fa-arrow-right float-right freccia-codice-viaggio" aria-hidden="true"></i></a>
                    </div>
                    <div class="form-group col-xs-6 col-md-6 no-padding input-codice-viaggio" style="display:none">
                        <input type="" class="form-control codice-viaggio-input" id="" placeholder="Inserisci codice viaggio">
                        <button type="submit" class="btn btn-default codice-viaggio-button">Entra</button>
                      </div>
                  </div>
                  
                </form>
                
                <a id="search-mobile" class="box-ricerca-mobile" href="#">CERCA</a>
        
        	</div>
    	</div>
    </section>
    
    <div class="clear"></div>
    
    <section>
    	<div class="container container-slider-cataloghi">
        
        		 <!-- Add Arrows -->
                 <div class="swiper-button-next"></div>
                 <div class="swiper-button-prev"></div>
            
				 <div class="swiper-container">
                    <div class="swiper-wrapper">

                        <?php foreach($cataloghi as $catalogo) : ?>
                        <div class="swiper-slide box-catalogo" onclick="location.href='<?php echo $catalogo->link; ?>'">
                        	<img src="<?php echo $catalogo->percorso_immagine_e_plecholder_front(); ?>" alt="" />
                            <div class="box-rosso-cat">
                                <p><?php echo $catalogo->nome_catalogo; ?></p>
                            </div>
                            <div class="contenuto-box-catalogo">
                            	<h3><?php echo $catalogo->nome_catalogo; ?></h3>
                                <p><?php echo $catalogo->breve_desc; ?></p>
                            </div>
                        </div>
                        <?php endforeach; ?>

                        
                    </div>
                </div>
    	</div>
    </section>
    
    <div class="clear"></div>
    
    <section>
    	<div class="container container-slider-evidenza">
        
        		<span class="titolo-section">In evidenza</span>
        
        		 <!-- Add Arrows -->
                 <div class="swiper-button-next-2"></div>
                 <div class="swiper-button-prev-2"></div>
            
				 <div class="swiper-container-2">
                    <div class="swiper-wrapper">
                    	
                        <?php foreach ($offrete as $offerta): ?>
                        <div class="swiper-slide box-evidenza" onclick="location.href='<?php echo $offerta->link; ?>'">
                        	<img src="<?php echo $offerta->percorso_immagine_e_plecholder_front(); ?>" alt="<?php echo $offerta->img_alt; ?>" />
                            <h4 class="titolo-evidenza"><?php echo $offerta->titolo; ?></h4>
							<p><?php echo $offerta->breve_desc; ?></p>
                            <a href="<?php echo $offerta->link; ?>" class="link-evidenza">scopri di più »</a>
                        </div>
                        <?php endforeach; ?>
                    </div>
                </div>
    	 </div>
     </section>
    
	<div class="clear"></div>
     
     <section>
     	<div class="container container-elementi-home">
        
        	<hr>
            <div class="col-md-3 colonna-home-sx">
            	<h4 class="titolo-home-sx">MLA World</h4>
            </div>
            
            <div class="col-md-9 colonna-home-dx">
            	<p id="collapseExample" class="">È sopravvissuto non solo a più di è un testo segnaposto utilizzato nel settore della tipografia e della stampa. Lorem Ipsum è considerato il testo segnaposto standard sin dal sedicesimo secolo, quando un anonimo tipografo prese una cassetta di caratteri e li assemblò per preparare un testo campione. È sopravvissuto non solo a più di cinque secoli, ma anche al passaggio alla videoimpaginazione, pervenendoci sostanzialmente sto segnaposto standard sin dal sedicesimo secolo, quando un anonimo tipografo prese una cassetta di caratteri e li assemblò per preparare un testo campione. È sopravvissuto non solo a più di cinque secoli, ma anche al passaggio alla videoimpaginazione, pervenendoci sostanzialmente inalterato. Fu reso popolare, negli anni ’60, con la diffusione dei fogli di caratteri trasferibili “Letraset.</p>	
            </div>
        
        </div>
     </section>
     
     <?php include 'includes/incontri-footer.php'; ?>
     
     <?php include 'includes/test-inglese-footer.php'; ?>
     
     <?php include 'includes/cataloghi-footer.php'; ?>
     
     <?php include 'includes/40anni-footer.php'; ?>
     
     <?php include 'includes/blog-footer.php'; ?>
     
     <?php include 'includes/footer.php'; ?>


</body>
</html>