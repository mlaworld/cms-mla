/* BOOTSTRAP TOGGLE */
$(function () {
  $('[data-toggle="popover"]').popover()
});

$(document).ready(function () {
	if ($('li').hasClass('sottomenu'))
	{
		$(".sottomenu").each(function() {
			$(this).find('a').first().attr("class", "dropdown-toggle");
			$(this).find('a').first().attr("data-toggle", "dropdown");
			$(this).find('a').first().attr("role", "button");
			$(this).find('a').first().attr("aria-haspopup", "true");
			$(this).find('a').first().attr("aria-expanded", "false");
		});
	}
	document.addEventListener('touchstart', this.touchstart);
	document.addEventListener('touchmove', this.touchmove);
	
	function touchstart(e) {
		e.preventDefault()
	}
	
	function touchmove(e) {
		e.preventDefault()
	}
});


/* BOOTSTRAP TOGGLE CODICE VIAGGIO */

$(document).ready(function(){
	$(".action-codice-viaggio").click(function(){
		$(".input-codice-viaggio").toggle();
	});
});


/* TAGLIO TESTO HOMEPAGE DA MOBILE */

$(document).ready(function(){
	if ($(window).width() < 768){
		$('#collapseExample').addClass("more");
	} else {
		$('#collapseExample').removeClass("more");
	}
});


// SHOW MORE / LESS HOMEPAGE

$(document).ready(function() {
    // Configure/customize these variables.
    var showChar = 200;  // How many characters are shown by default
    var ellipsestext = "...";
    var moretext = "Leggi di più <i class='fa fa-arrow-down' aria-hidden='true'>";
    var lesstext = "Nascondi <i class='fa fa-arrow-up' aria-hidden='true'>";
    

    $('.more').each(function() {
        var content = $(this).html();
 
        if(content.length > showChar) {
 
            var c = content.substr(0, showChar);
            var h = content.substr(showChar, content.length - showChar);
 
            var html = c + '<span class="moreellipses">' + ellipsestext+ '&nbsp;</span><span class="morecontent"><span>' + h + '</span>&nbsp;&nbsp;<a href="" class="morelink" data-toggle="collapse" href="#collapseExample" aria-expanded="false" aria-controls="collapseExample">' + moretext + '</a></span>';
 
            $(this).html(html);
        }
 
    });
 
    $(".morelink").click(function(){
        if($(this).hasClass("less")) {
            $(this).removeClass("less");
            $(this).html(moretext);
        } else {
            $(this).addClass("less");
            $(this).html(lesstext);
        }
        $(this).parent().prev().toggle();
        $(this).prev().toggle();
        return false;
    });
});

// SHOW MORE / LESS BLOG FEED

$(document).ready(function() {
    // Configure/customize these variables.
    var showCharBlog = 150;  // How many characters are shown by default
    var ellipsestext = "...";
    var moretext = "Leggi di più »";
    var lesstext = "Nascondi";
    

    $('.more-blog').each(function() {
        var content = $(this).html();
 
        if(content.length > showCharBlog) {
 
            var c = content.substr(0, showCharBlog);
            var h = content.substr(showCharBlog, content.length - showCharBlog);
 
            var html = c + '<span class="moreellipses">' + ellipsestext+ '&nbsp;</span><span class="morecontent"><span>' + h + '</span>&nbsp;&nbsp;<a class="link-leggi-blog" href="#">' + moretext + '</a></span>';
 
            $(this).html(html);
        }
 
    });
 
});


$(document).ready(function() {
	
	/* DISPLAY SEARCH MOBILE */
	
	$('#search-mobile').click(function(){
		$('#form-mobile').show();
	});
	
	$('#close-mobile').click(function(){
		$('#search-mobile').hide();
	});
	
	
	// CAROUSEL HOMEPAGE
	
	var swiper = new Swiper('.swiper-container', {
		nextButton: '.swiper-button-next',
        prevButton: '.swiper-button-prev',
        slidesPerView: 'auto',
        spaceBetween: 13,
		slidesOffsetAfter: 0,
        freeMode: true
    });
	
	var swiper2 = new Swiper('.swiper-container-2', {
		nextButton: '.swiper-button-next-2',
        prevButton: '.swiper-button-prev-2',
        slidesPerView: 'auto',
        spaceBetween: 13,
		slidesOffsetAfter: 0,
        freeMode: true
    });
	
	var swiper3 = new Swiper('.swiper-container-3', {
        slidesPerView: 'auto',
        spaceBetween: 13,
		slidesOffsetAfter: 0,
        freeMode: true
    });
	
	var swiper4 = new Swiper('.swiper-container-4', {
        slidesPerView: 'auto',
        spaceBetween: 13,
		slidesOffsetAfter: 0,
        freeMode: true
    });
	
	
	$('#sistemazione-1').one('click', function (e) {
		e.preventDefault();
		$('.wrap-5').addClass('csspinner');
        $.ajax({
            type:'GET',
            success: function(){
                var swiper5 = new Swiper('.swiper-container-5', {
                    nextButton: '.swiper-button-next-5',
                    prevButton: '.swiper-button-prev-5',
                    slidesPerView: 'auto',
                    spaceBetween: 13,
                    slidesOffsetAfter: 0,
                    slidesOffsetBefore: 0,
                    freeMode: true,
                    resizeReInit: true
                });
                $( ".container-slider-gallery-centro").append(swiper5);
				$('.wrap-5').removeClass('csspinner');
            }
        });
    });

	
    $('#scuola1').one('click', function (e) {

		e.preventDefault();
		$('.wrap-6').addClass('csspinner');

        $.ajax({
            type:'GET',
            success: function(){

					var swiper6 = new Swiper('.swiper-container-6', {
						nextButton: '.swiper-button-next-6',
						prevButton: '.swiper-button-prev-6',
						slidesPerView: 'auto',
						spaceBetween: 13,
						slidesOffsetAfter: 0,
						freeMode: true,
						resizeReInit: true
					});
					$('.swiper-container-6').append(swiper6);
					$('.wrap-6').removeClass('csspinner');
			}
        });
     });
	
	
	
});

/* GALLERY OVERLAY CENTRO */

$( document ).ready(function() {
	$( '.swipebox' ).swipebox();
});

/* BLOCCO SOTTOMENU IN ALTO ALLO SCROLL */

$(document).ready(function(){
	if(location.href === 'http://test.mlaworld.com/cms-mla/centro') {
		var navHeight = $('.contenitore-generale-interno').offset().top;
		$(window).scroll(function () {
			if ($(window).scrollTop() > navHeight + 3) {
				$('.contenitore-generale-interno').addClass('sottomenu-fixed');
			} else {
				$('.contenitore-generale-interno').removeClass('sottomenu-fixed');
			}
		});
	};
});

/* BLOCCO TAB CENTRO IN ALTO ALLO SCROLL */

/*$(document).ready(function(){
	var navHeight = $('.tab-centro').offset().top;
	$(window).scroll(function(){
		if ($(window).scrollTop() > navHeight-108){
			$('.tab-centro').addClass('tab-fixed');
			$(document).ready(function(){
				$('.tab-fixed a').on('shown.bs.tab', function(e) {
					$('html,body').animate({scrollTop:650}, '500');
				});
			});
		} else {
			$('.tab-centro').removeClass('tab-fixed');
		}
	});
});*/

$(document).ready(function(){
	if(location.href === 'http://test.mlaworld.com/cms-mla/centro') {
		var navHeight = $('.tab-centro').offset().top;
		$(window).scroll(function () {
			if ($(window).scrollTop() > navHeight - 108) {
				$('.tab-centro').addClass('tab-fixed');
			} else {
				$('.tab-centro').removeClass('tab-fixed');
			}
		});
	}
	
	$('.corso').on('show.bs.tab', function(){
	$('html,body').animate({scrollTop:650}, 500);
	});
	
	$('.sistemazione').on('show.bs.tab', function(){
	$('html,body').animate({scrollTop:650}, 500);
	});
	
	$('.messages').on('show.bs.tab', function(){
	$('html,body').animate({scrollTop:650}, 500);
	});
	
	$('.scuola').on('show.bs.tab', function(){
	$('html,body').animate({scrollTop:650}, 500);
	});
	
});

// CODICE PROMO HOMEPAGE TOP

        $("#change").click(function () {
            if ($(this).attr("data-name") == 'show') {
                $(".banner-promo").animate({
                    height: '700px'
                }).show();
                $('.banner-open').attr("data-name", "hide");
                $('#toggleIco').addClass('glyphicon-remove');
                $('#toggleIco').removeClass('glyphicon-menu-down');
            } else {
                $(".banner-promo").animate({
                    height: '20px'
                }).show();
                $('.banner-open').attr("data-name", "show");
                $('#toggleIco').removeClass('glyphicon-remove');
                $('#toggleIco').addClass('glyphicon-menu-down');
            }
        });



// AJAX HOME FORM REQUEST
$(document).ready(function(){

	$('#lingua').html('<option value="0">Selezionare prima un età</option>').attr('disabled','disabled');
	$('#destinazione').html('<option value="0">Selezionare prima un lingua</option>').attr('disabled','disabled');
	$('#eta').on('change', function(){
		var etaVal = $(this).val();
		if(etaVal){
			$.ajax({
				type:'GET',
				url:'includes/form-ajax-lingua.php',
				contentType: 'text/html; charset=UTF-8',
				data:{eta:etaVal},
				// dataType: "hmtl",
				success:function(html){

					$('#lingua').html(html).removeAttr('disabled');
					$('#destinazione').html('<option value="">Seleziona una lingua prima.</option>');
					if($('#eta').val() == 0){$('#lingua').html('<option value="0">Selezionare prima un età</option>').attr('disabled','disabled')};

				}
			});
		}else{
			$('#lingua').html('<option value="">Selezionare prima un età</option>').attr('disabled','disabled');
			$('#destinazione').html('<option value="">Selezionare una lingua.</option>').attr('disabled','disabled');

		}
	});

	$('#lingua').on('change',function(){
		var lingVal = $(this).val();
		var etaVal = $('#eta').val();
		if(lingVal){
			$.ajax({
				type:'GET',
				url:'includes/form-ajax-nazione.php',
				contentType: 'text/html; charset=UTF-8',
				data:'lingua=' + lingVal +'&'+ 'eta=' + etaVal,
				// dataType: "html",
				success:function(html){
					if($('#lingua').val() == 0){
						$('#destinazione').html('<option value="">Selezionare prima un lingua</option>').attr('disabled','disabled');
					} else {
						$('#destinazione').html(html).removeAttr('disabled');
					}

				}
			});
		}else{
			$('#destinazione').html('<option value="">Selezionare prima un lingua</option>').attr('disabled','disabled');
		}
	});

});
// FINE AJAX HOME FORM REQUEST

// FORM
$(document).ready(function(){

		$('#form-mobile').validetta({
			showErrorMessages : true,
			display : 'bubble',
			errorTemplateClass : 'validetta-bubble',
			errorClass : 'validetta-error',
			validClass : 'validetta-valid',
			bubblePosition: 'bottom',
			bubbleGapLeft: 15,
			bubbleGapTop: 0,
			validators: {
				callback: {
					eta: {
						callback:  function( el, value ) {
							if ( value >= 8 ){
								return true
							} else {
								return false
							}

						},
						errorMessage: "Valore non valido!"
					}
				}
			},
			realTime : true
		});
});

// FINE FORM HOME PAGE


// EVENTI HOME PAGE
$("#clickEventi").click(function() {
	$('#event-no-list').show();
	$('#clickEventiNasc').show();
	$(this).hide();

});

$('#clickEventiNasc').on('click', function () {
	$('#clickEventiNasc').hide();
	$('#event-no-list').hide();
	$("#clickEventi").show();
});

if($('#empity-event').length ==1){
	console.log('true');
}

$(document).ready(function(){


	$("#clickEventi").one('click', function() {

		$.ajax({
			url : 'includes/eventi-ajax.php',
			type: 'GET',
			dataType: 'html',
			success: function(data){

				$('#clickEventi').addClass('allEvent');


				$('#event-no-list').html(data);

				$('#clickEventi').hide();

				if ($("#empity-event")[0]){

					$(".visualizza-calendario-eventi").hide();

				}

			}
		});
	});
});

// FINE EVENTI HOME PAGE

// FORM PAGINA CONTATTI
jQuery(function ($) {
    $('#contatti').validetta({
        showErrorMessages : true,
        display : 'bubble',
        errorTemplateClass : 'validetta-bubble',
        errorClass : 'validetta-error',
        validClass : 'validetta-valid',
        bubblePosition: 'bottom',
        bubbleGapLeft: 15,
        bubbleGapTop: 0,
        realTime : true,
        onValid : function( event ) {
            $('#contatti').submit();
            $('#invioContatti').attr('disabled', true);
            event.preventDefault();
            $('#alert')
                .empty()
                .append('<div class="alert alert-success">Messaggio inviato Correttamente</div>');
        },
        onError : function( event ){
            $('#alert')
                .empty()
                .append('<div class="alert alert-danger">Compilare cortesemente tutti i campi!</div>');
        }
    });
});

(function($){
    $.fn.validettaLanguage = {};
    $.validettaLanguage = {
        init : function(){
            $.validettaLanguage.messages = {
                required		: 'Campo obbligatorio.',
                email			: 'Formato email non valido.',
                number		: 'Questo campo accetta solo numeri',
                maxLength		: 'Questo campo accetta al massimo {count} caratteri.',
                minLength		: 'Questo campo richiede minimo {count} caratteri.',
                notEqual		: 'I campi non coincidono.',
                creditCard	: 'Carata di credito non valida.'
            };
        }
    };
    $.validettaLanguage.init();
})(jQuery);


// FINE FORM PAGINA CONTATTI