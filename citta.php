<?php
/**
 * Created da Giuseppe Alessandro De Blasio.
 * User: Giuseppe Alessandro De Blasio
 * Date: 09/05/17
 * Time: 10:01
 */
include 'includes/header.php';

//$categorieCat = CategoriePagine::seleziona_tutti();

//$arrCat = array ();

//foreach ($categorieCat as $categoria){
//
//    $arrCat[] = $categoria->urlFriendly;
//}

//if(in_array(trim($_GET['urlFriendly']), $arrCat)){
//    $categorieCat = CategoriePage::seleziona_per_campo_custom("urlFriCat", "{$_GET['urlFriendly']}");
////    var_dump($categorieCat);
//}
//else {
//    redirect('../404');
//}

//$listaNazioni = Nazioni::seleziona_tutti_per_stato();

//$arrNaz[] =  array ();
//
//foreach ($listaNazioni as $nazione){
//
//    $arrNaz[] = strtolower($nazione->nazione);
//}
//var_dump($arrNaz);

//if(in_array(trim($_GET['nazione']), $arrNaz)){
//    global $databasePDO;
//    $databasePDO->query("SELECT * FROM view_nazioni_page WHERE nazioni =  :nazione  AND categoria = :categoria ");
//    $databasePDO->bind('nazione', $_GET['nazione']);
//    $databasePDO->bind('categoria', $_GET['urlFriendly']);
//    $obj = $databasePDO->resulsetObj();
//    foreach ($obj as $objAss){
//
//        $nazioniObj = $objAss;
//
//    }
//}
//else {
//    redirect('../404');
//}

//$cittaArr = CittaPage::seleziona_tutti();

//$arrCitta = array ();

//foreach ($cittaArr as $citta){

//    $arrCitta[] = $citta->urlFriendly;
//}

//if(in_array(trim($_GET['citta']), $arrCitta)){

//echo $_GET['urlFriendly'] . "<br>";
//echo $_GET['nazione'] . "<br>";
//echo $_GET['citta'] . "<br>";

    global $databasePDO;
    $databasePDO->query("SELECT * FROM view_citta_page WHERE catUrlFri = :categoria AND urlFriendly = :nazione  AND cittaUrlFri = :citta ");
    $databasePDO->bind(':categoria', $_GET['urlFriendly']);
    $databasePDO->bind(':nazione', $_GET['nazione']);
    $databasePDO->bind(':citta', $_GET['citta']);
    $obj = $databasePDO->resulsetObj();

    if($databasePDO->rowCount() > 0):

        foreach ($obj as $objAss){

            $cittaObj = $objAss;

//            var_dump($cittaObj);
            if($cittaObj->nessun_testo):
                redirect("$cittaObj->urlRedirect");
            endif;

        }

        else:
            redirect('../404');

    endif;

//    var_dump($cittaObj);
//}
//else {
//    redirect('../404');
//}

?>

<title><?php echo ($cittaObj->metaTitle)? $cittaObj->metaTitle : $cittaObj->nome_citta; ?></title>
<meta name="description" content="<?php echo $cittaObj->metaDescription; ?>">
<?php echo (empty($cittaObj->canonicalUrl))? null : '<link rel="canonical" href="'.$cittaObj->canonicalUrl.'"/>' ?>
<?php echo ($cittaObj->metaRobots === 'index')? null :  '<meta name="robots" content="noindex,nofollow"' ?>

</head>

<body>

<header>

    <?php include 'includes/menu.php'; ?>

</header>

<section>
    <div class="contenitore-generale-interno">
        <div class="container">
            <div class="contenitore-menu-interno">

                <a class="brand-menu-interno" href="index.php">Move Language Ahead</a>
                <?php echo easymenu(2, 'class="nav navbar-nav menu-interno"'); ?>

            </div>
        </div>
    </div>
</section>

<section>

    <div class="contenitore-generale-interno">
        <div class="container">

            <div class="col-md-12 no-padding contenitore-breadcrumb">
                <ol class="breadcrumb lista-breadcrumb">
                    <li><a href="#">Home</a></li>
                    <li><a href="<?php echo "/".$cittaObj->catUrlFri ?>"><?php echo ucwords($cittaObj->titolo_categoria); ?></a></li>
                    <li><a href="<?php echo $cittaObj->catUrlFri."/".$cittaObj->urlFriendly ?>"><?php echo ucwords($cittaObj->urlFriendly); ?></a></li>
                    <li class="active"><?php echo ucwords(strtolower($cittaObj->nome_citta)) ?></li>
                </ol>
            </div>

            <div class="col-md-12 box-titolo-pagine">
                <?php  echo $cittaObj->desc_titolo; ?>
            </div>

        </div>
    </div>

</section>

<section>

    <div class="contenitore-generale-interno">
        <div class="container">
            <div class="collapse-catalogo">

                <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">

                    <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="heading1">
                            <h2 class="panel-title">
                                <a role="button" data-toggle="collapse" data-parent="#accordion1" href="#sogg1" aria-expanded="true" aria-controls="sogg1">
                                    <?php echo $cittaObj->titBox1; ?>
                                </a>
                            </h2>
                        </div>
                        <div id="sogg1" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="heading1">
                            <div class="panel-body no-padding">
                                <?php echo $cittaObj->box1; ?>
                            </div>
                        </div>
                    </div>

                    <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="heading2">
                            <h2 class="panel-title">
                                <a role="button" data-toggle="collapse" data-parent="#accordion2" href="#sogg2" aria-expanded="false" aria-controls="sogg2" class="collapsed">
                                    <?php echo $cittaObj->titBox2; ?>
                                </a>
                            </h2>
                        </div>
                        <div id="sogg2" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading2" style="height: 0px;">
                            <div class="panel-body no-padding">
                                <?php echo $cittaObj->box2; ?>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>

</section>

<?php $nazioneId = $cittaObj->id_nazione; $categorieId =  $cittaObj->id_categoria; $cittaId = $cittaObj->id; ?>

<section>

    <div class="contenitore-generale-interno">
        <div class="container contenitore-destinazioni">

            <span class="titolo-section">Le nostre destinazioni per i soggiorni studio all’estero</span>

            <div class="row row-destinazioni">


                <?php
//                global $database;
//                $sql = $database->query("SELECT * FROM centro WHERE id_categoria = '{$categorieId}' AND id_nazione = '{$nazioneId}' AND id_citta = {$cittaId} ");
//                while ($obj  = mysqli_fetch_object($sql)):
//                    ?>
<!--                    <div class="col-sm-6 col-md-4-5 col-lg-1-5 singola-destinazione">-->
<!--                        <a href="--><?php //echo $obj->cittaUrl; ?><!--">-->
<!--                            <img class="img-responsive" src="--><?php //echo ($obj->image =="")? "images/image-not-found.png":"images/citta/".$obj->image; ?><!--" alt="--><?php //echo $obj->img_alt; ?><!--" />-->
<!--                        </a>-->
<!--                        <a class="link-singola-destinazione" href="--><?php //echo $obj->cittaUrl; ?><!--">--><?php //echo $obj->desc_titolo; //$nomeNazione = Nazioni::seleziona_per_id($obj->id_nazione); echo $nomeNazione->nazione; ?><!--</a>-->
<!--                    </div>-->
<!--                --><?php //endwhile; ?>
                <?php
                global $databasePDO;
                $databasePDO->query("SELECT * FROM centro WHERE id_categoria = :categoria AND id_nazione = :nazione AND id_citta = :citta ");
                $databasePDO->bind(':categoria', $categorieId);
                $databasePDO->bind(':nazione', $nazioneId);
                $databasePDO->bind(':citta', $cittaId);
                $obj = $databasePDO->resultClass('Centro');


                ?>
                <?php foreach($obj as $centro): ?>
                <div class="col-sm-6 col-md-4-5 col-lg-1-5 singola-destinazione">
                    <a href="<?php echo $centro->CentroUrl; ?>">
                        <img class="img-responsive" src="<?php echo $centro->percorso_immagine_e_plecholder();//echo ($obj->Foto =="")? "images/image-not-found.png":"images/centro/".$obj->Foto; ?>" alt="<?php echo $centro->img_alt; ?>" title="<?php echo $centro->img_title ?>" />
                    </a>
                    <a class="link-singola-destinazione" href="<?php echo $centro->CentroUrl; ?>"><?php echo $centro->Centro; //$nomeNazione = Nazioni::seleziona_per_id($obj->id_nazione); echo $nomeNazione->nazione; ?></a>
                </div>
                <?php endforeach; ?>

            </div>

        </div>
    </div>

</section>

<?php include 'includes/test-inglese-footer.php'; ?>
<?php include 'includes/cataloghi-footer.php'; ?>
<?php include 'includes/40anni-footer.php'; ?>
<?php include 'includes/blog-footer.php'; ?>
<?php include 'includes/footer.php'; ?>

</body>
</html>
