<?php
/**
 * Created da Giuseppe Alessandro De Blasio.
 * User: Giuseppe Alessandro De Blasio
 * Date: 09/05/17
 * Time: 10:01
 */
include 'includes/header.php';


    global $databasePDO;
    $databasePDO->query("SELECT * FROM view_nazioni_page WHERE nazioni = :nazione  AND categoria = :categoria ");
    $databasePDO->bind('nazione', $_GET['nazione']);
    $databasePDO->bind('categoria', $_GET['urlFriendly']);
    $obj = $databasePDO->resulsetObj();
//    echo $databasePDO->rowCount();
    if($databasePDO->rowCount() == 1):
    foreach ($obj as $objAss){

        $nazioniObj = $objAss;

    }
    else:
        redirect('../404');
    endif;

//    var_dump($nazioniObj);
?>
<?php //$feeds = simplexml_load_file('http://www.mlaworld.com/blog/feed'); ?>

<title><?php echo $nazioniObj->metaTitle; ?></title>
<meta name="description" content="<?php echo $nazioniObj->metaDescription; ?>">
<?php echo (empty($nazioniObj->canonicalUrl))? null : '<link rel="canonical" href="'.$nazioniObj->canonicalUrl.'"/>' ?>
<?php echo ($nazioniObj->metaIndex === 'index')? null :  '<meta name="robots" content="noindex,nofollow"' ?>

</head>

<body>

<header>

    <?php include 'includes/menu.php'; ?>

</header>

<section>
    <div class="contenitore-generale-interno">
        <div class="container">
            <div class="contenitore-menu-interno">

                <a class="brand-menu-interno" href="index.php">Move Language Ahead</a>
                <?php echo easymenu(2, 'class="nav navbar-nav menu-interno"'); ?>

            </div>
        </div>
    </div>
</section>

<section>

    <div class="contenitore-generale-interno">
        <div class="container">

            <div class="col-md-12 no-padding contenitore-breadcrumb">
                <ol class="breadcrumb lista-breadcrumb">
                    <li><a href="#">Home</a></li>
                    <li><a href="<?php echo "/".$nazioniObj->categoria ?>"><?php echo $nazioniObj->titolo_categoria; ?></a></li>
                    <li class="active"><?php echo ucwords($nazioniObj->nazioni); ?></li>
                </ol>
            </div>

            <div class="col-md-12 box-titolo-pagine">
                <?php  echo $nazioniObj->desc_titolo; ?>
            </div>

        </div>
    </div>

</section>

<section>

    <div class="contenitore-generale-interno">
        <div class="container">
            <div class="collapse-catalogo">

                <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">

                    <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="heading1">
                            <h2 class="panel-title">
                                <a role="button" data-toggle="collapse" data-parent="#accordion1" href="#sogg1" aria-expanded="true" aria-controls="sogg1">
                                    <?php echo $nazioniObj->titBox1; ?>
                                </a>
                            </h2>
                        </div>
                        <div id="sogg1" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="heading1">
                            <div class="panel-body no-padding">
                                <?php echo $nazioniObj->box1; ?>
                            </div>
                        </div>
                    </div>

                    <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="heading2">
                            <h2 class="panel-title">
                                <a role="button" data-toggle="collapse" data-parent="#accordion2" href="#sogg2" aria-expanded="false" aria-controls="sogg2" class="collapsed">
                                    <?php echo $nazioniObj->titBox2; ?>
                                </a>
                            </h2>
                        </div>
                        <div id="sogg2" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading2" style="height: 0px;">
                            <div class="panel-body no-padding">
                                <?php echo $nazioniObj->box2; ?>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>

</section>

<?php $nazioneId = $nazioniObj->id; $categorieId =  $nazioniObj->id_categoria;?>

<section>

    <div class="contenitore-generale-interno">
        <div class="container contenitore-destinazioni">

            <span class="titolo-section">Le nostre destinazioni per i soggiorni studio all’estero</span>

            <div class="row row-destinazioni">


<!--                --><?php
//                global $database;
//                $sql = $database->query("SELECT * FROM citta_page WHERE id_categoria = '{$database->escape($categorieId)}' AND id_nazione = '{$database->escape($nazioneId)}' ");
//                while ($obj  = mysqli_fetch_object($sql)):
//                    ?>
<!--                    <div class="col-sm-6 col-md-4-5 col-lg-1-5 singola-destinazione">-->
<!--                        <a href="--><?php //echo $obj->cittaUrl; ?><!--">-->
<!--                            <img class="img-responsive" src="--><?php //echo ($obj->image =="")? "images/image-not-found.png":"images/citta/".$obj->image; ?><!--" alt="--><?php //echo $obj->img_alt; ?><!--" />-->
<!--                        </a>-->
<!--                        <a class="link-singola-destinazione" href="--><?php //echo $obj->cittaUrl; ?><!--">--><?php //echo $obj->desc_titolo; //$nomeNazione = Nazioni::seleziona_per_id($obj->id_nazione); echo $nomeNazione->nazione; ?><!--</a>-->
<!--                    </div>-->
<!--                --><?php //endwhile; ?>

                <?php
                global $databasePDO;

                $databasePDO->query("SELECT * FROM citta_page WHERE id_categoria = :categoria AND id_nazione = :nazione ");
                $databasePDO->bind(':categoria', $categorieId);
                $databasePDO->bind(':nazione', $nazioneId);
                $cittaObj = $databasePDO->resultClass('CittaPage');
//                var_dump($cittaObj);
                ?>
                <?php foreach ($cittaObj as $citta): ?>

                    <div class="col-sm-6 col-md-4-5 col-lg-1-5 singola-destinazione">
                        <a href="<?php echo $citta->cittaUrl; ?>">
                            <img class="img-responsive" src="<?php echo ($citta->image =="")? "images/image-not-found.png":"images/citta/".$citta->image; ?>" alt="<?php echo $citta->img_alt; ?>" />
                        </a>
                        <a class="link-singola-destinazione" href="<?php echo $citta->cittaUrl; ?>"><?php echo $citta->nome_citta; ?></a>
                    </div>

                <?php endforeach; ?>

            </div>

        </div>
    </div>

</section>

<?php include 'includes/test-inglese-footer.php'; ?>
<?php include 'includes/cataloghi-footer.php'; ?>
<?php include 'includes/40anni-footer.php'; ?>
<?php include 'includes/blog-footer.php'; ?>
<?php include 'includes/footer.php'; ?>

</body>
</html>
