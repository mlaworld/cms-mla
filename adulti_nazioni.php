	<?php include 'includes/header.php'; ?>
    <?php $feeds = simplexml_load_file('http://www.mlaworld.com/blog/feed'); ?>
</head>

<body>

    <header>
                
        <?php include 'includes/menu.php'; ?>
        
    </header>
    
    <section>
        <div class="contenitore-generale-interno">
            <div class="container">	
                <div class="contenitore-menu-interno">
                    
                    <a class="brand-menu-interno" href="index.php">Move Language Ahead</a>
                    <?php echo easymenu(2, 'class="nav navbar-nav menu-interno"'); ?>
                
                </div>
            </div>
        </div>
    </section>
    
	<section>
    	
        <div class="contenitore-generale-interno">
        	<div class="container">
            	
                <div class="col-md-12 no-padding contenitore-breadcrumb">
                    <ol class="breadcrumb lista-breadcrumb">
                      <li><a href="#">Home</a></li>
                      <li class="active">Adulti</li>
                    </ol>
                </div>
                
                <div class="col-md-12 box-titolo-pagine">
                	<h1 class="titolo-pagina-catalogo">Soggiorni Studio all’estero<br>
                    <span>per adulti dai 17 anni in su</span></h1>
                </div>
                
            </div>
        </div>
        
    </section>
    
    <section>
    
    	<div class="contenitore-generale-interno">
        	<div class="container">
            	<div class="collapse-catalogo">
            
                	<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                
                      <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="heading1">
                          <h2 class="panel-title">
                            <a role="button" data-toggle="collapse" data-parent="#accordion1" href="#sogg1" aria-expanded="true" aria-controls="sogg1">
                              Un soggiorno linguistico all’estero è un investimento per il tuo futuro!
                            </a>
                          </h2>
                        </div>
                        <div id="sogg1" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="heading1">
                          <div class="panel-body no-padding">
                            <p>I soggiorni studio di MLA sono rivolti a persone che cercano un corso di lingua all’estero e desiderano un prodotto altamente personalizzabile in base alle proprie esigenze. Quando parliamo di personalizzazione ci riferiamo ad un concetto a 360° nel quale è possibile scegliere destinazione, tipo di corso e certificazione che si desidera conseguire, tipo di alloggio e durata del soggiorno. In base alla lingua che desideri imparare, o approfondire, puoi scegliere la destinazione più consona alle tue esigenze tra le innumerevoli proposte del catalogo MLA. Cina, Australia, Stati Uniti, Canada, Gran Bretagna… Questi sono solo alcuni dei posti nei quali hai la possibilità di effettuare un soggiorno studio con i nostri programmi. I soggiorni linguistici di MLA nascono da un lunga esperienza sul campo durante la quale un team altamente qualificato da sempre seleziona le migliori scuole di lingua nel mondo nelle quali è possibile seguire corsi generali, intensivi, superintensivi e addirittura business partendo da un livello principiante fino all’avanzato.</p>
                          </div>
                        </div>
                      </div>
                      
                      <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="heading2">
                          <h2 class="panel-title">
                            <a role="button" data-toggle="collapse" data-parent="#accordion2" href="#sogg2" aria-expanded="false" aria-controls="sogg2" class="collapsed">
                              Le opzioni per i soggiorni linguistici MLA
                            </a>
                          </h2>
                        </div>
                        <div id="sogg2" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading2" style="height: 0px;">
                          <div class="panel-body no-padding">
                            <p>Per il tuo viaggio studio all’estero non c’è limite alle opzioni disponibili:
                            	<ul>	
                                	<li>Durata del soggiorno: la durata del soggiorno è a scelta dello studente. Solitamente la durata minima parte da 1 settimana, ma per alcuni tipi di corso o alcune destinazioni può essere richiesto un soggiorno minimo dai 2 settimane o più.</li>
                                    <li>Acquistare solo il corso: Se hai già un punto di appoggio o comunque un posto dove alloggiare durante il tuo soggiorno, MLA ti da la possibilità di acquistare solo il corso di lingue.</li>
                                    <li>Pacchetto completo: i pacchetti completi di MLA comprendono, alloggio a scelta tra diverse tipologie, il corso di lingue della durata che si preferisce e ovviamente tutte le opzioni riguardanti assicurazioni, sicurezza etc.</li>
                                    <li>Esami e certificazioni: i centri selezionati da MLA offrono la possibilità di conseguire le più importanti certificazioni riconosciute a livello internazionale (IELTS, TOEFL, KET, PET, ZD, DELE, DALF, etc.).</li>
                                    <li>Stage in azienda: Per chi parte già da un livello di inglese intermedio, alcune località consentono di effettuare uno stage in azienda, in un settore specifico che si sposa perfettamente con il percorso formativo dello studente.</li>
                                </ul>
                            </p>
                            
                          </div>
                        </div>
                      </div>
                      
                      <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="heading3">
                          <h2 class="panel-title">
                            <a role="button" data-toggle="collapse" data-parent="#accordion3" href="#sogg3" aria-expanded="false" aria-controls="sogg3" class="collapsed">
                              Perché scegliere un viaggio studio all’estero
                            </a>
                          </h2>
                        </div>
                        <div id="sogg3" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading3" style="height: 0px;">
                          <div class="panel-body no-padding">
                            <p>Un soggiorno studio all’estero è l’occasione per aggiungere quel “qualcosa in più” al TUO CURRICULUM! Conoscere una lingua straniera è un elemento imprescindibile per entrare oggi nel mondo del lavoro e soprattutto per farlo senza “limiti geografici”. Per impararla velocemente è necessario mettere subito in pratica ciò che si studia, applicandolo alla vita di tutti i giorni, e questo è quello che necessariamente accade quando si studia all’estero. Questo tipo di esperienza non forma solo a livello culturale ma anche personale: ti consente di uscire dalla tua “comfort zone” e di immergerti in un mondo totalmente diverso dal tuo dove tutto ciò che conoscevi prima diventa relativo. Un soggiorno linguistico all’estero è un investimento per il tuo futuro!</p>
                          </div>
                        </div>
                      </div>
                      
                    </div>
                
                </div>
            </div>
        </div>
    
    </section>
    
    <section>
    
    	<div class="contenitore-generale-interno">
        	<div class="container contenitore-destinazioni">
    
    			<span class="titolo-section">Le nostre destinazioni per i soggiorni studio all’estero</span>
                
                <div class="row row-destinazioni">
                
                    <div class="col-sm-6 col-md-4-5 col-lg-1-5 singola-destinazione">
                       <a href="#">
                       		<img class="img-responsive" src="images/adulti-nazioni/london.jpg" alt="" />
                       </a>
                       <a class="link-singola-destinazione" href="#">Inghilterra</a>
                    </div>
                    
                    <div class="col-sm-6 col-md-4-5 col-lg-1-5 singola-destinazione">
                       <a href="#">
                       		<img class="img-responsive" src="images/adulti-nazioni/newyork.jpg" alt="" />
                       </a>
                       <a class="link-singola-destinazione" href="#">USA</a>
                    </div>
                    
                    <div class="col-sm-6 col-md-4-5 col-lg-1-5 singola-destinazione">
                       <a href="#">
                       		<img class="img-responsive" src="images/adulti-nazioni/parigi.jpg" alt="" />
                       </a>
                       <a class="link-singola-destinazione" href="#">Francia</a>
                    </div>
                    
                    <div class="col-sm-6 col-md-4-5 col-lg-1-5 singola-destinazione">
                       <a href="#">
                       		<img class="img-responsive" src="images/adulti-nazioni/delhi.jpg" alt="" />
                       </a>
                       <a class="link-singola-destinazione" href="#">Spagna</a>
                    </div>
                    
                    <div class="col-sm-6 col-md-4-5 col-lg-1-5 singola-destinazione">
                       <a href="#">
                       		<img class="img-responsive" src="images/adulti-nazioni/tokyo.jpg" alt="" />
                       </a>
                       <a class="link-singola-destinazione" href="#">Giappone</a>
                    </div>
                    
                </div>
                
                <div class="row row-destinazioni">
                
                    <div class="col-sm-6 col-md-4-5 col-lg-1-5 singola-destinazione">
                       <a href="#">
                       		<img class="img-responsive" src="images/adulti-nazioni/berlino.jpg" alt="" />
                       </a>
                       <a class="link-singola-destinazione" href="#">Germania</a>
                    </div>
                    
                    <div class="col-sm-6 col-md-4-5 col-lg-1-5 singola-destinazione">
                       <a href="#">
                       		<img class="img-responsive" src="images/adulti-nazioni/pechino.jpg" alt="" />
                       </a>
                       <a class="link-singola-destinazione" href="#">Cina</a>
                    </div>
                    
                    <div class="col-sm-6 col-md-4-5 col-lg-1-5 singola-destinazione">
                       <a href="#">
                       		<img class="img-responsive" src="images/adulti-nazioni/egitto.jpg" alt="" />
                       </a>
                       <a class="link-singola-destinazione" href="#">Egitto</a>
                    </div>
                    
                    <div class="col-sm-6 col-md-4-5 col-lg-1-5 singola-destinazione">
                       <a href="#">
                       		<img class="img-responsive" src="images/adulti-nazioni/london.jpg" alt="" />
                       </a>
                       <a class="link-singola-destinazione" href="#">Inghilterra</a>
                    </div>
                    
                    <div class="col-sm-6 col-md-4-5 col-lg-1-5 singola-destinazione">
                       <a href="#">
                       		<img class="img-responsive" src="images/adulti-nazioni/newyork.jpg" alt="" />
                       </a>
                       <a class="link-singola-destinazione" href="#">Usa</a>
                    </div>
                    
                </div>
                
            </div>
        </div>
        
    </section>
    
	<?php include 'includes/test-inglese-footer.php'; ?>
    <?php include 'includes/cataloghi-footer.php'; ?>
    <?php include 'includes/40anni-footer.php'; ?>
    <?php include 'includes/blog-footer.php'; ?>
    <?php include 'includes/footer.php'; ?>

</body>
</html>