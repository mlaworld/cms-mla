<?php
/**
 * Created da Giuseppe Alessandro De Blasio.
 * User: Giuseppe Alessandro De Blasio
 * Date: 18/04/17
 * Time: 10:31
 */

include 'includes/init.php';

if(isset($_POST['id_sist_centro'])){
//    $sistemazioni = Prezzi::seleziona_per_campo_custom_prezzi_centro('id_centro', $_POST['mostro_id_centro']);

    global  $database;
    $sql = "SELECT * FROM view_selezione_alloggi ";
    $sql .= " WHERE id_corso_adulti= " .  $_POST['id_sist_centro'];
    $result = $database->query($sql);



while($row = mysqli_fetch_assoc($result)): ?>
        <div class="col-sm-4">
            <div class="form-group">
                <label for="<?php echo $row['nome_alloggio']; ?>"
                       class="col-sm-5 control-label"><?php echo $row['nome_alloggio']; ?></label>
                <label class="checkbox-inline c-checkbox">
                    <div id="checkboxes">
                        <input id="checkSist"  data-id="<?php echo $row['id']?>" onclick="clickValue(this);" type="checkbox" value="0" <?php echo ($row['stato'] == 1)? 'checked' : '' ?>>
                        <span class="fa fa-check"></span>
                    </div>
                </label>
            </div>
        </div>
<?php endwhile;
}
?>
