<?php
/**
 * Created da Giuseppe Alessandro De Blasio.
 * User: Giuseppe Alessandro De Blasio
 * Date: 30/09/2016
 * Time: 16:37
 */
?>

<?php include 'includes/init.php'; ?>

<?php if(!$sessione->loggato()){ redirect('accedi.php');} ?>

<?php

if(empty($_GET['id'])){
    redirect('cataloghi.php');
}

$catalogo = Cataloghi::seleziona_per_id($_GET['id']);

if($catalogo){
    echo '<div data-toggle="notify" data-onload data-message="Catalogo <b>Eliminato</b> Correttamente" data-options="{&quot;status&quot;:&quot;success&quot;, &quot;pos&quot;:&quot;bottom-right&quot;}" class="hidden-xs"></div>';
    $catalogo->cancella();
    redirect("cataloghi.php");
} else {
    redirect("cataloghi.php");
}