<?php
/**
 * Created by Giuseppe Alessandro De Blasio.
 * User: Giuseppe Alessandro De Blasio
 * Date: 28/09/2016
 * Time: 15:13
 */

?>

<?php include 'includes/init.php'; ?>

<?php if(!$sessione->loggato()){ redirect('accedi.php');} ?>

<?php

if(empty($_GET['id'])){
    redirect('categorie_post.php');
}

$cat_post = CategoriePagine::seleziona_per_id($_GET['id']);

if($cat_post){
    $sessione->messaggio('<div data-toggle="notify" data-onload data-message="Categoria <b>Eliminata</b> Correttamente" data-options="{&quot;status&quot;:&quot;success&quot;, &quot;pos&quot;:&quot;bottom-right&quot;}" class="hidden-xs"></div>');
    $cat_post->cancella();
    redirect("categorie_post.php");
} else {
    redirect("categorie_post.php");
}