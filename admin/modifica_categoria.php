<?php
/**
 * Created da Giuseppe Alessandro De Blasio.
 * User: Giuseppe Alessandro De Blasio
 * Date: 10/05/17
 * Time: 10:42
 */

include 'includes/header.php'; ?>
<?php if(!$sessione->loggato()){ redirect('accedi.php');} ?>

<?php

$utente_ses = Utente::seleziona_per_id($_SESSION['id_utente']);

$paginaCategoria = CategoriePage::seleziona_per_id($_GET['id']);

if(isset($_POST['aggiorna_pagina_cat'])){
    if($paginaCategoria){
        $paginaCategoria->titolo = $_POST['titolo_categoria'];
        $paginaCategoria->titBox1 = $_POST['titolo-box-1'];
        $paginaCategoria->box1 = $_POST['box-1'];
        $paginaCategoria->titBox2 = $_POST['titolo-box-2'];
        $paginaCategoria->box2 = $_POST['box-2'];
        $paginaCategoria->titBox3 = $_POST['titolo-box-3'];
        $paginaCategoria->box3 = $_POST['box-3'];

        $dataOption = $_POST['categorie'];
        $dataExplode = explode('|', $dataOption);

        $paginaCategoria->id_cat = $dataExplode[0];
        $paginaCategoria->urlFriCat = $dataExplode[1];

//        $paginaCategoria->autore = $_POST['autore'];
        $paginaCategoria->metaTitle = $_POST['meta_title'];
        $paginaCategoria->metaDescription = $_POST['meta_description'];
        $paginaCategoria->canonicalUrl = $_POST['canonical'];
        $paginaCategoria->metaRobots = $_POST['selezione_index'];
        $paginaCategoria->stato = $_POST['stato'];
//        $paginaCategoria->data_creazione = date('Y-m-d');
        $paginaCategoria->salva();

        echo '<div data-toggle="notify" data-onload data-message="Categoria <b>Modificata</b> Correttamente" data-options="{&quot;status&quot;:&quot;success&quot;, &quot;pos&quot;:&quot;bottom-right&quot;}" class="hidden-xs"></div>';
    } else{

        echo '<div data-toggle="notify" data-onload data-message="<b>Errore modifica Categoria</b>" data-options="{&quot;status&quot;:&quot;danger&quot;, &quot;pos&quot;:&quot;bottom-right&quot;}" class="hidden-xs"></div>';
    }


}

?>


    <!-- Barra di Navigazione -->
<?php include ("includes/navbar.php") ?>

<?php include ("includes/aside.php") ?>



<section class="main-content">
    <h1 class="page-header">Aggiungi Pagina Categoria</h1>
    <div class="panel-body">
        <form method="POST" action="" class="form-horizontal" enctype="multipart/form-data" data-parsley-validate>
            <div class="row">
                <div class="col-md-12">
                    <div class="col-md-8">
                        <div class="form-group">
                            <label for="titolo_categoria" class="control-label">Titolo Categoria</label>
                            <textarea id="titolo_categoria" name="titolo_categoria" class="form-control"><?php echo $paginaCategoria->titolo; ?></textarea>
                        </div>
                        <div class="form-group">
                            <label for="titolo-box-1" class="control-label">Titolo Box 1</label>
                            <input type="text" id="titolo-box-1" class="form-control" name="titolo-box-1" value="<?php echo $paginaCategoria->titBox1; ?>"/>
                        </div>
                        <div class="form-group">
                            <label for="box-1" class="control-label">Box 1</label>
                            <textarea id="box-1" name="box-1"><?php echo $paginaCategoria->box1; ?></textarea>
                        </div>
                        <div class="form-group">
                            <label for="titolo-box-2" class="control-label">Titolo Box 2</label>
                            <input type="text" id="titolo-box-2" class="form-control" name="titolo-box-2" value="<?php echo $paginaCategoria->titBox2; ?>"/>
                        </div>
                        <div class="form-group">
                            <label for="box-2" class="control-label">Box 2</label>
                            <textarea id="box-2" name="box-2"><?php echo $paginaCategoria->box2; ?></textarea>
                        </div>
                        <div class="form-group">
                            <label for="titolo-box-3" class="control-label">Titolo Box 3</label>
                            <input type="text" id="titolo-box-3" class="form-control" name="titolo-box-3" value="<?php echo $paginaCategoria->titBox3; ?>"/>
                        </div>
                        <div class="form-group">
                            <label for="box-3" class="control-label">Box 3</label>
                            <textarea id="box-3" name="box-3"><?php echo $paginaCategoria->box3; ?></textarea>
                        </div>


                    </div>
                    <div class="pull-right col-md-4">
                        <div class="panel panel-default">
                            <div class="panel-heading"><i style="color: black;" class=""></i> Pubblica</div>
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-md-10">
                                        <div class="col-md-6">
                                            <p><i class="fa fa-hand-o-right"></i> Stato: <b>Non Pubblicato</p>
                                        </div>
                                        <div class="col-md-2 pull-right">
                                            <button name="aggiorna_pagina_cat" class="btn btn-oval btn-info">Aggiorna Pagina Categoria</button>
                                        </div>
                                    </div>
                                </div>

                            </div> <!-- panel-body -->
                        </div> <!-- panel panel-default -->


<!--                        <div class="panel panel-default">-->
<!--                            <div class="panel-heading"><i style="color: #009ab8" class="fa fa-users"></i> Autore</div>-->
<!--                            <div class="panel-body">-->
<!--                                <!-- <label>Utente</label> -->
<!--                                <select name="autore" class="form-control">-->
<!--                                    --><?php //if($utente_ses->ruolo == 'admin'): ?>
<!--                                        --><?php //$utenti = Utente::seleziona_tutti(); ?>
<!--                                        --><?php //foreach($utenti as $utente): ?>
<!--                                            <option --><?php //echo ($utente->username == $paginaCategoria->autore )? 'selected ="selected" ' : "";  ?><!-- >--><?php //echo $utente->username; ?><!--</option>-->
<!--                                        --><?php //endforeach; ?>
<!--                                    --><?php //endif; ?>
<!--                                    --><?php //if($utente_ses->ruolo != 'admin'): ?>
<!--                                        <option>--><?php //echo $utente_ses->username; ?><!--</option>-->
<!--                                    --><?php //endif; ?>
<!--                                </select>-->
<!--                            </div>-->
<!--                        </div>-->
                        <div class="panel panel-default">
                            <div class="panel-heading"><i style="color: red;" class="fa fa-exclamation-triangle"></i> Stato</div>
                            <div class="panel-body">
                                <!-- <label>Stato</label> -->
                                <select name="stato" class="form-control" required>
                                    <option value="">Seleziona Stato</option>
                                    <option <?php echo ($paginaCategoria->stato == 1 )? 'selected ="selected" ' : "";  ?> value="pubblicato">Pubblicato</option>
                                    <option <?php echo ($paginaCategoria->stato == 0 )? 'selected ="selected" ' : "";  ?> value="bozza">Bozza</option>
                                </select>
                            </div> <!-- panel-body -->
                        </div> <!-- panel panel-default -->

                        <div class="panel panel-default">
                            <div class="panel-heading"><i class="fa fa-folder-o"></i> Categorie</div>
                            <div class="panel-body">
                                <select name="categorie" class="form-control" required>
                                    <?php $categorie = CategoriePagine::seleziona_tutti(); ?>
                                    <option value="">Seleziona Categoria</option>
                                    <?php foreach($categorie as $chiave => $categoria): ?>
                                        <option value="<?php echo $categoria->id."|$categoria->urlFriendly" ?>" <?php echo ($categoria->urlFriendly == $paginaCategoria->urlFriCat )? 'selected ="selected" ' : "";  ?>><?php echo $categoria->titolo_categoria; ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                        </div>

                        <div class="panel panel-default">
                            <div class="panel-heading"><i style="color: #e80079" class="fa fa-header"></i> <b>Meta Title</b></div>
                            <div class="panel-body">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <div class="col-sm-12">
                                            <input maxlength="80" onkeyup="contaCaratteriTitle(this)" name="meta_title" type="text" class="form-control" value="<?php echo $paginaCategoria->metaTitle;  ?>">
                                            <div id="meta_title_caratteri"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading"><i style="color: #CBE86B" class="fa fa fa-pencil"></i> <b>Meta Description</b></div>
                            <div class="panel-body">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <div class="col-sm-12">
                                            <input maxlength="160" onkeyup="contaCaratteriDescrizione(this)" name="meta_description" type="text" class="form-control" value="<?php echo $paginaCategoria->metaDescription;  ?>">
                                            <div id="meta_descrizione_caratteri"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading"><i style="color: #0A246A" class="fa fa-globe"></i> <b>Canonical Url</b></div>
                            <div class="panel-body">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <div class="col-sm-12">
                                            <input name="canonical" type="url" value="<?php echo $paginaCategoria->canonicalUrl ?>" class="form-control">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading"><i style="color: #8e79e8 class="fa fa-indent"></i> <b>Meta Robots Index</b></div>
                            <div class="panel-body">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <div class="col-sm-12">
                                            <select name="selezione_index" id="selezione_index" class="form-control">
                                                <option value="index">Index</option>
                                                <option value="noindex">NoIndex</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div> <!-- pull-right col-md-4 -->
                </div> <!-- col-md-12 -->
            </div>  <!-- row -->
        </form>
    </div> <!-- panel-body -->




</section>


<script type="text/javascript" xmlns="http://www.w3.org/1999/html">
    tinymce.init({
        selector: 'textarea',
        theme: 'modern',
//        width: 1100,
        height: 200,
        language: 'it',
        extended_valid_elements : 'span[class]',
        plugins: [
            "advlist autolink lists link image charmap print preview anchor",
            "searchreplace visualblocks code fullscreen",
            "insertdatetime media table contextmenu paste imagetools wordcount"
        ],
        toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | preview" +
        "code | ",
        images_upload_url: 'upload.php',
        images_upload_base_path: '/admin/immagini',
        media_live_embeds: true,
        style_formats_merge: true,
        menubar: true,
        image_caption: true,
        /*file_browser_callback: function(field_name, url, type, win) {
         if(type=='image') $('#my_form input').click();
         },*/
        image_prepend_url: "http://localhost/mla-cms/admin/immagini/",
        language_url: 'js/tinymce/js/tinymce/langs/it.js' ,
        //preview_styles: true,
        menu: {
            file: {title: 'File', items: 'newdocument'},
            edit: {title: 'Modifica', items: 'undo redo | cut copy paste pastetext | selectall'},
            insert: {title: 'Inserisci', items: 'link media | template hr'},
            view: {title: 'Vedi', items: 'visualaid preview visualblocks'},
            format: {
                title: 'Formato',
                items: 'bold italic underline strikethrough superscript subscript | formats | removeformat'
            },
            table: {title: 'Tabella', items: 'inserttable tableprops deletetable | cell row column'},
            tools: {title: 'Tools', items: 'spellchecker code'},
            style_formats: [
                {
                    title: 'Classe Personalizzata',
                    items:
                        [
                            {
                                title : 'Classe Personalizzata', selector : 'h1', classes : 'titolo-pagina-catalogo'
                            }
                        ]
                }
            ]
        },
        content_css: [
            'css/style.css'],
        style_formats: [
            {
                title: 'Classe Personalizzata',
                items:
                    [
                        {
                            title : 'Classe Personalizzata', selector : 'h1', classes : 'titolo-pagina-catalogo'
                        }
                    ]
            }
        ]
    });
</script>




<?php include 'includes/footer.php'; ?>
