<?php include 'includes/header.php'; ?>
<?php include 'includes/modale_foto_utenti.php'; ?>

<?php if(!$sessione->loggato()){ redirect('accedi.php');} ?>

        <!-- Barra di Navigazione -->
        <?php include ("includes/navbar.php") ?>
        
        <?php include ("includes/aside.php") ?>
        <!-- End aside-->
      
 <?php include ("includes/aside_utenti.php") ?> 
        
   <?php 
   
   $articoli = new Articoli();
   
    if(isset($_POST['crea_articolo'])){
        if($articoli){
            $articoli->titolo = $_POST['titolo_articolo'];
            $articoli->contenuto = $_POST['testo_articolo'];
            $articoli->utente = $_POST['utente'];
            $articoli->stato = $_POST['stato'];
            $articoli->categoria_id = $_POST['categorie'];
            $articoli->data = date("Y-m-d");
            //$articoli->tags = $_POST['tags'];
            $articoli->impostazione_file_articolo($_FILES['immagine_articolo']);
            
            $articoli->caricamento_foto_articolo();
            $sessione->messaggio('<div data-toggle="notify" data-onload data-message="Articolo <b>Aggiunto</b> Correttamente" data-options="{&quot;status&quot;:&quot;success&quot;, &quot;pos&quot;:&quot;bottom-right&quot;}" class="hidden-xs"></div>');
            $articoli->salva();
            redirect('aggiungi_articolo.php');
            
        }    
    }
        
    ?>            
    <br>
    <section class="main-content">
        <div class="panel-body">
              
        <form method="POST" action="" class="form-horizontal" enctype="multipart/form-data" data-parsley-validate>
        <div class="col-lg-6">  
        <div class="form-group">
            <label class="control-label">Titolo Articolo</label>
            <input type="text" name="titolo_articolo" class="form-control form-control-rounded">
        </div>
        <div class="form-group">
            <label class="control-label">Testo Articolo</label>
            <br>
            <br>
            <div class="col-sm-12">
                <textarea name="testo_articolo" style="height: 380px;"></textarea>
            </div>
        </div>
            <div class="form-group">
                <div class="row">
                    <div class="col-md-2">
                        <button name="crea_articolo" class="btn btn-md btn-primary">Crea Articolo</button>
                    </div>
                </div>
            </div>            
            
        </div>
            <div class="col-lg-2"></div>
            <div class="col-lg-4">
                <div class="panel panel-default">
                     <div class="panel-heading">Autore & Stato</div>
                     <div class="panel-body">
                     <div class="col-sm-12">
                        <label>Utente</label>
                        <select name="utente" class="form-control">
                            <?php $utenti = Utente::seleziona_tutti(); ?> 
                            <?php foreach($utenti as $utente): ?>
                            <option><?php echo $utente->username; ?></option>
                            <?php endforeach; ?>
                        </select>
                     </div>
                         <div class="col-sm-12">
                             <br>
                         </div>  
                     <div class="col-sm-12">
                        <label>Stato</label>
                        <select name="stato" class="form-control">
                            <option>Pubblicato</option>
                            <option>Bozza</option>
                        </select>
                     </div>                         
                     </div>
                  </div>
                
                <div class="panel panel-default">
                     <div class="panel-heading">Categorie</div>
                     <div class="panel-body">
                         <div class="col-sm-12">
                         <select name="categorie" class="form-control">
                            <?php $categorie = CategorieBlog::seleziona_tutti(); ?> 
                            <?php foreach($categorie as $chiave => $categoria): ?> 
                            <option value="<?php echo $categoria->id; ?>"><?php echo $categoria->titolo_categoria; ?></option>
                            <?php endforeach; ?>
                         </select>
                         </div>
                     </div>
                  </div>

                <div class="panel panel-default">
                     <div class="panel-heading">Tags</div>
                     <div class="panel-body">
                        <div class="col-md-12">
                           <div class="form-group">
                           <div class="col-sm-12">
                              <input name="tags" type="text" data-role="tagsinput" value="" class="form-control">
                           </div>
                        </div>
                        </div>                         
                    </div>
                </div>
                
                <div class="panel panel-default">
                     <div class="panel-heading">Immagine Articolo</div>
                     <div class="panel-body">
                        <div class="text-center form-group">
                            <img class="img-thumbnail img-responsive" style="width: 240px; height: 240px;" src="<?php echo $articoli->percorso_immagine_e_plecholder(); ?>" alt="<?php echo $articoli->immagine_articolo; ?>">
                            <br>
                        </div>                         
                        <div class="col-sm-12 form-group">
                            <input type="file" name="immagine_articolo" class="form-control">
                        </div>
                     </div>
                  </div>               

        
        </div>
    </form>  
    </div>
        
 </section>       
        
    
    
    
  <script src="http://cdn.tinymce.com/4/tinymce.min.js"></script>
  <script>tinymce.init({ selector:'textarea' });</script>
<?php include ("includes/footer.php") ?>
