<?php
/**
 * Created da Giuseppe Alessandro De Blasio.
 * User: Giuseppe Alessandro De Blasio
 * Date: 13/04/17
 * Time: 09:38
 */
?>
<?php include 'includes/header.php'; ?>
<?php if(!$sessione->loggato()){ redirect('accedi.php');} ?>

<!-- Barra di Navigazione -->
<?php include ("includes/navbar.php") ?>

<?php include ("includes/aside.php") ?>
<!-- End aside-->
<?php



    if(isset($_POST['id_centro']) && $_POST['id_centro'] != 0){
        $aggiorna_centro = Centro::seleziona_per_id($_POST['id_centro']);
        //$aggiorna_centro->id =
    }

    if(isset($_POST['crea_centro']) && $_POST['id_centro'] != 0){

        //$aggiungo_centro = new Centro();
        $aggiungo_centro = Centro::seleziona_per_id($_POST['id_centro']);

        if($aggiungo_centro){
            $aggiungo_centro->id_catalogo = $_POST['catalogo'];
            $aggiungo_centro->Centro = $_POST['nome_centro'];
            $urlRewrite = rewriteUrl(trim(strtolower($aggiungo_centro->Centro)));
            $aggiungo_centro->urlfriendly = $urlRewrite;
            $aggiungo_centro->Lingua = $_POST['lingua'];
            $aggiungo_centro->id_nazione = $_POST['nazione'];
            $aggiungo_centro->EtaMin = $_POST['etaMin'];
            $aggiungo_centro->EtaMax = $_POST['etaMax'];
            $aggiungo_centro->Sistemazione = $_POST['sistemazione'];
            $aggiungo_centro->PrezziDa = $_POST['prezzi_da'];
            $catobj = CategoriePagine::seleziona_per_id($_POST['categorie']); $catUrl = $catobj->urlFriendly;
            $nazobj = Nazioni::seleziona_per_id($_POST['nazione']); $nazUrl = $nazobj->urlFriendly;
            $citobj = CittaPage::seleziona_per_id( $_POST['citta']); $citUrl = $citobj->urlFriendly;
            $aggiungo_centro->CentroUrl =/*$_POST['centro_url'];*/"/".$catUrl."/".$nazUrl."/".$citUrl."/".$urlRewrite;
            $aggiungo_centro->BreveDescrizione = $_POST['breve_descrizione'];
            $aggiungo_centro->DurataMin = $_POST['durata_min'];
            $aggiungo_centro->College = isset($_POST['college']) == 1;
            $aggiungo_centro->Famiglia = isset($_POST['famiglia']) == 1;
            $aggiungo_centro->Hotel = isset($_POST['hotel']) == 1;
            $aggiungo_centro->Residence = isset($_POST['residence']) == 1;
            $aggiungo_centro->GoldPlus = isset($_POST['goldPlus']) == 1;
            $aggiungo_centro->FootballAcademy = isset($_POST['FootballAcademy']) == 1;
            $aggiungo_centro->DanceAcademy = isset($_POST['DanceAcademy']) == 1;
            $aggiungo_centro->BasketAcademy = isset($_POST['BasketAcademy']) == 1;
            $aggiungo_centro->TheatreAcademy = isset($_POST['TheatreAcademy']) == 1;
            $aggiungo_centro->BagnoPrivato = isset($_POST['BagnoPrivato']) == 1;
            $aggiungo_centro->MedicoItaliano = isset($_POST['MedicoItaliano']) == 1;
            $aggiungo_centro->Escursioni = isset($_POST['Escursioni']) == 1;
            $aggiungo_centro->Trinity = isset($_POST['Trinity']) == 1;
            $aggiungo_centro->Studio = isset($_POST['Studio']) == 1;
            $aggiungo_centro->Appartamento = isset($_POST['Appartamento']) == 1;
            $aggiungo_centro->Monolocale = isset($_POST['Monolocale']) == 1;
            $aggiungo_centro->stato = $_POST['stato'];
            $aggiungo_centro->id_citta = $_POST['citta'];
            $aggiungo_centro->id_categoria = $_POST['categorie'];
//            $aggiungo_centro->id_prezzo = $_POST['categorie'];
            $aggiungo_centro->Foto = $_POST[$_FILES['file']];
            $aggiungo_centro->img_alt = $_POST['img_alt'];
            $aggiungo_centro->img_title = $_POST['img_title'];
            $aggiungo_centro->metaTitle = $_POST['meta_title'];
            $aggiungo_centro->metaDescription = $_POST['meta_description'];
            $aggiungo_centro->canonicalUrl = $_POST['canonical'];
            $aggiungo_centro->metaIndex = $_POST['selezione_index'];
            $aggiungo_centro->impostazione_file_centro($_FILES['file']);
            $aggiungo_centro->caricamento_foto_centro();
            $aggiungo_centro->salva();
            redirect('elenco_centri.php');
        }
    }
    
    $sistemazioni = AlloggioAdulti::seleziona_tutti();

//global $database;
//$ultimoId = Centro::ultimo_id();
//$sql = "SELECT * FROM view_sistemazioni_attive ";
//$sql .= "WHERE id_corso_adulti = $ultimoId ";
//
//$result = $database->query($sql);

//while ($row = mysqli_fetch_object($result)):
//
//    $data[] = $row;
//
//endwhile;




//foreach ($sist_sel as $value => $key){
//    echo $key->nome_alloggio;
//}
//while ($row = mysqli_fetch_array($result)) {

//    foreach ($row as $attributo => $valore) {
//
//
//        $oggetto = $$attributo = $valore;
//
//        $oggetto_array[] = $oggetto;
//    }
//}


//var_dump($oggetto_array);

/*$messaggio = "";

if(isset($_FILES['file'])){

    $immagine = new Image();
    $immagine->impostazione_file_foto($_FILES['file']);
    $immagine->data_immagine = date("Y-m-d");
    if($immagine->salva_immagine_post()){

        $messaggio = "Immagine caricata Correttamente";
    } else
    {
        $messaggio = join("<br>", $immagine->errore);
    }

}*/

//$inserisco_centro = new Centro();
//$inserisco_carat = new caratCentro();

/*if(isset($_POST['ins_nuovo_centr'])){
    if($inserisco_centro) {
        $inserisco_centro->Centro = $_POST['nome_nuovo_centro'];
    }
}
$ultimo_id = $inserisco_centro->id = $_SESSION['ultimo_id'];

if(isset($_POST['crea_pagina'])){
    if($inserisco_centro) {
        $inserisco_centro->Centro = $_POST['nome_centro'];
        $inserisco_carat->id_centro = $inserisco_centro->id;
        $inserisco_centro->salva();
    }
}*/

$selziona_catalogo = Cataloghi::seleziona_tutti();
$selziona_nazione = Nazioni::seleziona_tutti();
$utente_ses = Utente::seleziona_per_id($_SESSION['id_utente']);
$sel_car_centr = caratCentro::seleziona_per_campo_custom('id_centro', 0);


?>


<section class="main-content">
    <h1 class="page-header">Aggiungi Centro Adulti <?php echo isset($_SESSION['ultimo_id']) ?></h1>
    <?php if($messaggio) :?>
        <div class="row">
            <div class="col-md-12">
                <div class="alert <?php echo ($messaggio == "Immagine caricata Correttamente") ? 'alert-success' : 'alert-danger' ?> alert-dismissable">
                    <button type="button" data-dismiss="alert" aria-hidden="true" class="close">×</button>
                    <?php  echo "<b class='dz-success'>" . $messaggio . "</b>"; ?>.
                </div>
            </div>
        </div>
    <?php endif; ?>
    <div class="panel-body">
        <form method="POST" action="" class="form-horizontal" enctype="multipart/form-data" data-parsley-validate>
            <input type="hidden" id="lastId" value="<?php echo $ultimoId = Centro::ultimo_id(); ?>">
            <div class="row">
                <div class="col-md-12">
                    <div class="col-md-8">
                        <input type="hidden" name="id_centro" id="id_centro" value="">
                        <div class="form-group">
                            <label for="nome_centro" class="control-label">Nome Centro</label>
                            <input type="text" name="nome_centro" id="nome_ritorno" class="form-control" value="" placeholder="Inserisci nome Centro" required>
                        </div>

                        <div class="form-group">
                            <label for="urlFirendly" class="control-label">Url Friendly</label>
                            <input type="text" name="urlFirendly" id="urlFirendly" class="form-control" value="" disabled>
                        </div>

                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="catalogo" class="control-label">Seleziona Catalogo</label>
                                    <select class="form-control" name="catalogo" id="catalogo" required>
                                        <option value="">Seleziona Catalogo</option>
                                        <?php foreach ($selziona_catalogo as $catalogo): ?>
                                            <?php if ($catalogo->stato == 1): ?>
                                                <option value="<?php echo $catalogo->id ?>"><?php echo $catalogo->nome_catalogo ?></option>
                                            <?php endif; ?>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="lingua" class="control-label">Lingua</label>
                                    <select name="lingua" class="form-control" required>
                                        <option value="">Seleziona Lingua</option>
                                        <option value="italiano">Italiano</option>
                                        <option value="inglese">Inglese</option>
                                        <option value="francese">Francese</option>
                                        <option value="spagnolo">Spagnolo</option>
                                        <option value="tedesco">Tedesco</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="catalogo" class="control-label">Seleziona Categoria</label>
                                    <select name="categorie" id="catAajx" class="form-control" required>
                                        <?php $pagine = CategoriePagine::seleziona_tutti(); ?>
                                        <option value="">Seleziona Categoria</option>
                                        <?php foreach($pagine as $chiave => $pagina): ?>
                                            <option value="<?php echo $pagina->id; ?>"><?php echo $pagina->titolo_categoria; ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <label for="etaMin" class="control-label">Età Minima</label>
                                <select  class="form-control" name="etaMin" id="etaMin" required>
                                    <option value="">Seleziona età Minima</option>
                                    <option value="8">8</option>
                                    <option value="9">9</option>
                                    <option value="10">10</option>
                                    <option value="11">11</option>
                                    <option value="12">12</option>
                                    <option value="13">13</option>
                                    <option value="14">14</option>
                                    <option value="15">15</option>
                                    <option value="16">16</option>
                                    <option value="17">17</option>
                                </select>
                            </div>
                            <div class="col-md-4">
                                <label for="etaMax" class="control-label">Età Massima</label>
                                <select  class="form-control" name="etaMax" id="etaMax" required>
                                    <option value="">Seleziona età Massima</option>
                                    <option value="13">13</option>
                                    <option value="14">14</option>
                                    <option value="15">15</option>
                                    <option value="16">16</option>
                                    <option value="17">17</option>
                                    <option value="18">18</option>
                                    <option value="19">19</option>
                                </select>
                            </div>
                            <div class="col-md-4">
                                <label for="sistemazione" class="control-label">Sistemazione</label>
                                <select name="sistemazione" class="form-control" required>
                                    <option value="">Selelziona Sistemazione</option>
                                    <option value="famiglia">Famiglia</option>
                                    <option value="college">College</option>
                                    <option value="residence o famiglia">Residence o Famiglia</option>
                                    <option value="college o famiglia">College o Famiglia</option>
                                    <option value="residence">Residence</option>
                                    <option value="hotel">Hotel</option>
                                    <option value="hotel o famiglia">Hotel o Famiglia</option>
                                    <option value="famiglia o residence o appartamento">Famiglia o Residence o Appartamento</option>
                                    <option value="famiglia o residence o app o monolocale">Famiglia o Residence o Appartamento o Monolocale</option>
                                    <option value="famiglia o residence o studio">Famiglia o Residence o Studio</option>
                                </select>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-4">
                                <label for="prezzi_da" class="control-label">A partire da</label>
                                <input type="number" name="prezzi_da" min="0" id="prezzi_da" class="form-control" placeholder="A partire da">
                            </div>
                            <div class="col-md-4">
                                <label for="centro_url" class="control-label">Centro URL</label>
                                <input type="text" name="centro_url" class="form-control" placeholder="Inserisci URL" required />
                            </div>
                            <div class="col-md-4">
                                <label for="durata_min" class="control-label">Durata Minima</label>
                                <select name="durata_min" class="form-control" required>
                                    <option value="">Durata Minima</option>
                                    <option value="2 settimane">Due Settimane</option>
                                    <option value="15 giorni">15 Giorni</option>
                                    <option value="14 giorni">14 Giorni</option>
                                    <option value="8 giorni">8 Giorni</option>
                                    <option value="7 giorni">7 Giorni</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="breve_descrizione" class="control-label">Breve Descrizione</label>
                            <input type="text" name="breve_descrizione" class="form-control" placeholder="Inserisci breve descrizione" required>
                        </div>

                        <h3 class="page-header">Caratteristiche Centro</h3>
                        <dt class="text-primary">Sistemazioni</dt><br>
                        <button class="btn btn-primary pull-right" type="button" data-toggle="modal" id="sistemazioneAdd" data-target="#sistemazione">Aggiungi Sistemazione</button>
                        <br><br><br><br><br>
                        <div class="row" id="aggSist">
<!--                            <div class="col-sm-3">-->
<!--                                <div class="form-group">-->
<!--                                    <label for="college" class="col-sm-5 control-label">College</label>-->
<!--                                    <label class="checkbox-inline c-checkbox">-->
<!--                                        <input id="college" name="college" type="checkbox" value="0">-->
<!--                                        <span class="fa fa-check"></span></label>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                            <div class="col-sm-3">-->
<!--                                <div class="form-group">-->
<!--                                    <label for="famiglia" class="col-sm-5 control-label">Famiglia</label>-->
<!--                                    <label class="checkbox-inline c-checkbox">-->
<!--                                        <input id="famiglia" name="famiglia" type="checkbox" value="0">-->
<!--                                        <span class="fa fa-check"></span></label>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                            <div class="col-sm-3">-->
<!--                                <div class="form-group">-->
<!--                                    <label for="hotel" class="col-sm-5 control-label">Hotel</label>-->
<!--                                    <label class="checkbox-inline c-checkbox">-->
<!--                                        <input id="hotel" name="hotel" type="checkbox" value="0">-->
<!--                                        <span class="fa fa-check"></span></label>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                            <div class="col-sm-3">-->
<!--                                <div class="form-group">-->
<!--                                    <label for="residence" class="col-sm-5 control-label">Residence</label>-->
<!--                                    <label class="checkbox-inline c-checkbox">-->
<!--                                        <input id="residence" name="residence" type="checkbox" value="0">-->
<!--                                        <span class="fa fa-check"></span></label>-->
<!--                                </div>-->
<!--                            </div>-->
                            <?php foreach ($sistemazioni as $sistemazione): ?>
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <b><?php echo $sistemazione->nome_alloggio; ?></b>
                                </div>
                            </div>
                            <?php endforeach; ?>
          
                        </div>

                        <hr>
<!--                        <dt class="text-green">I Plus del Centro</dt><br>-->
<!--                        <div class="row">-->
<!--                            <div class="col-sm-3">-->
<!--                                <div class="form-group">-->
<!--                                    <label for="goldPlus" class="col-sm-5 control-label">GoldPlus</label>-->
<!--                                    <label class="checkbox-inline c-checkbox">-->
<!--                                        <input id="goldPlus" name="goldPlus" type="checkbox" value="0">-->
<!--                                        <span class="fa fa-check"></span></label>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                            <div class="col-sm-3">-->
<!--                                <div class="form-group">-->
<!--                                    <label for="FootballAcademy" class="col-sm-5 control-label">Football Academy</label>-->
<!--                                    <label class="checkbox-inline c-checkbox">-->
<!--                                        <input id="FootballAcademy" name="FootballAcademy" type="checkbox" value="0">-->
<!--                                        <span class="fa fa-check"></span></label>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                            <div class="col-sm-3">-->
<!--                                <div class="form-group">-->
<!--                                    <label for="DanceAcademy" class="col-sm-5 control-label">Dance Academy</label>-->
<!--                                    <label class="checkbox-inline c-checkbox">-->
<!--                                        <input id="DanceAcademy" name="DanceAcademy" type="checkbox" value="0">-->
<!--                                        <span class="fa fa-check"></span></label>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                            <div class="col-sm-3">-->
<!--                                <div class="form-group">-->
<!--                                    <label for="BasketAcademy" class="col-sm-5 control-label">Basket Academy</label>-->
<!--                                    <label class="checkbox-inline c-checkbox">-->
<!--                                        <input id="BasketAcademy" name="BasketAcademy" type="checkbox" value="0">-->
<!--                                        <span class="fa fa-check"></span></label>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                        </div>-->
<!---->
<!---->
<!--                        <div class="row">-->
<!--                            <div class="col-sm-3">-->
<!--                                <div class="form-group">-->
<!--                                    <label for="TheatreAcademy" class="col-sm-5 control-label">Theatre Academy</label>-->
<!--                                    <label class="checkbox-inline c-checkbox">-->
<!--                                        <input id="TheatreAcademy" name="TheatreAcademy" type="checkbox" value="0">-->
<!--                                        <span class="fa fa-check"></span></label>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                            <div class="col-sm-3">-->
<!--                                <div class="form-group">-->
<!--                                    <label for="Appartamento" class="col-sm-5 control-label">Appartamento</label>-->
<!--                                    <label class="checkbox-inline c-checkbox">-->
<!--                                        <input id="Appartamento" name="Appartamento" type="checkbox" value="0">-->
<!--                                        <span class="fa fa-check"></span></label>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                            <div class="col-sm-3">-->
<!--                                <div class="form-group">-->
<!--                                    <label for="Trinity" class="col-sm-5 control-label">Trinity</label>-->
<!--                                    <label class="checkbox-inline c-checkbox">-->
<!--                                        <input id="Trinity" name="Trinity" type="checkbox" value="0">-->
<!--                                        <span class="fa fa-check"></span></label>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                            <div class="col-sm-3">-->
<!--                                <div class="form-group">-->
<!--                                    <label for="Monolocale" class="col-sm-5 control-label">Monolocale</label>-->
<!--                                    <label class="checkbox-inline c-checkbox">-->
<!--                                        <input id="Monolocale" name="Monolocale" type="checkbox" value="0">-->
<!--                                        <span class="fa fa-check"></span></label>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                        </div>-->
                        <dt class="text-green">Aggiungi Corso al Centro</dt><br>
                        <div class="col-xs-12 col-md-12 col-lg-12">
                            <button type="button" class="btn btn-primary btn-lg text-center col-md-12" id="aggCorsoId" data-toggle="modal" data-target="#aggCorso">
                                Aggiungi Corso al Centro
                            </button>
                        </div><br><br><br>


                        <hr>

                        <dt class="text-danger">Altro</dt><br>
                        <div class="row">
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <label for="MedicoItaliano" class="col-sm-5 control-label">Medico Italiano</label>
                                    <label class="checkbox-inline c-checkbox">
                                        <input id="MedicoItaliano" name="MedicoItaliano" type="checkbox" value="0">
                                        <span class="fa fa-check"></span></label>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <label for="Studio" class="col-sm-5 control-label">Studio</label>
                                    <label class="checkbox-inline c-checkbox">
                                        <input id="Studio" name="Studio" type="checkbox" value="0">
                                        <span class="fa fa-check"></span></label>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <label for="BagnoPrivato" class="col-sm-5 control-label">Bagno Privato</label>
                                    <label class="checkbox-inline c-checkbox">
                                        <input id="BagnoPrivato" name="BagnoPrivato" type="checkbox" value="">
                                        <span class="fa fa-check"></span></label>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <label for="Escursioni" class="col-sm-5 control-label">Escursioni</label>
                                    <label class="checkbox-inline c-checkbox">
                                        <input id="Escursioni" name="Escursioni" type="checkbox" value="0">
                                        <span class="fa fa-check"></span></label>
                                </div>
                            </div>
                        </div>
                        <hr>
                        <dt class="text-success">Caratteristiche Centro</dt><a href="#" onclick="leggi_centro();"><i class="fa fa-refresh fa-3 pull-right" aria-hidden="true" ></i></a><br>
                        <div class="form-group">
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered table-hover" id="mostra_carat">
                                    <h3 id="carat-null" class='text-center'>Nessuna Caratteristica del centro Inserita</h3>
                                </table>
                            </div>
                        </div>


                        <div class="col-xs-12 col-md-12 col-lg-12">
                            <button type="button" class="btn btn-primary btn-lg text-center col-md-12" data-toggle="modal" data-target="#caratCentro">
                                Aggiungi Caratteristica al Centro
                            </button>
                        </div>

                        <br><br>
                        <h3 class="page-header">Prezzi Centro</h3>
                        <dt class="text-primary">Prezzi</dt><a href="#" onclick="leggi_prezzo();"><i class="fa fa-refresh fa-3 pull-right" aria-hidden="true" ></i></a><br>
                        <div class="form-group">
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered table-hover" id="mostra_prezzi">
                                    <h3 id="carat-null-prezzo" class='text-center'>Nessuna Prezzo inserito nel centro</h3>
                                </table>
                            </div>
                        </div>
                        <div class="col-xs-12 col-md-12 col-lg-12">
                            <button type="button" class="btn btn-primary btn-lg text-center col-md-12" data-toggle="modal" data-target="#prezzoCentro">
                                Aggiungi Prezzo al Centro
                            </button>
                        </div>
                        <br><br>
                        <h3 class="page-header">Mappa Centro</h3>
                        <div class="col-xs-6 col-md-6 col-lg-6">
                            <label for="lingua" class="control-label">Inserisci Nome Centro</label>
                            <input class="form-control" type="text" id="descMapLoc" >
                        </div>
                        <div class="col-xs-3 col-md-3 col-lg-3">
                            <label for="lat" class="control-label">Latitudine</label>
                            <div class="form-control" type="text" id="lat"></div>
                        </div>
                        <div class="col-xs-3 col-md-3 col-lg-3">
                            <label for="lng" class="control-label">Longitudine</label>
                            <div class="form-control" type="text" id="lng"></div>
                        </div>
                        <br><br>
                        <div id="iframeMaps">
                            <br><br>
                            <iframe id="iframeMappa" width="1050" height="300" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src=""></iframe>
                            <!--                           <iframe id="mapIframe" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3007.4271935049164!2d-74.17681208415658!3d41.08151192286315!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x89c2e1a40234ee67%3A0x1af95a2c2c4e35e7!2sRamapo+College+of+New+Jersey!5e0!3m2!1sit!2sit!4v1491310303385" width="1050" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>-->
                        </div>
                        <script>
                            $(document).ready(function(){

                                var id_centro = $('#id_centro').val();

                                $('#descMapLoc').on( "change",function () {

                                    var localita = $(this).val();
                                    console.log(localita);


                                    $.ajax({
                                        method: "GET",
                                        url: "https://maps.googleapis.com/maps/api/geocode/json?address=" + localita +"&key=AIzaSyCACRLPAzMdtZT5DVF5k1ToSq-Y-CiA9Ik",
                                        dataType: "json",
                                        success: function (data) {
                                            console.log(data);
                                            var result = data.results[0];
                                            lat = result.geometry.location.lat;
                                            lng = result.geometry.location.lng;
                                            var locationAddr = data.results[0].formatted_address;
                                            console.log(locationAddr);
//                                            $('#iframeMaps').append('<iframe width="1050" height="300" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://maps.google.com/maps?q='+locationAddr+','+lat+','+lng+'&hl=es;z=14&amp;output=embed&q="></iframe>');
                                            $('#descMapLoc').val(locationAddr);
                                            $('#lat').html(lat);
                                            $('#lng').html(lng);
//                                            $('#iframeMappa').attr("src" , "https://maps.google.com/maps?q="+locationAddr+","+lat+","+lng+"&hl=es;z=14&output=embed");
//                                            $('#iframeMappa').attr("src" , "https://maps.google.com/maps?q="+locationAddr+","+lat+","+lng+"&hl=es;z=14&output=embed");
                                            $('#iframeMappa').attr("src" , "https://maps.google.com/maps?q="+locationAddr+"&hl=es;z=14&output=embed");
//                                            console.log(lat);
//                                            console.log(lng);
                                            var id_centro_maps = $('#id_centro').val();

                                            $.ajax({
                                                method: "PUT",
                                                url: "includes/api.php/centro/"+id_centro_maps,
                                                dataType: "json",
                                                data: {lat:lat, lng:lng, descMaps:locationAddr},
                                                headers: {
                                                    'Content-type': 'application/json',
                                                    'Access-Control-Allow-Headers': 'Content-Type',
                                                    'Access-Control-Allow-Origin': '*',
                                                    'X-XSRF-TOKEN': '<?php echo (isset($_COOKIE['XSRF-TOKEN']))? $_COOKIE['XSRF-TOKEN'] : ''?>'
                                                },
                                                success: function (data) {

                                                }

                                            });

                                        }
                                    });
                                });

                                $('#lat').addClass('csspinner');
                                $('#lng').addClass('csspinner');
                                $.ajax({
                                    method: "GET",
                                    url: "includes/api.php/centro?filter[]=id,eq,"+id_centro+"&columns=descMaps,lat,lng",
                                    dataType: "json",
                                    headers: {
                                        'Content-type': 'application/json',
                                        'Access-Control-Allow-Headers': 'Content-Type',
                                        'Access-Control-Allow-Origin': '*',
                                        'X-XSRF-TOKEN': '<?php echo (isset($_COOKIE['XSRF-TOKEN']))? $_COOKIE['XSRF-TOKEN'] : ''?>'
                                    },
                                    success: function (data) {
                                        if(data !== 'undefined' && data.length > 0){
                                            var getLocalita = data.centro.records[0][0];
                                            var getlat = data.centro.records[0][1];
                                            var getlng = data.centro.records[0][2];
                                            $('#lat').removeClass('csspinner');
                                            $('#lng').removeClass('csspinner');
                                            $('#descMapLoc').val(getLocalita);
                                            $('#lat').html(getlat);
                                            $('#lng').html(getlng);
//                                        $('#iframeMaps').append('<iframe width="1050" height="300" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://maps.google.com/maps?q='+getLocalita+','+getlat+','+getlng+'&hl=es;z=14&amp;output=embed&q="></iframe>');
//                                        $('#iframeMappa').attr("src" , "https://maps.google.com/maps?q="+getLocalita+","+getlat+","+getlng+"&hl=es;z=14&output=embed");
                                            $('#iframeMappa').attr("src" , "https://maps.google.com/maps?q="+getLocalita+"&hl=es;z=14&output=embed");
//                                        $('#iframeMappa').attr("src" , "https://www.google.com/maps/embed/v1/place?key=AIzaSyCACRLPAzMdtZT5DVF5k1ToSq-Y-CiA9Ik&q="+getLocalita+","+getlat+","+getlng +"");
//                                        $('#iframeMaps').append('<iframe width="450" height="250" frameborder="0" style="border:0"b src="https://www.google.com/maps/embed/v1/search?key=AIzaSyCACRLPAzMdtZT5DVF5k1ToSq-Y-CiA9Ik&q='+getLocalita+','+getlat+', '+getlng+'" allowfullscreen> </iframe>');
                                        } else {
                                            var getLocalita = "";
                                            var getlat = 0;
                                            var getlng = 0;

                                            $('#lat').removeClass('csspinner');
                                            $('#lng').removeClass('csspinner');
                                            $('#descMapLoc').val(getLocalita);
                                            $('#lat').html(getlat);
                                            $('#lng').html(getlng);
                                        }

                                    }
                                })

                            });
                        </script>


                        <!--<div class="panel panel-default">
                            <div class="panel-heading"><i style="color: #e80079" class="fa fa-header"></i> <b>Meta Title</b></div>
                            <div class="panel-body">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <div class="col-sm-12">
                                            <input maxlength="80" onkeyup="contaCaratteriTitle(this)" name="meta_title" type="text" class="form-control">
                                            <div id="meta_title_caratteri"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading"><i style="color: #CBE86B" class="fa fa fa-pencil"></i> <b>Meta Description</b></div>
                            <div class="panel-body">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <div class="col-sm-12">
                                            <input maxlength="160" onkeyup="contaCaratteriDescrizione(this)" name="meta_description" type="text" class="form-control">
                                            <div id="meta_descrizione_caratteri"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading"><i style="color: #0A246A" class="fa fa-globe"></i> <b>Canonical Url</b></div>
                            <div class="panel-body">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <div class="col-sm-12">
                                            <input name="canonical" type="url" value="" class="form-control">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading"><i style="color: #8e79e8 class="fa fa-indent"></i> <b>Meta Robots Index</b></div>
                            <div class="panel-body">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <div class="col-sm-12">
                                            <select name="selezione_index" id="selezione_index" class="form-control">
                                                <option value="index">Index</option>
                                                <option value="noindex">NoIndex</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div> -->

                        <!-- <div class="panel panel-default">
                            <div class="panel-heading"><i style="color: #1dc116" class="fa fa-key"></i> <b>Meta Keywords</b></div>
                            <div class="panel-body">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <div class="col-sm-12">
                                            <input name="keywords" type="text" data-role="tagsinput" value="" class="form-control">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div> -->

                    </div>
                    <div class="pull-right col-md-4">
                        <div class="panel panel-default">
                            <div class="panel-heading"><i style="color: black;" class=""></i> Pubblica</div>
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-md-10">
                                        <div class="col-md-6">
                                            <p><i class="fa fa-hand-o-right"></i> Stato: <b>Non Pubblicato</p>
                                        </div>
                                        <div class="col-md-2 pull-right">
                                            <button name="crea_centro" id="creaCentro" class="btn btn-oval btn-info">Crea Centro</button>
                                        </div>
                                    </div>
                                </div>

                            </div> <!-- panel-body -->
                        </div> <!-- panel panel-default -->


                        <div class="panel panel-default">
                            <div class="panel-heading"><i style="color: #009ab8" class="fa fa-users"></i> Autore</div>
                            <div class="panel-body">
                                <!-- <label>Utente</label> -->

                                <select name="autore" class="form-control">
                                    <?php if($utente_ses->ruolo == 'admin'): ?>
                                        <?php $utenti = Utente::seleziona_tutti(); ?>
                                        <?php foreach($utenti as $utente): ?>
                                            <option <?php echo ($utente->username == $utente_ses->username)? 'selected="selected"' : ""?> ><?php echo $utente->username; ?></option>
                                        <?php endforeach; ?>
                                    <?php endif; ?>
                                    <?php if($utente_ses->ruolo != 'admin'): ?>
                                        <option><?php echo $utente_ses->username; ?></option>
                                    <?php endif; ?>
                                </select>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading"><i style="color: red;" class="fa fa-exclamation-triangle"></i> Stato</div>
                            <div class="panel-body">
                                <!-- <label>Stato</label> -->
                                <select name="stato" class="form-control" required>
                                    <option value="">Seleziona Stato</option>
                                    <option value="pubblicato">Pubblicato</option>
                                    <option value="bozza">Bozza</option>
                                </select>
                            </div> <!-- panel-body -->
                        </div> <!-- panel panel-default -->


                        <div class="panel panel-default">
                            <div class="panel-heading"><i class="fa fa-folder-o"></i> Seleziona Nazione</div>
                            <div class="panel-body">
                                <select class="form-control" name="nazione" id="nazione" required>
                                    <option value="">Seleziona Nazione</option>
<!--                                    --><?php //foreach ($selziona_nazione as $nazione): ?>
<!--                                        --><?php //if ($nazione->stato == 1): ?>
<!--                                            <option-->
<!--                                                value="--><?php //echo $nazione->id ?><!--">--><?php //echo $nazione->nazione ?><!--</option>-->
<!--                                        --><?php //endif; ?>
<!--                                    --><?php //endforeach; ?>
                                </select>
                            </div>
                        </div>


                        <div class="panel panel-default">
                            <div class="panel-heading"><i class="fa fa-folder-o"></i> Seleziona Città</div>
                            <div class="panel-body">
                                <select name="citta" class="form-control" id="cittAjax" required>
<!--                                    --><?php //$citta = CittaPage::seleziona_tutti(); ?>
                                    <option value="">Seleziona Città</option>
<!--                                    --><?php //foreach($citta as $chiave => $prendo_citta): ?>
<!--                                        <option value="--><?php //echo $prendo_citta->id; ?><!--">--><?php //echo strtoupper($prendo_citta->nome_citta); ?><!--</option>-->
<!--                                    --><?php //endforeach; ?>
                                </select>
                            </div>
                        </div>
                        <!--<div class="panel panel-default">
                            <div class="panel-heading"><i style="color: #c77405" class="fa fa-tags"></i> Tags</div>
                            <div class="panel-body">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <div class="col-sm-12">
                                            <input name="tags" type="text" data-role="tagsinput" value="" class="form-control">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div> -->

                        <div class="panel panel-default">
                            <div class="panel-heading">Immagine Centro</div>
                            <div class="panel-body">
                                <div class="text-center form-group">
                                    <img class="img-thumbnail img-responsive" style="width: 350px; height: 350px;" src="https://placeholdit.imgix.net/~text?txtsize=33&txt=350%C3%97350&w=350&h=350<?php //echo $articoli->percorso_immagine_e_plecholder(); ?>" alt="<?php //echo $articoli->immagine_articolo; ?>">
                                    <br>
                                </div>
                                <div class="col-sm-12 form-group">
                                    <input type="file" name="file" class="form-control">
                                </div>
                            </div>
                        </div>

                        <div class="panel panel-default">
                            <div class="panel-heading"><i style="color: #e80079" class="fa fa-picture-o"></i> <b>Image Alt</b></div>
                            <div class="panel-body">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <div class="col-sm-12">
                                            <input name="img_alt" type="text" class="form-control">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="panel panel-default">
                            <div class="panel-heading"><i style="color: #e80079" class="fa fa-picture-o"></i> <b>Image Title</b></div>
                            <div class="panel-body">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <div class="col-sm-12">
                                            <input name="img_title" type="text" class="form-control">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="panel panel-default">
                            <div class="panel-heading"><i style="color: #e80079" class="fa fa-header"></i> <b>Meta Title</b></div>
                            <div class="panel-body">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <div class="col-sm-12">
                                            <input maxlength="80" onkeyup="contaCaratteriTitle(this)" name="meta_title" type="text" class="form-control">
                                            <div id="meta_title_caratteri"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading"><i style="color: #CBE86B" class="fa fa fa-pencil"></i> <b>Meta Description</b></div>
                            <div class="panel-body">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <div class="col-sm-12">
                                            <input maxlength="160" onkeyup="contaCaratteriDescrizione(this)" name="meta_description" type="text" class="form-control">
                                            <div id="meta_descrizione_caratteri"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading"><i style="color: #0A246A" class="fa fa-globe"></i> <b>Canonical Url</b></div>
                            <div class="panel-body">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <div class="col-sm-12">
                                            <input name="canonical" type="url" value="" class="form-control">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading"><i style="color: #8e79e8 class="fa fa-indent"></i> <b>Meta Robots Index</b></div>
                            <div class="panel-body">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <div class="col-sm-12">
                                            <select name="selezione_index" id="selezione_index" class="form-control">
                                                <option value="index">Index</option>
                                                <option value="noindex">NoIndex</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>


                    </div> <!-- pull-right col-md-4 -->
                </div> <!-- col-md-12 -->
            </div>  <!-- row -->
        </form>
    </div> <!-- panel-body -->




</section>




<!-- Modal -->
<div class="modal fade" id="caratCentro" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Inserisci Caratteristica Centro</h4>
            </div>
            <form action="" method="post">
                <div class="modal-body">
                    <div class="form-group">
                        <label for="titolo_carat">Titolo Caratteristica</label>
                        <input type="text" name="titolo_carat" id="titolo_carat" class="form-control" placeholder="Inserisci la caratteristica">
                    </div>
                    <div class="form-group">
                        <label for="titolo_carat">Seleleziona Posizione Box</label>
                        <select name="posizione_box" id="pos_box"  class="form-control" required>
                            <option value="">Seleziona posizione caratteristica</option>
                            <option value="box-sopra">Box Sopra</option>
                            <option value="box-sotto">Box Sotto</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="desc_carat">Descrizione Caratteristica</label>
                        <textarea  name="desc_carat" id="desc_carat" class="form-control"></textarea>
                    </div>
                        <input type="hidden" id="id_ass_centro" name="id_ass_centro">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancella</button>
                    <button type="button" name="invia_carat" id="invia_carat" data-dismiss="modal" class="btn btn-primary">Salva Caratteristica</button>
                </div>
            </form>
        </div>
    </div>
</div>


<!-- Modal Aggiungi Alloggio-->
<div class="modal fade" id="sistemazione" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"><!--data-backdrop="static" data-keyboard="false"-->
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close close_nome_centro" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Aggiungi Alloggio</h4>
            </div>
            <form action="" method="post" data-parsley-validate>
                <div class="modal-body">
                    <input type="hidden" id="centroIdSist" value="">
                    <div class="form-group">
                        <label for="nome_nuovo_centro">Nome Alloggio</label>
                        <input type="text" name="nome_alloggio" id="nome_alloggio" class="form-control" required="required">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default close_nome_centro" data-dismiss="modal">Cancella</button>
                    <button type="button" name="ins_nuova_sistemazione" id="ins_nuova_sistemazione" data-dismiss="modal" class="btn btn-primary">Salva Nuovo Alloggio</button>
                </div>
            </form>
        </div>
    </div>
</div>


<!-- Modal -->
<div class="modal fade" id="ins_new_cent" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close close_nome_centro" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Inserisci Nuovo Centro</h4>
            </div>
            <form action="" method="post" data-parsley-validate>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="nome_nuovo_centro">Nome del Centro</label>
                        <input type="text" name="nome_nuovo_centro" id="nome_nuovo_centro" class="form-control" required="required">
                        <p id="errore_nome" style="display: none">Non puo essere vuoto</p>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default close_nome_centro" data-dismiss="modal">Cancella</button>
                    <button type="button" name="ins_nuovo_centr" id="ins_nuovo_centr" data-dismiss="modal" class="btn btn-primary">Salva Nuovo Centro</button>
                </div>
            </form>
        </div>
    </div>
</div>


<!-- Modal Inserisci Nuovo Prezzo -->
<div class="modal fade" id="prezzoCentro" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close close_nome_centro" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Inserisci Nuovo Prezzo al Centro</h4>
            </div>
            <form action="" method="post" data-parsley-validate>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="prezzo_centro">Importo</label>
                        <input type="text" name="prezzo_centro" id="importo_centro" class="form-control" required="required">
                        <p id="errore_nome" style="display: none">Non puo essere vuoto</p>
                    </div>
                    <div class="form-group">

                        <label for="data_inizio"><i class="fa fa-calendar"></i> Data Inizio</label>
                        <input type="date" name="data_inizio" id="data_inizio" class="form-control" required="required">
                        <!--                            <span class="input-group-addon"><span class="fa-calendar fa"></span></span>-->
                        <p id="errore_nome" style="display: none">Non puo essere vuoto</p>

                        <!--                        <input type="date" name="data_inizio" id="data_inizio" class="form-control" required="required">-->
                        <p id="errore_nome" style="display: none">Non puo essere vuoto</p>
                    </div>
                    <div class="form-group">
                        <label for="data_fine"><i class="fa fa-calendar"></i> Data Fine</label>
                        <input type="date" name="data_fine" id="data_fine" class="form-control" required="required">
                        <p id="errore_nome" style="display: none">Non puo essere vuoto</p>
                    </div>
                    <input type="hidden" id="id_ass_centro" name="id_ass_centro">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default close_nome_centro" data-dismiss="modal">Cancella</button>
                    <button type="button" name="ins_nuovo_prezzo_centr" id="ins_nuovo_prezzo_centr" data-dismiss="modal" class="btn btn-primary">Salva Nuovo Prezzo Centro</button>
                </div>
            </form>
        </div>
    </div>
</div>


<!-- Modal Aggingi Corso -->
<div class="modal fade" id="aggCorso" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close close_nome_centro" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Inserisci Nuovo Corso</h4>
            </div>
            <form action="" method="post" data-parsley-validate>
                <input type="hidden" id="idCentroCorso" name="idCentro" value="">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="nome_corso">Nome Corso</label>
                                <input type="text" name="nome_corso" id="nome_corso" class="form-control" placeholder="Nome Corso" required="required">
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <label for="num_sett">Numero Settimane</label>
                                <input type="text" name="num_sett" id="num_sett" class="form-control" placeholder="Solo numero" required="required">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="num_sett">Livello</label>
                                <input type="text" name="livello" id="livello" class="form-control" placeholder="Livello" required="required">
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <label for="num_sett">A partire da</label>
                                <input type="text" name="prezzo_da_corso" id="prezzo_da_corso" class="form-control" placeholder="A partire da" required="required">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-2">
                            <div class="form-group">
                                <label for="eta_min">Età min</label>
                                <input type="text" name="eta_min" id="eta_min" class="form-control" placeholder="Eta Min" required="required">
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <label for="durata">Durata Corso</label>
                                <input type="text" name="durata" id="durata" class="form-control" placeholder="Durata" required="required">
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="durata_lezione">Ogni Lezione</label>
                                <input type="text" name="durata_lezione" id="durata_lezione" class="form-control" placeholder="Durata Lezione" required="required">
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <label for="orario_lezione">Orario</label>
                                <input type="text" name="orario_lezione" id="orario_lezione" class="form-control" placeholder="Orario Lezione" required="required">
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="studenti_classe">Studenti Classe</label>
                                <input type="text" name="studenti_classe" id="studenti_classe" class="form-control" placeholder="Studenti Classe" required="required">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="arrivo">Arrivo</label>
                                <input type="text" name="arrivo" id="arrivo" class="form-control" placeholder="Arrivo" required="required">
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="partenze">Partenze</label>
                                <input type="text" name="partenze" id="partenze" class="form-control" placeholder="Partenze" required="required">
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="chiusura_scuola">Chiusura Scuola</label>
                                <input type="text" name="chiusura_scuola" id="chiusura_scuola" class="form-control" placeholder="Chiusura Scuola" required="required">
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="inizio_corso">Inizio Corso</label>
                                <input type="text" name="inizio_corso" id="inizio_corso" class="form-control" placeholder="Inizio Corso" required="required">
                            </div>
                        </div>
                    </div>
                    <input type="hidden" id="id_ass_centro" name="id_ass_centro">
                    <hr>
                    <dt class="text-primary">Scegli Sistemazioni</dt><br>
                    <div class="row mostroSist">
<!--                    --><?php //foreach ($sistemazioni as $sistemazione): ?>
<!--                            <div class="col-sm-4">-->
<!--                                <div class="form-group">-->
<!--                                    <label for="--><?php //echo $sistemazione->nome_alloggio; ?><!--" class="col-sm-5 control-label">--><?php //echo $sistemazione->nome_alloggio; ?><!--</label>-->
<!--                                    <label class="checkbox-inline c-checkbox">-->
<!--                                        <div id="checkboxes">-->
<!--                                        <input id="check --><?php //// echo $sistemazione->id; ?><!--" data-id="--><?php //echo $sistemazione->id; ?><!--" onclick="clickValue(this);" name="sistArr--><?php ////echo $sistemazione->id; ?><!--" type="checkbox" value="0" --><?php // ?><!-->
<!--                                        <span class="fa fa-check"></span>-->
<!--                                        </div>-->
<!--                                    </label>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                    --><?php //endforeach; ?>
                    </div>
                    <dt class="text-primary">Importo Sistemazioni</dt><br>
                    <button type="button" id="aggPrezzoCentro" class="btn btn-primary pull-right">Aggiungi</button>
                    <div id="showTabPrezzi">

                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default close_corso" data-dismiss="modal">Cancella</button>
                    <button type="button" name="ins_nuovo_corso" id="ins_nuovo_corso" data-dismiss="modal" class="btn btn-primary">Salva Nuovo Corso</button>
                </div>
            </form>
        </div>
    </div>
</div>


<?php include 'modale_canc_carat_centro.php'; ?>

<?php include 'includes/footer.php'; ?>

<script type="text/javascript">

    $("#aggCorso").on("shown.bs.modal", function () {
        $('#aggPrezzoCentro').on('click', function () {
            $('#aggCorso').find('.modal-body').append('fail');
        });

    });

//    $(document).ready(function () {
        $('#checkSist').on('click', function () {
//x
//            $.ajax({
//                type: "POST",
//                url: 'mostra_tabella_sistemazioni.php',
//                data: {id_sist_centro: idCentroPreso},
//                dataType: 'html',
//                async: true,
//                success: function (mostra_tab_sist_adulti) {
//                    if (!mostra_tab_sist_adulti.error) {
//                        $(' #showTabPrezzi').html(mostra_tab_sist_adulti);
//                    }
//                }
//
//            });
//
//
//        });
    });



    function clickValue(data) {
        var dataId = $(data).data("id");
        var idCentroPreso = $('#lastId').val();
//        console.log(idCentroPreso);

        console.log(dataId);
//        console.log($(this).val());
//        alert("Clicked, new value = " + cb.checked);
        var boolSist = (data.checked);
        console.log(boolSist);

        $.ajax({
                url: 'includes/api.php/stato_alloggio?filter=id_corso_adulti,eq,'+ idCentroPreso +'&filter=id_alloggio,eq,' + dataId + '&transform=1',
                type: 'GET',
                data: {id_alloggio : dataId, id_corso_adulti:idCentroPreso, stato:boolSist },
                headers: {
                    'Content-type': 'application/json',
                    'Access-Control-Allow-Headers': 'Content-Type',
                    'Access-Control-Allow-Origin': '*',
                    'X-XSRF-TOKEN': '<?php echo (isset($_COOKIE['XSRF-TOKEN']))? $_COOKIE['XSRF-TOKEN'] : ''?>'
                },
                dataType: 'json',
                async: true,
                success: function (data) {
//                    console.log(data);

                    Object.size = function(obj) {
                        var size = 0, key;
                        for (key in obj) {
                            if (obj.hasOwnProperty(key)) size++;
                        }
                        return size;
                    };

                    var size = Object.size(data.stato_alloggio);



                    if(size === 1 ){
                        console.log('esiste');

                        var idAlloggio = data.stato_alloggio[0].id_alloggio;

                        var idStatoAlloggio = data.stato_alloggio[0].id;

                        var idCorsoAdulti = data.stato_alloggio[0].id_corso_adulti;

                        if(boolSist === true){
                            boolSist = 1;
                            $(this).attr('checked',  true);
                        }

                        if(boolSist === false){
                            boolSist = 0;
                            $(this).attr('checked',  false);
                        }

                        if (dataId == idAlloggio) {
                            $.ajax({
                                url: 'includes/api.php/stato_alloggio/'+idStatoAlloggio,
                                type: 'PUT',
                                data: {stato:boolSist},
                                headers: {
                                    'Content-type': 'application/json',
                                    'Access-Control-Allow-Headers': 'Content-Type',
                                    'Access-Control-Allow-Origin': '*',
                                    'X-XSRF-TOKEN': '<?php echo (isset($_COOKIE['XSRF-TOKEN'])) ? $_COOKIE['XSRF-TOKEN'] : ''?>'
                                },
                                dataType: 'json',
                                async: true,
                                success: function (data) {

                                }

                            });
                        }

                    }

                    if(size === 0 ){
                        console.log('NON esiste');
                            $.ajax({
                                url: 'includes/api.php/stato_alloggio',
                                type: 'POST',
                                data: {id_alloggio: dataId, id_corso_adulti: idCentroPreso, stato: 1},
                                headers: {
                                    'Content-type': 'application/json',
                                    'Access-Control-Allow-Headers': 'Content-Type',
                                    'Access-Control-Allow-Origin': '*',
                                    'X-XSRF-TOKEN': '<?php echo (isset($_COOKIE['XSRF-TOKEN'])) ? $_COOKIE['XSRF-TOKEN'] : ''?>'
                                },
                                dataType: 'json',
                                async: true,
                                success: function (data) {

                                }
                            });
                    }
                }

    });



//        if(boolSist === true){
//            $.ajax({
//                url: 'includes/api.php/stato_alloggio',
//                type: 'post',
//                data: {id_alloggio : dataId, id_corso_adulti:idCentroPreso, stato:boolSist },
//                headers: {
//                    'Content-type': 'application/json',
//                    'Access-Control-Allow-Headers': 'Content-Type',
//                    'Access-Control-Allow-Origin': '*',
//                    'X-XSRF-TOKEN': '<?php //echo (isset($_COOKIE['XSRF-TOKEN']))? $_COOKIE['XSRF-TOKEN'] : ''?>//'
//                },
//                dataType: 'json',
//                async: true,
//                success: function () {
//
//                }
//            });
//        }
//        if(boolSist === false){
//            console.log("falso");
//        }


    }



    /*** URL FRIENDLY ***/
    function convertToSlug(Text)
    {
        return Text
            .toLowerCase()
            .replace(/[^\w ]+/g,'')
            .replace(/ +/g,'-');
    }

//    $('#nome_ritorno').on( "change",function () {
//        var titolo = $(this).val();
//        var urlFre = convertToSlug(titolo);
//
//        $.ajax({
//            url: "includes/api.php/centro/" + id_centro,
//            type: "PUT",
//            dataType: 'json',
//            data: {urlfriendly: urlFre},
//            headers: {
//                'Content-type': 'application/json',
//                'Access-Control-Allow-Headers': 'Content-Type',
//                'Access-Control-Allow-Origin': '*',
//                'X-XSRF-TOKEN': '<?php //echo (isset($_COOKIE['XSRF-TOKEN'])) ? $_COOKIE['XSRF-TOKEN'] : ''?>//'
//            },
//            success: function (data) {
//                console.log(data);
//            }
//        });
//    });







//    $(window).load(function(){
//        $('#ins_new_cent').modal('show',{backdrop: 'static', keyboard: false});
//        $('#ins_nuovo_centr').attr('disabled', true);
//        if($('#nome_nuovo_centro').val() == null){
//        $("#ins_new_cent").on("hidden.bs.modal", function () {
//                window.location = "elenco_centri.php";
//            });
//        }
//
//        $('#nome_nuovo_centro').on("change", function () {
//            var titolo = $(this).val();
//            $.trim(titolo);
//            var urlFre = convertToSlug(titolo);
////            console.log(urlFre);
//
//
//            $.ajax({
//                url: "includes/api.php/centro?filter=urlfriendly,eq," + urlFre,
//                type: "GET",
//                dataType: 'json',
//                data:{urlfriendly:urlFre},
//                headers: {
//                    'Content-type': 'application/json',
//                    'Access-Control-Allow-Headers': 'Content-Type',
//                    'Access-Control-Allow-Origin': '*',
//                    'X-XSRF-TOKEN': '<?php //echo (isset($_COOKIE['XSRF-TOKEN']))? $_COOKIE['XSRF-TOKEN'] : ''?>//'
//                },
//                success: function (data) {
//                    console.log(data.centro.records);
//
//                    if($.isEmptyObject(data.centro.records) == true){
//                        $('#ins_nuovo_centr').attr('disabled', false);
//                    }
//
//                    if($.isEmptyObject(data.centro.records) == false){
//                        $('#ins_nuovo_centr').attr('disabled', true);
//                        swal(
//                            'Oops...',
//                            'Il nome del centro già esiste!',
//                            'error'
//                        );
//                    }
//                }
//            });
//        });
//
//        $('.close_nome_centro').on('click', function (e) {
//
//            e.preventDefault();
//
//            window.location = "elenco_centri.php";
//        })
//
//    });

    $('#aggCorsoId').on('click', function () {
        var id_centro_corso = $('#lastId').val();
//        console.log(id_centro_corso);
        $('#idCentroCorso').val(id_centro_corso);
        $.ajax({
            type: "POST",
            url: 'mostra_alloggi_adulti.php',
            data: {id_sist_centro:id_centro_corso},
            dataType: 'html',
            async: true,
            success: function (mostra_alloggi_adulti) {
                if (!mostra_alloggi_adulti.error) {
                    $('.mostroSist').html(mostra_alloggi_adulti);
                }
            }

        });
        $.ajax({
            type: "POST",
            url: 'mostra_tabella_sistemazioni.php',
            data: {id_sist_centro:id_centro_corso},
            dataType: 'html',
            async: true,
            success: function (mostra_tab_sist_adulti) {
                if (!mostra_tab_sist_adulti.error) {
                    $(' #showTabPrezzi').html(mostra_tab_sist_adulti);
                }
            }

        });

    });

    $('#ins_nuova_sistemazione').on('click', function () {
        var nuovaSistemazione = $('#nome_alloggio').val();
        $.ajax({
            url: 'includes/api.php/alloggio_adulti',
            type: 'post',
            data: {nome_alloggio : nuovaSistemazione},
            headers: {
                'Content-type': 'application/json',
                'Access-Control-Allow-Headers': 'Content-Type',
                'Access-Control-Allow-Origin': '*',
                'X-XSRF-TOKEN': '<?php echo (isset($_COOKIE['XSRF-TOKEN']))? $_COOKIE['XSRF-TOKEN'] : ''?>'
            },
            dataType: 'json',
            async: true,
            success: function () {

                $('#aggSist').append(
                    '<div class="col-sm-3">' +
                        '<div class="form-group">'+
                            '<b> ' + nuovaSistemazione + ' </b>' +
                        '</div>' +
                    '</div>'
                );

                swal(
                    'Inserimento Avvenuto con Successo!',
                    'La sistemazione ' + nuovaSistemazione + ' è stata aggiunta.',
                    'success'
                )
            },
            error: function () {
                swal(
                    'Oops...',
                    'Qualche cosa non è andata bene!',
                    'error'
                )
            }
        });

    });

    $('#ins_nuovo_centr').on('click', function (e) {

        e.preventDefault();

        var nome_nuovo_centro = $('#nome_nuovo_centro').val();
        if( nome_nuovo_centro.length === 0 ) {
            $('#errore_nome').show();
            window.location = "elenco_centri.php";
        } else {

            var id = null;

            $.ajax({
                url: "insert_nuovo_centro.php",
                type: "POST",
                data: {nome_nuovo_centro : nome_nuovo_centro},
                dataType: 'json',
                success: function(data){

                    var id = data.id;
                    var nomeCentro = data.Centro;
                    //console.log(nomeCentro);

                    $('#id_centro').val(id);
                    $('#id_ass_centro').val(id);
                    var titolo = $('#nome_ritorno').val(nomeCentro);
                    var urlFre = convertToSlug(titolo);
                    $('#urlFirendly').val(urlFre);
                    //console.log(data.id);
                    //console.log(nome_nuovo_centro);
                    //data;
                    //console.log(titolo);
                    //console.log(descrizione);
                    //alert(data.titolo);

                },
                error: function () {
                    alert("errore nell'inerimento");
                }
            });

        }

        setTimeout(function () {
            //        var nome_nuovo_centro = $('#titolo_carat').val();
//            console.log(nome_nuovo_centro);
            var urlFre = convertToSlug(nome_nuovo_centro);
//            console.log(urlFre);
            var id_centro_preso = $('#id_centro').val();
//            console.log(id_centro_preso);


            $.ajax({
                url: "includes/api.php/centro/" + id_centro_preso,
                type: "PUT",
                dataType: 'json',
                data: {urlfriendly: urlFre},
                headers: {
                    'Content-type': 'application/json',
                    'Access-Control-Allow-Headers': 'Content-Type',
                    'Access-Control-Allow-Origin': '*',
                    'X-XSRF-TOKEN': '<?php echo (isset($_COOKIE['XSRF-TOKEN'])) ? $_COOKIE['XSRF-TOKEN'] : ''?>'
                },
                success: function (data) {
                    if(data){
                        $('#urlFirendly').val(urlFre);
                    }

                }
            });
        }, 1000);
    });


    $('#invia_carat').on('click', function (e) {

             e.preventDefault();

        var titolo = $('#titolo_carat').val();
        var descrizione = $('#desc_carat').val();
        var posizione = $('#pos_box').val();
        var id_ass_centro = $('#id_ass_centro').val();
        //var sorted;

            $.ajax({
                url: "insert_carat_centro.php",
                type: "POST",
                data: {titolo : titolo, descrizione : descrizione, id_ass_centro :id_ass_centro, posizione:posizione},

                success: function(data){
                     //data;
                        //console.log(titolo);
                        //console.log(descrizione);
                    //alert(data.titolo);

                },
                error: function () {
                    alert("errore nell'inerimento");
                }
            });



            leggi_centro();


     });

// Inserisco il prezzo al centro tramite Ajax

$('#ins_nuovo_prezzo_centr').on('click', function (e) {
   e.preventDefault();

    var importo = $('#importo_centro').val();
    var data_inizio = $('#data_inizio').val();
    var data_fine = $('#data_fine').val();
    var id_ass_centro = $('#id_ass_centro').val();

    $.ajax({
        url: "insert_prezzo_centro.php",
        type: "POST",
        data: {importo : importo, data_inizio : data_inizio, data_fine: data_fine, id_ass_centro :id_ass_centro},

        success: function(data){
            //data;
            //console.log(titolo);
            //console.log(descrizione);
            //alert(data.titolo);

        },
        error: function () {
            alert("errore nell'inerimento");
        }
    });

    leggi_prezzo();


});


    function leggi_centro() {

        var id_centro_preso = $('#id_ass_centro').val();

        $.ajax({
            url: 'mostra_carat_cent.php',
            type: 'POST',
            data: {mostro_id_centro:id_centro_preso},
            dataType: 'html',
            async: true,
            success: function (mostra_carat_centro) {
                $('#carat-null').hide();
                if (!mostra_carat_centro.error) {
                    $("#mostra_carat").html(mostra_carat_centro);
                }
                if(mostra_carat_centro.length === 0){
                    $("#mostra_carat").html("<h1>Nessuna Caratteristica Inserita</h1>");
                }
            }
        });
    }

function leggi_prezzo() {

    var id_centro_preso = $('#id_ass_centro').val();

    $.ajax({
        url: 'mostra_prezzi_cent.php',
        type: 'POST',
        data: {mostro_id_centro:id_centro_preso},
        dataType: 'html',
        async: true,
        success: function (mostra_prezzo_centro) {
            $('#carat-null-prezzo').hide();
            if (!mostra_prezzo_centro.error) {
                $("#mostra_prezzi").html(mostra_prezzo_centro);
            }
            console.log(mostra_prezzo_centro);
        }
    });
}


//    $(document).ready(function () {
//        $('#modPrezzo').on('click', function () {
//            alert('ok');
//        });
//    });


    /*$('#delete_carat').on('click', function (e) {

        e.preventDefault();

        var id_carat = $('#el_carat_cent').val();

        console.log(id_carat);


        $.ajax({
            url: 'elimina_carat_centro.php',
            type: 'POST',
            data: {id_carat: id_carat},
            async: false,
            success: function (id_carat) {

            }
        })


    })*/

    /*$(document).ready(function () {
        $('tbody').sortable({
            axis: 'y',
            update: function (event, ui) {
                var data = $(this).sortable('serialize');

                $.ajax({
                    data: data,
                    type: 'POST',
                    url: '/your/url/here'
                });
            }
        });
    });*/

</script>

<script type="text/javascript" xmlns="http://www.w3.org/1999/html">
    tinymce.init({
        //selector: '#desc_carat',
        selector: "textarea",
        setup: function (editor) {
            editor.on('change', function () {
                editor.save();
            });
        },
        theme: 'modern',
        width: 850,
        height: 450,
        language: 'it',
        plugins: [
            "advlist autolink lists link image charmap print preview anchor",
            "searchreplace visualblocks code fullscreen",
            "insertdatetime media table contextmenu paste imagetools wordcount"
        ],
        toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | preview" +
        "code | ",
        images_upload_url: 'upload.php',
        images_upload_base_path: '/admin/immagini',
        media_live_embeds: true,
        style_formats_merge: true,
        menubar: true,
        image_caption: true,
        /*file_browser_callback: function(field_name, url, type, win) {
         if(type=='image') $('#my_form input').click();
         },*/
        image_prepend_url: "http://localhost/mla-cms/admin/immagini/",
        language_url: 'js/tinymce/js/tinymce/langs/it.js' ,
        //preview_styles: true,
        menu: {
            file: {title: 'File', items: 'newdocument'},
            edit: {title: 'Modifica', items: 'undo redo | cut copy paste pastetext | selectall'},
            insert: {title: 'Inserisci', items: 'link media | template hr'},
            view: {title: 'Vedi', items: 'visualaid preview visualblocks'},
            format: {
                title: 'Formato',
                items: 'bold italic underline strikethrough superscript subscript | formats | removeformat'
            },
            table: {title: 'Tabella', items: 'inserttable tableprops deletetable | cell row column'},
            tools: {title: 'Tools', items: 'spellchecker code'},
            style_formats: {title: 'Coustom Menu', items: 'button'}
        },
        style_formats: [
            {title: 'Custom Menu', items: [
                {title : 'Button', selector : 'a', classes : 'button'}
            ]}
        ]
    });

    $('#catAajx').on('change', function () {

        var idCat = $(this).val();
        console.log(idCat);
        $('#nazione option').remove();
        $('#nazione').append($('<option>', {
            value: '',
            text: 'Seleziona Nazione'
        }));
        $('#cittAjax option').remove();
        $('#cittAjax').append($('<option>', {
                        value: '',
                        text: 'Seleziona Città'
                    }));


                    $.ajax({
                        url: "includes/api.php/nazione_page?&filter=id_categoria,eq," + idCat +"&transform=1",
                        type: "GET",
                        dataType: 'json',
                        headers: {
                            'Content-type': 'application/json',
                            'Access-Control-Allow-Headers': 'Content-Type',
                            'Access-Control-Allow-Origin': '*',
                            'X-XSRF-TOKEN': '<?php echo (isset($_COOKIE['XSRF-TOKEN'])) ? $_COOKIE['XSRF-TOKEN'] : ''?>'
                        },
                        success: function (data) {
                            if(data) {
//                    console.log(data);
//                    $('#urlFirendly').val(urlFre);
//                        console.log(data.nazioni);
                                var nazArr = [];

                                $.each(data.nazione_page, function (i, val) {
//                        console.log(i);
//                        console.log(val);
                                    var nazid;
                                    nazid = val.id_nazione;
//                        console.log(nazid);
                                    nazArr.push(nazid);
                                });
                                console.log(nazArr);

//                }

//                    Object.keys(data).map(function(objectKey, index) {
//                        var value = object[objectKey];
//                        console.log(value);
//                    });
//                    $('#catAajx').on('change', function () {

//                    if (typeof nazArr !== 'undefined' && nazArr.length == 0) {
//                        console.log('vuoto');


                                $.ajax({
                                    url: "includes/api.php/nazioni/" + nazArr + "&transform=1",
                                    type: "GET",
                                    dataType: 'json',
                                    headers: {
                                        'Content-type': 'application/json',
                                        'Access-Control-Allow-Headers': 'Content-Type',
                                        'Access-Control-Allow-Origin': '*',
                                        'X-XSRF-TOKEN': '<?php echo (isset($_COOKIE['XSRF-TOKEN'])) ? $_COOKIE['XSRF-TOKEN'] : ''?>'
                                    },
                                    success: function (data) {
//                            console.log(data);

                                        if (data.length > 1) {
//
                                            $.each(data, function (i, val) {
                                                console.log(val.nazione);
                                                $('#nazione').append($('<option>', {
                                                    value: val.id,
                                                    text: val.nazione
                                                }));
                                            });
                                        } else {
                                            $('#nazione').append($('<option>', {
                                                value: data.id,
                                                text: data.nazione
                                            }));
                                        }
                                    }
                                });
//                    });
                    }

            }
        })

    });

    var idCat;
    $('#catAajx').on('change', function () {
        idCat = $(this).val();
    });


    $('#nazione').on('change', function () {
        var idNaz = $(this).val();
        $('#cittAjax option').remove();


        $('#cittAjax').append($('<option>', {
            value: '',
            text: 'Seleziona Città'
        }));

        $.ajax({
            url: "includes/api.php/citta_page?&filter[]=id_nazione,eq," + idNaz +"&filter[]=id_categoria,eq," + idCat +"&transform=1",
            type: "GET",
            dataType: 'json',
            headers: {
                'Content-type': 'application/json',
                'Access-Control-Allow-Headers': 'Content-Type',
                'Access-Control-Allow-Origin': '*',
                'X-XSRF-TOKEN': '<?php echo (isset($_COOKIE['XSRF-TOKEN'])) ? $_COOKIE['XSRF-TOKEN'] : ''?>'
            },
            success: function (data) {

                if(data){
                    var cittArr = [];

//                    $.each(data.citta_page, function (i, val) {

//                        console.log(i);
//                        console.log(val);
//                        console.log(val.nome_citta);

//                        if (data.citta_page.length > 1) {
//                                console.log('piu di uno');
                            $.each(data.citta_page, function (i, val) {
//                                console.log(val.nome_citta);
                                $('#cittAjax').append($('<option>', {
                                    value: val.id,
                                    text: val.nome_citta
                                }));
                            });
                        }
//                        else {
//                            $('#cittAjax').append($('<option>', {
//                                value: data.id,
//                                text: data.nome_citta
//                            }));
//                        }
//                        var citid;
//                        citid = val.citta;
//                        console.log(nazid);
//                        cittArr.push(citid);

//                        console.log(val.nazione_page);
//                            var naz = data.nazione_page;
//
//                    });
//                    console.log(citid);
                }

//            }
        });


    });
</script>
