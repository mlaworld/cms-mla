<?php include 'includes/header.php'; ?>
<?php if(!$sessione->loggato()){ redirect('accedi.php');} ?>

<!-- Barra di Navigazione -->
<?php include ("includes/navbar.php") ?>

<?php include ("includes/aside.php") ?>
<!-- End aside-->

<?php //include ("includes/aside_utenti.php") ?>

<?php
/**
 * Created by Giuseppe Alessandro De Blasio.
 * User: Giuseppe Alessandro De Blasio
 * Date: 15/10/2016
 * Time: 09:52
 */

$centri= Centro::seleziona_tutti();

//$caratCentro = caratCentro::seleziona_tutti();

$pagina = !empty($_GET['pagina']) ? (int)$_GET['pagina'] : 1;

$elementi_per_pagina = 10;

$elementi_totali = Page::conta_elementi();

$paginazione = new Paginazione($pagina, $elementi_per_pagina, $elementi_totali);

$sql = "SELECT * FROM centro ";
$sql.= "LIMIT {$elementi_per_pagina} ";
$sql.= "OFFSET {$paginazione->offset()}";
$pagine = Page::cerca_con_query($sql);

if(isset($_POST['cerca_frase'])){
    $cerca = $_POST['cerca'];
    if(!empty($cerca)) {
        $sql = "SELECT * FROM view_cataloghi";
        $sql .= " WHERE Centro LIKE '%$cerca%' OR nome_catalogo LIKE '%$cerca%' ";
        $centri = Centro::cerca_con_query($sql);
//        var_dump($centri);
//        var_dump(Centro::view_cataloghi($cerca));


    }
}

if(isset($_GET['duplica'])){

    $duplico = new Centro();
    $duplico->duplicazioneRow($_GET['duplica']);
    //redirect('elenco_centri.php');

}



?>
<br>
<section class="main-content">
    <h1 class="page-header">Elenco Centri</h1>
    <div class="panel panel-default">
        <div class="panel-heading">Elenco Centri
            <a title="" class="pull-right" href="javascript:void(0);" data-original-title="Collapse Panel"
               data-toggle="tooltip" data-perform="panel-collapse">
                <em class="fa fa-minus"></em>
            </a>
        </div> <!-- panel-heading -->

        <form action="" method="POST">
            <div class="panel-wrapper collapse in" style="height: auto;">
                <div class="panel-body">
                    <div class="table-responsive">
                        <hr>
                        <div class="form-group col-sm-2">
                            <label class="control-label">Scegli Categoria
                                <select  name="categoria" id="categoriaSelect" class="form-control" required>
                                    <?php $categorie = CategoriePagine::seleziona_tutti(); ?>
                                    <option value="removeCat">Seleziona Categoria</option>
                                    <?php foreach($categorie as $categoria): ?>
                                        <option value="<?php echo $categoria->titolo_categoria; ?>"><?php echo formattazioneTesto($categoria->titolo_categoria); ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </label>
                        </div>

                        <div class="form-group col-sm-2">
                            <label class="control-label">Scegli Nazione
                                <select  name="nazione" id="nazioneSelect" class="form-control" required>
                                    <?php $nazioneSel = Nazioni::seleziona_tutti(); ?>
                                    <option value="removeNaz">Seleziona Nazione</option>
                                    <?php foreach($nazioneSel as $nazione): ?>
                                        <option value="<?php echo $nazione->nazione; ?>"><?php echo formattazioneTesto($nazione->nazione); ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </label>
                        </div>

                        <div class="form-group col-sm-2">
                            <label class="control-label">Scegli Citta
                                <select  name="citta" id="cittaSelect" class="form-control" required>
                                    <?php $citta = CittaPage::seleziona_tutti(); ?>
                                    <option value="remove">Seleziona Città</option>
                                    <?php foreach($citta as $chiave => $prendo_citta): ?>
                                        <option value="<?php echo $prendo_citta->nome_citta; ?>"><?php echo formattazioneTesto($prendo_citta->nome_citta); ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </label>
                        </div>
                        <div class="clearfix"></div>

                        <hr>
                        <br>
                        <table class="table table-bordered table-hover" id="tabCentri">
                            <thead>
                            <tr>
                                <th>Id</th>
                                <th>Nome Centro</th>
                                <th>Categoria Centro</th>
                                <th>Nazione Centro</th>
                                <th>Citta Centro</th>
                                <th>Modifica</th>
                                <th>Elimina</th>
                                <th>Vedi Centro</th>
                                <th>Duplica</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php foreach ($centri as $centro): ?>

                                <tr>
                                    <td><?php echo $centro->id; ?></td>
                                    <td><?php echo $centro->Centro; ?></td>
                                    <td>
                                        <?php $categoria = CategoriePagine::seleziona_per_id($centro->id_categoria);
                                        echo (empty($categoria->titolo_categoria)) ? "" : $categoria->titolo_categoria ?>
                                    </td>
                                    <td>
                                        <?php $nazioneSelect = Nazioni::seleziona_per_id($centro->id_nazione);
                                        echo (empty($nazioneSelect->nazione)) ? "" : $nazioneSelect->nazione ?>
                                    </td>
                                    <td>
                                        <?php
                                        $cittaSel = CittaPage::seleziona_per_id($centro->id_citta);
                                        echo $cittaSel->nome_citta;
                                        ?>
                                    </td>
                                    <td><a style="color: rgba(83, 86, 93, 0.68)"
                                           href="modifica_centro.php?id=<?php echo $centro->id; ?>"><i
                                                style="color: rgb(5, 25, 93)" ; class=" fa fa-edit"></i> Modifica</a>
                                    </td>
                                    <td><a href="#" data-record-id="<?php echo $centro->id; ?>"
                                           data-record-title="<?php echo $centro->Centro; ?>" data-toggle="modal"
                                           data-target="#canc-centro"><i style="color: #e90811;"
                                                                         class="fa fa-trash-o"></i>
                                            Elimina</a></td>
                                    <td><a href="<?php echo "/".$categoria->urlFriendly."/".$nazioneSelect->urlFriendly."/".$cittaSel->urlFriendly."/".$centro->urlfriendly?>" target="_blank">Vedi Pagina</a></td>
                                    <td><a name="duplica" href="?duplica=<?php echo $centro->id ?>">Duplica</a></td>
                                    <?php //foreach ($caratCentro as $caratteristica): ?>

                                    <?php //endforeach; ?>
                                </tr>
                            <?php endforeach; ?>
                            </tbody>

                        </table>
                        <!--                <div class="panel-footer">-->
                        <!--                <div class="row">-->
                        <!--                    <div class="col-lg-2">-->
                        <!--                        <div class="input-group">-->
                        <!--                            <input class="input-sm form-control" type="text" name="cerca" id="cerca" placeholder="Cerca">-->
                        <!--                            <span class="input-group-btn">-->
                        <!--                               <button class="btn btn-sm btn-default" type="submit" name="cerca_frase" id="cerca_frase">Cerca</button>-->
                        <!--                               --><?php //  if(isset($_POST['cerca_frase'])){ echo "<button class=\"btn btn-sm btn-danger\" type=\"submit\" name=\"cerca_frase\" onclick='aggiorna();' id=\"cerca_frase\">Annulla</button>"; function aggiorna(){redirect('elenco_centri.php');}; unset($_POST['cerca_frase']); }?>
                        <!--                           </span>-->
                        <!--                        </div>-->
                        <!--                        <div class="col-lg-8">-->
                        <!--                            <ul class="pagination pull-right">-->
                        <!--                                --><?php
                        //                                if($paginazione->pagine_totali() > 1){
                        //                                    if($paginazione->e_prima()){
                        //                                        echo "<li><a href='elenco_articoli.php?pagina={$paginazione->prima()}'>Prima</a></li>";
                        //                                    }
                        //                                    for ($i = 1; $i <= $paginazione->pagine_totali(); $i++){
                        //                                        if($i == $paginazione->pagina_corrente){
                        //                                            echo "<li class='active'><a href='elenco_articoli.php?pagina={$i}'>{$i}</a></li>";
                        //                                        } else {
                        //                                            echo "<li><a href='elenco_articoli.php?pagina={$i}'>{$i}</a></li>";
                        //                                        }
                        //                                    }
                        //
                        //                                    if($paginazione->e_dopo()){
                        //                                        echo "<li><a href='elenco_articoli.php?pagina={$paginazione->dopo()}'>Dopo</a></li>";
                        //                                    }
                        //                                }
                        //
                        //                                ?>
                        <!--                            </ul>-->
                        <!--                        </div>-->
                        <!--                    </div>-->
                        <!--                </div>-->
                        <!--                </div><!-- panel-footer -->
                    </div><!-- table-responsive -->
                </div><!-- panel-body -->
            </div><!-- panel-wrapper collapse in -->
        </form>
    </div> <!-- panel panel-default -->
</section>









<?php include 'modale_canc_centro.php'; ?>

<?php include 'includes/footer.php'; ?>

<script>
    $(document).ready(function(){
        var TabCentri =  $('#tabCentri').dataTable({
//            initComplete: function () {
//                this.api().columns().every( function () {
//                    var column = this;
//                    var select = $('<select><option value=""></option></select>')
//                        .appendTo( $(column.footer()).empty() )
//                        .on( 'change', function () {
//                            var val = $.fn.dataTable.util.escapeRegex(
//                                $(this).val()
//                            );
//
//                            column
//                                .search( val ? '^'+val+'$' : '', true, false )
//                                .draw();
//                        } );
//
//                    column.data().unique().sort().each( function ( d, j ) {
//                        select.append( '<option value="'+d+'">'+d+'</option>' )
//                    } );
//                } );
//            },
            "language": {
                "url": "js/datatables/traduzione/Italian.json"
            }
        });

//        var cittaSel = 2;//you want it on the third column
//        $("thead th").each( function ( i ) {
//            if(i === cittaSel){
//                this.innerHTML = var r='<select><option value=""></option>';( TabCentri.fnGetColumnData(i) );
//                $('select', this).change( function () {
//                    TabCentri.fnFilter( $(this).val(), i );
//                } );
//            }
//        } );

        $('#categoriaSelect').on('change', function () {
            var selectCategoria = $(this).children(":selected").val();
            console.log(selectCategoria);
            if(selectCategoria === "removeCat"){
//                TabCentri.fnFilterClear('Categoria Centro');
                TabCentri.fnFilter('',2);
            } else {
                TabCentri.fnFilter("^" + selectCategoria + "$", 2, true);
            }
        });

        $('#nazioneSelect').on('change', function () {
            var selectNazione = $(this).children(":selected").val();
            console.log(selectNazione);
            if(selectNazione === "removeNaz"){
//                TabCentri.fnFilterClear('Categoria Centro');
                TabCentri.fnFilter('',3);
            } else {
                TabCentri.fnFilter("^" + selectNazione + "$", 3, true);
            }
        });

            $('#cittaSelect').on('change', function () {
                var selectCitta = $(this).children(":selected").val();
                console.log(selectCitta);
                if(selectCitta === "remove"){
//                    TabCentri.fnFilterClear(4);
                    TabCentri.fnFilter('',4);

                } else {
                    TabCentri.fnFilter("^" + selectCitta + "$", 4, true);
                }
            });




    });

//    function filtroCitta() {
//        var citta= $('input:selected[name="citta"]').val(function() {
//            return '^' + this.value + '\$';
//        }).get().join('|');
//        console.log(citta);
//        // variabile e valore column array
//        TabCentri.fnFilter(citta, 4, true, false, false, false);
//    }


    /**=========================================================
     * Module: DATATABLE per rimuovere il filtro
     =========================================================*/
        jQuery.fn.dataTableExt.oApi.fnFilterClear  = function ( oSettings )
        {
            var i, iLen;

            /* Remove global filter */
            oSettings.oPreviousSearch.sSearch = "";

            /* Remove the text of the global filter in the input boxes */
            if ( typeof oSettings.aanFeatures.f != 'undefined' )
            {
                var n = oSettings.aanFeatures.f;
                for ( i=0, iLen=n.length ; i<iLen ; i++ )
                {
                    $('input', n[i]).val( '' );
                }
            }

            /* Remove the search text for the column filters - NOTE - if you have input boxes for these
             * filters, these will need to be reset
             */
            for ( i=0, iLen=oSettings.aoPreSearchCols.length ; i<iLen ; i++ )
            {
                oSettings.aoPreSearchCols[i].sSearch = "";
            }

            /* Redraw */
            oSettings.oApi._fnReDraw( oSettings );
        };
</script>