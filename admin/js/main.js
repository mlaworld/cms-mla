$(function(){
	
	// Cache the window object
	var $windows = $(window);
	
	//Parallax background effect
	
	$('section[data-type="background"]').each(function(){
		
		var $bgobj = $(this); // assegnamo l'oggetto
		
		$(window).scroll(function(){
			
			// velocità scrol background
			// la yPos è negativa perchè lo scrolling è UP!
			
			var yPos = -($windows.scrollTop() / $bgobj.data('speed'));
			
			// Put toghether our final background position
			var coords = '50% ' + yPos + 'px';
			
			//	Move the background
			$bgobj.css({ backgroundPosition: coords	});
			
		});//	end windows scroll
	});
		
});