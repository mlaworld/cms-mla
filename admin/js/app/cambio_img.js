/**=========================================================
 * Module: elimina.js
 * Toggle the aside between normal an collapsed state
 * State applied to body so all elements can be notified
 * Targeted elements must have [data-toggle="aside"]
 =========================================================*/
$(document).ready(function(){
   
    var utente_href;
    var utente_href_splitted;
    var utente_id;
    var immagine_src;
    var immagine_href_splitted;
    var immagine_nome;
    var foto_id;
    
    
    $(".moadal_thumbnails").click(function(){
       
        $("#imposta_immagine_utente").prop('disabled', false);
        
        utente_href = $("#utente-id").prop('href');
        utente_href_splitted = utente_href.split("=");
        utente_id = utente_href_splitted[utente_href_splitted.length -1];
        
        immagine_src = $(this).prop("src");
        immagine_href_splitted = immagine_src.split("/");
        immagine_nome = immagine_href_splitted[immagine_href_splitted.length -1];
        
        foto_id = $(this).attr("data");
        
        $.ajax({
            
            url: "includes/codice_ajax.php",
            data: {foto_id:foto_id},
            type: "POST",
            success: function(data){
                if(!data.error){
                    $("#modal_sidebar").html(data);
                }
            }
        });
    });
});


     $("#imposta_immagine_utente").click(function(){
        
         $.ajax({
            
            url: "includes/codice_ajax.php",
            data: {immagine_nome:immagine_nome, utente_id: utente_id },
            type: "POST",
            success: function(data){
                if(!data.error){
                    $(".utente_immagine_box a img").prop('src', data);
                }
            }
        });         

     });

