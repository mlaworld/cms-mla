<?php include 'includes/init.php'; ?>

<?php if(!$sessione->loggato()){ redirect('accedi.php');} ?>

<?php

if(empty($_GET['id'])){
    redirect('elenco_articoli.php');
}

$articolo = Articoli::seleziona_per_id($_GET['id']);

if($articolo){
    $sessione->messaggio('<div data-toggle="notify" data-onload data-message="Articolo <b>Eliminato</b> Correttamente" data-options="{&quot;status&quot;:&quot;success&quot;, &quot;pos&quot;:&quot;bottom-right&quot;}" class="hidden-xs"></div>');
    $articolo->cancella();
    redirect("elenco_articoli.php");
} else {
    redirect("elenco_articoli.php");
}