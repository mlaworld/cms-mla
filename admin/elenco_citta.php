<?php include 'includes/header.php'; ?>
<?php if(!$sessione->loggato()){ redirect('accedi.php');} ?>

<!-- Barra di Navigazione -->
<?php include ("includes/navbar.php") ?>

<?php include ("includes/aside.php") ?>
<!-- End aside-->

<?php //include ("includes/aside_utenti.php") ?>

<?php
/**
 * Created by Giuseppe Alessandro De Blasio.
 * User: Giuseppe Alessandro De Blasio
 * Date: 22/09/2016
 * Time: 13:12
 */

$citta_page= CittaPage::seleziona_tutti();

$pagina = !empty($_GET['pagina']) ? (int)$_GET['pagina'] : 1;

$elementi_per_pagina = 10;

$elementi_totali = Page::conta_elementi();

$paginazione = new Paginazione($pagina, $elementi_per_pagina, $elementi_totali);

$sql = "SELECT * FROM page ";
$sql.= "LIMIT {$elementi_per_pagina} ";
$sql.= "OFFSET {$paginazione->offset()}";
$pagine = Page::cerca_con_query($sql)


?>
<br>
<section class="main-content">
    <h1 class="page-header">Elenco Città</h1>
    <div class="panel panel-default">
        <div class="panel-heading">Elenco Città
            <a title="" class="pull-right" href="javascript:void(0);" data-original-title="Collapse Panel" data-toggle="tooltip" data-perform="panel-collapse">
                <em class="fa fa-minus"></em>
            </a>
        </div> <!-- panel-heading -->
        <div class="panel-body">
        <div class="table-responsive">
            <hr>

            <div class="form-group">
                <label class="control-label">Scegli Categoria</label> <br>

                        <?php $categorie = CategoriePagine::seleziona_tutti_per_stato(); foreach ($categorie as $categoria): ?>
                            <label class="checkbox-inline c-checkbox">
                                <input onchange="filtroCitta()" name="citta" type="checkbox" value="<?php echo $categoria->titolo_categoria;  ?>">
                                <span class="fa fa-check"></span><?php echo $categoria->titolo_categoria; ?>
                            </label>
                        <?php endforeach; ?>

            </div>
                <hr>
                <br>

<!--        <div class="panel-wrapper collapse in" style="height: auto;"><div class="table-responsive"> 5-->
                <table id="tableCitta" class="table table-bordered table-hover">
                    <thead>
                        <tr>
                            <th>Id</th>
                            <th>Citta</th>
                            <th>Categoria</th>
                            <th>Nazione</th>
                            <th>Descrizione</th>
                            <!--<th>Data</th> -->
                            <th>Modifica</th>
                            <th>Elimina</th>
                            <th>Vedi Pagina</th>
                        </tr>
                    </thead>
                    <tbody>
                    <form action="" method="POST">
                    <?php foreach ($citta_page as $citta): ?>
                        <?php $nazioneSel = Nazioni::seleziona_per_id($citta->id_nazione); ?>
                        <tr>
                            <td><?php echo $citta->id; ?></td>
                            <td><?php echo strip_tags($citta->nome_citta); ?></td>
                            <td><?php $catSel = CategoriePagine::seleziona_per_id($citta->id_categoria); echo $catSel->titolo_categoria?></td>
                            <td><?php $nazSel = Nazioni::seleziona_per_id($citta->id_nazione); echo $nazSel->nazione; ?></td>
                            <td><?php echo $citta->descrizione;  ?></td>
                            <!--<td><?php //echo data_it($nazione->data_creazione); ?></td>-->
                            <td><a style="color: rgba(83, 86, 93, 0.68)" href="modifica_citta.php?id=<?php echo $citta->id; ?>"><i style="color: rgb(5, 25, 93)"; class=" fa fa-edit"></i> Modifica</a></td>
                            <td><a class="" href="#" data-record-id="<?php echo $citta->id; ?>" data-record-title="<?php echo strip_tags($citta->desc_titolo); ?>" data-toggle="modal" data-target="#cancella-citta-page"><i style="color: #e90811;" class="fa fa-trash-o"></i> Elimina</a></td>
                            <td><a href="<?php echo "/".$catSel->urlFriendly."/".$nazioneSel->urlFriendly."/".$citta->urlFriendly?>" target="_blank">Vedi Pagina</a></td>
                        </tr>
                    <?php endforeach; ?>
                    </form>
                    </tbody>
                </table>
<!--              <div class="panel-footer">-->
<!--                <div class="row">-->
<!--                    <div class="col-lg-2">-->
<!--                        <div class="input-group">-->
<!--                            <input class="input-sm form-control" type="text" name="cerca" id="cerca" placeholder="Cerca">-->
<!--                            <span class="input-group-btn">-->
<!--                               <button class="btn btn-sm btn-default" type="submit" name="cerca_frase" id="cerca_frase">Cerca</button>-->
<!--                           </span>-->
<!--                        </div>-->
<!--                        <div class="col-lg-8">-->
<!--                            <ul class="pagination pull-right">-->
<!--                                --><?php
//                                if($paginazione->pagine_totali() > 1){
//                                    if($paginazione->e_prima()){
//                                        echo "<li><a href='elenco_citta.php?pagina={$paginazione->prima()}'>Prima</a></li>";
//                                    }
//                                    for ($i = 1; $i <= $paginazione->pagine_totali(); $i++){
//                                        if($i == $paginazione->pagina_corrente){
//                                            echo "<li class='active'><a href='elenco_citta.php?pagina={$i}'>{$i}</a></li>";
//                                        } else {
//                                            echo "<li><a href='elenco_citta.php?pagina={$i}'>{$i}</a></li>";
//                                        }
//                                    }
//
//                                    if($paginazione->e_dopo()){
//                                        echo "<li><a href='elenco_citta.php?pagina={$paginazione->dopo()}'>Dopo</a></li>";
//                                    }
//                                }
//
//                                ?>
<!--                            </ul>-->
<!--                        </div>-->
<!--                    </div>-->
<!--                </div>-->
<!--                </div><!-- panel-footer -->
            </div><!-- table-responsive -->
        </div><!-- panel-wrapper collapse in -->
    </div> <!-- panel panel-default -->
</section>









<?php include 'modale_canc_citta_page.php'; ?>

<?php include 'includes/footer.php'; ?>

<script>
   var tabCitta = $('#tableCitta').dataTable({
        "language": {
            "url": "js/datatables/traduzione/Italian.json"
        }
    });

   function filtroCitta() {
       var citta= $('input:checkbox[name="citta"]:checked').map(function() {
           return '^' + this.value + '\$';
       }).get().join('|');
       // variabile e valore column array
       tabCitta.fnFilter(citta, 2, true, false, false, false);
   }
</script>
