<?php
/**
 * Created by Giuseppe Alessandro De Blasio.
 * User: Giuseppe Alessandro De Blasio
 * Date: 06/10/2016
 * Time: 11:10
 */
?>

<?php include 'includes/init.php'; ?>

<?php if(!$sessione->loggato()){ redirect('accedi.php');} ?>

<?php

if(empty($_GET['id'])){
    redirect('nazione.php');
}

$nazione = Nazioni::seleziona_per_id($_GET['id']);

if($nazione){
    $sessione->messaggio('<div data-toggle="notify" data-onload data-message="Nazione <b>Eliminata</b> Correttamente" data-options="{&quot;status&quot;:&quot;success&quot;, &quot;pos&quot;:&quot;bottom-right&quot;}" class="hidden-xs"></div>');
    //$nazione = new Nazioni();
    $nazione->cancella();
    redirect("nazione.php");
} else {
    redirect("nazione.php");
}