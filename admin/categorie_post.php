<?php include 'includes/header.php'; ?>
<?php //include 'includes/modale_foto_utenti.php'; ?>


<?php  if(!$sessione->loggato()){ redirect('accedi.php');} ?>

        <!-- Barra di Navigazione -->
        <?php include ("includes/navbar.php") ?>
        
        <?php include ("includes/aside.php") ?>
        <!-- End aside-->
      
<!--        --><?php //include ("includes/aside_utenti.php") ?>
        
        <?php $categorie_post = CategoriePagine::seleziona_tutti(); ?>
        
        <?php
        $categorie = new CategoriePagine();
        
        if(isset($_POST['inserisci_categoria'])){
            if($categorie){
                $categorie->titolo_categoria = $_POST['categoria'];
                $urlRewrite = rewriteUrl(trim(strtolower($categorie->titolo_categoria)));
                $categorie->urlFriendly = $urlRewrite;
                $categorie->stato = 1;
                if($categorie->salva()){
                $sessione->messaggio('<div data-toggle="notify" data-onload data-message="Categoria <b>Aggiunta</b> Correttamente" data-options="{&quot;status&quot;:&quot;success&quot;, &quot;pos&quot;:&quot;bottom-right&quot;}" class="hidden-xs"></div>');
                redirect('categorie_post.php');
                }

            }
        }
        
       
        /* ELIMINO CATEGORIA */               
        if(isset($_POST['elimina'])){
            /*$categorie = CategoriePagine::seleziona_per_id($_POST['elimina']);
            if($categorie){
            //$sessione->messaggio('<div data-toggle="notify" data-onload data-message="Categoria <b>Eliminata</b> Correttamente" data-options="{&quot;status&quot;:&quot;success&quot;, &quot;pos&quot;:&quot;bottom-right&quot;}" class="hidden-xs"></div>');
            $categorie->cancella();
            redirect("categorie_post.php");
            } else {
            redirect("categorie_post.php");
            }*/
            redirect("elimina_cat_post.php?id={$_POST['elimina']}");

        }
        
        ?>

        
        <br>
     <section class="main-content">


         <h1 class="page-header">Categorie Pagine</h1>
     
        <div class="col-md-4">       
                <div class="panel panel-default">
                     <div class="panel-heading">Aggiungi Categorie alle Pagine</div>
                     <div class="panel-body">
                         <form role="form" method="POST" action="" data-parsley-validate>
                           <div class="form-group">
                              <label>Inserisci Categoria</label>
                              <input type="text" name="categoria" placeholder="Inserisci Categoria" class="form-control" required>
                           </div>
                             <button type="submit" name="inserisci_categoria" class="btn btn-sm btn-default pull-right">Inserisci</button>
                        </form>
                     </div>
                  </div>
        </div>
         
         <div class="col-md-2"></div>
         
         <div class="col-md-6">
         <div class="panel panel-default">
                     <div class="panel-heading">Categorie Inserite</div>
                     <div class="panel-body">
                        <!-- START table-responsive-->
                        <div class="table-responsive">
                           <table class="table table-striped table-bordered table-hover">
                              <thead>
                                 <tr>
                                    <th>#</th>
                                    <th>Titolo Categoria</th>
                                    <th>Stato</th>
                                    <th>Modifica</th>
                                    <th>Elimina</th>
                                 </tr>
                              </thead>
                              <tbody>
                                  <?php foreach($categorie_post as $categorie): ?>
                                 <tr>
                                    <td><?php echo $categorie->id; ?></td>
                                    <td><?php echo $categorie->titolo_categoria; ?></td>
                                    <td>
                                        <label class='switch text-center'>
                                            <input type='checkbox' data-record-id="<?php echo $categorie->id; ?>" class="statoCat" name='attivo_utente[]' <?php echo($categorie->stato == 1)? "checked='checked'": "" ?>  value='<?php  echo$categorie->stato; ?>'>
                                            <span></span>
                                        </label></td>
                                    <td><a style="color: blue; font-weight: bold" href='categorie_post.php?modifica=<?php echo $categorie->id; ?>'><i class="fa fa-edit"></i> Modifica</a></td>
                                    <td><a href="#" data-record-id="<?php echo $categorie->id; ?>" data-record-title="<?php echo $categorie->titolo_categoria; ?>" data-toggle="modal" data-target="#cancella-cat-post"><i style="color: red;" class="fa fa-trash-o"> Elimina</i></a></td>
                                 </tr>
                                 <?php endforeach; ?>
                              </tbody>
                           </table>
                        </div>
                        <!-- END table-responsive-->
                     </div>
                  </div>
             </div>
         
         <?php 
            /* MODIFICO CATEGORIA */
            if(isset($_GET['modifica'])){
                include "aggiorna_categoria.php";
            }
         
         ?>
    </section>

<script>
    $( ".statoCat" ).each(function(index) {
        $(this).on("click", function(){
            var val = $(this).val();
            var idCat = $(this).data('recordId');
            if(val == 0){
                val = 1;
            } else if(val == 1){
                val = 0;
            }

            console.log(val);
            console.log(idCat);
            $.ajax({
                url: "includes/api.php/categorie_post/" + idCat,
                type: "PUT",
                dataType: 'json',
                data: {stato: val},
                headers: {
                    'Content-type': 'application/json',
                    'Access-Control-Allow-Headers': 'Content-Type',
                    'Access-Control-Allow-Origin': '*',
                    'X-XSRF-TOKEN': '<?php echo (isset($_COOKIE['XSRF-TOKEN'])) ? $_COOKIE['XSRF-TOKEN'] : ''?>'
                },
                success: function (data) {
                    if(data){
                        $(this).val(val);
                    }

                }
            });
        });
    });

</script>

<?php include 'modale_canc_cat_post.php'; ?>

<?php include 'includes/footer.php'; ?>


