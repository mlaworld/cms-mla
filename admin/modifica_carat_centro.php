<?php
/**
 * Created da Giuseppe Alessandro De Blasio.
 * User: Giuseppe Alessandro De Blasio
 * Date: 25/10/16
 * Time: 11:52
 */
include 'includes/header.php';
if(!$sessione->loggato()){ redirect('accedi.php');}

if (empty($_GET['id'])) {
    redirect('elenco_centri.php');
}

$carat_centro = caratCentro::seleziona_per_id($_GET['id']);

if (isset($_POST['aggiorna_carat'])) {
    if ($carat_centro) {
        $carat_centro->titolo = $_POST['titolo_carat'];
        $carat_centro->posizione_row = $_POST['posizione_box'];
        $carat_centro->descrizione = $_POST['desc_carat'];
        $carat_centro->sorted = $_POST['sorted'];
        $carat_centro->salva();
        echo "<script>leggi_centro();</script>";
        echo "<script>window.close();</script>";




   }
}

?>


    <h1 class="page-header">Aggiorna Caratteristica Centro</h1>
<!-- Modal -->
            <form action="" method="post">
                <div class="modal-body">
                    <div class="form-group">
                        <label for="titolo_carat">Titolo Caratteristica</label>
                        <input type="text" name="titolo_carat" id="titolo_carat" value="<?php echo $carat_centro->titolo; ?>" class="form-control" placeholder="Inserisci la caratteristica">
                    </div>
                    <div class="form-group">
                        <label for="titolo_carat">Seleleziona Posizione Box</label>
                        <select name="posizione_box" id="pos_box"  class="form-control" required>
                            <option value="">Seleziona posizione caratteristica</option>
                            <option <?php echo ($carat_centro->posizione_row == 'box-sopra')? 'selected' : ''  ?> value="box-sopra">Box Sopra</option>
                            <option <?php echo ($carat_centro->posizione_row == 'box-sotto')? 'selected' : ''  ?> value="box-sotto">Box Sotto</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="desc_carat">Descrizione Caratteristica</label>
                        <textarea  name="desc_carat" id="desc_carat" class="form-control" ><?php echo $carat_centro->descrizione ?></textarea>
                    </div>
                    <input type="hidden" id="id_ass_centro" name="id_ass_centro">
                    <input type="hidden" id="sorted" name="sorted" value="<?php echo $carat_centro->sorted; ?>">
                </div>
                <div class="modal-footer">
                    <button type="button" id="canc_carat" class="btn btn-default" onclick="window.close()">Cancella</button>
                    <button type="submit" name="aggiorna_carat" id="agg_carat" class="btn btn-primary">Aggiorna Caratteristica</button>
                </div>
            </form>



<script type="text/javascript" xmlns="http://www.w3.org/1999/html">
    tinymce.init({
        //selector: '#desc_carat',
        selector: "textarea",
        setup: function (editor) {
            editor.on('change', function () {
                editor.save();
            });
        },
        theme: 'modern',
        width: 850,
        height: 450,
        language: 'it',
        plugins: [
            "advlist autolink lists link image charmap print preview anchor",
            "searchreplace visualblocks code fullscreen",
            "insertdatetime media table contextmenu paste imagetools wordcount"
        ],
        toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | preview" +
        "code | ",
        images_upload_url: 'upload.php',
        images_upload_base_path: '/admin/immagini',
        media_live_embeds: true,
        style_formats_merge: true,
        menubar: true,
        image_caption: true,
        /*file_browser_callback: function(field_name, url, type, win) {
         if(type=='image') $('#my_form input').click();
         },*/
        image_prepend_url: "http://localhost/mla-cms/admin/immagini/",
        language_url: 'js/tinymce/js/tinymce/langs/it.js' ,
        //preview_styles: true,
        menu: {
            file: {title: 'File', items: 'newdocument'},
            edit: {title: 'Modifica', items: 'undo redo | cut copy paste pastetext | selectall'},
            insert: {title: 'Inserisci', items: 'link media | template hr'},
            view: {title: 'Vedi', items: 'visualaid preview visualblocks'},
            format: {
                title: 'Formato',
                items: 'bold italic underline strikethrough superscript subscript | formats | removeformat'
            },
            table: {title: 'Tabella', items: 'inserttable tableprops deletetable | cell row column'},
            tools: {title: 'Tools', items: 'spellchecker code'},
            style_formats: {title: 'Coustom Menu', items: 'button'}
        },
        style_formats: [
            {title: 'Custom Menu', items: [
                {title : 'Button', selector : 'a', classes : 'button'}
            ]}
        ]
    });





    function leggi_centro() {

        var id_centro_preso = $('#id_centro').val();

        $.ajax({
            url: 'mostra_carat_cent.php',
            type: 'POST',
            data: {mostro_id_centro:id_centro_preso},
            dataType: 'html',
            async: true,
            success: function (mostra_carat_centro) {
                $('#carat-null').hide();
                if (!mostra_carat_centro.error) {
                    $("#mostra_carat").html(mostra_carat_centro);
                }
            }
        })
    }
</script>
