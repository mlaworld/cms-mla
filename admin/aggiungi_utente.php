<?php include 'includes/header.php'; ?>

<?php if(!$sessione->loggato()){redirect("accedi.php") ; } ?>


<?php  $utente = Utente::seleziona_per_id($_SESSION['id_utente']); $utente->ruolo == "admin" ? NULL : redirect("index.php") ;  ?>

        <!-- Barra di Navigazione -->
        <?php include ("includes/navbar.php") ?>
        
        <?php include ("includes/aside.php") ?>
        <!-- End aside-->
      
        <?php include ("includes/aside_utenti.php") ?>

<?php 

    $utente = new Utente();
    
    if(isset($_POST['crea_utente'])){
        if($utente){
            $utente->username = $_POST['username'];
            $utente->nome = $_POST['nome'];
            $utente->cognome = $_POST['cognome'];
            $utente->password = $utente->password_crypt($_POST['password']);
            $utente->email = $_POST['email'];
            $timestamp = date('Y-m-d G:i:s');
            $utente->ultimo_accesso = $timestamp;
            $utente->ruolo = $_POST['ruoli'];
            $utente->codice_validazione = md5($utente->username . microtime());
            
            $utente->impostazione_file($_FILES['immagine_utente']);
            
            $utente->caricamento_foto();
            $utente->salva();
        }
    }

?>
        
        
    <br>
    <section class="main-content">
        <?php if(isset($_POST['crea_utente'])) { echo '<div data-toggle="notify" data-onload data-message="Utente Creato Correttamente" data-options="{&quot;status&quot;:&quot;success&quot;, &quot;pos&quot;:&quot;bottom-right&quot;}" class="hidden-xs"></div>';} ?>
        <div class="panel panel-default">
            <div class="panel-heading">Aggiungi Utente
                <a title="" class="pull-right" href="javascript:void(0);" data-original-title="Close Panel" data-toggle="tooltip" data-perform="panel-dismiss">
                    <em class="fa fa-times"></em>
                </a>
                <a title="" class="pull-right" href="javascript:void(0);" data-original-title="Collapse Panel" data-toggle="tooltip" data-perform="panel-collapse">
                    <em class="fa fa-minus"></em>
                </a>
            </div>
            <div class="panel-body">
                <form action="" class="form-horizontal" method="POST" enctype="multipart/form-data" data-parsley-validate>
                    
                    <div class="form-group col-sm-12">
                        <label for="username" class="col-sm-1 control-label">Username</label>
                        <div class="col-sm-3">
                            <input name="username" class="form-control" type="text" required="required">
                        </div>
                        <label for="password" class="col-sm-1 control-label">Password</label>
                        <div class="col-sm-3">
                            <input name="password" class="form-control" type="password" required="required">
                        </div>
                        <label for="email" class="col-sm-1 control-label">Email</label>
                        <div class="col-sm-3">
                            <input name="email" class="form-control" type="email" required="required">
                        </div>   
                    </div>
                    
                    <div class="form-group col-sm-12">
                        <label for="nome" class="col-sm-1 control-label">Nome</label>
                        <div class="col-sm-3">
                            <input name="nome" class="form-control" type="text" required="required">
                        </div>
                        <label for="cognome" class="col-sm-1 control-label">Cognome</label>
                        <div class="col-sm-3">
                            <input name="cognome" class="form-control" type="text" required="required">
                        </div>
                        <label for="immagine_utente" class="col-sm-1 control-label">Immagine Utente</label>
                        <div class="col-sm-3">
                            <input name="immagine_utente" tabindex="-1" class="filestyle form-control" id="filestyle-0" style="left: -9999px; position: absolute;" type="file" data-classinput="form-control inline" data-classbutton="btn btn-default">
                            <div tabindex="0" class="bootstrap-filestyle" style="display: inline; display: none;">
                            <input name="immagine_utente" style="display: none;" disabled="" class="form-control inline" type="text" data-classinput="form-control inline" data-classbutton="btn btn-default">
                            <label class="btn btn-default"  for="filestyle-0"><i class="icon-folder-open"></i> <span>Scegli Immagine</span></label></div> 
                            <!--<input name="immagine_utente" class="form-control inline" type="file" data-classbutton="btn btn-default">-->                         
                        </div>   
                    </div>
                
                    <div class="form-group col-sm-4">
                        <fieldset>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Ruolo</label>
                           <div class="col-sm-5">
                               <?php 
                               $permessi = array(
                                   1 => 'sottoscrittore',
                                   2 => 'editor',
                                   3 => 'moderatore',
                                   4 => 'admin',
                               );
                               ?>
                               <select name="ruoli" class="form-control" required>
                                   <option value="">Seleziona Permessi<hr></option>
                                   <?php foreach($permessi as $permesso => $valore): ?>
                                       <option value="<?php echo $valore; ?>"><?php echo $valore ; ?></option>
                                   <?php endforeach; ?>
                               </select> 
                           </div>
                        </div>
                        </fieldset>
                        </div>
                        <button class="btn btn-default col-sm-5" name="crea_utente" type="submit">Aggiungi Utente</button>
                        <div class="col-sm-3"></div>
                    </form>        
            </div>
                    

            </div>
        
    </section>
        
<?php include ("includes/footer.php") ?>
