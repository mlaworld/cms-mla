<?php
/**
 * Created da Giuseppe Alessandro De Blasio.
 * User: Giuseppe Alessandro De Blasio
 * Date: 17/10/16
 * Time: 11:36
 */
?>

<?php include 'includes/init.php'; ?>

<?php if(!$sessione->loggato()){ redirect('accedi.php');} ?>

<?php

if(empty($_GET['id'])){
    redirect('elenco_centri.php');
}

$elimina_centro = Centro::seleziona_per_id($_GET['id']);

if($elimina_centro){
    //$sessione->messaggio('<div data-toggle="notify" data-onload data-message="Caratteristica <b>Eliminata</b> Correttamente" data-options="{&quot;status&quot;:&quot;success&quot;, &quot;pos&quot;:&quot;bottom-right&quot;}" class="hidden-xs"></div>');
    $elimina_centro->cancella();
    //redirect("elenco_centri.php");
}

$elimino_carat_centro = caratCentro::seleziona_per_id_centro($_GET['id']);

$elimino_carat = new caratCentro();

if($elimino_carat){
    $elimino_carat->cancella_carat_centro($_GET['id']);
}

