<?php
/**
 * Created da Giuseppe Alessandro De Blasio.
 * User: Giuseppe Alessandro De Blasio
 * Date: 22/03/17
 * Time: 10:40
 */
?>
<?php include 'includes/header.php'; ?>

<?php if (!$sessione->loggato()) {
    redirect('accedi.php');
} ?>

    <!-- Barra di Navigazione -->
<?php include("includes/navbar.php") ?>

<?php include("includes/aside.php") ?>
    <!-- End aside-->

<?php

$cat_eventi = categoriaEventi::seleziona_tutti();
$ins_evento = new categoriaEventi();

if(isset($_POST['nomeCategoria'])):

    $ins_evento->titolo_categoria = $_POST['nomeCategoria'];
    $ins_evento->salva();

endif;


if(isset($_POST['idEvento'])):
    $elim_evento = categoriaEventi::seleziona_per_id($_POST['idEvento']);
//    $ins_evento->id = $_POST['idEvento'];
    $elim_evento->cancella();
//    redirect('categoria_eventi.php');

endif;

if (isset($_POST['idCat'])):
    $mod_evento = categoriaEventi::seleziona_per_id($_POST['idCat']);
//    $mod_evento->id = $_POST['idCat'];
    $mod_evento->titolo_categoria = $_POST['titoloCat'];
    $mod_evento->salva();

endif;


?>

<br>

<section class="main-content">

    <h1 class="page-header">Categoria Eventi</h1>

    <div class="col-md-4">
        <div class="panel panel-default">
            <div class="panel-heading">Aggiungi Categoria Evento</div>
            <div class="panel-body">
                <form role="form" method="POST" action="" enctype="multipart/form-data" data-parsley-validate>
                    <div class="form-group">
                        <label>Inserisci Nome Evento</label>
                        <input type="text" name="nome_evento" id="nomeCategoria" placeholder="Inserisci Nome Evento"  class="form-control" required>
                    </div>
                    <button type="submit" name="inserisci_evento" id="inserisciEvento" class="btn btn-sm btn-primary pull-right">Inserisci</button>
                </form>
            </div>
        </div>
    </div>

    <!--    <div class="col-md-2"></div>-->

    <div class="col-md-8">
        <div class="panel panel-default">
            <div class="panel-heading">Categoria Eventi Presenti</div>
            <div class="panel-body">
                <!-- START table-responsive-->
                <div class="table-responsive">
                    <table id="tabCataloghi" class="table table-striped table-bordered table-hover">
                        <thead>
                            <tr>
                                <th>Nome Evento</th>
                                <th>Modifica</th>
                                <th>Elimina</th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php foreach ($cat_eventi as $evento): ?>
                            <tr>
                                <td><?php echo $evento->titolo_categoria; ?></td>
                                <td><form action="" method="POST">
                                        <a style="color: blue;" href="#modale_cat_eventi" name="modale_cat_eventi"
                                           data-record-id="<?php echo $evento->id; ?>"
                                           data-record-titolo="<?php echo $evento->titolo_categoria; ?>"
                                           data-toggle="modal"
                                           data-target="#modale_cat_eventi">
                                            <i class="fa fa-edit"></i> Modifica
                                        </a>
                                        <input name="idEvento" type="hidden" value="<?php echo $evento->id;  ?>" />
                                        <input name="nomeEvento" type="hidden" value="<?php echo $evento->titolo_categoria;  ?>" />
                                    </form>
                                </td>
                                <td>
                                    <a href="#" class="eliminaEvento" data-record-id="<?php echo $evento->id; ?>" data-record-titolo="<?php echo $evento->titolo_categoria; ?>" data-toggle="modal" data-target="#cancella-catalogo"><i style="color: red;" class="fa fa-trash-o"> Elimina</i></a>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
                <!-- END table-responsive-->
            </div>
        </div>
    </div>
</section>


<div id="modale_cat_eventi" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Aggiorna Nome Evento</h4>
            </div>
            <form action="" method="post" enctype="multipart/form-data">
                <div class="modal-body">
                    <input type="hidden" value="" name="eventi_id" class="eventi_id" />
                    <div class="form-group">
                        <label>Aggiorna Categoria Evento</label>
                        <input id="evento_nome" type="text" name="evento_nome" placeholder="Inserisci Nome Evento" class="form-control evento_nome">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" name="annulla" class="btn btn-default" data-dismiss="modal">Cancella</button>
                    <button type="submit" name="aggiorna_evento" class="btn btn-primary agg_catalogo">Aggiorna Categoria Evento</button>
                </div>
            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

   <?php include 'includes/footer.php'; ?>

<script type="text/javascript">
    $(document).ready(function(){

        $('#modale_cat_eventi').on('show.bs.modal', function (e) {
            var data = $(e.relatedTarget).data();

            var idCat = $(e.relatedTarget).data('recordId');
            var titoloCat = $(e.relatedTarget).data('recordTitolo');
            console.log(titoloCat);
            console.log(idCat);
            $(e.currentTarget).find('input[name="evento_nome"]').val(titoloCat);
            $('.evento_nome').val(titoloCat).text(titoloCat);

            $('.agg_catalogo').on('click', function (e) {

                var nuovoTitolo = $('#evento_nome').val();

//                alert(nuovoTitolo);

                if(nuovoTitolo !== ''){
                    $('section').addClass('csspinner');
                    $.ajax({
                        dataType : 'php',
                        method: "POST",
                        data: {titoloCat:nuovoTitolo, idCat:idCat},
                        success: function(data){
                            $('section').removeClass('csspinner');
                            console.log(data);
                        }
                    });
                }
            });
        });



//
//        var idEvento = $(e.relatedTarget).data('recordId');
//        $(idEvento).data('recordId').val();
//        console.log(idEvento);

        $('#inserisciEvento').on('click', function () {
            var nomeCategoria = $('#nomeCategoria').val();

            if(nomeCategoria !== ''){
                $.ajax({
                    dataType : 'php',
                    method: "POST",
                    data: {nomeCategoria:nomeCategoria},
                    success: function(data){
                        console.log(data);
                    }
                });
            }
        });

    });

    $(document).ready(function(){
        $('.eliminaEvento').on('click', function (e) {
            e.preventDefault();
            var idEvento = $(this).data('recordId');
            var titoloEvento = $(this).data('recordTitolo');
//            console.log(idEvento);
            swal({
                title: 'Eliminazione ' + titoloEvento,
                text: titoloEvento + ' sta per essere eliminato.',
                type: 'warning',
                showCancelButton: true,
                cancelButtonText: 'Annulla!',
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Sei sicuro di voler Cancellare ' + titoloEvento + '?!'
            }).then(function () {
                swal(
                    'Cancellato!',
                    titoloEvento + ' è stato cancellato con Successo.',
                    'success'
                );
                $.post("categoria_eventi.php", {idEvento:idEvento});
                $('.swal2-confirm').hide();
                setTimeout(function () {
                    window.location = 'categoria_eventi.php';
                }, 2000);
            });
        });
    });
</script>