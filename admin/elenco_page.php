<?php include 'includes/header.php'; ?>
<?php if(!$sessione->loggato()){ redirect('accedi.php');} ?>

<!-- Barra di Navigazione -->
<?php include ("includes/navbar.php") ?>

<?php include ("includes/aside.php") ?>
<!-- End aside-->

<?php //include ("includes/aside_utenti.php") ?>

<?php
/**
 * Created by Giuseppe Alessandro De Blasio.
 * User: Giuseppe Alessandro De Blasio
 * Date: 22/09/2016
 * Time: 13:12
 */

$pagine = Page::seleziona_tutti();

$pagina = !empty($_GET['pagina']) ? (int)$_GET['pagina'] : 1;

$elementi_per_pagina = 10;

$elementi_totali = Page::conta_elementi();

$paginazione = new Paginazione($pagina, $elementi_per_pagina, $elementi_totali);

$sql = "SELECT * FROM page ";
$sql.= "LIMIT {$elementi_per_pagina} ";
$sql.= "OFFSET {$paginazione->offset()}";
$pagine = Page::cerca_con_query($sql)


?>
<br>
<section class="main-content">
    <h1 class="page-header">Elenco Pagine</h1>
    <div class="panel panel-default">
        <div class="panel-heading">Elenco Pagine
            <a title="" class="pull-right" href="javascript:void(0);" data-original-title="Collapse Panel" data-toggle="tooltip" data-perform="panel-collapse">
                <em class="fa fa-minus"></em>
            </a>
        </div> <!-- panel-heading -->

    <form action="" method="POST">
        <div class="panel-wrapper collapse in" style="height: auto;"><div class="table-responsive">
                <table class="table table-bordered table-hover">
                    <thead>
                        <tr>
                            <th>Id</th>
                            <th>Titolo</th>
                            <th>Autore</th>
                            <th>Data</th>
                            <th>Modifica</th>
                            <th>Elimina</th>
                            <th>Vedi Pagina</th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php foreach ($pagine as $pagina): ?>
                        <tr>
                            <td><?php echo $pagina->id; ?></td>
                            <td><?php echo $pagina->titolo; ?></td>
                            <td><?php echo $pagina->autore;  ?></td>
                            <td><?php echo data_it($pagina->data_creazione); ?></td>
                            <td><a style="color: rgba(83, 86, 93, 0.68)" href="modifica_page.php?id=<?php echo $pagina->id; ?>"><i style="color: rgb(5, 25, 93)"; class=" fa fa-edit"></i> Modifica</a></td>
                            <td><a class="" href="#" data-record-id="<?php echo $pagina->id; ?>" data-record-title="<?php echo $pagina->titolo; ?>" data-toggle="modal" data-target="#cancella-pagine"><i style="color: #e90811;" class="fa fa-trash-o"></i> Elimina</a></td>
                            <td></td>
                        </tr>
                    <?php endforeach; ?>
                    </tbody>
                </table>
                <div class="panel-footer">
                <div class="row">
                    <div class="col-lg-2">
                        <div class="input-group">
                            <input class="input-sm form-control" type="text" name="cerca" id="cerca" placeholder="Cerca">
                            <span class="input-group-btn">
                               <button class="btn btn-sm btn-default" type="submit" name="cerca_frase" id="cerca_frase">Cerca</button>
                           </span>
                        </div>
                        <div class="col-lg-8">
                            <ul class="pagination pull-right">
                                <?php
                                if($paginazione->pagine_totali() > 1){
                                    if($paginazione->e_prima()){
                                        echo "<li><a href='elenco_articoli.php?pagina={$paginazione->prima()}'>Prima</a></li>";
                                    }
                                    for ($i = 1; $i <= $paginazione->pagine_totali(); $i++){
                                        if($i == $paginazione->pagina_corrente){
                                            echo "<li class='active'><a href='elenco_articoli.php?pagina={$i}'>{$i}</a></li>";
                                        } else {
                                            echo "<li><a href='elenco_articoli.php?pagina={$i}'>{$i}</a></li>";
                                        }
                                    }

                                    if($paginazione->e_dopo()){
                                        echo "<li><a href='elenco_articoli.php?pagina={$paginazione->dopo()}'>Dopo</a></li>";
                                    }
                                }

                                ?>
                            </ul>
                        </div>
                    </div>
                </div>
                </div><!-- panel-footer -->
            </div><!-- table-responsive -->
        </div><!-- panel-wrapper collapse in -->
    </form>
    </div> <!-- panel panel-default -->
</section>









<?php include 'modale_canc_page.php'; ?>

<?php include 'includes/footer.php'; ?>
