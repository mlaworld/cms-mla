<?php
/**
 * Created by Giuseppe Alessandro De Blasio.
 * User: Giuseppe Alessandro De Blasio
 * Date: 28/09/2016
 * Time: 12:20
 */
?>

<?php include 'includes/init.php'; ?>

<?php if(!$sessione->loggato()){ redirect('accedi.php');} ?>

<?php

if(empty($_GET['id'])){
    redirect('gestione_upload.php');
}

$immagini = Image::seleziona_per_id($_GET['id']);

if($immagini){
    $sessione->messaggio('<div data-toggle="notify" data-onload data-message="Immagine <b>Eliminato</b> Correttamente" data-options="{&quot;status&quot;:&quot;success&quot;, &quot;pos&quot;:&quot;bottom-right&quot;}" class="hidden-xs"></div>');
    $immagine = new Image();
    $immagine->cancella_foto($immagini->id, $immagini->nomefile);
    redirect("gestione_upload.php");
} else {
    redirect("gestione_upload.php");
}