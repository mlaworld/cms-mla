<?php
/**
 * Created by Giuseppe Alessandro De Blasio.
 * User: Giuseppe Alessandro De Blasio
 * Date: 06/10/2016
 * Time: 11:10
 */
?>

<?php include 'includes/init.php'; ?>

<?php if(!$sessione->loggato()){ redirect('accedi.php');} ?>

<?php

if(empty($_GET['id'])){
    redirect('elenco_citta.php');
}

$citta_elim = CittaPage::seleziona_per_id($_GET['id']);

if($citta_elim){
    $sessione->messaggio('<div data-toggle="notify" data-onload data-message="Citta <b>Eliminata</b> Correttamente" data-options="{&quot;status&quot;:&quot;success&quot;, &quot;pos&quot;:&quot;bottom-right&quot;}" class="hidden-xs"></div>');
    //$nazione = new Nazioni();
    $citta_elim->cancella();
    redirect("elenco_citta.php");
} else {
    redirect("elenco_citta.php");
}