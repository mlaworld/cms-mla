<?php include 'includes/header.php'; ?>
<?php if(!$sessione->loggato()){ redirect('accedi.php');} ?>

<!-- Barra di Navigazione -->
<?php include ("includes/navbar.php") ?>

<?php include ("includes/aside.php") ?>
<!-- End aside-->

<?php //include ("includes/aside_utenti.php") ?>

<?php
/**
 * Created by Giuseppe Alessandro De Blasio.
 * User: Giuseppe Alessandro De Blasio
 * Date: 22/09/2016
 * Time: 13:12
 */

$utente_ses = Utente::seleziona_per_id($_SESSION['id_utente']);


$pages = new Page();

if(isset($_POST['crea_pagina'])){
    if($pages){
        $pages->titolo = $_POST['titolo_pagina'];
        $pages->contenuto = $_POST['descrizione_pagina'];
        $pages->url_friendly = rewriteUrl($_POST['titolo_pagina']);
        $pages->autore = $_POST['autore'];
        $pages->meta_title = $_POST['meta_title'];
        $pages->meta_description = $_POST['meta_description'];
        $pages->canonical_url = $_POST['canonical'];
        $pages->meta_robots = $_POST['selezione_index'];
        $pages->stato = $_POST['stato'];
        $pages->data_creazione = date('Y-m-d');
        $pages->salva();

         echo '<div data-toggle="notify" data-onload data-message="Pagina <b>Creata</b> Correttamente" data-options="{&quot;status&quot;:&quot;success&quot;, &quot;pos&quot;:&quot;bottom-right&quot;}" class="hidden-xs"></div>';
    } else{

        echo '<div data-toggle="notify" data-onload data-message="<b>Errore creazione Pagina</b>" data-options="{&quot;status&quot;:&quot;danger&quot;, &quot;pos&quot;:&quot;bottom-right&quot;}" class="hidden-xs"></div>';
    }


}

?>


<script type="text/javascript" xmlns="http://www.w3.org/1999/html">
    tinymce.init({
        selector: '#descrizione_pagina',
        theme: 'modern',
        width: 1100,
        height: 650,
        language: 'it',
        plugins: [
            "advlist autolink lists link image charmap print preview anchor",
            "searchreplace visualblocks code fullscreen",
            "insertdatetime media table contextmenu paste imagetools wordcount"
        ],
        toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | preview" +
        "code | ",
        images_upload_url: 'upload.php',
        images_upload_base_path: '/admin/immagini',
        media_live_embeds: true,
        style_formats_merge: true,
        menubar: true,
        image_caption: true,
        /*file_browser_callback: function(field_name, url, type, win) {
            if(type=='image') $('#my_form input').click();
        },*/
        image_prepend_url: "http://localhost/mla-cms/admin/immagini/",
        language_url: 'js/tinymce/js/tinymce/langs/it.js' ,
        //preview_styles: true,
        menu: {
            file: {title: 'File', items: 'newdocument'},
            edit: {title: 'Modifica', items: 'undo redo | cut copy paste pastetext | selectall'},
            insert: {title: 'Inserisci', items: 'link media | template hr'},
            view: {title: 'Vedi', items: 'visualaid preview visualblocks'},
            format: {
                title: 'Formato',
                items: 'bold italic underline strikethrough superscript subscript | formats | removeformat'
            },
            table: {title: 'Tabella', items: 'inserttable tableprops deletetable | cell row column'},
            tools: {title: 'Tools', items: 'spellchecker code'},
            style_formats: {title: 'Coustom Menu', items: 'button'}
        },
        style_formats: [
            {title: 'Custom Menu', items: [
                {title : 'Button', selector : 'a', classes : 'button'}
            ]}
            ]
    });
</script>





<section class="main-content">
    <h1 class="page-header">Aggiungi Pagina</h1>
    <div class="panel-body">
        <form method="POST" action="" class="form-horizontal" enctype="multipart/form-data" data-parsley-validate>
            <div class="row">
                <div class="col-md-12">
                    <div class="col-md-8">
                        <div class="form-group">
                            <label for="titolo_pagina" class="control-label">Titolo Page</label>
                            <input type="text" name="titolo_pagina" class="form-control" required>
                        </div>
                        <div class="form-group">
                            <label for="descrizione_pagina" class="control-label">Descrizione Pagina</label>
                            <textarea id="descrizione_pagina" name="descrizione_pagina"></textarea>
                            <!--<iframe id="form_target" name="form_target" style="display:none"></iframe>-->
                            <!--<form id="my_form" action="" target="form_target" method="post" enctype="multipart/form-data" style="width:0px;height:0;overflow:hidden">
                                <input name="image" type="file" onchange="$('#my_form').submit();this.value='';">
                            </form>-->
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading"><i style="color: #e80079" class="fa fa-header"></i> <b>Meta Title</b></div>
                            <div class="panel-body">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <div class="col-sm-12">
                                            <input maxlength="80" onkeyup="contaCaratteriTitle(this)" name="meta_title" type="text" class="form-control">
                                            <div id="meta_title_caratteri"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading"><i style="color: #CBE86B" class="fa fa fa-pencil"></i> <b>Meta Description</b></div>
                            <div class="panel-body">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <div class="col-sm-12">
                                            <input maxlength="160" onkeyup="contaCaratteriDescrizione(this)" name="meta_description" type="text" class="form-control">
                                            <div id="meta_descrizione_caratteri"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading"><i style="color: #0A246A" class="fa fa-globe"></i> <b>Canonical Url</b></div>
                            <div class="panel-body">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <div class="col-sm-12">
                                            <input name="canonical" type="url" value="" class="form-control">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading"><i style="color: #8e79e8 class="fa fa-indent"></i> <b>Meta Robots Index</b></div>
                            <div class="panel-body">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <div class="col-sm-12">
                                            <select name="selezione_index" id="selezione_index" class="form-control">
                                                <option value="index">Index</option>
                                                <option value="noindex">NoIndex</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!-- <div class="panel panel-default">
                            <div class="panel-heading"><i style="color: #1dc116" class="fa fa-key"></i> <b>Meta Keywords</b></div>
                            <div class="panel-body">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <div class="col-sm-12">
                                            <input name="keywords" type="text" data-role="tagsinput" value="" class="form-control">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div> -->

                    </div>
                    <div class="pull-right col-md-4">
                        <div class="panel panel-default">
                            <div class="panel-heading"><i style="color: black;" class=""></i> Pubblica</div>
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-md-10">
                                        <div class="col-md-6">
                                            <p><i class="fa fa-hand-o-right"></i> Stato: <b>Non Pubblicato</p>
                                        </div>
                                        <div class="col-md-2 pull-right">
                                            <button name="crea_pagina" class="btn btn-oval btn-info">Crea Pagina</button>
                                        </div>
                                    </div>
                                </div>

                            </div> <!-- panel-body -->
                        </div> <!-- panel panel-default -->


                         <div class="panel panel-default">
                             <div class="panel-heading"><i style="color: #009ab8" class="fa fa-users"></i> Autore</div>
                             <div class="panel-body">
                                 <!-- <label>Utente</label> -->

                                 <select name="autore" class="form-control">
                                     <?php if($utente_ses->ruolo == 'admin'): ?>
                                     <?php $utenti = Utente::seleziona_tutti(); ?>
                                     <?php foreach($utenti as $utente): ?>
                                         <option <?php echo ($utente->username == $utente_ses->username)? 'selected="selected"' : ""?> ><?php echo $utente->username; ?></option>
                                     <?php endforeach; ?>
                                     <?php endif; ?>
                                     <?php if($utente_ses->ruolo != 'admin'): ?>
                                         <option><?php echo $utente_ses->username; ?></option>
                                     <?php endif; ?>
                                 </select>
                             </div>
                         </div>
                            <div class="panel panel-default">
                                <div class="panel-heading"><i style="color: red;" class="fa fa-exclamation-triangle"></i> Stato</div>
                                    <div class="panel-body">
                                        <!-- <label>Stato</label> -->
                                        <select name="stato" class="form-control" required>
                                            <option value="">Seleziona Stato</option>
                                            <option value="pubblicato">Pubblicato</option>
                                            <option value="bozza">Bozza</option>
                                        </select>
                                    </div> <!-- panel-body -->
                            </div> <!-- panel panel-default -->

                        <div class="panel panel-default">
                            <div class="panel-heading"><i class="fa fa-folder-o"></i> Categorie</div>
                                <div class="panel-body">
                                    <select name="categorie" class="form-control" required>
                                        <?php $pagine = CategoriePagine::seleziona_tutti(); ?>
                                        <option value="">Seleziona Categoria</option>
                                        <?php foreach($pagine as $chiave => $pagina): ?>
                                            <option value="<?php echo $pagina->id; ?>"><?php echo $pagina->titolo_categoria; ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                        </div>

                        <!--<div class="panel panel-default">
                            <div class="panel-heading"><i style="color: #c77405" class="fa fa-tags"></i> Tags</div>
                            <div class="panel-body">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <div class="col-sm-12">
                                            <input name="tags" type="text" data-role="tagsinput" value="" class="form-control">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div> -->


                            </div> <!-- pull-right col-md-4 -->
                        </div> <!-- col-md-12 -->
                    </div>  <!-- row -->
                </form>
    </div> <!-- panel-body -->




</section>









<?php include 'includes/footer.php'; ?>
