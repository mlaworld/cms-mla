<?php include 'includes/header.php'; ?>
<?php include 'includes/modale_foto_utenti.php'; ?>

<?php if(!$sessione->loggato()){ redirect('accedi.php');} ?>

        <!-- Barra di Navigazione -->
        <?php include ("includes/navbar.php") ?>
        
        <?php include ("includes/aside.php") ?>
        <!-- End aside-->
      
 <?php include ("includes/aside_utenti.php") ?>
        
   <?php
    
   
    if(isset($_GET['id'])){
       $commenti = Commento::seleziona_tutti();
       
       $sql = "SELECT * FROM commenti ";
       $sql.= "WHERE articolo_id = " . $_GET['id'];
       
       $commenti = Commento::cerca_con_query($sql);
   }  else {
       
   }
   
   if(!isset($_GET['id'])){
    $commenti = Commento::seleziona_tutti();
       
    $pagina = !empty($_GET['pagina']) ? (int)$_GET['pagina'] : 1;
    
    $elementi_per_pagina = 10;
    
    $elementi_totali = Commento::conta_elementi();
    
    $paginazione = new Paginazione($pagina, $elementi_per_pagina, $elementi_totali);
    
    $sql = "SELECT * FROM commenti ";
    $sql.= "LIMIT {$elementi_per_pagina} ";
    $sql.= "OFFSET {$paginazione->offset()}";
    $commenti = Commento::cerca_con_query($sql);
   }
?>    
        
<?php
    
    
    
    
if(isset($_POST['checkbox_array']) AND !empty($_POST['checkbox_array'])) {

    
foreach($_POST['checkbox_array'] as $commento->id ){
        
  $bulk_opzione = $_POST['bulk_opzione'];
        
        switch($bulk_opzione) {
        case 'approvato':
 
        global $database;
$query = "UPDATE commenti SET stato = '{$bulk_opzione}' WHERE id = {$commento->id}  ";
        
$approvazione = mysqli_query($database->conn, $query);
            
$database->conferma_query( $approvazione);

redirect('commenti.php');

$sessione->messaggio('<div data-toggle="notify" data-onload data-message="Il Commento ha cambiato stato in <b>Approvato</b>" data-options="{&quot;status&quot;:&quot;success&quot;, &quot;pos&quot;:&quot;bottom-right&quot;}" class="hidden-xs"></div>');
            
         break;
            
            
case 'non approvato':
        
global $database;
    
$query = "UPDATE commenti SET stato = '{$bulk_opzione}' WHERE id = {$commento->id}  ";
        
$non_approvato = mysqli_query($database->conn, $query);
            
$database->conferma_query($non_approvato);

redirect('commenti.php');

$sessione->messaggio('<div data-toggle="notify" data-onload data-message="Il Commento ha cambiato stato in <b>Non Approvato</b>" data-options="{&quot;status&quot;:&quot;success&quot;, &quot;pos&quot;:&quot;bottom-right&quot;}" class="hidden-xs"></div>');
            
         break;
            
  

case 'Cancella':

//echo '<script type="text/javascript">alert("Sei sicuro di voler eliminare questo commento?");</script>';


//$query = "DELETE FROM commenti WHERE id = {$commento->id}  ";
        
//$cancella_commento = mysqli_query($database->conn,$query);
//
//$database->conferma_query($cancella_commento);

//redirect('commenti.php');

$sessione->messaggio('<div data-toggle="notify" data-onload data-message="Commento <b>Cancellato</b> Correttamente" data-options="{&quot;status&quot;:&quot;success&quot;, &quot;pos&quot;:&quot;bottom-right&quot;}" class="hidden-xs"></div>');

         break;
        
  
        }
    
    
    } 

}

   ?>  

<?php 


 
 /*if(isset($_GET['articolo_id'])){ 
     $_GET['articolo_id'] = $articolo_id;
    $commenti = Commento::seleziona_per_id($_GET[$articolo_id]);
 }*/

?>
    
    
        
     
     <br>
     <section class="main-content">    
        <div class="panel panel-default">
               <div class="panel-heading">Elenco Commenti
                  <!--<a title="" class="pull-right" href="javascript:void(0);" data-original-title="Close Panel" data-toggle="tooltip" data-perform="panel-dismiss">
                     <em class="fa fa-times"></em>
                  </a>-->
                  <a title="" class="pull-right" href="javascript:void(0);" data-original-title="Collapse Panel" data-toggle="tooltip" data-perform="panel-collapse">
                     <em class="fa fa-minus"></em>
                  </a>
               </div>
               <!-- START table-responsive-->
               <form action="" method="POST"> 
               <div class="panel-wrapper collapse in" style="height: auto;"><div class="table-responsive">
                  <table class="table table-bordered table-hover">
                     <thead>
                        <tr>
                            <th>#</th>
                            <th>Autore</th>
                            <th>Commento</th>
                            <th>Email</th>
                            <th>Stato</th>
                            <th>Risposta su Articolo</th>
                            <th>Data</th>
                            <th>Risp. Utente</th>
                            <th class="check-all" style="width: 5%">
                              <div title=""  class="checkbox c-checkbox" data-original-title="" data-toggle="tooltip" data-title="Check All">
                                 <label>
                                    <input type="checkbox">
                                    <span class="fa fa-check"></span>
                                 </label>
                              </div>
                            </th>
                        </tr>
                     </thead>
                     <tbody>
                         
                         <?php foreach ($commenti as $commento): ?>
                        <tr>
                            <td><?php echo $commento->id; ?></td>
                            <td><?php echo $commento->autore; ?></td>
                            <td><?php echo $commento->testo_commento; ?></td>
                            <td><?php echo $commento->email; ?></td>
                            <td><?php echo $commento->stato; ?></td>
                            <th><?php
                               
             
                                global $database;
                                $query = "SELECT * FROM articoli WHERE id = $commento->articolo_id ";
                                $seleziona_titolo = mysqli_query($database->conn,$query);
                                while($row = mysqli_fetch_assoc($seleziona_titolo)){
                                $titolo = $row['titolo'];
                                
                                    echo '<a href="../articolo.php?id='.$commento->articolo_id.'">'. $titolo.'</a>';
                             
                                }
                               ?>
                            </th>
                            <td><?php echo data_it($commento->data); ?></td>
                            <td><?php echo ($commento->id != $commento->risp_commento_id)? $commento->autore : NULL?></td>
                           <td>
                              <div class="checkbox c-checkbox">
                                 <label>
                                     <input name="checkbox_array[]" type="checkbox" value="<?php echo $commento->id; ?>">
                                    <span class="fa fa-check"></span>
                                 </label>
                              </div>
                           </td>
                        </tr>
                        <?php endforeach; ?>
                        

                     </tbody>
                  </table>
               </div>
                   
                 <div class="panel-footer">
                  <div class="row">
                     <div class="col-lg-2">
                        <div class="input-group">
                           <input class="input-sm form-control" type="text" placeholder="Cerca">
                           <span class="input-group-btn">
                              <button class="btn btn-sm btn-default" type="button">Cerca</button>
                           </span>
                        </div>
                     </div>
                      <div class="col-lg-8">
                              <ul class="pagination pull-right">
                                  <?php
                                  if(isset($_GET['id'])){ NULL; } else {
                                  if($paginazione->pagine_totali() > 1){
                                      if($paginazione->e_prima()){
                                          echo "<li><a href='commenti.php?pagina={$paginazione->prima()}'>Prima</a></li>";
                                      }
                                      for ($i = 1; $i <= $paginazione->pagine_totali(); $i++){
                                          if($i == $paginazione->pagina_corrente){
                                              echo "<li class='active'><a href='commenti.php?pagina={$i}'>{$i}</a></li>";
                                          } else {
                                              echo "<li><a href='commenti.php?pagina={$i}'>{$i}</a></li>";
                                          }
                                      }

                                      if($paginazione->e_dopo()){ 
                                          echo "<li><a href='commenti.php?pagina={$paginazione->dopo()}'>Dopo</a></li>";
                                      }                                      
                                    }
                                  }
                                  
                                  ?>
                              </ul>
                      </div>
                     <div class="col-lg-2">
                        <div class="input-group pull-right">
                            <?php $azioni = array(
                                0 => "Azione",
                                1 => "approvato",
                                2 => "non approvato",
                                3 => "Cancella"
                            ); ?> 
                           <select name="bulk_opzione" class="input-sm form-control">
                               <?php foreach ($azioni as $key => $azione): ?>
                               <option class="cancella_messaggio" value="<?php echo $azione ?>"><?php echo $azione ?></option>
                               <?php endforeach; ?>
                           </select>
                           <span class="input-group-btn">
                              <button name="applica" class="btn btn-sm btn-default">Applica</button>
                           </span>
                        </div>
                     </div>
                  </div>
               </div>
             </form>  
               </div>
               <!-- END table-responsive-->
            </div>
     </section>

                            
<?php include 'includes/footer.php'; ?>