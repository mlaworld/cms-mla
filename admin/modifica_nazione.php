<?php
/**
 * Created da Giuseppe Alessandro De Blasio.
 * User: Giuseppe Alessandro De Blasio
 * Date: 20/10/16
 * Time: 10:15
 */

?>

<?php include 'includes/header.php'; ?>
<?php if(!$sessione->loggato()){ redirect('accedi.php');} ?>

<!-- Barra di Navigazione -->
<?php include ("includes/navbar.php") ?>

<?php include ("includes/aside.php") ?>
<!-- End aside-->
<?php


if(empty($_GET['id'])){
    redirect('elenco_nazioni.php');
}
$selziona_nazione = Nazioni::seleziona_tutti();
$utente_ses = Utente::seleziona_per_id($_SESSION['id_utente']);

$nazioni = NazioniPage::seleziona_per_id($_GET['id']);

if(isset($_POST['aggiorna_nazione'])){

    //$aggiungo_centro = new Centro();
    //$aggiungo_nazione = NazioniPage::seleziona_per_id($_POST['id_centro']);
    

    if($nazioni){

        $nazioni->desc_titolo = $_POST['titolo_nazione'];
//        $nazioni->descrizione = $_POST['desc_naz'];
        $nazioni->titBox1 = $_POST['titolo-box-1'];
        $nazioni->box1 = $_POST['box-1'];
        $nazioni->titBox2 = $_POST['titolo-box-2'];
        $nazioni->box2 = $_POST['box-2'];
        $nazioni->titolo_desc = $_POST['titolo_box_nazioni'];
//        $nazioni->nazioneUrl = $_POST['url'];
        $cat = CategoriePagine::seleziona_per_id($_POST['categorie']);
        $categoria = $cat->urlFriendly;
        $naz = Nazioni::seleziona_per_id($_POST['nazione']);
        $nazione = $naz->urlFriendly;
        $nazioni->nazioneUrl = "/".$categoria."/".$nazione;
        $nazioni->stato = $_POST['stato'];
        $nazioni->id_categoria = $_POST['categorie'];
        $nazioni->id_nazione = $_POST['nazione'];
        $nazioni->img_alt = $_POST['img_alt'];
        $nazioni->img_title = $_POST['img_title'];
        $nazioni->metaDescription = $_POST['meta_description'];
        $nazioni->metaTitle = $_POST['meta_title'];
        $nazioni->canonicalUrl = $_POST['canonical'];
        $nazioni->metaIndex = $_POST['selezione_index'];
        $nazioni->impostazione_file_nazione($_FILES['file']);
        $nazioni->caricamento_foto_nazioni();
//        $nazioni->salva();
        $nazioni->esiste_nazioni_categoria_modifica("{$_POST['categorie']}", "{$_POST['nazione']}", "{$_POST['id_nazione']}");
//        $nazioni->salva();
       //redirect('elenco_centri.php');
//        echo '<div data-toggle="notify" data-onload data-message="Nazione <b>Modificata</b> Correttamente" data-options="{&quot;status&quot;:&quot;success&quot;, &quot;pos&quot;:&quot;bottom-right&quot;}" class="hidden-xs"></div>';

    }
}

/*$messaggio = "";

if(isset($_FILES['file'])){

    $immagine = new Image();
    $immagine->impostazione_file_foto($_FILES['file']);
    $immagine->data_immagine = date("Y-m-d");
    if($immagine->salva_immagine_post()){

        $messaggio = "Immagine caricata Correttamente";
    } else
    {
        $messaggio = join("<br>", $immagine->errore);
    }

}*/

//$inserisco_centro = new Centro();
//$inserisco_carat = new caratCentro();

/*if(isset($_POST['ins_nuovo_centr'])){
    if($inserisco_centro) {
        $inserisco_centro->Centro = $_POST['nome_nuovo_centro'];
    }
}
$ultimo_id = $inserisco_centro->id = $_SESSION['ultimo_id'];

if(isset($_POST['crea_pagina'])){
    if($inserisco_centro) {
        $inserisco_centro->Centro = $_POST['nome_centro'];
        $inserisco_carat->id_centro = $inserisco_centro->id;
        $inserisco_centro->salva();
    }
}*/

//$selziona_catalogo = Cataloghi::seleziona_tutti();
//$selziona_nazione = Nazioni::seleziona_tutti();
$utente_ses = Utente::seleziona_per_id($_SESSION['id_utente']);
//$sel_car_centr = caratCentro::seleziona_per_campo_custom('id_centro', 0);


?>

<section class="main-content">
    <h1 class="page-header">Modifica Nazione <?php echo isset($_SESSION['ultimo_id']) ?></h1>
    <?php if($messaggio) :?>
        <div class="row">
            <div class="col-md-12">
                <div class="alert <?php echo ($messaggio == "Immagine caricata Correttamente") ? 'alert-success' : 'alert-danger' ?> alert-dismissable">
                    <button type="button" data-dismiss="alert" aria-hidden="true" class="close">×</button>
                    <?php  echo "<b class='dz-success'>" . $messaggio . "</b>"; ?>
                </div>
            </div>
        </div>
    <?php endif; ?>

    <?php if($nazioni->errore): ?>
    <?php foreach ($nazioni->errore as $prendo_errore): ?>
    <div class="row">
        <div class="col-md-12">
            <div class="alert <?php echo ($prendo_errore == "Immagine caricata Correttamente") ? 'alert-success' : 'alert-danger' ?> alert-dismissable">
                <button type="button" data-dismiss="alert" aria-hidden="true" class="close">×</button>
                <?php  echo "<b class='dz-success'>" .$prendo_errore. "</b>"; ?>
            </div>
        </div>
    </div>
    <?php endforeach; ?>
    <?php endif; ?>
    <div class="panel-body">
        <form method="POST" action="" class="form-horizontal" enctype="multipart/form-data" data-parsley-validate>
            <div class="row">
                <div class="col-md-12">
                    <div class="col-md-8">
                        <input type="hidden" name="id_nazione" id="id_nazione" value="<?php echo $nazioni->id; ?>">
                        <div class="form-group">
                            <label for="nome_centro" class="control-label">Titolo Nazione</label>
                            <textarea name="titolo_nazione" id="nome_ritorno" class="form-control" placeholder="Inserisci Titolo Nazione" required><?php echo $nazioni->desc_titolo; ?></textarea>
                        </div>
<!--                        <div class="form-group">-->
<!--                            <label for="nome_centro" class="control-label">Descrizione Nazione</label>-->
<!--                            <textarea name="desc_naz" id="desc_naz" class="form-control"  placeholder="Inserisci Descrizione Nazione" required>--><?php //echo $nazioni->descrizione; ?><!--</textarea>-->
<!--                        </div>-->
                        <div class="form-group">
                            <label for="titolo-box-1" class="control-label">Titolo Box 1</label>
                            <input type="text" id="titolo-box-1" class="form-control" name="titolo-box-1" value="<?php echo $nazioni->titBox1; ?>"/>
                        </div>
                        <div class="form-group">
                            <label for="box-1" class="control-label">Box 1</label>
                            <textarea id="box-1" name="box-1"><?php echo $nazioni->box1; ?></textarea>
                        </div>
                        <div class="form-group">
                            <label for="titolo-box-2" class="control-label">Titolo Box 2</label>
                            <input type="text" id="titolo-box-2" class="form-control" name="titolo-box-2" value="<?php echo $nazioni->titBox2; ?>"/>
                        </div>
                        <div class="form-group">
                            <label for="box-2" class="control-label">Box 2</label>
                            <textarea id="box-2" name="box-2"><?php echo $nazioni->box2; ?></textarea>
                        </div>
                        <div class="form-group">
                            <label for="nome_centro" class="control-label">Titolo Box Nazioni</label>
                            <input type="text" name="titolo_box_nazioni" id="titolo_box_nazioni" class="form-control" value="<?php echo $nazioni->titolo_desc;  ?>" placeholder="Inserisci Titolo Box Nazioni" required>
                        </div>
<!--                        <div class="form-group">-->
<!--                            <label for="url" class="control-label">Nazioni URL</label>-->
<!--                            <input type="url" name="url" id="url" class="form-control" value="--><?php //echo $nazioni->nazioneUrl; ?><!--" placeholder="Inserisci Url" required>-->
<!--                        </div>-->





                    </div>
                    <div class="pull-right col-md-4">
                        <div class="panel panel-default">
                            <div class="panel-heading"><i style="color: black;" class=""></i> Pubblica</div>
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-md-10">
                                        <div class="col-md-8">
                                            <p><i class="fa fa-hand-o-right"></i> Stato: <b><?php echo ucfirst($nazioni->stato); ?></b></p>
                                        </div>
                                        <div class="col-md-2 pull-right">
                                            <button name="aggiorna_nazione" class="btn btn-oval btn-info">Aggiorna Nazione</button>
                                        </div>
                                    </div>
                                </div>

                            </div> <!-- panel-body -->
                        </div> <!-- panel panel-default -->


                        <div class="panel panel-default">
                            <div class="panel-heading"><i style="color: #009ab8" class="fa fa-users"></i> Autore</div>
                            <div class="panel-body">
                                <!-- <label>Utente</label> -->

                                <select name="autore" class="form-control">
                                    <?php if($utente_ses->ruolo == 'admin'): ?>
                                        <?php $utenti = Utente::seleziona_tutti(); ?>
                                        <?php foreach($utenti as $utente): ?>
                                            <option <?php echo ($utente->username == $utente_ses->username)? 'selected="selected"' : ""?> ><?php echo $utente->username; ?></option>
                                        <?php endforeach; ?>
                                    <?php endif; ?>
                                    <?php if($utente_ses->ruolo != 'admin'): ?>
                                        <option><?php echo $utente_ses->username; ?></option>
                                    <?php endif; ?>
                                </select>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading"><i style="color: red;" class="fa fa-exclamation-triangle"></i> Stato</div>
                            <div class="panel-body">
                                <!-- <label>Stato</label> -->
                                <select name="stato" class="form-control" required>
                                    <option <?php echo ($nazioni->stato == "pubblicato" )? 'selected ="selected" ' : "";  ?> value="pubblicato">Pubblicato</option>
                                    <option  <?php echo ($nazioni->stato == "bozza" )? 'selected ="selected" ' : "";  ?> value="bozza">Bozza</option>
                                </select>
                            </div> <!-- panel-body -->
                        </div> <!-- panel panel-default -->


                        <div class="panel panel-default">
                            <div class="panel-heading"><i class="fa fa-folder-o"></i> Categorie</div>
                            <div class="panel-body">
                                <select name="categorie" id="catAjax" class="form-control" required>
                                    <?php $pagine = CategoriePagine::seleziona_tutti(); ?>
                                    <option value="">Seleziona Categoria</option>
                                    <?php foreach($pagine as $chiave => $pagina): ?>
                                        <option <?php echo ($nazioni->id_categoria == $pagina->id )? 'selected ="selected" ' : "";  ?> value="<?php echo $pagina->id; ?>"><?php echo $pagina->titolo_categoria; ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                        </div>


                        <div class="panel panel-default">
                            <div class="panel-heading"><i style="color: #1e8449;" class="fa fa-globe"></i> Seleziona Nazione</div>
                            <div class="panel-body">
                                <select name="nazione" id="nazione" class="form-control" required>
                                    <option value="">Seleziona Nazione</option>
                                    <?php foreach ($selziona_nazione as $nazione): ?>
                                        <?php if($nazione->stato == 1): ?>
                                            <option <?php echo ($nazione->id == $nazioni->id_nazione )? 'selected ="selected" ' : ""; ?> value="<?php echo $nazione->id?>"><?php echo $nazione->nazione?></option>
                                        <?php endif; ?>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                        </div>


                        <div class="panel panel-default">
                            <div class="panel-heading">Immagine in Evidenza</div>
                            <div class="panel-body">
                                <div class="text-center form-group">
                                    <img class="img-thumbnail img-responsive" style="width: 450px; height: 350px;" src="<?php echo $nazioni->percorso_immagine_e_plecholder(); ?>" alt="<?php echo $nazioni->image; ?>" />
                                    <br>
                                </div>
                                <div class="col-sm-12 form-group">
                                    <input type="file" name="file" class="form-control">
                                </div>
                            </div>
                        </div>

                        <div class="panel panel-default">
                            <div class="panel-heading"><i style="color: #e80079" class="fa fa-picture-o"></i> <b>Image Alt</b></div>
                            <div class="panel-body">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <div class="col-sm-12">
                                            <input maxlength="80"  value="<?php echo $nazioni->img_alt; ?>" name="img_alt" type="text" class="form-control">
                                            <div id="meta_title_caratteri"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="panel panel-default">
                            <div class="panel-heading"><i style="color: #e80079" class="fa fa-picture-o"></i> <b>Image Title</b></div>
                            <div class="panel-body">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <div class="col-sm-12">
                                            <input maxlength="80"  value="<?php echo $nazioni->img_title; ?>" name="img_title" type="text" class="form-control">
                                            <div id="meta_title_caratteri"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="panel panel-default">
                            <div class="panel-heading"><i style="color: #e80079" class="fa fa-header"></i> <b>Meta Title</b></div>
                            <div class="panel-body">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <div class="col-sm-12">
                                            <input maxlength="80" onkeyup="contaCaratteriTitle(this)" value="<?php echo $nazioni->metaTitle; ?>" name="meta_title" type="text" class="form-control">
                                            <div id="meta_title_caratteri"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading"><i style="color: #CBE86B" class="fa fa fa-pencil"></i> <b>Meta Description</b></div>
                            <div class="panel-body">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <div class="col-sm-12">
                                            <input maxlength="160" onkeyup="contaCaratteriDescrizione(this)" value="<?php echo $nazioni->metaDescription; ?>" name="meta_description" type="text" class="form-control">
                                            <div id="meta_descrizione_caratteri"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading"><i style="color: #0A246A" class="fa fa-globe"></i> <b>Canonical Url</b></div>
                            <div class="panel-body">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <div class="col-sm-12">
                                            <input name="canonical" type="url" value="<?php echo $nazioni->canonicalUrl; ?>" class="form-control">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading"><i style="color: #8e79e8 class="fa fa-indent"></i> <b>Meta Robots Index</b></div>
                            <div class="panel-body">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <div class="col-sm-12">
                                            <select name="selezione_index" id="selezione_index" class="form-control">
                                                <option <?php echo ($nazioni->metaIndex =='index')? 'selected="selected" ' : "" ?> value="index">Index</option>
                                                <option <?php echo ($nazioni->metaIndex == 'noindex')? 'selected="selected" ' : "" ?> value="noindex">NoIndex</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>


                    </div> <!-- pull-right col-md-4 -->
                </div> <!-- col-md-12 -->
            </div>  <!-- row -->
        </form>
    </div> <!-- panel-body -->



</section>



<?php include 'includes/footer.php'; ?>


<script type="text/javascript" xmlns="http://www.w3.org/1999/html">
    tinymce.init({
        //selector: '#desc_carat',

        selector: "textarea",
        setup: function (editor) {
            editor.on('change', function () {
                editor.save();
            });
            editor.addButton('classe', {
                text: 'Classe Personalizzata',
                icon: false,
                onclick: function () {
//                    editor.getNode()({title : 'Classe Personalizzata', selector : 'h1', classes : 'titolo-pagina-catalogo'});
                    tinyMCE.activeEditor.dom.addClass(tinyMCE.activeEditor.selection.getNode(), 'titolo-pagina-catalogo');
                }
            })
        },
        theme: 'modern',
//        width: 100%,
        height: 200,
        language: 'it',
        extended_valid_elements : 'span[class]',
        plugins: [
            "advlist autolink lists link image charmap print preview anchor",
            "searchreplace visualblocks code fullscreen",
            "insertdatetime media table contextmenu paste imagetools wordcount"
        ],
        toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | preview" +
        "code | classe",
        images_upload_url: 'upload.php',
        images_upload_base_path: '/admin/immagini',
        media_live_embeds: true,
        style_formats_merge: true,
        menubar: true,
        image_caption: true,
        /*file_browser_callback: function(field_name, url, type, win) {
         if(type=='image') $('#my_form input').click();
         },*/
        image_prepend_url: "http://localhost/mla-cms/admin/immagini/",
        language_url: 'js/tinymce/js/tinymce/langs/it.js' ,
        //preview_styles: true,
        menu: {
            file: {title: 'File', items: 'newdocument'},
            edit: {title: 'Modifica', items: 'undo redo | cut copy paste pastetext | selectall'},
            insert: {title: 'Inserisci', items: 'link media | template hr'},
            view: {title: 'Vedi', items: 'visualaid preview visualblocks'},
            format: {
                title: 'Formato',
                items: 'bold italic underline strikethrough superscript subscript | formats | removeformat'
            },
            table: {title: 'Tabella', items: 'inserttable tableprops deletetable | cell row column'},
            tools: {title: 'Tools', items: 'spellchecker code'},
            style_formats: [
                {
                    title: 'Classe Personalizzata',
                    items:
                        [
                            {
                                title : 'Classe Personalizzata', selector : 'h1', classes : 'titolo-pagina-catalogo'
                            }
                        ]
                }
            ]
        },
        content_css: [
            'css/style.css'],
        style_formats: [
            {
                title: 'Classe Personalizzata',
                items:
                    [
                        {
                            title : 'Classe Personalizzata', selector : 'h1', classes : 'titolo-pagina-catalogo'
                        }
                    ]
            }
        ]
    });



    $('#catAjax').on('change', function () {

        var idCat = $(this).val();
        console.log(idCat);
        $('#nazione option').remove();
        $('#nazione').append($('<option>', {
            value: '',
            text: 'Seleziona Nazione'
        }));
        $('#cittAjax option').remove();
        $('#cittAjax').append($('<option>', {
            value: '',
            text: 'Seleziona Città'
        }));


        $.ajax({
            url: "includes/api.php/nazione_page?&filter=id_categoria,eq," + idCat +"&transform=1",
            type: "GET",
            dataType: 'json',
            headers: {
                'Content-type': 'application/json',
                'Access-Control-Allow-Headers': 'Content-Type',
                'Access-Control-Allow-Origin': '*',
                'X-XSRF-TOKEN': '<?php echo (isset($_COOKIE['XSRF-TOKEN'])) ? $_COOKIE['XSRF-TOKEN'] : ''?>'
            },
            success: function (data) {
                if(data) {
//                    console.log(data);
//                    $('#urlFirendly').val(urlFre);
//                        console.log(data.nazioni);
                    var nazArr = [];

                    $.each(data.nazione_page, function (i, val) {
//                        console.log(i);
//                        console.log(val);
                        var nazid;
                        nazid = val.id_nazione;
//                        console.log(nazid);
                        nazArr.push(nazid);
                    });
                    console.log(nazArr);


                    $.ajax({
                        url: "includes/api.php/nazioni/" + nazArr + "&transform=1",
                        type: "GET",
                        dataType: 'json',
                        headers: {
                            'Content-type': 'application/json',
                            'Access-Control-Allow-Headers': 'Content-Type',
                            'Access-Control-Allow-Origin': '*',
                            'X-XSRF-TOKEN': '<?php echo (isset($_COOKIE['XSRF-TOKEN'])) ? $_COOKIE['XSRF-TOKEN'] : ''?>'
                        },
                        success: function (data) {
//                            console.log(data);

                            if (data.length > 1) {
//
                                $.each(data, function (i, val) {
//                                    console.log(val.nazione);
                                    $('#nazione').append($('<option>', {
                                        value: val.id,
                                        text: val.nazione
                                    }));
                                });
                            } else {
                                $('#nazione').append($('<option>', {
                                    value: data.id,
                                    text: data.nazione
                                }));
                            }
                        }
                    });

                }
            }
        });
    });

    var getUrlParameter = function getUrlParameter(sParam) {
        var sPageURL = decodeURIComponent(window.location.search.substring(1)),
            sURLVariables = sPageURL.split('&'),
            sParameterName,
            i;

        for (i = 0; i < sURLVariables.length; i++) {
            sParameterName = sURLVariables[i].split('=');

            if (sParameterName[0] === sParam) {
                return sParameterName[1] === undefined ? true : sParameterName[1];
            }
        }
    };



    $(document).ready(function () {
        catSel = getUrlParameter('id');
        if(catSel){
            var catId = $('#catAjax').val();

            $('#nazione option').remove();
            $('#nazione').append($('<option>', {
                value: '',
                text: 'Seleziona Nazione'
            }));
//        console.log(catId);

            $.ajax({
                url: "includes/api.php/nazione_page?&filter=id_categoria,eq," + catId +"&transform=1",
                type: "GET",
                dataType: 'json',
                headers: {
                    'Content-type': 'application/json',
                    'Access-Control-Allow-Headers': 'Content-Type',
                    'Access-Control-Allow-Origin': '*',
                    'X-XSRF-TOKEN': '<?php echo (isset($_COOKIE['XSRF-TOKEN'])) ? $_COOKIE['XSRF-TOKEN'] : ''?>'
                },
                success: function (data) {
                    if(data) {
//                    console.log(data);
//                    $('#urlFirendly').val(urlFre);
//                        console.log(data.nazioni);
                        var nazArr = [];

                        $.each(data.nazione_page, function (i, val) {
//                        console.log(i);
//                        console.log(val);
                            var nazid;
                            nazid = val.id_nazione;
//                        console.log(nazid);
                            nazArr.push(nazid);
                        });



                        $.ajax({
                            url: "includes/api.php/nazioni/" + nazArr + "&transform=1",
                            type: "GET",
                            dataType: 'json',
                            headers: {
                                'Content-type': 'application/json',
                                'Access-Control-Allow-Headers': 'Content-Type',
                                'Access-Control-Allow-Origin': '*',
                                'X-XSRF-TOKEN': '<?php echo (isset($_COOKIE['XSRF-TOKEN'])) ? $_COOKIE['XSRF-TOKEN'] : ''?>'
                            },
                            success: function (data) {
//                            console.log(data);

                                if (data.length > 1) {
//

                                    $.each(data, function (i, val) {
//                                        console.log(val.nazione);
                                        $('#nazione')
                                            .append($('<option>')
                                                .attr('id', val.id)
                                                .attr('value', val.id)
                                                .text(val.nazione));
                                    });
                                } else {
                                    $('#nazione')
                                        .append($('<option>')
                                            .attr('id', data.id)
                                            .attr('value', data.id)
                                            .text(data.nazione));
                                }
                            }
                        });

                    }

                }
            });
        }

        var nazOp=[];
        var idData = <?php echo $nazioni->id_nazione; ?>;
        console.log(idData);
        var refreshId = setTimeout( function()
        {
            var length = $('#nazione option').length;
            $('#'+idData).attr("selected", true);
        }, 1000);

    });
</script>
