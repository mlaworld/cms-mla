<?php include 'includes/init.php'; ?>

<?php if(!$sessione->loggato()){ redirect('accedi.php');} ?>

<?php

if(empty((int)$_GET['id'])){
    redirect('utenti.php');
}

$utente = Utente::seleziona_per_id($_GET['id']);

if($utente){
    $sessione->messaggio('<div data-toggle="notify" data-onload data-message="Utente <b>Eliminato</b> Correttamente" data-options="{&quot;status&quot;:&quot;success&quot;, &quot;pos&quot;:&quot;bottom-right&quot;}" class="hidden-xs"></div>');
    $utente->cancella();
    redirect("utenti.php");
} else {
    redirect("utenti.php");
}
