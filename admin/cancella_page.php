<?php
/**
 * Created by Giuseppe Alessandro De Blasio.
 * User: Giuseppe Alessandro De Blasio
 * Date: 27/09/2016
 * Time: 18:21
 */
?>

<?php include 'includes/init.php'; ?>

<?php if(!$sessione->loggato()){ redirect('accedi.php');} ?>

<?php

if(empty($_GET['id'])){
    redirect('elenco_page.php');
}

$pagine = Page::seleziona_per_id($_GET['id']);

if($pagine){
    $sessione->messaggio('<div data-toggle="notify" data-onload data-message="Pagina <b>Eliminato</b> Correttamente" data-options="{&quot;status&quot;:&quot;success&quot;, &quot;pos&quot;:&quot;bottom-right&quot;}" class="hidden-xs"></div>');
    $pagine->cancella();
    redirect("elenco_page.php");
} else {
    redirect("elenco_page.php");
}