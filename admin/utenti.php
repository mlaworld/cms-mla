<?php include 'includes/header.php'; ?>

<?php if(!$sessione->loggato()){redirect("accedi.php"); } ?>

<?php  $utente = Utente::seleziona_per_id($_SESSION['id_utente']); $utente->ruolo == "admin" ? NULL : redirect("index.php") ;  ?>

<?php $utenti = Utente::seleziona_tutti(); ?>


<?php
    
    $pagina = !empty($_GET['pagina']) ? (int)$_GET['pagina'] : 1;
    
    $elementi_per_pagina = 10;
    
    $elementi_totali = Utente::conta_elementi();
    
    $paginazione = new Paginazione($pagina, $elementi_per_pagina, $elementi_totali);
    
    $sql = "SELECT * FROM utenti ";
    $sql.= "LIMIT {$elementi_per_pagina} ";
    $sql.= "OFFSET {$paginazione->offset()}";
    $utenti = Utente::cerca_con_query($sql);

?>
        <!-- Barra di Navigazione -->
        <?php include ("includes/navbar.php") ?>
        
        <?php include ("includes/aside.php") ?>
        <!-- End aside-->
      
        <?php include ("includes/aside_utenti.php") ?>
        
<?php


    if(isset($_POST['applica'])){

        if($_POST['azione'] == "Cancella" ){

//            echo "<script type='text/javascript'>confirm('Sei sicuro di voler eliminare questo campo?');</script>";


            if(!empty($_POST['checkbox_utenti'])){

                    foreach($_POST['checkbox_utenti'] as $utente->id ) {

                        global $database;

                        $query = "SELECT ruolo FROM utenti WHERE id='{$utente->id}' ";

                        $result = mysqli_query($database->conn, $query);

                        while ($row = mysqli_fetch_assoc($result)) {

                            $ruolo_opzione = $row['ruolo'];

                        }

                        if($ruolo_opzione == 'admin') {

                                if ($utente->ultimoAdmin($ruolo_opzione) == TRUE){
                                    echo "<script type='text/javascript'>alert('Impossibile eliminare l\'ultimo Admin')</script>";
                                    break;
                                }
                        }
                        echo "<script type='text/javascript'>confirm('Sei sicuro di voler eliminare questo campo?');</script>";
                        redirect("elimina_utente.php?id={$utente->id}");
                        $sessione->messaggio('<div data-toggle="notify" data-onload data-message="L\'utente è stato <b>Eliminato</b> Correttamente" data-options="{&quot;status&quot;:&quot;success&quot;, &quot;pos&quot;:&quot;bottom-right&quot;}" class="hidden-xs"></div>');
                    }
            }

        }

    }


//if(isset($_POST['attivo_utente'])){
//
//    foreach($_POST['attivo_utente'] as $utente->id ) {
//
//        $attivo = new Utente();
//
//        global $database;
//
//        $query = "SELECT attivo FROM utenti WHERE id='{$utente->id}' ";
//
//        $result = mysqli_query($database->conn, $query);
//
//        while ($row = mysqli_fetch_array($result)) {
//
//            echo $attivo_opzione = $row['attivo'];
//
//        }
//
////        $attivo->attivoUser($utente->id, 1);
//
////        if($attivo_opzione == 0){
////            $attivo->attivoUser($utente->id, 1);
////        } else {
////            $attivo->attivoUser($utente->id, 0);
////        }
//
//    }
//
//}
    
    if(isset($_POST['cerca_utente'])){
        $cerca = $_POST['cerca'];
        if(!empty($cerca)){
            $sql = "SELECT * FROM utenti ";
            $sql.= " WHERE username LIKE '%$cerca%' ";
            $utenti = Utente::cerca_con_query($sql);
        } 
    }

?>      
<script>
    function attivo_user(utenteId, utenteValore)
    {

        $.ajax({
            url: "aggiorna_attivo.php",
            type: "GET",
            data: { 'utenteAttivo': utenteId, 'utenteValore': utenteValore},
            success: function(data)
            {
                console.log(utenteValore);
                if(utenteValore == 1){
                    console.log('utente attivato');
                } else{
                    console.log('utente disattivato');
                }
                alert("modifica effettuata!!");

            },
            error: function () {
              console.log("Impossibile dialogare");   
            }
        });
    }

</script>
    <br>
    <section class="main-content">
        <div class="panel panel-default">
               <div class="panel-heading">Utenti Registrati
                  <!--<a title="" class="pull-right" href="javascript:void(0);" data-original-title="Close Panel" data-toggle="tooltip" data-perform="panel-dismiss">
                     <em class="fa fa-times"></em>
                  </a>-->
                  <a title="" class="pull-right" href="javascript:void(0);" data-original-title="Collapse Panel" data-toggle="tooltip" data-perform="panel-collapse">
                     <em class="fa fa-minus"></em>
                  </a>
               </div>
               <!-- START table-responsive-->
               <form action="" method="POST"> 
               <div class="panel-wrapper collapse in" style="height: auto;"><div class="table-responsive">
                  <table class="table table-bordered table-hover">
                     <thead>
                        <tr>
                           <th style="width: 3%">ID</th>
                           <th style="width: 5%">Immagine</th>
                           <th>Username</th>
                           <th>Nome</th>
                           <th>Cognome</th>
                           <th>Email</th>
                           <th>Ruolo</th>
                           <th>Attivo?</th>
                           <!--<th style="width: 5%">Profilo</th>-->
                           <th>Ultimo Accesso</th>
                           <th>Data Registrazione</th>
                           <th class="check-all" style="width: 5%">
                              <div title=""  class="checkbox c-checkbox" data-original-title="" data-toggle="tooltip" data-title="Check All">
                                 <label>
                                    <input type="checkbox">
                                    <span class="fa fa-check"></span>
                                 </label>
                              </div>
                           </th>
                        </tr>
                     </thead>
                     <tbody>
                         
                         <?php foreach ($utenti as $utente): ?>
                        <tr>
                           <td><?php echo $utente->id; ?></td>
                           <td>
                              <div class="media">
                                 <img class="img-responsive img-circle" alt="<?php echo $utente->immagine_utente; ?>" src="<?php echo $utente->percorso_immagine_e_plecholder(); ?>">
                              </div>
                           </td>
                           <td><?php echo $utente->username; ?></td>
                           <td><?php echo $utente->nome; ?></td>
                           <td><?php echo $utente->cognome; ?></td>
                           <td><?php echo $utente->email; ?></td>
                           <td><?php
                           
                         switch ($utente->ruolo) {
                            case "admin":
                                echo '<div class="label label-danger pull-center" value="'.$utente->ruolo.'">'. $utente->ruolo .'</div><input type="hidden" name="ruolo_admin" value="'.$utente->ruolo.'" >';
                                break;
                            case "sottoscrittore":
                                echo '<div class="label label-success pull-center" value="'.$utente->ruolo.'">'.$utente->ruolo.'</div><input type="hidden" value="'.$utente->ruolo.'" name="ruolo_sottoscrittore">';
                                break;
                            case "editor":
                                echo '<div class="label label-info pull-center" value="'.$utente->ruolo.'">'.$utente->ruolo.'</div><input type="hidden" value="'.$utente->ruolo.'" name="ruolo_editor">';
                                break;
                            case "moderatore":
                                echo '<div class="label label-warning pull-center" value="'.$utente->ruolo.'">'.$utente->ruolo.'</div><input type="hidden" value="'.$utente->ruolo.'" name="ruolo_moderatore">';
                                break;
                             default:
                                echo '<div class="label label-success pull-center" value="'.$utente->ruolo.'">'.$utente->ruolo.'</div><input type="hidden" value="'.$utente->ruolo.'" name="ruolo_default">';
                                break;
                         }
                           
                           ?></td>
                           <td>
                           <?php if ($utente->attivo == 1) {
                               
                              echo "<label class='switch text-center'>
                                    <input type='checkbox' name='attivo_utente[]' checked='' onclick='attivo_user({$utente->id}, {$utente->attivo})' value='{$utente->id}'>
                                        <span></span>
                                    </label>";
                           } else {
                               echo "<label class='switch text-center'>
                                    <input type='checkbox' name='attivo_utente[]'  onclick='attivo_user({$utente->id}, {$utente->attivo})' value='{$utente->id}'>
                                        <span></span>
                                    </label>";
                           }
                   
                           
                           ?>
                           </td>
                           <!-- <td class="text-center">
                              <div class="radial-bar radial-bar-25 radial-bar-xs" data-label="25%"></div>
                           </td> -->
                           <td><?php echo datatime_it($utente->ultimo_accesso); ?></td>
                           <td><?php echo data_it($utente->data_registrazione); ?></td>
                           <td>
                              <div class="checkbox c-checkbox">
                                 <label>
                                     <input name="checkbox_utenti[]" type="checkbox" value="<?php echo $utente->id; ?>">
                                    <span class="fa fa-check"></span>
                                 </label>
                              </div>
                           </td>
                        </tr>
                        <?php endforeach; ?>
                        

                     </tbody>
                  </table>
               </div>
                   
                 <div class="panel-footer">
                  <div class="row">
                     <div class="col-lg-2">
                        <div class="input-group">
                           <input class="input-sm form-control" type="text" name="cerca" id="cerca" placeholder="Cerca">
                           <span class="input-group-btn">
                               <button class="btn btn-sm btn-default" name="cerca_utente" id="cerca_utente" type="submit">Cerca</button>
                           </span>
                        </div>
                     </div>
                      <div class="col-lg-8">
                              <ul class="pagination pull-right">
                                  <?php
                                  if($paginazione->pagine_totali() > 1){
                                      if($paginazione->e_prima()){
                                          echo "<li><a href='utenti.php?pagina={$paginazione->prima()}'>Prima</a></li>";
                                      }
                                      for ($i = 1; $i <= $paginazione->pagine_totali(); $i++){
                                          if($i == $paginazione->pagina_corrente){
                                              echo "<li class='active'><a href='utenti.php?pagina={$i}'>{$i}</a></li>";
                                          } else {
                                              echo "<li><a href='utenti.php?pagina={$i}'>{$i}</a></li>";
                                          }
                                      }

                                      if($paginazione->e_dopo()){ 
                                          echo "<li><a href='utenti.php?pagina={$paginazione->dopo()}'>Dopo</a></li>";
                                      }                                      
                                  }
                                  
                                  ?>
                              </ul>
                      </div>
                     <div class="col-lg-2">
                        <div class="input-group pull-right">
                            <?php $azioni = array(
                                0 => "Azione",
                                1 => "Cancella",
                                2 => "Modifica"
                            ); ?> 
                           <select name="azione" class="input-sm form-control">
                               <?php foreach ($azioni as $key => $azione): ?>
                               <option <?php //if(isset($_POST['applica']) && $azione == 'Cancella') { echo 'class="cancella_messaggio" ';}?> value="<?php echo $azione ?>"><?php echo $azione ?></option>
                               <?php endforeach; ?>
                           </select>
                           <span class="input-group-btn">
                              <button name="applica" class="btn btn-sm btn-default">Applica</button>
                           </span>
                        </div>
                     </div>
                  </div>
               </div>
             </form>  
               </div>
               <!-- END table-responsive-->
            </div>
                    
        </section>       
    <?php include ("includes/footer.php") ?>
               
               