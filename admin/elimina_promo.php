<?php
/**
 * Created da Giuseppe Alessandro De Blasio.
 * User: Giuseppe Alessandro De Blasio
 * Date: 13/03/17
 * Time: 10:45
 */
?>

<?php include 'includes/init.php'; ?>

<?php if(!$sessione->loggato()){ redirect('accedi.php');} ?>

<?php

if(empty($_GET['id'])){
    redirect('elenco_promo.php');
}

$elimina_promo = Promo::seleziona_per_id($_GET['id']);

if($elimina_promo){
    $elimina_promo->cancella();
    redirect('elenco_promo.php');
}
