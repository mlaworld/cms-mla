<?php
/**
 * Created da Giuseppe Alessandro De Blasio.
 * User: Giuseppe Alessandro De Blasio
 * Date: 20/10/16
 * Time: 10:15
 */

?>

<?php include 'includes/header.php'; ?>
<?php if(!$sessione->loggato()){ redirect('accedi.php');} ?>

<!-- Barra di Navigazione -->
<?php include ("includes/navbar.php") ?>

<?php include ("includes/aside.php") ?>
<!-- End aside-->
<?php


if(empty($_GET['id'])){
    redirect('elenco_nazioni.php');
}

$seleziona_nazione = Nazioni::seleziona_tutti();

$selziona_centro = Centro::seleziona_tutti();

$selziona_citta= CittaPage::seleziona_tutti();
$utente_ses = Utente::seleziona_per_id($_SESSION['id_utente']);

$citta = CittaPage::seleziona_per_id($_GET['id']);

if(isset($_POST['aggiorna_citta'])){

    //$aggiungo_centro = new Centro();
    //$aggiungo_nazione = NazioniPage::seleziona_per_id($_POST['id_centro']);
    

    if($citta){

        $citta->nome_citta = $_POST['nome_citta'];
        $citta->desc_titolo = $_POST['titolo_citta'];
        $urlRewrite = rewriteUrl(strtolower(trim($citta->nome_citta)));
        $citta->urlFriendly = $urlRewrite;
//        $citta->descrizione = $_POST['desc_naz'];
//        $citta->titolo_desc = $_POST['titolo_box_nazioni'];
        $citta->titBox1 = $_POST['titolo-box-1'];
        $citta->box1 = $_POST['box-1'];
        $citta->titBox2 = $_POST['titolo-box-2'];
        $citta->box2 = $_POST['box-2'];
        $cat = CategoriePagine::seleziona_per_id($_POST['categorie']);
        $categoria = $cat->urlFriendly;
        $naz = Nazioni::seleziona_per_id($_POST['nazione']);
        $nazione = $naz->urlFriendly;
        $citta->cittaUrl = "/".$categoria."/".$nazione."/". $urlRewrite;
        $citta->stato = $_POST['stato'];
        $citta->id_nazione = $_POST['nazione'];
        $citta->id_categoria = $_POST['categorie'];
        $citta->img_alt = $_POST['img_alt'];
        $citta->img_title = $_POST['img_title'];
        $citta->metaDescription = $_POST['meta_description'];
        $citta->metaTitle = $_POST['meta_title'];
        $citta->canonicalUrl = $_POST['canonical'];
        $citta->metaIndex = $_POST['selezione_index'];
        $citta->nessun_testo = $_POST['testoNullo'];
        $citta->urlRedirect = "/".$categoria."/".$nazione."/".$citta->urlFriendly."/".$_POST['centroRedirect'];
        $citta->impostazione_file_citta($_FILES['file']);
        //var_dump($citta->impostazione_file_citta($_FILES['file']));
        $citta->caricamento_foto_citta();
//        $citta->salva();
        $citta->esiste_nazioni_categoria_modifica("{$_POST['categorie']}", "{$_POST['nazione']}", "{$_POST['id_citta']}");
//        $msg_instant->success('Città <b>modificata</b> correttamente');
//        $msg_instant->display();
       //redirect('elenco_centri.php');
    }
}

/*$messaggio = "";

if(isset($_FILES['file'])){

    $immagine = new Image();
    $immagine->impostazione_file_foto($_FILES['file']);
    $immagine->data_immagine = date("Y-m-d");
    if($immagine->salva_immagine_post()){

        $messaggio = "Immagine caricata Correttamente";
    } else
    {
        $messaggio = join("<br>", $immagine->errore);
    }

}*/

//$inserisco_centro = new Centro();
//$inserisco_carat = new caratCentro();

/*if(isset($_POST['ins_nuovo_centr'])){
    if($inserisco_centro) {
        $inserisco_centro->Centro = $_POST['nome_nuovo_centro'];
    }
}
$ultimo_id = $inserisco_centro->id = $_SESSION['ultimo_id'];

if(isset($_POST['crea_pagina'])){
    if($inserisco_centro) {
        $inserisco_centro->Centro = $_POST['nome_centro'];
        $inserisco_carat->id_centro = $inserisco_centro->id;
        $inserisco_centro->salva();
    }
}*/

//$selziona_catalogo = Cataloghi::seleziona_tutti();
//$selziona_nazione = Nazioni::seleziona_tutti();
$utente_ses = Utente::seleziona_per_id($_SESSION['id_utente']);
//$sel_car_centr = caratCentro::seleziona_per_campo_custom('id_centro', 0);


?>


<!--suppress ALL -->
<section class="main-content">
    <h1 class="page-header">Modifica Città <?php echo isset($_SESSION['ultimo_id']) ?></h1>
    <?php if($messaggio) :?>
        <div class="row">
            <div class="col-md-12">
                <div class="alert <?php echo ($messaggio == "Immagine caricata Correttamente") ? 'alert-success' : 'alert-danger' ?> alert-dismissable">
                    <button type="button" data-dismiss="alert" aria-hidden="true" class="close">×</button>
                    <?php  echo "<b class='dz-success'>" . $messaggio . "</b>"; ?>
                </div>
            </div>
        </div>
    <?php endif; ?>

    <?php if($citta->errore): ?>
        <?php foreach ($citta->errore as $prendo_errore): ?>
            <div class="row">
                <div class="col-md-12">
                    <div class="alert <?php echo ($prendo_errore == "Immagine caricata Correttamente") ? 'alert-success' : 'alert-danger' ?> alert-dismissable">
                        <button type="button" data-dismiss="alert" aria-hidden="true" class="close">×</button>
                        <?php  echo "<b class='dz-success'>" .$prendo_errore. "</b>"; ?>
                    </div>
                </div>
            </div>
        <?php endforeach; ?>
    <?php endif; ?>

    <div class="panel-body">
        <form method="POST" action="" class="form-horizontal" enctype="multipart/form-data" data-parsley-validate>
            <div class="row">
                <div class="col-md-12">
                    <div class="col-md-8">

                        <?php if($citta->nessun_testo): ?>
                            <div id="messNotifica" class="alert alert-info"><strong>ATTENZIONE!</strong> Su questa città c'è il redirect al seguente link: <b><?php echo $citta->urlRedirect; ?></b>.</div>
                        <?php endif; ?>

                        <input type="hidden" name="id_citta" id="id_citta" value="<?php echo $citta->id; ?>">
                        <div class="form-group">
                            <label for="nome_centro" class="control-label">Nome Città</label>
                            <input type="text" name="nome_citta" id="nome_citta" class="form-control" value="<?php echo $citta->nome_citta; ?>" placeholder="Inserisci Nome Città" required />
                        </div>
                        <div class="form-group">
                            <label for="nome_centro" class="control-label">Titolo Pagina Città</label>
                            <textarea type="text" name="titolo_citta" id="nome_ritorno" class="form-control" placeholder="Inserisci Nome Città" required><?php echo $citta->desc_titolo; ?></textarea>
                        </div>
                        <div class="form-group">
                            <label for="titolo-box-1" class="control-label">Titolo Box 1</label>
                            <input type="text" id="titolo-box-1" class="form-control" name="titolo-box-1" value="<?php echo $citta->titBox1; ?>"/>
                        </div>
                        <div class="form-group">
                            <label for="box-1" class="control-label">Box 1</label>
                            <textarea id="box-1" name="box-1"><?php echo $citta->box1; ?></textarea>
                        </div>
                        <div class="form-group">
                            <label for="titolo-box-2" class="control-label">Titolo Box 2</label>
                            <input type="text" id="titolo-box-2" class="form-control" name="titolo-box-2" value="<?php echo $citta->titBox2; ?>"/>
                        </div>
                        <div class="form-group">
                            <label for="box-2" class="control-label">Box 2</label>
                            <textarea id="box-2" name="box-2"><?php echo $citta->box2; ?></textarea>
                        </div>
<!--                        <div class="form-group">-->
<!--                        <div class="form-group">-->
<!--                            <label for="nome_centro" class="control-label">Descrizione Nazione</label>-->
<!--                            <textarea name="desc_naz" id="desc_naz" class="form-control"  placeholder="Inserisci Descrizione Nazione" required>--><?php //echo $citta->descrizione; ?><!--</textarea>-->
<!--                        </div>-->
<!--                        <div class="form-group">-->
<!--                            <label for="nome_centro" class="control-label">Titolo Box Nazioni</label>-->
<!--                            <input type="text" name="titolo_box_nazioni" id="titolo_box_nazioni" class="form-control" value="--><?php //echo $citta->titolo_desc;  ?><!--" placeholder="Inserisci Titolo Box Nazioni" required>-->
<!--                        </div>-->
                        <div class="form-group">
                            <label for="url" class="control-label">Città URL</label>
                            <input type="text" id="url" class="form-control" value="<?php echo ($citta->nessun_testo)? $citta->urlRedirect :$citta->cittaUrl; ?>" placeholder="Inserisci Url" disabled><!-- name="url" -->
                        </div>

                        <div class="form-group">
                            <div class="checkbox c-checkbox">
                                <label>
                                    <input type="checkbox" name="testoNullo" id="testoNullo" value="" <?php echo ($citta->nessun_testo)? 'checked' : ''; ?> >
                                    <span class="fa fa-check"></span>Selezionare se la seguete città non ha la descrizione</label>
                            </div>
                        </div>


                    <div class="form-group">
                        <select name="centroRedirect" class="form-control" id="centroAjax" style="display: none;">
                            <option value="">Seleziona il Centro sul quale si vuole effettuare il redirect</option>
                        </select>
                    </div>
                        
                        <?php if($citta->nessun_testo): ?>
                            <div id="messNotifica" class="alert alert-info"><strong>ATTENZIONE!</strong> Su questa città c'è il redirect al seguente link: <b><?php echo $citta->urlRedirect; ?></b>.</div>
                        <?php endif; ?>

                </div>

                    <div class="pull-right col-md-4">
                        <div class="panel panel-default">
                            <div class="panel-heading"><i style="color: black;" class=""></i> Pubblica</div>
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-md-10">
                                        <div class="col-md-8">
                                            <p><i class="fa fa-hand-o-right"></i> Stato: <b><?php echo ucfirst($citta->stato); ?></b></p>
                                        </div>
                                        <div class="col-md-2 pull-right">
                                            <button name="aggiorna_citta" class="btn btn-oval btn-info">Aggiorna Citta</button>
                                        </div>
                                    </div>
                                </div>

                            </div> <!-- panel-body -->
                        </div> <!-- panel panel-default -->


                        <div class="panel panel-default">
                            <div class="panel-heading"><i style="color: #009ab8" class="fa fa-users"></i> Autore</div>
                            <div class="panel-body">
                                <!-- <label>Utente</label> -->

                                <select name="autore" class="form-control">
                                    <?php if($utente_ses->ruolo == 'admin'): ?>
                                        <?php $utenti = Utente::seleziona_tutti(); ?>
                                        <?php foreach($utenti as $utente): ?>
                                            <option <?php echo ($utente->username == $utente_ses->username)? 'selected="selected"' : ""?> ><?php echo $utente->username; ?></option>
                                        <?php endforeach; ?>
                                    <?php endif; ?>
                                    <?php if($utente_ses->ruolo != 'admin'): ?>
                                        <option><?php echo $utente_ses->username; ?></option>
                                    <?php endif; ?>
                                </select>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading"><i style="color: red;" class="fa fa-exclamation-triangle"></i> Stato</div>
                            <div class="panel-body">
                                <!-- <label>Stato</label> -->
                                <select name="stato" class="form-control" required>
                                    <option <?php echo ($citta->stato == "pubblicato" )? 'selected ="selected" ' : "";  ?> value="pubblicato">Pubblicato</option>
                                    <option  <?php echo ($citta->stato == "bozza" )? 'selected ="selected" ' : "";  ?> value="bozza">Bozza</option>
                                </select>
                            </div> <!-- panel-body -->
                        </div> <!-- panel panel-default -->

                        <!--<div class="panel panel-default">
                            <div class="panel-heading"><i style="color: #1e8449;" class="fa fa-globe"></i> Seleziona Centro</div>
                            <div class="panel-body">
                                <select name="centro" class="form-control" required>
                                    <option value="">Seleziona Centro</option>
                                    <?php //foreach ($seleziona_centro as $centro): ?>
                                        <?php // if($centro->stato == "pubblicato"): ?>
                                            <option value="<?php // echo $centro->id?>"><?php //echo $centro->Centro?></option>
                                        <?php // endif; ?>
                                    <?php // endforeach; ?>
                                </select>
                            </div>
                        </div> -->


                        <div class="panel panel-default">
                            <div class="panel-heading"><i class="fa fa-folder-o"></i> Categorie</div>
                            <div class="panel-body">
                                <select name="categorie" id="catAjax" class="form-control" required>
                                    <?php $pagine = CategoriePagine::seleziona_tutti(); ?>
                                    <option value="">Seleziona Categoria</option>
                                    <?php foreach($pagine as $chiave => $pagina): ?>
                                        <option <?php echo ($pagina->id == $citta->id_categoria )? 'selected ="selected" ' : "";  ?> value="<?php echo $pagina->id; ?>"><?php echo $pagina->titolo_categoria; ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                        </div>


                        <div class="panel panel-default">
                            <div class="panel-heading"><i style="color: #1e8449;" class="fa fa-globe"></i> Seleziona Nazione</div>
                            <div class="panel-body">
                                <select name="nazione" id="nazione" class="form-control" required>
                                    <option value="">Seleziona Nazione</option>
                                    <?php foreach ($seleziona_nazione as $nazione): ?>
                                        <?php  if($nazione->stato == 1): ?>
                                            <option <?php echo ($nazione->id == $citta->id_nazione)? 'selected ="selected" ' : "" ?> value="<?php  echo $nazione->id?>"><?php  echo $nazione->nazione?></option>
                                        <?php  endif; ?>
                                    <?php  endforeach; ?>
                                </select>
                            </div>
                        </div>


                        <div class="panel panel-default">
                            <div class="panel-heading">Immagine in Evidenza</div>
                            <div class="panel-body">
                                <div class="text-center form-group">
                                    <img class="img-thumbnail img-responsive" style="width: 450px; height: 350px;" src="<?php echo $citta->percorso_immagine_e_plecholder(); ?>" alt="<?php echo $citta->image; ?>" />
                                    <br>
                                </div>
                                <div class="col-sm-12 form-group">
                                    <input type="file" name="file" class="form-control">
                                </div>
                                <div class="col-sm-12 btn btn-info"><h4><i class="fa fa-cut"><strong>  &ensp; Larghezza: 400px - Altezza: 267px </strong> </i></h4></div>
                            </div>
                        </div>

                        <div class="panel panel-default">
                            <div class="panel-heading"><i style="color: #e80079" class="fa fa-picture-o"></i> <b>Image Alt</b></div>
                            <div class="panel-body">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <div class="col-sm-12">
                                            <input value="<?php echo $citta->img_alt; ?>" name="img_alt" type="text" class="form-control">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="panel panel-default">
                            <div class="panel-heading"><i style="color: #e80079" class="fa fa-picture-o"></i> <b>Image Title</b></div>
                            <div class="panel-body">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <div class="col-sm-12">
                                            <input value="<?php echo $citta->img_title; ?>" name="img_title" type="text" class="form-control">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="panel panel-default">
                            <div class="panel-heading"><i style="color: #e80079" class="fa fa-header"></i> <b>Meta Title</b></div>
                            <div class="panel-body">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <div class="col-sm-12">
                                            <input maxlength="80" onkeyup="contaCaratteriTitle(this)" value="<?php echo $citta->metaTitle; ?>" name="meta_title" type="text" class="form-control">
                                            <div id="meta_title_caratteri"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading"><i style="color: #CBE86B" class="fa fa fa-pencil"></i> <b>Meta Description</b></div>
                            <div class="panel-body">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <div class="col-sm-12">
                                            <input maxlength="160" onkeyup="contaCaratteriDescrizione(this)" value="<?php echo $citta->metaDescription; ?>" name="meta_description" type="text" class="form-control">
                                            <div id="meta_descrizione_caratteri"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading"><i style="color: #0A246A" class="fa fa-globe"></i> <b>Canonical Url</b></div>
                            <div class="panel-body">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <div class="col-sm-12">
                                            <input name="canonical" type="url" value="<?php echo $citta->canonicalUrl; ?>" class="form-control">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading"><i style="color: #8e79e8 class="fa fa-indent"></i> <b>Meta Robots Index</b></div>
                            <div class="panel-body">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <div class="col-sm-12">
                                            <select name="selezione_index" id="selezione_index" class="form-control">
                                                <option <?php ($citta->metaIndex == 'index')? 'selected="selected" ' : "" ?> value="index">Index</option>
                                                <option <?php ($citta->metaIndex == 'noindex')? 'selected="selected" ' : "" ?> value="noindex">NoIndex</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>


                    </div> <!-- pull-right col-md-4 -->
                </div> <!-- col-md-12 -->
            </div>  <!-- row -->
        </form>
    </div> <!-- panel-body -->



</section>



<?php include 'includes/footer.php'; ?>


<script type="text/javascript" xmlns="http://www.w3.org/1999/html">
    tinymce.init({
        //selector: '#desc_carat',
        selector: "textarea",
        setup: function (editor) {
            editor.on('change', function () {
                editor.save();
            });
        },
        theme: 'modern',
//        width: 1100,
        height: 200,
        language: 'it',
        extended_valid_elements : 'span[class]',
        plugins: [
            "advlist autolink lists link image charmap print preview anchor",
            "searchreplace visualblocks code fullscreen",
            "insertdatetime media table contextmenu paste imagetools wordcount"
        ],
        toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | preview" +
        "code | ",
        images_upload_url: 'upload.php',
        images_upload_base_path: '/admin/immagini',
        media_live_embeds: true,
        style_formats_merge: true,
        menubar: true,
        image_caption: true,
        /*file_browser_callback: function(field_name, url, type, win) {
         if(type=='image') $('#my_form input').click();
         },*/
        image_prepend_url: "http://localhost/mla-cms/admin/immagini/",
        language_url: 'js/tinymce/js/tinymce/langs/it.js' ,
        //preview_styles: true,
        menu: {
            file: {title: 'File', items: 'newdocument'},
            edit: {title: 'Modifica', items: 'undo redo | cut copy paste pastetext | selectall'},
            insert: {title: 'Inserisci', items: 'link media | template hr'},
            view: {title: 'Vedi', items: 'visualaid preview visualblocks'},
            format: {
                title: 'Formato',
                items: 'bold italic underline strikethrough superscript subscript | formats | removeformat'
            },
            table: {title: 'Tabella', items: 'inserttable tableprops deletetable | cell row column'},
            tools: {title: 'Tools', items: 'spellchecker code'},
            style_formats: {title: 'Coustom Menu', items: 'button'}
        },
        style_formats: [
            {title: 'Custom Menu', items: [
                {title : 'Button', selector : 'a', classes : 'button'}
            ]}
        ]
    });



    $('#catAjax').on('change', function () {

        var idCat = $(this).val();
        console.log(idCat);
        $('#nazione option').remove();
        $('#nazione').append($('<option>', {
            value: '',
            text: 'Seleziona Nazione'
        }));
        $('#cittAjax option').remove();
        $('#cittAjax').append($('<option>', {
            value: '',
            text: 'Seleziona Città'
        }));


        $.ajax({
            url: "includes/api.php/nazione_page?&filter=id_categoria,eq," + idCat +"&transform=1",
            type: "GET",
            dataType: 'json',
            headers: {
                'Content-type': 'application/json',
                'Access-Control-Allow-Headers': 'Content-Type',
                'Access-Control-Allow-Origin': '*',
                'X-XSRF-TOKEN': '<?php echo (isset($_COOKIE['XSRF-TOKEN'])) ? $_COOKIE['XSRF-TOKEN'] : ''?>'
            },
            success: function (data) {
                if(data) {
//                    console.log(data);
//                    $('#urlFirendly').val(urlFre);
//                        console.log(data.nazioni);
                    var nazArr = [];

                    $.each(data.nazione_page, function (i, val) {
//                        console.log(i);
//                        console.log(val);
                        var nazid;
                        nazid = val.id_nazione;
//                        console.log(nazid);
                        nazArr.push(nazid);
                    });
                    console.log(nazArr);

//                }

//                    Object.keys(data).map(function(objectKey, index) {
//                        var value = object[objectKey];
//                        console.log(value);
//                    });
//                    $('#catAajx').on('change', function () {

//                    if (typeof nazArr !== 'undefined' && nazArr.length == 0) {
//                        console.log('vuoto');


                    $.ajax({
                        url: "includes/api.php/nazioni/" + nazArr + "&transform=1",
                        type: "GET",
                        dataType: 'json',
                        headers: {
                            'Content-type': 'application/json',
                            'Access-Control-Allow-Headers': 'Content-Type',
                            'Access-Control-Allow-Origin': '*',
                            'X-XSRF-TOKEN': '<?php echo (isset($_COOKIE['XSRF-TOKEN'])) ? $_COOKIE['XSRF-TOKEN'] : ''?>'
                        },
                        success: function (data) {
//                            console.log(data);

                            if (data.length > 1) {
//
                                $.each(data, function (i, val) {
//                                    console.log(val.nazione);
                                    $('#nazione').append($('<option>', {
                                        value: val.id,
                                        text: val.nazione
                                    }));
                                });
                            } else {
                                $('#nazione').append($('<option>', {
                                    value: data.id,
                                    text: data.nazione
                                }));
                            }
                        }
                    });
//                    });
                }

            }
        })

    });

    var getUrlParameter = function getUrlParameter(sParam) {
        var sPageURL = decodeURIComponent(window.location.search.substring(1)),
            sURLVariables = sPageURL.split('&'),
            sParameterName,
            i;

        for (i = 0; i < sURLVariables.length; i++) {
            sParameterName = sURLVariables[i].split('=');

            if (sParameterName[0] === sParam) {
                return sParameterName[1] === undefined ? true : sParameterName[1];
            }
        }
    };



    $(document).ready(function () {
        catSel = getUrlParameter('id');
    if(catSel){
            var catId = $('#catAjax').val();

            $('#nazione option').remove();
            $('#nazione').append($('<option>', {
                value: '',
                text: 'Seleziona Nazione'
            }));
//        console.log(catId);

            $.ajax({
                url: "includes/api.php/nazione_page?&filter=id_categoria,eq," + catId +"&transform=1",
                type: "GET",
                dataType: 'json',
                headers: {
                    'Content-type': 'application/json',
                    'Access-Control-Allow-Headers': 'Content-Type',
                    'Access-Control-Allow-Origin': '*',
                    'X-XSRF-TOKEN': '<?php echo (isset($_COOKIE['XSRF-TOKEN'])) ? $_COOKIE['XSRF-TOKEN'] : ''?>'
                },
                success: function (data) {
                    if(data) {
//                    console.log(data);
//                    $('#urlFirendly').val(urlFre);
//                        console.log(data.nazioni);
                        var nazArr = [];

                        $.each(data.nazione_page, function (i, val) {
//                        console.log(i);
//                        console.log(val);
                            var nazid;
                            nazid = val.id_nazione;
//                        console.log(nazid);
                            nazArr.push(nazid);
                        });
//                        console.log(nazArr);

//                }

//                    Object.keys(data).map(function(objectKey, index) {
//                        var value = object[objectKey];
//                        console.log(value);
//                    });
//                    $('#catAajx').on('change', function () {

//                    if (typeof nazArr !== 'undefined' && nazArr.length == 0) {
//                        console.log('vuoto');


                        $.ajax({
                            url: "includes/api.php/nazioni/" + nazArr + "&transform=1",
                            type: "GET",
                            dataType: 'json',
                            headers: {
                                'Content-type': 'application/json',
                                'Access-Control-Allow-Headers': 'Content-Type',
                                'Access-Control-Allow-Origin': '*',
                                'X-XSRF-TOKEN': '<?php echo (isset($_COOKIE['XSRF-TOKEN'])) ? $_COOKIE['XSRF-TOKEN'] : ''?>'
                            },
                            success: function (data) {
//                            console.log(data);

                                if (data.length > 1) {
//

                                    $.each(data, function (i, val) {

//                                        console.log(val.nazione);
//                                        $('#nazione').append($('<option>', {
////                                            id: data.id,
//                                            attr:(val.id),
//                                            value: val.id,
//                                            text: val.nazione
//                                        }));
                                        $('#nazione')
                                            .append($('<option>')
                                                .attr('id', val.id)
                                                .attr('value', val.id)
                                                .text(val.nazione));

//                                        if(idOption === val.id){
//                                            $(this).attr('selected', true);
//                                        }
                                    });
                                } else {
//                                    $('#nazione')
//                                        .append($('<option>', {
//                                        attr:(data.id),
//                                        value: data.id,
//                                        text: data.nazione
//                                    }
//                                    ));
                                    $('#nazione')
                                        .append($('<option>')
                                            .attr('id', data.id)
                                            .attr('value', data.id)
                                            .text(data.nazione));
                                }
                            }
                        });
//                    });
                    }

                }
            });
        }


//        $.ajax({
//            url: "includes/api.php/nazioni/<?php //echo $citta->id_nazione;  ?>///&transform=1",
//            type: "GET",
//            dataType: 'json',
//            headers: {
//                'Content-type': 'application/json',
//                'Access-Control-Allow-Headers': 'Content-Type',
//                'Access-Control-Allow-Origin': '*',
//                'X-XSRF-TOKEN': '<?php //echo (isset($_COOKIE['XSRF-TOKEN'])) ? $_COOKIE['XSRF-TOKEN'] : ''?>//'
//            },
//            success: function (data) {
//
//
//                var idNaz = data.id;
////                console.log(idNaz);
//
////                var nazTesr = $('#nazione option[value="41"]');
////                $(nazTesr).attr("selected", true);
////                console.log(nazTesr);
////                console.log(test2);
//                $("#nazioni option").each(function()
//                {
//                    var val = $(this).val();
////                    console.log(val);
////                    if(this.val() === idNaz) {
////                      var test =  $(this).attr("selected", true);
////                      console.log(test);
////                    }
//                });
//
//            }
//        });

//        console.log('test');
//        var ind;
//        $("#nazioni option").each(function(index, val) {
////            ind = $('#nazione option')[0];
////            var val = this.val();
//            console.log(val);
//            console.log(index);
//
//        });
        var nazOp=[];
        var idData = <?php echo $citta->id_nazione; ?>;
        console.log(idData);
        var refreshId = setTimeout( function()
        {
            var length = $('#nazione option').length;
            $('#'+idData).attr("selected", true);
        }, 1000);

    });

    $('#testoNullo').change(function() {
        $('#centroAjax').toggle();
    });

    $('#catAjax').on('change', function () {
        var idCat = $(this).val();
    });

    $('#nazione').on('change', function () {
        var idNaz = $(this).val();
//        console.log(idNaz + ' change');
    });


    var checked = $('#testoNullo').is(":checked");
    if(checked){
        $('#centroAjax').attr('required', true);
        $('#centroAjax').show();
        $('#centroAjax option').remove();
        $('#centroAjax')
            .append($('<option>')
//            .attr('required', true)
            .val('')
            .text('Seleziona il Centro sul quale si vuole effettuare il redirect'));
//        };

        var idNaz = $('#nazione').val();
        var idCat = $('#catAjax').val();
        console.log(idNaz);

        $.ajax({
            url: "includes/api.php/centro?&filter[]=id_nazione,eq," + idNaz +"&filter[]=id_categoria,eq," + idCat +"&transform=1",
            type: "GET",
            dataType: 'json',
            headers: {
                'Content-type': 'application/json',
                'Access-Control-Allow-Headers': 'Content-Type',
                'Access-Control-Allow-Origin': '*',
                'X-XSRF-TOKEN': '<?php echo (isset($_COOKIE['XSRF-TOKEN'])) ? $_COOKIE['XSRF-TOKEN'] : ''?>'
            },
            success: function (data) {
                if(data){
                    $.each(data.centro, function (i, val) {

                        $('#centroAjax').append($('<option>', {
                            value: val.urlfriendly,
                            text: val.Centro
                        }));
                    });
                }

            }

        });
    }

    $('#testoNullo').on('click', function (idCat, idNaz) {
        var checked = $(this).is(":checked");

        if(checked){
            $('#centroAjax').attr('required', true);
            $(this).val(1);
        } else {
            $('#centroAjax').attr('required', false);
            $('#centroAjax').attr('data-parsley-required', false);
            $(this).val(0);
        }

        $('#centroAjax option').remove();
        $('#centroAjax').append($('<option>', {
            value: '',
            text: 'Seleziona il Centro sul quale si vuole effettuare il redirect'
        }));

        var idNaz = $('#nazione').val();
        var idCat = $('#catAjax').val();
        console.log(idNaz);
//        if(!idNaz){
//            idNaz = <?php //echo $citta->id_nazione; ?>//;
//            console.log(idNaz);
//        }
//        if (!idCat){
//            idCat = <?php //echo $citta->id_categoria; ?>//;
//            console.log(idCat);
//        }

            $.ajax({
                url: "includes/api.php/centro?&filter[]=id_nazione,eq," + idNaz +"&filter[]=id_categoria,eq," + idCat +"&transform=1",
                type: "GET",
                dataType: 'json',
                headers: {
                    'Content-type': 'application/json',
                    'Access-Control-Allow-Headers': 'Content-Type',
                    'Access-Control-Allow-Origin': '*',
                    'X-XSRF-TOKEN': '<?php echo (isset($_COOKIE['XSRF-TOKEN'])) ? $_COOKIE['XSRF-TOKEN'] : ''?>'
                },
                success: function (data) {
//                    console.log(data);

                    if(data){
                        $.each(data.centro, function (i, val) {
    //                                console.log(val.nome_citta);
//                                    console.log(val.Centro);
//                            console.log(val.Centro);
                            $('#centroAjax').append($('<option>', {
                                value: val.urlfriendly,
                                text: val.Centro
                            }));
                        });
                    }

                }

            });

    });
</script>
