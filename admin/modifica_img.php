<?php include 'includes/header.php'; ?>
<?php if(!$sessione->loggato()){ redirect('accedi.php');} ?>

<!-- Barra di Navigazione -->
<?php include ("includes/navbar.php") ?>

<?php include ("includes/aside.php") ?>
<!-- End aside-->

<?php include ("includes/aside_utenti.php") ?>
<?php
/**
 * Created by Giuseppe Alessandro De Blasio.
 * User: Giuseppe Alessandro De Blasio
 * Date: 23/09/2016
 * Time: 12:38
 */



if (empty((int)$_GET['id_img'])) {
redirect('gestione_upload.php');
}


$mod_img = Image::seleziona_per_id($_GET['id_img']);


if(isset($_POST['modifica_immagine'])){

    $mod_img->titolo = $_POST['titolo'];
    $mod_img->didascalia = $_POST['didascalia'];
    $mod_img->descrizione = $_POST['descrizione'];
    $mod_img->testo_alternativo = $_POST['testo_alternativo'];
    $mod_img->salva();
    $messaggio = "Immagine Modificata Correttamente";
}

if(isset($_POST['annulla_modifiche'])){

    redirect('gestione_upload.php');
}

?>


<section class="main-content">

    <div class="col-lg-12">
        <h1 class="page-header">Modifica Immagini</h1>
    </div>

    <?php if($messaggio) :?>
        <div class="row">
            <div class="col-md-12">
                <div class="alert <?php echo ($messaggio == "Immagine Modificata Correttamente") ? 'alert-success' : 'alert-danger' ?> alert-dismissable">
                    <button type="button" data-dismiss="alert" aria-hidden="true" class="close">×</button>
                    <?php  echo "<b class='dz-success'>" . $messaggio . "</b>"; ?>.
                </div>
            </div>
        </div>
    <?php endif; ?>


    <div class="row">
        <div class="col-md-6">
            <form action="" method="post" enctype="multipart/form-data" data-parsley-validate>
                <div class="form-group">
                    <label for="titolo">Titolo Immagine</label>
                    <input type="text" name="titolo" class="form-control" value="<?php echo $mod_img->titolo;  ?>">
                </div>
                <div class="form-group">
                    <label for="didascalia">Didascalia</label>
                    <input type="text" name="didascalia" class="form-control" value="<?php echo $mod_img->didascalia;  ?>">
                </div>
                <div class="form-group">
                    <label for="descrizione">Descrizione</label>
                    <input type="text" name="descrizione" class="form-control" value="<?php echo $mod_img->descrizione; ?>">
                </div>
                <div class="form-group">
                    <label for="testo_alternativo">Testo Alternativo</label>
                    <input type="text" name="testo_alternativo" class="form-control" value="<?php echo $mod_img->testo_alternativo; ?>">
                </div>
                <div class="form-group">
                    <label for="testo_alternativo">Tipo</label>
                    <input type="text" name="tipo" class="form-control" value="<?php echo $mod_img->tipo; ?>" disabled>
                </div>
                <div class="form-group">
                    <label for="testo_alternativo">Grandezza</label>
                    <input type="text" name="grandezza" class="form-control" value="<?php echo $mod_img->grandezza; ?>" disabled>
                </div>
                <input type="submit" class="btn btn-primary" name="modifica_immagine" value="Modifica Immagine">
                <input type="submit" class="btn btn-danger" name="annulla_modifiche" value="Annulla">
            </form>
        </div>

        <div class="col-md-6">
            <img src='<?php  echo "../images/$mod_img->nomefile"?>' alt="<?php echo $mod_img->testo_alternativo;?>" class="img-thumbnail img-responsive" width="686.25px" height="226.5px">
        </div>

    </div>


</section>




















<?php include 'includes/footer.php'; ?>

