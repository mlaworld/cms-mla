<?php
/**
 * Created da Giuseppe Alessandro De Blasio.
 * User: Giuseppe Alessandro De Blasio
 * Date: 21/09/2016
 * Time: 11:14
 */
?>

<?php include 'includes/header.php'; ?>

<?php if(!$sessione->loggato()){ redirect('accedi.php');} ?>
<!-- Barra di Navigazione -->
<?php include ("includes/navbar.php") ?>

<?php include ("includes/aside.php") ?>
<!-- End aside-->

<?php include ("includes/aside_utenti.php") ?>

<?php
$galleria_immagini = Image::seleziona_tutti();

$pagina = !empty($_GET['pagina']) ? (int)$_GET['pagina'] : 1;

$elementi_per_pagina = 12;

$elementi_totali = Image::conta_elementi();

$paginazione = new Paginazione($pagina, $elementi_per_pagina, $elementi_totali);

$sql = "SELECT * FROM image ";
$sql.= "LIMIT {$elementi_per_pagina} ";
$sql.= "OFFSET {$paginazione->offset()}";
$galleria_immagini = Image::cerca_con_query($sql);



//if(isset($_POST['cancella'])) {


    //$cancella_img = Image::seleziona_per_id($_POST['cancella_img']);

    /*if ($cancella_img == false) {

        $elimina = new Image();
        $elimina->cancella_foto($_POST['cancella_img'], $_POST['nome_img']);
        redirect("gestione_upload.php");
    }*/
    //$sessione->messaggio('<div data-toggle="notify" data-onload data-message="Immagine <b>Eliminata</b> Correttamente" data-options="{&quot;status&quot;:&quot;success&quot;, &quot;pos&quot;:&quot;bottom-right&quot;}" class="hidden-xs"></div>');

//}

if(isset($_POST['modifica'])){

    redirect("modifica_img.php?id_img={$_POST['mod_img']}");

}

if(isset($_POST['cancella'])){

    redirect("modifica_img.php?id={$_POST['cancella_img']}");
}

?>


 <section class="main-content">

    <div class="col-lg-12">
        <h1 class="page-header">Galleria Immagini</h1>
    </div>

<!--<div class="container-fluid">
     <div class="row">
         <div class="row-masonry row-masonry-md-4 row-masonry-sm-2">
        <?php  //foreach($galleria_immagini as $immagine) :?>
            <form action="" method="post">
                <div class="col-masonry">
                    <img src='<?php  //echo "images/$immagine->nomefile"?>' alt="<?php //echo $immagine->nomefile;?>" class="img-thumbnail img-responsive" width="386.25px" height="226.5px !important">
                    <input type="hidden" name="nome_img" value="<?php  // echo $immagine->nomefile;?>">
                    <div class="row" style="margin-left: 0px; margin-right: 0px; padding-top: 5px; padding-bottom: 5px;">
                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-md-6 col button_l">
                                    <button type="button" class="btn btn-danger col-md-12" data-record-id="<?php  // echo $immagine->id; ?>" data-record-title="<?php // echo  $immagine->nomefile;?>" data-toggle="modal" data-target="#cancella-img-pagine"><i class="fa fa-trash-o"></i> Elimina</button>
                                    <input type="hidden" name="cancella_img" value="<?php// echo $immagine->id; ?>" />
                                </div>
                                <div class="col-md-6 button_r">
                                    <button class="btn btn-info col-md-12" type="submit" name="modifica"><i class="fa fa-edit"></i> Modifica</button>
                                    <input type="hidden" name="mod_img" value="<?php  // echo $immagine->id; ?>" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        <?php  // endforeach; ?>
        </div>
    </div>
</div> -->

     <div class="container-fluid pad">
         <div class="box row destinazioni">
             <div class="col-lg-12">
            <?php  foreach($galleria_immagini as $immagine) :?>
                 <form action="" method="post">
                     <div class="style_box col-xs-12 col-sm-3 col-lg-3">
                         <img src='<?php echo "../images/$immagine->nomefile"?>' alt="<?php echo $immagine->nomefile;?>"  alt="<?php echo "../images/$immagine->nomefile"  ?>" title="<?php echo "../images/$immagine->nomefile" ?>">
                         <button class="blu" type="submit" name="modifica"><i class="fa fa-edit"></i> Modifica</button>
                         <input type="hidden" name="mod_img" value="<?php  echo $immagine->id; ?>" />
                         <button class="red" type="button"  data-record-id="<?php  echo $immagine->id; ?>" data-record-title="<?php echo $immagine->nomefile;?>" data-toggle="modal" data-target="#cancella-img-pagine"><i class="fa fa-trash-o"></i> Elimina</button>
                         <input type="hidden" name="cancella_img" value="<?php echo $immagine->id; ?>" />
                     </div>
                 </form>
            <?php  endforeach; ?>
             </div>
         </div>
     </div>


     <div class="col-lg-8">
         <ul class="pagination pull-right">
             <?php
             if($paginazione->pagine_totali() > 1){
                 if($paginazione->e_prima()){
                     echo "<li><a href='gestione_upload.php?pagina={$paginazione->prima()}'>Prima</a></li>";
                 }
                 for ($i = 1; $i <= $paginazione->pagine_totali(); $i++){
                     if($i == $paginazione->pagina_corrente){
                         echo "<li class='active'><a href='gestione_upload.php?pagina={$i}'>{$i}</a></li>";
                     } else {
                         echo "<li><a href='gestione_upload.php?pagina={$i}'>{$i}</a></li>";
                     }
                 }

                 if($paginazione->e_dopo()){
                     echo "<li><a href='gestione_upload.php?pagina={$paginazione->dopo()}'>Dopo</a></li>";
                 }
             }

             ?>
         </ul>
     </div>


</section>

<?php include 'modale_canc_img_post.php'; ?>

<?php include 'includes/footer.php'; ?>