<?php
/**
 * Created da Giuseppe Alessandro De Blasio.
 * User: Giuseppe Alessandro De Blasio
 * Date: 31/03/17
 * Time: 10:05
 */

include ("includes/header.php");


?>
    <!doctype html>
    <html lang="it">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport"
              content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>Auth</title>
    </head>
    <body id="authSpin">
        <form method="post" action="includes/api.php/" id="auth" style="display: none">
            <input name="username" value="<?php echo $username ?>"/>
            <input name="password" value="<?php echo $password ?>"/>
            <input type="submit" value="ok">
         </form>

    </body>
    </html>
<script>
    $('#authSpin').addClass('spinner');

    document.getElementById('auth').submit();
</script>

<?php //if (empty($_REQUEST)) {redirect('accedi.php');} ?>
