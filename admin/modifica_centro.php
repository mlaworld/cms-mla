<?php
/**
 * Created da Giuseppe Alessandro De Blasio.
 * User: Giuseppe Alessandro De Blasio
 * Date: 10/10/16
 * Time: 12:28
 */
?>
<?php include 'includes/header.php'; ?>
<link rel="stylesheet" href="css/jqueryui/css/no-theme/jquery-ui-1.10.4.custom.min.css">
<?php if(!$sessione->loggato()){ redirect('accedi.php');} ?>

<!-- Barra di Navigazione -->
<?php include ("includes/navbar.php") ?>

<?php include ("includes/aside.php") ?>
<!-- End aside-->
<?php


if (empty($_GET['id'])) {
    redirect('elenco_centri.php');
}

$centro = Centro::seleziona_per_id($_GET['id']);
$prezzo = Prezzi::seleziona_per_id($_GET['id']);

if (isset($_POST['aggiorna_centro'])) {
    if ($centro) {

        $centro->id_catalogo = $_POST['catalogo'];
        $centro->Centro = $_POST['nome_centro'];
        $catobj = CategoriePagine::seleziona_per_id($_POST['categorie']); $catUrl = $catobj->urlFriendly;
        $nazobj = Nazioni::seleziona_per_id($_POST['nazione']); $nazUrl = $nazobj->urlFriendly;
        $citobj = CittaPage::seleziona_per_id( $_POST['citta']); $citUrl = $citobj->urlFriendly;
        $urlRewrite = rewriteUrl(trim(strtolower($centro->Centro)));
        $centro->urlfriendly = $urlRewrite;
        $centro->Lingua = $_POST['lingua'];
        $centro->id_nazione = $_POST['nazione'];
        $centro->EtaMin = $_POST['etaMin'];
        $centro->EtaMax = $_POST['etaMax'];
        $centro->Sistemazione = $_POST['sistemazione'];
        $centro->PrezziDa = $_POST['prezzi_da'];
        $centro->CentroUrl = /*$_POST['centro_url'];*/"/".$catUrl."/".$nazUrl."/".$citUrl."/".$urlRewrite;
        $centro->BreveDescrizione = $_POST['breve_descrizione'];
        $centro->DurataMin = $_POST['durata_min'];
        $centro->College = isset($_POST['college']) == 1;
        $centro->Famiglia = isset($_POST['famiglia']) == 1;
        $centro->Hotel = isset($_POST['hotel']) == 1 ;
        $centro->Residence = isset($_POST['residence']) == 1;
        $centro->GoldPlus = isset($_POST['goldPlus']) == 1;
        $centro->FootballAcademy = isset($_POST['FootballAcademy']) == 1;
        $centro->DanceAcademy = isset($_POST['DanceAcademy']) == 1;
        $centro->BasketAcademy = isset($_POST['BasketAcademy']) == 1;
        $centro->TheatreAcademy = isset($_POST['TheatreAcademy']) == 1;
        $centro->BagnoPrivato = isset($_POST['BagnoPrivato']) == 1;
        $centro->MedicoItaliano = isset($_POST['MedicoItaliano']) == 1;
        $centro->Escursioni = isset($_POST['Escursioni']) == 1;
        $centro->Trinity = isset($_POST['Trinity']) == 1;
        $centro->Studio = isset($_POST['Studio']) == 1;
        $centro->Appartamento = isset($_POST['Appartamento']) == 1;
        $centro->Monolocale = isset($_POST['Monolocale']) == 1;
        $centro->stato = $_POST['stato'];
        $centro->id_citta = $_POST['citta'];
        $centro->id_categoria = $_POST['categorie'];
        $centro->img_alt = $_POST['img_alt'];
        $centro->img_title = $_POST['img_title'];
        $centro->metaDescription = $_POST['meta_description'];
        $centro->metaTitle = $_POST['meta_title'];
        $centro->canonicalUrl = $_POST['canonical'];
        $centro->metaIndex = $_POST['selezione_index'];
        $centro->impostazione_file_centro($_FILES['file']);
        $centro->caricamento_foto_centro();
        //$centro->impostazione_file_articolo($_FILES['file']);
        //$centro->caricamento_foto_articolo();
        $centro->salva();
        $msg_instant->success('Centro <b>Modificato</b> correttamente');
        $msg_instant->display();

    }
}


$selziona_catalogo = Cataloghi::seleziona_tutti();
$seleziona_nazione = Nazioni::seleziona_tutti();
$utente_ses = Utente::seleziona_per_id($_SESSION['id_utente']);
//$sel_car_centr = caratCentro::seleziona_per_campo_custom('id_centro', 0);


?>


<section class="main-content" onload="">
    <br>
    <button type="button" onclick="location.href='elenco_centri.php'" class="btn btn-labeled btn-primary pull-right">
               <span class="btn-label"><i class="fa fa-arrow-left"></i></span>Ritorna a Elenco Centri
    </button>
    <h1 class="page-header">Modifica Centro <?php echo isset($_SESSION['ultimo_id']) ?></h1>

    <?php if($messaggio) :?>
        <div class="row">
            <div class="col-md-12">
                <div class="alert <?php echo ($messaggio == "Immagine caricata Correttamente") ? 'alert-success' : 'alert-danger' ?> alert-dismissable">
                    <button type="button" data-dismiss="alert" aria-hidden="true" class="close">×</button>
                    <?php  echo "<b class='dz-success'>" . $messaggio . "</b>"; ?>.
                </div>
            </div>
        </div>
    <?php endif; ?>
    <div class="panel-body">
        <form method="POST" action="" class="form-horizontal" enctype="multipart/form-data" data-parsley-validate>
            <div class="row">
                <div class="col-md-12">
                    <div class="col-md-8">
                        <input type="hidden" name="id_centro" id="id_centro" value="<?php echo $centro->id; ?>">
                        <div class="form-group">
                            <label for="nome_centro" class="control-label">Nome Centro</label>
                            <input type="text" name="nome_centro" id="nome_ritorno" class="form-control" value="<?php echo $centro->Centro; ?>" placeholder="Inserisci nome Centro" required>
                        </div>

                        <div class="form-group">
                            <label for="urlFirendly" class="control-label">Url Friendly</label>
                            <input type="text" name="urlFirendly" id="urlFirendly" class="form-control" value="<?php echo $centro->urlfriendly; ?>" disabled>
                        </div>

                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="catalogo" class="control-label">Seleziona Catalogo</label>
                                    <select  class="form-control" name="catalogo" id="catalogo" required>
                                        <option value="">Seleziona Catalogo</option>
                                        <?php foreach ($selziona_catalogo as $catalogo): ?>
                                            <?php if($catalogo->stato == 1): ?>
                                                <option <?php echo ($catalogo->id == $centro->id_catalogo )? 'selected ="selected" ' : "";  ?> value="<?php echo $catalogo->id?>"><?php echo $catalogo->nome_catalogo?></option>
                                            <?php endif; ?>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="lingua" class="control-label">Lingua</label>
                                    <select name="lingua" class="form-control" required>
                                        <option value="">Seleziona Lingua</option>
                                        <option <?php echo ($centro->Lingua == "italiano" )? 'selected ="selected" ' : "";  ?> value="italiano">Italiano</option>
                                        <option <?php echo ($centro->Lingua == "inglese" )? 'selected ="selected" ' : "";  ?> value="inglese">Inglese</option>
                                        <option <?php echo ($centro->Lingua == "francese" )? 'selected ="selected" ' : "";  ?> value="francese">Francese</option>
                                        <option <?php echo ($centro->Lingua == "spagnolo" )? 'selected ="selected" ' : "";  ?> value="spagnolo">Spagnolo</option>
                                        <option <?php echo ($centro->Lingua == "tedesco" )? 'selected ="selected" ' : "";  ?> value="tedesco">Tedesco</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="catalogo" class="control-label">Seleziona Categoria</label>
                                    <select name="categorie" class="form-control" required>
                                        <?php $pagine = CategoriePagine::seleziona_tutti(); ?>
                                        <option value="">Seleziona Categoria</option>
                                        <?php foreach($pagine as $chiave => $pagina): ?>
                                            <option <?php echo ($pagina->id == $centro->id_categoria )? 'selected ="selected" ' : "";  ?>value="<?php echo $pagina->id; ?>"><?php echo $pagina->titolo_categoria; ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <label for="etaMin" class="control-label">Età Minima</label>
                                <select  class="form-control" name="etaMin" id="etaMin" required>
                                    <option value="">Seleziona età Minima</option>
                                    <option <?php echo ($centro->EtaMin == 8 )? 'selected ="selected" ' : "";  ?> value="8">8</option>
                                    <option <?php echo ($centro->EtaMin == 9 )? 'selected ="selected" ' : "";  ?> value="9">9</option>
                                    <option <?php echo ($centro->EtaMin == 10 )? 'selected ="selected" ' : "";  ?> value="10">10</option>
                                    <option <?php echo ($centro->EtaMin == 11 )? 'selected ="selected" ' : "";  ?> value="11">11</option>
                                    <option <?php echo ($centro->EtaMin == 12 )? 'selected ="selected" ' : "";  ?> value="12">12</option>
                                    <option <?php echo ($centro->EtaMin == 13 )? 'selected ="selected" ' : "";  ?> value="13">13</option>
                                    <option <?php echo ($centro->EtaMin == 14 )? 'selected ="selected" ' : "";  ?> value="14">14</option>
                                    <option <?php echo ($centro->EtaMin == 15 )? 'selected ="selected" ' : "";  ?> value="15">15</option>
                                    <option <?php echo ($centro->EtaMin == 16 )? 'selected ="selected" ' : "";  ?> value="16">16</option>
                                    <option <?php echo ($centro->EtaMin == 17 )? 'selected ="selected" ' : "";  ?> value="17">17</option>
                                </select>
                            </div>
                            <div class="col-md-4">
                                <label for="etaMax" class="control-label">Età Massima</label>
                                <select  class="form-control" name="etaMax" id="etaMax" required>
                                    <option value="">Seleziona età Massima</option>
                                    <option <?php echo ($centro->EtaMax == 13 )? 'selected ="selected" ' : "";  ?> value="13">13</option>
                                    <option <?php echo ($centro->EtaMax == 14 )? 'selected ="selected" ' : "";  ?> value="14">14</option>
                                    <option <?php echo ($centro->EtaMax == 15 )? 'selected ="selected" ' : "";  ?> value="15">15</option>
                                    <option <?php echo ($centro->EtaMax == 16 )? 'selected ="selected" ' : "";  ?> value="16">16</option>
                                    <option <?php echo ($centro->EtaMax == 17 )? 'selected ="selected" ' : "";  ?> value="17">17</option>
                                    <option <?php echo ($centro->EtaMax == 18 )? 'selected ="selected" ' : "";  ?> value="18">18</option>
                                    <option <?php echo ($centro->EtaMax == 19 )? 'selected ="selected" ' : "";  ?> value="19">19</option>
                                </select>
                            </div>
                            <div class="col-md-4">
                                <label for="sistemazione" class="control-label">Sistemazione</label>
                                <select name="sistemazione" class="form-control" required>
                                    <option  value="">Selelziona Sistemazione</option>
                                    <option <?php echo ($centro->Sistemazione == "famiglia" )? 'selected ="selected" ' : "";  ?> value="famiglia">Famiglia</option>
                                    <option <?php echo ($centro->Sistemazione == "college" )? 'selected ="selected" ' : "";  ?> value="college">College</option>
                                    <option <?php echo ($centro->Sistemazione == "residence o famiglia" )? 'selected ="selected" ' : "";  ?> value="residence o famiglia">Residence o Famiglia</option>
                                    <option <?php echo ($centro->Sistemazione == "college o famiglia" )? 'selected ="selected" ' : "";  ?> value="college o famiglia">College o Famiglia</option>
                                    <option <?php echo ($centro->Sistemazione == "residence" )? 'selected ="selected" ' : "";  ?> value="residence">Residence</option>
                                    <option <?php echo ($centro->Sistemazione == "hotel" )? 'selected ="selected" ' : "";  ?> value="hotel">Hotel</option>
                                    <option <?php echo ($centro->Sistemazione == "hotel o famiglia" )? 'selected ="selected" ' : "";  ?> value="hotel o famiglia">Hotel o Famiglia</option>
                                    <option <?php echo ($centro->Sistemazione == "famiglia o residence o appartamento" )? 'selected ="selected" ' : "";  ?> value="famiglia o residence o appartamento">Famiglia o Residence o Appartamento</option>
                                    <option <?php echo ($centro->Sistemazione == "famiglia o residence o app o monolocale" )? 'selected ="selected" ' : "";  ?> value="famiglia o residence o app o monolocale">Famiglia o Residence o Appartamento o Monolocale</option>
                                    <option <?php echo ($centro->Sistemazione == "famiglia o residence o studio" )? 'selected ="selected" ' : "";  ?> value="famiglia o residence o studio">Famiglia o Residence o Studio</option>
                                </select>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-4">
                                <label for="prezzi_da" class="control-label">A partire da</label>
                                <input name="prezzi_da" class="form-control" value="<?php echo $centro->PrezziDa; ?>" required />
                            </div>
                            <div class="col-md-4">
                                <label for="centro_url" class="control-label">Centro URL</label>
                                <input type="text" name="centro_url"  class="form-control copia-testo-js" value="<?php echo $centro->CentroUrl ?>" placeholder="Inserisci URL" required />
                                <button id="url" class="copia-testo"><i class="fa fa-clipboard"></i></button>
                            </div>
                            <div class="col-md-4">
                                <label for="durata_min" class="control-label">Durata Minima</label>
                                <select name="durata_min" class="form-control" required>
                                    <option value="">Durata Minima</option>
                                    <option <?php echo ($centro->DurataMin == "2 settimane" )? 'selected ="selected" ' : "";  ?> value="2 settimane">Due Settimane</option>
                                    <option <?php echo ($centro->DurataMin == "15 giorni" )? 'selected ="selected" ' : "";  ?> value="15 giorni">15 Giorni</option>
                                    <option <?php echo ($centro->DurataMin == "14 giorni" )? 'selected ="selected" ' : "";  ?> value="14 giorni">14 Giorni</option>
                                    <option <?php echo ($centro->DurataMin == "8 giorni" )? 'selected ="selected" ' : "";  ?> value="8 giorni">8 Giorni</option>
                                    <option <?php echo ($centro->DurataMin == "7 giorni" )? 'selected ="selected" ' : "";  ?> value="7 giorni">7 Giorni</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="breve_descrizione" class="control-label">Breve Descrizione</label>
                            <input type="text" name="breve_descrizione" class="form-control" value="<?php echo $centro->BreveDescrizione ?>" placeholder="Inserisci breve descrizione" required>
                        </div>

                        <h3 class="page-header">Caratteristiche Centro</h3>
                        <dt class="text-primary">Sistemazioni</dt><br>
                        <div class="row">
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <label for="college" class="col-sm-5 control-label">College</label>
                                    <label class="checkbox-inline c-checkbox">
                                        <input id="college" name="college" type="checkbox" <?php echo ($centro->College == 1 )? "checked" : "";  ?> value="0">
                                        <span class="fa fa-check"></span></label>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <label for="famiglia" class="col-sm-5 control-label">Famiglia</label>
                                    <label class="checkbox-inline c-checkbox">
                                        <input id="famiglia" name="famiglia" type="checkbox" <?php echo ($centro->Famiglia == 1 )? "checked" : "";  ?> value="0">
                                        <span class="fa fa-check"></span></label>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <label for="hotel" class="col-sm-5 control-label">Hotel</label>
                                    <label class="checkbox-inline c-checkbox">
                                        <input id="hotel" name="hotel" type="checkbox" <?php echo ($centro->Hotel == 1 )? "checked" : "";  ?> value="0">
                                        <span class="fa fa-check"></span></label>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <label for="residence" class="col-sm-5 control-label">Residence</label>
                                    <label class="checkbox-inline c-checkbox">
                                        <input id="residence" name="residence" type="checkbox" <?php echo ($centro->Residence == 1 )? "checked" : "";  ?> value="0">
                                        <span class="fa fa-check"></span></label>
                                </div>
                            </div>
                        </div>

                        <hr>
                        <dt class="text-green">I Plus del Centro</dt><br>
                        <div class="row">
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <label for="goldPlus" class="col-sm-5 control-label">GoldPlus</label>
                                    <label class="checkbox-inline c-checkbox">
                                        <input id="goldPlus" name="goldPlus" type="checkbox" <?php echo ($centro->GoldPlus == 1 )? "checked" : "";  ?> value="0">
                                        <span class="fa fa-check"></span></label>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <label for="FootballAcademy" class="col-sm-5 control-label">Football Academy</label>
                                    <label class="checkbox-inline c-checkbox">
                                        <input id="FootballAcademy" name="FootballAcademy" type="checkbox" <?php echo ($centro->FootballAcademy == 1 )? "checked" : "";  ?> value="0">
                                        <span class="fa fa-check"></span></label>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <label for="DanceAcademy" class="col-sm-5 control-label">Dance Academy</label>
                                    <label class="checkbox-inline c-checkbox">
                                        <input id="DanceAcademy" name="DanceAcademy" type="checkbox" <?php echo ($centro->DanceAcademy == 1 )? "checked" : "";  ?> value="0">
                                        <span class="fa fa-check"></span></label>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <label for="BasketAcademy" class="col-sm-5 control-label">Basket Academy</label>
                                    <label class="checkbox-inline c-checkbox">
                                        <input id="BasketAcademy" name="BasketAcademy" type="checkbox" <?php echo ($centro->BasketAcademy == 1 )? "checked" : "";  ?> value="0">
                                        <span class="fa fa-check"></span></label>
                                </div>
                            </div>
                        </div>


                        <div class="row">
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <label for="TheatreAcademy" class="col-sm-5 control-label">Theatre Academy</label>
                                    <label class="checkbox-inline c-checkbox">
                                        <input id="TheatreAcademy" name="TheatreAcademy" type="checkbox" <?php echo ($centro->TheatreAcademy == 1 )? "checked" : "";  ?> value="0">
                                        <span class="fa fa-check"></span></label>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <label for="Appartamento" class="col-sm-5 control-label">Appartamento</label>
                                    <label class="checkbox-inline c-checkbox">
                                        <input id="Appartamento" name="Appartamento" type="checkbox" <?php echo ($centro->Appartamento == 1 )? "checked" : "";  ?> value="0">
                                        <span class="fa fa-check"></span></label>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <label for="Trinity" class="col-sm-5 control-label">Trinity</label>
                                    <label class="checkbox-inline c-checkbox">
                                        <input id="Trinity" name="Trinity" type="checkbox" <?php echo ($centro->Trinity == 1 )? "checked" : "";  ?> value="0">
                                        <span class="fa fa-check"></span></label>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <label for="Monolocale" class="col-sm-5 control-label">Monolocale</label>
                                    <label class="checkbox-inline c-checkbox">
                                        <input id="Monolocale" name="Monolocale" type="checkbox" <?php echo ($centro->Monolocale == 1 )? "checked" : "";  ?> value="0">
                                        <span class="fa fa-check"></span></label>
                                </div>
                            </div>
                        </div>
                        <hr>

                        <dt class="text-danger">Altro</dt><br>
                        <div class="row">
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <label for="MedicoItaliano" class="col-sm-5 control-label">Medico Italiano</label>
                                    <label class="checkbox-inline c-checkbox">
                                        <input id="MedicoItaliano" name="MedicoItaliano" type="checkbox" <?php echo ($centro->MedicoItaliano == 1 )? "checked" : "";  ?> value="0">
                                        <span class="fa fa-check"></span></label>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <label for="Studio" class="col-sm-5 control-label">Studio</label>
                                    <label class="checkbox-inline c-checkbox">
                                        <input id="Studio" name="Studio" type="checkbox" <?php echo ($centro->Studio == 1 )? "checked" : "";  ?> value="0">
                                        <span class="fa fa-check"></span></label>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <label for="BagnoPrivato" class="col-sm-5 control-label">Bagno Privato</label>
                                    <label class="checkbox-inline c-checkbox">
                                        <input id="BagnoPrivato" name="BagnoPrivato" type="checkbox"  <?php echo ($centro->BagnoPrivato == 1 )? "checked" : "";  ?> value="">
                                        <span class="fa fa-check"></span></label>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <label for="Escursioni" class="col-sm-5 control-label">Escursioni</label>
                                    <label class="checkbox-inline c-checkbox">
                                        <input id="Escursioni" name="Escursioni" type="checkbox" <?php echo ($centro->Escursioni == 1 )? "checked" : "";  ?> value="0">
                                        <span class="fa fa-check"></span></label>
                                </div>
                            </div>
                        </div>
                        <hr>
                        <dt class="text-success">Caratteristiche Centro</dt><a href="#" onclick="leggi_centro();"><i class="fa fa-refresh fa-3 pull-right" aria-hidden="true" ></i></a><br>
                        <div class="form-group">
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered table-hover" id="mostra_carat">
                                    <h3 id="carat-null" class='text-center'>Nessuna Caratteristica del centro Inserita</h3>
                                </table>
                            </div>
                        </div>
                        <div class="col-xs-12 col-md-12 col-lg-12">
                            <button type="button" class="btn btn-primary btn-lg text-center col-md-12" data-toggle="modal" data-target="#caratCentro">
                                Aggiungi Caratteristica al Centro
                            </button>
                        </div>


                        <br><br>
                        <h3 class="page-header">Prezzi Centro</h3>
                        <dt class="text-primary">Prezzi</dt><a href="#" onclick="leggi_prezzo();"><i class="fa fa-refresh fa-3 pull-right" aria-hidden="true" ></i></a><br>
                        <div class="form-group">
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered table-hover" id="mostra_prezzi">
                                    <h3 id="carat-null-prezzo" class='text-center'>Nessuna Prezzo inserito nel centro <?php echo $centro->Centro; ?></h3>
                                </table>
                            </div>
                        </div>
                        <div class="col-xs-12 col-md-12 col-lg-12">
                            <button type="button" class="btn btn-primary btn-lg text-center col-md-12" data-toggle="modal" data-target="#prezzoCentro">
                                Aggiungi Prezzo al Centro
                            </button>
                        </div>
                        <br><br>
                        <h3 class="page-header">Mappa Centro</h3>
                        <div class="col-xs-6 col-md-6 col-lg-6">
                            <label for="lingua" class="control-label">Inserisci Nome Centro</label>
                            <input class="form-control" type="text" id="descMapLoc" >
                        </div>
                        <div class="col-xs-3 col-md-3 col-lg-3">
                            <label for="lat" class="control-label">Latitudine</label>
                            <div class="form-control" type="text" id="lat"></div>
                        </div>
                        <div class="col-xs-3 col-md-3 col-lg-3">
                            <label for="lng" class="control-label">Longitudine</label>
                            <div class="form-control" type="text" id="lng"></div>
                        </div>
                        <br><br>
                        <div id="iframeMaps">
                            <br><br>
                            <iframe id="iframeMappa" width="1050" height="300" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src=""></iframe>
<!--                           <iframe id="mapIframe" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3007.4271935049164!2d-74.17681208415658!3d41.08151192286315!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x89c2e1a40234ee67%3A0x1af95a2c2c4e35e7!2sRamapo+College+of+New+Jersey!5e0!3m2!1sit!2sit!4v1491310303385" width="1050" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>-->
                        </div>
                        <script>
                            $(document).ready(function(){

                                var id_centro = $('#id_centro').val();

                                $('#descMapLoc').on( "change",function () {

                                   var localita = $(this).val();
                                    console.log(localita);


                                    $.ajax({
                                        method: "GET",
                                        url: "https://maps.googleapis.com/maps/api/geocode/json?address=" + localita +"&key=AIzaSyCACRLPAzMdtZT5DVF5k1ToSq-Y-CiA9Ik",
                                        dataType: "json",
                                        success: function (data) {
                                            console.log(data);
                                            var result = data.results[0];
                                            lat = result.geometry.location.lat;
                                            lng = result.geometry.location.lng;
                                            var locationAddr = data.results[0].formatted_address;
                                            console.log(locationAddr);
//                                            $('#iframeMaps').append('<iframe width="1050" height="300" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://maps.google.com/maps?q='+locationAddr+','+lat+','+lng+'&hl=es;z=14&amp;output=embed&q="></iframe>');
                                            $('#descMapLoc').val(locationAddr);
                                            $('#lat').html(lat);
                                            $('#lng').html(lng);
//                                            $('#iframeMappa').attr("src" , "https://maps.google.com/maps?q="+locationAddr+","+lat+","+lng+"&hl=es;z=14&output=embed");
//                                            $('#iframeMappa').attr("src" , "https://maps.google.com/maps?q="+locationAddr+","+lat+","+lng+"&hl=es;z=14&output=embed");
                                            $('#iframeMappa').attr("src" , "https://maps.google.com/maps?q="+locationAddr+"&hl=es;z=14&output=embed");
//                                            console.log(lat);
//                                            console.log(lng);
                                            var id_centro_maps = $('#id_centro').val();

                                            $.ajax({
                                                method: "PUT",
                                                url: "includes/api.php/centro/"+id_centro_maps,
                                                dataType: "json",
                                                data: {lat:lat, lng:lng, descMaps:locationAddr},
                                                headers: {
                                                    'Content-type': 'application/json',
                                                    'Access-Control-Allow-Headers': 'Content-Type',
                                                    'Access-Control-Allow-Origin': '*',
                                                    'X-XSRF-TOKEN': '<?php echo (isset($_COOKIE['XSRF-TOKEN']))? $_COOKIE['XSRF-TOKEN'] : ''?>'
                                                },
                                                success: function (data) {
                                                    console.log(data);
                                                }

                                            });

                                        }
                                    });
                                });

                                $('#lat').addClass('csspinner');
                                $('#lng').addClass('csspinner');
                                $.ajax({
                                    method: "GET",
                                    url: "includes/api.php/centro?filter[]=id,eq,"+id_centro+"&columns=descMaps,lat,lng",
                                    dataType: "json",
                                    headers: {
                                        'Content-type': 'application/json',
                                        'Access-Control-Allow-Headers': 'Content-Type',
                                        'Access-Control-Allow-Origin': '*',
                                        'X-XSRF-TOKEN': '<?php echo (isset($_COOKIE['XSRF-TOKEN']))? $_COOKIE['XSRF-TOKEN'] : ''?>'
                                    },
                                    success: function (data) {
                                        var getLocalita = data.centro.records[0][0];
                                        var getlat = data.centro.records[0][1];
                                        var getlng = data.centro.records[0][2];
                                        $('#lat').removeClass('csspinner');
                                        $('#lng').removeClass('csspinner');
                                        $('#descMapLoc').val(getLocalita);
                                        $('#lat').html(getlat);
                                        $('#lng').html(getlng);
//                                        $('#iframeMaps').append('<iframe width="1050" height="300" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://maps.google.com/maps?q='+getLocalita+','+getlat+','+getlng+'&hl=es;z=14&amp;output=embed&q="></iframe>');
//                                        $('#iframeMappa').attr("src" , "https://maps.google.com/maps?q="+getLocalita+","+getlat+","+getlng+"&hl=es;z=14&output=embed");
                                        $('#iframeMappa').attr("src" , "https://maps.google.com/maps?q="+getLocalita+"&hl=es;z=14&output=embed");
//                                        $('#iframeMappa').attr("src" , "https://www.google.com/maps/embed/v1/place?key=AIzaSyCACRLPAzMdtZT5DVF5k1ToSq-Y-CiA9Ik&q="+getLocalita+","+getlat+","+getlng +"");
//                                        $('#iframeMaps').append('<iframe width="450" height="250" frameborder="0" style="border:0"b src="https://www.google.com/maps/embed/v1/search?key=AIzaSyCACRLPAzMdtZT5DVF5k1ToSq-Y-CiA9Ik&q='+getLocalita+','+getlat+', '+getlng+'" allowfullscreen> </iframe>');
                                }
                                })

                            });
                        </script>

<!--                        <script src="https://maps.googleapis.com/maps/api/geocode/json?address=1600+Amphitheatre+Parkway,+Mountain+View,+CA&key=AIzaSyCACRLPAzMdtZT5DVF5k1ToSq-Y-CiA9Ik"></script>-->
                        <!--<div class="panel panel-default">
                            <div class="panel-heading"><i style="color: #e80079" class="fa fa-header"></i> <b>Meta Title</b></div>
                            <div class="panel-body">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <div class="col-sm-12">
                                            <input maxlength="80" onkeyup="contaCaratteriTitle(this)" name="meta_title" type="text" class="form-control">
                                            <div id="meta_title_caratteri"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading"><i style="color: #CBE86B" class="fa fa fa-pencil"></i> <b>Meta Description</b></div>
                            <div class="panel-body">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <div class="col-sm-12">
                                            <input maxlength="160" onkeyup="contaCaratteriDescrizione(this)" name="meta_description" type="text" class="form-control">
                                            <div id="meta_descrizione_caratteri"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading"><i style="color: #0A246A" class="fa fa-globe"></i> <b>Canonical Url</b></div>
                            <div class="panel-body">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <div class="col-sm-12">
                                            <input name="canonical" type="url" value="" class="form-control">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading"><i style="color: #8e79e8 class="fa fa-indent"></i> <b>Meta Robots Index</b></div>
                            <div class="panel-body">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <div class="col-sm-12">
                                            <select name="selezione_index" id="selezione_index" class="form-control">
                                                <option value="index">Index</option>
                                                <option value="noindex">NoIndex</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div> -->

                        <!-- <div class="panel panel-default">
                            <div class="panel-heading"><i style="color: #1dc116" class="fa fa-key"></i> <b>Meta Keywords</b></div>
                            <div class="panel-body">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <div class="col-sm-12">
                                            <input name="keywords" type="text" data-role="tagsinput" value="" class="form-control">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div> -->

                    </div>
                    <div class="pull-right col-md-4">
                        <div class="panel panel-default">
                            <div class="panel-heading"><i style="color: black;" class=""></i> Pubblica</div>
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-md-10">
                                        <div class="col-md-6">
                                            <p><i class="fa fa-hand-o-right"></i> Stato: <b><?php echo ucfirst($centro->stato) ?></p>
                                        </div>
                                        <div class="col-md-2 pull-right">
                                            <button name="aggiorna_centro" class="btn btn-oval btn-info">Aggiorna Centro</button>
                                        </div>
                                    </div>
                                </div>

                            </div> <!-- panel-body -->
                        </div> <!-- panel panel-default -->


                        <div class="panel panel-default">
                            <div class="panel-heading"><i style="color: #009ab8" class="fa fa-users"></i> Autore</div>
                            <div class="panel-body">
                                <!-- <label>Utente</label> -->

                                <select name="autore" class="form-control">
                                    <?php if($utente_ses->ruolo == 'admin'): ?>
                                        <?php $utenti = Utente::seleziona_tutti(); ?>
                                        <?php foreach($utenti as $utente): ?>
                                            <option <?php echo ($utente->username == $utente_ses->username)? 'selected="selected"' : ""?> ><?php echo $utente->username; ?></option>
                                        <?php endforeach; ?>
                                    <?php endif; ?>
                                    <?php if($utente_ses->ruolo != 'admin'): ?>
                                        <option><?php echo $utente_ses->username; ?></option>
                                    <?php endif; ?>
                                </select>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading"><i style="color: red;" class="fa fa-exclamation-triangle"></i> Stato</div>
                            <div class="panel-body">
                                <!-- <label>Stato</label> -->
                                <select name="stato" class="form-control" required>
                                    <option value="">Seleziona Stato</option>
                                    <option <?php echo ($centro->stato == "pubblicato" )? 'selected ="selected" ' : "";  ?> value="pubblicato">Pubblicato</option>
                                    <option  <?php echo ($centro->stato == "bozza" )? 'selected ="selected" ' : "";  ?> value="bozza">Bozza</option>
                                </select>
                            </div> <!-- panel-body -->
                        </div> <!-- panel panel-default -->

                        <div class="panel panel-default">
                            <div class="panel-heading"><i class="fa fa-folder-o"></i> Seleziona Nazione</div>
                            <div class="panel-body">
                                <select class="form-control" name="nazione" id="nazione" required>
                                    <option value="">Seleziona Nazione</option>
                                    <?php foreach ($seleziona_nazione as $nazione): ?>
                                        <?php if ($nazione->stato == 1): ?>
                                            <option <?php echo ($nazione->id == $centro->id_nazione) ? 'selected ="selected" ' : ""; ?>
                                                value="<?php echo $nazione->id ?>"><?php echo $nazione->nazione ?></option>
                                        <?php endif; ?>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                        </div>

                        <div class="panel panel-default">
                            <div class="panel-heading"><i class="fa fa-folder-o"></i> Seleziona Città</div>
                            <div class="panel-body">
                                <select name="citta" class="form-control" required>
                                    <?php $citta = CittaPage::seleziona_tutti(); ?>
                                    <option value="">Seleziona Città</option>
                                    <?php foreach($citta as $chiave => $prendo_citta): ?>
                                        <option <?php echo ($centro->id_citta == $prendo_citta->id )? 'selected ="selected" ' : "";  ?> value="<?php echo $prendo_citta->id; ?>"><?php echo formattazioneTesto($prendo_citta->nome_citta); ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                        </div>

                        <!--<div class="panel panel-default">
                            <div class="panel-heading"><i style="color: #c77405" class="fa fa-tags"></i> Tags</div>
                            <div class="panel-body">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <div class="col-sm-12">
                                            <input name="tags" type="text" data-role="tagsinput" value="" class="form-control">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div> -->

                        <div class="panel panel-default">
                            <div class="panel-heading">Immagine Centro</div>
                            <div class="panel-body">
                                <div class="text-center form-group">
                                    <img class="img-thumbnail img-responsive" style="width: 350px; height: 350px;" src="<?php echo ($centro->percorso_immagine_e_plecholder_back())? $centro->percorso_immagine_e_plecholder_back() : "https://placeholdit.imgix.net/~text?txtsize=33&txt=350%C3%97350&w=350&h=350" ; ?>" alt="<?php echo $centro->Foto; ?>">
                                    <br>
                                </div>
                                <div class="col-sm-12 form-group">
                                    <input type="file" name="file" class="form-control">
                                </div>
                            </div>
                        </div>

                        <div class="panel panel-default">
                            <div class="panel-heading"><i style="color: #e80079" class="fa fa-picture-o"></i> <b>Image Alt</b></div>
                            <div class="panel-body">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <div class="col-sm-12">
                                            <input  value="<?php echo $centro->img_alt; ?>" name="img_alt" type="text" class="form-control">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="panel panel-default">
                            <div class="panel-heading"><i style="color: #e80079" class="fa fa-picture-o"></i> <b>Image Title</b></div>
                            <div class="panel-body">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <div class="col-sm-12">
                                            <input  value="<?php echo $centro->img_title; ?>" name="img_title" type="text" class="form-control">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="panel panel-default">
                            <div class="panel-heading"><i style="color: #e80079" class="fa fa-header"></i> <b>Meta Title</b></div>
                            <div class="panel-body">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <div class="col-sm-12">
                                            <input maxlength="80" onkeyup="contaCaratteriTitle(this)" value="<?php echo $centro->metaTitle; ?>" name="meta_title" type="text" class="form-control">
                                            <div id="meta_title_caratteri"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading"><i style="color: #CBE86B" class="fa fa fa-pencil"></i> <b>Meta Description</b></div>
                            <div class="panel-body">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <div class="col-sm-12">
                                            <input maxlength="160" onkeyup="contaCaratteriDescrizione(this)" value="<?php echo $centro->metaDescription; ?>" name="meta_description" type="text" class="form-control">
                                            <div id="meta_descrizione_caratteri"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading"><i style="color: #0A246A" class="fa fa-globe"></i> <b>Canonical Url</b></div>
                            <div class="panel-body">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <div class="col-sm-12">
                                            <input name="canonical" type="url" value="<?php echo $centro->canonicalUrl; ?>" class="form-control">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading"><i style="color: #8e79e8 class="fa fa-indent"></i> <b>Meta Robots Index</b></div>
                            <div class="panel-body">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <div class="col-sm-12">
                                            <select name="selezione_index" id="selezione_index" class="form-control">
                                                <option <?php ($centro->metaIndex == 'index')? 'selected="selected" ' : "" ?> value="index">Index</option>
                                                <option <?php ($centro->metaIndex == 'noindex')? 'selected="selected" ' : "" ?> value="noindex">NoIndex</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>


                    </div> <!-- pull-right col-md-4 -->
                </div> <!-- col-md-12 -->
            </div>  <!-- row -->
        </form>
    </div> <!-- panel-body -->

</section>




<!-- Modal -->
<div class="modal fade" id="caratCentro" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Inserisci Caratteristica Centro</h4>
            </div>
            <form action="" method="post">
                <div class="modal-body">
                    <div class="form-group">
                        <label for="titolo_carat">Titolo Caratteristica</label>
                        <input type="text" name="titolo_carat" id="titolo_carat" class="form-control" placeholder="Inserisci la caratteristica">
                    </div>
                    <div class="form-group">
                        <label for="titolo_carat">Seleleziona Posizione Box</label>
                        <select name="posizione_box" id="pos_box"  class="form-control" required>
                            <option value="">Seleziona posizione caratteristica</option>
                            <option value="box-sopra">Box Sopra</option>
                            <option value="box-sotto">Box Sotto</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="desc_carat">Descrizione Caratteristica</label>
                        <textarea  name="desc_carat" id="desc_carat" class="form-control"></textarea>
                    </div>
                    <input type="hidden" id="id_ass_centro" name="id_ass_centro">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancella</button>
                    <button type="button" name="invia_carat" id="invia_carat" data-dismiss="modal" class="btn btn-primary">Salva Caratteristica</button>
                </div>
            </form>
        </div>
    </div>
</div>



<!-- Modal Inserisci Nuovo Centro -->
<div class="modal fade" id="ins_new_cent" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close close_nome_centro" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Inserisci Nuovo Centro</h4>
            </div>
            <form action="" method="post" data-parsley-validate>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="nome_nuovo_centro">Nome del Centro</label>
                        <input type="text" name="nome_nuovo_centro" id="nome_nuovo_centro" class="form-control" required="required">
                        <p id="errore_nome" style="display: none">Non puo essere vuoto</p>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default close_nome_centro" data-dismiss="modal">Cancella</button>
                    <button type="button" name="ins_nuovo_centr" id="ins_nuovo_centr" data-dismiss="modal" class="btn btn-primary">Salva Nuovo Centro</button>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- Modal Inserisci Nuovo Prezzo -->
<div class="modal fade" id="prezzoCentro" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close close_nome_centro" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Inserisci Nuovo Prezzo al Centro</h4>
            </div>
            <form action="" method="post" data-parsley-validate>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="prezzo_centro">Importo</label>
                        <input type="text" name="prezzo_centro" id="importo_centro" class="form-control" required="required">
                        <p id="errore_nome" style="display: none">Non puo essere vuoto</p>
                    </div>
                    <div class="form-group">

                            <label for="data_inizio"><i class="fa fa-calendar"></i> Data Inizio</label>
                            <input type="date" name="data_inizio" class="form-control" required="required">
<!--                            <span class="input-group-addon"><span class="fa-calendar fa"></span></span>-->
                            <p id="errore_nome" style="display: none">Non puo essere vuoto</p>

<!--                        <input type="date" name="data_inizio" id="data_inizio" class="form-control" required="required">-->
                        <p id="errore_nome" style="display: none">Non puo essere vuoto</p>
                    </div>
                    <div class="form-group">
                        <label for="data_fine"><i class="fa fa-calendar"></i> Data Fine</label>
                        <input type="date" name="data_fine" id="data_fine" class="form-control" required="required">
                        <p id="errore_nome" style="display: none">Non puo essere vuoto</p>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default close_nome_centro" data-dismiss="modal">Cancella</button>
                    <button type="button" name="ins_nuovo_prezzo_centr" id="ins_nuovo_prezzo_centr" data-dismiss="modal" class="btn btn-primary">Salva Nuovo Prezzo Centro</button>
                </div>
            </form>
        </div>
    </div>
</div>




<?php include 'modale_canc_carat_centro.php'; ?>

<?php include 'includes/footer.php'; ?>


<script type="text/javascript">

    function convertToSlug(Text)
    {
        return Text
            .toLowerCase()
            .replace(/[^\w ]+/g,'')
            .replace(/ +/g,'-');
    }

    $('#nome_ritorno').on( "change",function () {
        var titolo = $(this).val();
        var urlFre = convertToSlug(titolo);
//        console.log(urlFre);

        $.ajax({
            url: "includes/api.php/centro?filter=urlfriendly,eq," + urlFre,
            type: "GET",
            dataType: 'json',
            data: {urlFre: urlFre},
            headers: {
                'Content-type': 'application/json',
                'Access-Control-Allow-Headers': 'Content-Type',
                'Access-Control-Allow-Origin': '*',
                'X-XSRF-TOKEN': '<?php echo (isset($_COOKIE['XSRF-TOKEN'])) ? $_COOKIE['XSRF-TOKEN'] : ''?>'
            },
            success: function (data) {
//                    console.log(data.centro.records);


                if (typeof data.centro.records !== 'undefined' && data.centro.records.length > 0) {
                    var record = data.centro.records[0];
                    if (urlFre === record[37]) {
                        swal(
                            'Oops...',
                            'Il nome del centro già esiste!',
                            'error'
                        )
                    }
                    else if (urlFre != record[37]) {
                        var id_centro = $('#id_centro').val();
//                        console.log(id_centro);
//                        $.ajax({
//                            url: "includes/api.php/centro/" + id_centro,
//                            type: "PUT",
//                            dataType: 'json',
//                            data:{urlfriendly:urlFre},
//                            headers: {
//                                'Content-type': 'application/json',
//                                'Access-Control-Allow-Headers': 'Content-Type',
//                                'Access-Control-Allow-Origin': '*',
//                                'X-XSRF-TOKEN': '<?php //echo (isset($_COOKIE['XSRF-TOKEN']))? $_COOKIE['XSRF-TOKEN'] : ''?>//'
//                            },
//                            success: function (data) {
//
//                            }
//                        });
                    }

                }
            }
        });
    });




    $(window).load(function(){
       /* $('#ins_new_cent').modal('show',{backdrop: 'static', keyboard: false});
        if($('#nome_nuovo_centro').val() == null){
            $("#ins_new_cent").on("hidden.bs.modal", function () {
                window.location = "elenco_centri.php";
            });
        }

        $('.close_nome_centro').on('click', function (e) {

            e.preventDefault();

            window.location = "elenco_centri.php";
        })
*/

        leggi_centro();
        leggi_prezzo();
    });

    $('#ins_nuovo_centr').on('click', function (e) {

        e.preventDefault();

        var nome_nuovo_centro = $('#nome_nuovo_centro').val();
        if( nome_nuovo_centro.length === 0 ) {
            $('#errore_nome').show();
            window.location = "elenco_centri.php";
        } else {

            var id = null;

            $.ajax({
                url: "http://localhost/mla-cms/admin/insert_nuovo_centro.php",
                type: "POST",
                data: {nome_nuovo_centro : nome_nuovo_centro},
                dataType: 'json',
                success: function(data){

                    var id = data.id;
                    var nomeCentro = data.Centro;
                    //console.log(nomeCentro);

                    $('#id_centro').val(id);
                    $('#id_ass_centro').val(id);
                    $('#nome_ritorno').val(nomeCentro);
                    //console.log(data.id);
                    //console.log(nome_nuovo_centro);
                    //data;
                    //console.log(titolo);
                    //console.log(descrizione);
                    //alert(data.titolo);

                },
                error: function () {
                    alert("errore nell'inerimento");
                }
            });

        }

    });

    $('#invia_carat').on('click', function (e) {

        e.preventDefault();

        var titolo = $('#titolo_carat').val();
        var descrizione = $('#desc_carat').val();
        var posizione = $('#pos_box').val();
        var id_ass_centro = $('#id_centro').val();

        $.ajax({
            url: "http://localhost/mla-cms/admin/insert_carat_centro.php",
            type: "POST",
            data: {titolo : titolo, descrizione : descrizione, id_ass_centro :id_ass_centro, posizione:posizione},

            success: function(data){
                //data;
                //console.log(titolo);
                //console.log(descrizione);
                //alert(data.titolo);

            },
            error: function () {
                alert("errore nell'inerimento");
            }
        });



        leggi_centro();


    });

    function leggi_centro() {

        var id_centro_preso = $('#id_centro').val();

        $.ajax({
            url: 'mostra_carat_cent.php',
            type: 'POST',
            data: {mostro_id_centro:id_centro_preso},
            dataType: 'html',
            async: true,
            success: function (mostra_carat_centro) {
                $('#carat-null').hide();
                if (!mostra_carat_centro.error) {
                    $("#mostra_carat").html(mostra_carat_centro);
                }
            }
        });
    }



    function leggi_prezzo() {

        var id_centro_preso = $('#id_centro').val();

        $.ajax({
            url: 'mostra_prezzi_cent.php',
            type: 'POST',
            data: {mostro_id_centro:id_centro_preso},
            dataType: 'html',
            async: true,
            success: function (mostra_prezzo_centro) {
                $('#carat-null-prezzo').hide();
                if (!mostra_prezzo_centro.error) {
                    $("#mostra_prezzo").html(mostra_prezzo_centro);
                }
            }
        });
    }


    /*$('#delete_carat').on('click', function (e) {

     e.preventDefault();

     var id_carat = $('#el_carat_cent').val();

     console.log(id_carat);


     $.ajax({
     url: 'elimina_carat_centro.php',
     type: 'POST',
     data: {id_carat: id_carat},
     async: false,
     success: function (id_carat) {

     }
     })


     })*/

    function aggChiudi() {
        window.location.reload()
}

        $('#modCarat').on('click', function () {
            window.refresh();
        });


    window.onunload = refreshParent;
    function refreshParent() {
        window.location.reload();
    }


    $(document).ready(function () {
        $('tbody').sortable({
            axis: 'y',
            update: function (event, ui) {
                var data = $(this).sortable('serialize');

                $.ajax({
                    data: data,
                    type: 'POST',
                    url: '/your/url/here'
                });
            }
        });
    });

</script>

<script type="text/javascript" xmlns="http://www.w3.org/1999/html">
    tinymce.init({
        //selector: '#desc_carat',
        selector: "textarea",
        setup: function (editor) {
            editor.on('change', function () {
                editor.save();
            });
        },
        theme: 'modern',
        width: 850,
        height: 450,
        language: 'it',
        plugins: [
            "advlist autolink lists link image charmap print preview anchor",
            "searchreplace visualblocks code fullscreen",
            "insertdatetime media table contextmenu paste imagetools wordcount"
        ],
        toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | preview" +
        "code | ",
        images_upload_url: 'upload.php',
        images_upload_base_path: '/admin/immagini',
        media_live_embeds: true,
        style_formats_merge: true,
        menubar: true,
        image_caption: true,
        /*file_browser_callback: function(field_name, url, type, win) {
         if(type=='image') $('#my_form input').click();
         },*/
        image_prepend_url: "http://localhost/mla-cms/admin/immagini/",
        language_url: 'js/tinymce/js/tinymce/langs/it.js' ,
        //preview_styles: true,
        menu: {
            file: {title: 'File', items: 'newdocument'},
            edit: {title: 'Modifica', items: 'undo redo | cut copy paste pastetext | selectall'},
            insert: {title: 'Inserisci', items: 'link media | template hr'},
            view: {title: 'Vedi', items: 'visualaid preview visualblocks'},
            format: {
                title: 'Formato',
                items: 'bold italic underline strikethrough superscript subscript | formats | removeformat'
            },
            table: {title: 'Tabella', items: 'inserttable tableprops deletetable | cell row column'},
            tools: {title: 'Tools', items: 'spellchecker code'},
            style_formats: {title: 'Coustom Menu', items: 'button'}
        },
        style_formats: [
            {title: 'Custom Menu', items: [
                {title : 'Button', selector : 'a', classes : 'button'}
            ]}
        ]
    });


    var BtnCopia = document.querySelector('#url');

    BtnCopia.addEventListener('click', function(event) {
        var copioMess = document.querySelector('.copia-testo-js');
        copioMess.select();

        try {
            var successo = document.execCommand('copy');
            var msg = successo ? 'successo' : 'insuccesso';
            console.log('Copia del testo avvenuta con ' + msg);
        } catch (err) {
            console.log('Oops, impossibile effettuare la copia');
        }
    });




    $(function(){
        var keyStop = {
            8: ":not(input:text, textarea, input:file, input:password)", // stop backspace = back
            13: "input:text, input:password", // stop enter = submit

            end: null
        };
        $(document).bind("keydown", function(event){
            var selector = keyStop[event.which];

            if(selector !== undefined && $(event.target).is(selector)) {
                event.preventDefault(); //stop event
            }
            return true;
        });
    });
</script>
