<?php
/**
 * Created da Giuseppe Alessandro De Blasio.
 * User: Giuseppe Alessandro De Blasio
 * Date: 20/10/16
 * Time: 10:15
 */

?>

<?php include 'includes/header.php'; ?>
<?php if(!$sessione->loggato()){ redirect('accedi.php');} ?>

<!-- Barra di Navigazione -->
<?php include ("includes/navbar.php") ?>

<?php include ("includes/aside.php") ?>
<!-- End aside-->
<?php



if(isset($_POST['crea_nazione'])){

    //$aggiungo_centro = new Centro();
    //$aggiungo_nazione = NazioniPage::seleziona_per_id($_POST['id_centro']);

    $aggiungo_nazione = new NazioniPage();

    if($aggiungo_nazione){

        $aggiungo_nazione->desc_titolo = $_POST['titolo_nazione'];;
//        $aggiungo_nazione->descrizione = $_POST['desc_naz'];
        $aggiungo_nazione->titBox1 = $_POST['titolo-box-1'];
        $aggiungo_nazione->box1 = $_POST['box-1'];
        $aggiungo_nazione->titBox2 = $_POST['titolo-box-2'];
        $aggiungo_nazione->box2 = $_POST['box-2'];
        $aggiungo_nazione->titolo_desc = $_POST['titolo_box_nazioni'];
//        $aggiungo_nazione->nazioneUrl = $_POST['url'];
        $cat = CategoriePagine::seleziona_per_id($_POST['categorie']);
        $categoria = $cat->urlFriendly;
        $naz = Nazioni::seleziona_per_id($_POST['nazione']);
        $nazione = $naz->urlFriendly;
        $aggiungo_nazione->nazioneUrl = "/".$categoria."/".$nazione;
        $aggiungo_nazione->stato = $_POST['stato'];
        $aggiungo_nazione->id_categoria = $_POST['categorie'];
        $aggiungo_nazione->id_nazione = $_POST['nazione'];
        $aggiungo_nazione->img_alt = $_POST['img_alt'];
        $aggiungo_nazione->img_title = $_POST['img_title'];
        $aggiungo_nazione->metaTitle = $_POST['meta_title'];
        $aggiungo_nazione->metaDescription = $_POST['meta_description'];
        $aggiungo_nazione->canonicalUrl = $_POST['canonical'];
        $aggiungo_nazione->metaIndex = $_POST['selezione_index'];
        $aggiungo_nazione->id_citta = 1;
        //$aggiungo_nazione->image = $_POST[$_FILES['file']];
        $aggiungo_nazione->impostazione_file_nazione($_FILES['file']);
        $aggiungo_nazione->caricamento_foto_nazioni();
//        $aggiungo_nazione->salva();
        $aggiungo_nazione->esiste_nazione_e_categoria("{$_POST['categorie']}", "{$_POST['nazione']}");
       //redirect('elenco_centri.php');

    }
}

/*$messaggio = "";

if(isset($_FILES['file'])){

    $immagine = new Image();
    $immagine->impostazione_file_foto($_FILES['file']);
    $immagine->data_immagine = date("Y-m-d");
    if($immagine->salva_immagine_post()){

        $messaggio = "Immagine caricata Correttamente";
    } else
    {
        $messaggio = join("<br>", $immagine->errore);
    }

}*/

//$inserisco_centro = new Centro();
//$inserisco_carat = new caratCentro();

/*if(isset($_POST['ins_nuovo_centr'])){
    if($inserisco_centro) {
        $inserisco_centro->Centro = $_POST['nome_nuovo_centro'];
    }
}
$ultimo_id = $inserisco_centro->id = $_SESSION['ultimo_id'];

if(isset($_POST['crea_pagina'])){
    if($inserisco_centro) {
        $inserisco_centro->Centro = $_POST['nome_centro'];
        $inserisco_carat->id_centro = $inserisco_centro->id;
        $inserisco_centro->salva();
    }
}*/
$seleziona_nazione = Nazioni::seleziona_tutti();
//$selziona_citta = CittaPage::seleziona_tutti();
//$selziona_nazione = Nazioni::seleziona_tutti();
$utente_ses = Utente::seleziona_per_id($_SESSION['id_utente']);
//$sel_car_centr = caratCentro::seleziona_per_campo_custom('id_centro', 0);



?>


<section class="main-content">
    <h1 class="page-header">Aggiungi Nazione <?php echo isset($_SESSION['ultimo_id']) ?></h1>
    <?php if($messaggio) :?>
        <div class="row">
            <div class="col-md-12">
                <div class="alert <?php echo ($messaggio == "Immagine caricata Correttamente") ? 'alert-success' : 'alert-danger' ?> alert-dismissable">
                    <button type="button" data-dismiss="alert" aria-hidden="true" class="close">×</button>
                    <?php  echo "<b class='dz-success'>" . $messaggio . "</b>"; ?>.
                </div>
            </div>
        </div>
    <?php endif; ?>
    <div class="panel-body">
        <form method="POST" action="" class="form-horizontal" enctype="multipart/form-data" data-parsley-validate>
            <div class="row">
                <div class="col-md-12">
                    <div class="col-md-8">
                        <input type="hidden" name="id_centro" id="id_centro" value="">
                        <div class="form-group">
                            <label for="nome_centro" class="control-label">Titolo Nazione H2</label>
                            <textarea name="titolo_nazione" id="nome_ritorno" class="form-control"  placeholder="Inserisci Titolo Nazione" required></textarea>
                        </div>
<!--                        <div class="form-group">-->
<!--                            <label for="nome_centro" class="control-label">Descrizione Nazione</label>-->
<!--                            <textarea name="desc_naz" id="desc_naz" class="form-control"  placeholder="Inserisci Descrizione Nazione" required></textarea>-->
<!--                        </div>-->
                        <div class="form-group">
                            <label for="titolo-box-1" class="control-label">Titolo Box 1</label>
                            <input type="text" id="titolo-box-1" class="form-control" name="titolo-box-1" />
                        </div>
                        <div class="form-group">
                            <label for="box-1" class="control-label">Box 1</label>
                            <textarea id="box-1" name="box-1"></textarea>
                        </div>
                        <div class="form-group">
                            <label for="titolo-box-2" class="control-label">Titolo Box 2</label>
                            <input type="text" id="titolo-box-2" class="form-control" name="titolo-box-2" />
                        </div>
                        <div class="form-group">
                            <label for="box-2" class="control-label">Box 2</label>
                            <textarea id="box-2" name="box-2"></textarea>
                        </div>
                        <div class="form-group">
                            <label for="nome_centro" class="control-label">Titolo Box Nazioni</label>
                            <input type="text" name="titolo_box_nazioni" id="titolo_box_nazioni" class="form-control" value="" placeholder="Inserisci Titolo Box Nazioni" required>
                        </div>
<!--                        <div class="form-group">-->
<!--                            <label for="url" class="control-label">Nazioni URL</label>-->
<!--                            <input type="url" name="url" id="url" class="form-control" value="" placeholder="Inserisci Url" required>-->
<!--                        </div>-->







                    </div>
                    <div class="pull-right col-md-4">
                        <div class="panel panel-default">
                            <div class="panel-heading"><i style="color: black;" class=""></i> Pubblica</div>
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-md-10">
                                        <div class="col-md-6">
                                            <p><i class="fa fa-hand-o-right"></i> Stato: <b>Non Pubblicato</p>
                                        </div>
                                        <div class="col-md-2 pull-right">
                                            <button name="crea_nazione" id="creaNazione" class="btn btn-oval btn-info">Crea Nazione</button>
                                        </div>
                                    </div>
                                </div>

                            </div> <!-- panel-body -->
                        </div> <!-- panel panel-default -->


                        <div class="panel panel-default">
                            <div class="panel-heading"><i style="color: #009ab8" class="fa fa-users"></i> Autore</div>
                            <div class="panel-body">
                                <!-- <label>Utente</label> -->

                                <select name="autore" class="form-control">
                                    <?php if($utente_ses->ruolo == 'admin'): ?>
                                        <?php $utenti = Utente::seleziona_tutti(); ?>
                                        <?php foreach($utenti as $utente): ?>
                                            <option <?php echo ($utente->username == $utente_ses->username)? 'selected="selected"' : ""?> ><?php echo $utente->username; ?></option>
                                        <?php endforeach; ?>
                                    <?php endif; ?>
                                    <?php if($utente_ses->ruolo != 'admin'): ?>
                                        <option><?php echo $utente_ses->username; ?></option>
                                    <?php endif; ?>
                                </select>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading"><i style="color: red;" class="fa fa-exclamation-triangle"></i> Stato</div>
                            <div class="panel-body">
                                <!-- <label>Stato</label> -->
                                <select name="stato" class="form-control" required>
                                    <option value="">Seleziona Stato</option>
                                    <option value="pubblicato">Pubblicato</option>
                                    <option value="bozza">Bozza</option>
                                </select>
                            </div> <!-- panel-body -->
                        </div> <!-- panel panel-default -->

                        <div class="panel panel-default">
                            <div class="panel-heading"><i class="fa fa-folder-o"></i> Categorie</div>
                            <div class="panel-body">
                                <select name="categorie" id="catAjax" onchange="cat_select(this.value);" class="form-control" required>
                                    <?php $pagine = CategoriePagine::seleziona_tutti(); ?>
                                    <option value="">Seleziona Categoria</option>
                                    <?php if(isset($_GET['cat'])): ?>
                                        <?php $catSelect = CategoriePagine::seleziona_per_id($_GET['cat']); ?>
                                        <option selected="selected" value="<?php echo $catSelect->id; ?>"><?php echo $catSelect->titolo_categoria; ?></option>
                                    <?php else: ?>
                                        <?php foreach($pagine as $chiave => $pagina): ?>
                                            <option value="<?php echo $pagina->id; ?>"><?php echo $pagina->titolo_categoria; ?></option>
                                        <?php endforeach; ?>
                                    <?php endif; ?>
                                </select>
                            </div>
                        </div>

                        <div class="panel panel-default">
                            <div class="panel-heading"><i style="color: #1e8449;" class="fa fa-globe"></i> Seleziona Nazione</div>
                            <div class="panel-body">
                                <select name="nazione" id="nazAjax" onchange="naz_select(this.value);" class="form-control" required>
                                    <option value="">Seleziona Nazione</option>
                                    <?php foreach ($seleziona_nazione as $nazione): ?>
                                        <?php if($nazione->stato == 1): ?>
                                            <option value="<?php echo $nazione->id?>"><?php echo $nazione->nazione?></option>
                                        <?php endif; ?>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                        </div>

                        <!-- <div class="panel panel-default">
                            <div class="panel-heading"><i style="color: #1e8449;" class="fa fa-building-o"></i> Seleziona Città</div>
                            <div class="panel-body">
                                <select name="citta" class="form-control" required>
                                    <option value="">Seleziona Città</option>
                                    <?php // foreach ($selziona_citta as $citta): ?>
                                        <?php // if($citta->stato == "pubblicato"): ?>
                                            <option value="<?php // echo $citta->id?>"><?php // echo $citta->desc_titolo; ?></option>
                                        <?php // endif; ?>
                                    <?php // endforeach; ?>
                                </select>
                            </div>
                        </div> -->


                        <div class="panel panel-default">
                            <div class="panel-heading">Immagine in Evidenza</div>
                            <div class="panel-body">
                                <div class="text-center form-group">
                                    <img class="img-thumbnail img-responsive" style="width: 350px; height: 350px;" src="https://placeholdit.imgix.net/~text?txtsize=56&txt=350%C3%97350&w=350&h=350" alt="<?php //echo $aggiungo_nazione->image; ?>" />
                                    <br>
                                </div>
                                <div class="col-sm-12 form-group">
                                    <input type="file" name="file" class="form-control">
                                </div>
                                <div class="col-sm-12 btn btn-info"><h4><i class="fa fa-cut"><strong>  &ensp; Larghezza: 400px - Altezza: 267px </strong> </i></h4></div>
                            </div>
                        </div>

                        <div class="panel panel-default">
                            <div class="panel-heading"><i style="color: #e80079" class="fa fa-picture-o"></i> <b>Image Alt</b></div>
                            <div class="panel-body">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <div class="col-sm-12">
                                            <input maxlength="80" onkeyup="contaCaratteriTitle(this)" name="img_alt" type="text" class="form-control">
                                            <div id="meta_title_caratteri"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="panel panel-default">
                            <div class="panel-heading"><i style="color: #e80079" class="fa fa-picture-o"></i> <b>Image Title</b></div>
                            <div class="panel-body">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <div class="col-sm-12">
                                            <input maxlength="80" onkeyup="contaCaratteriTitle(this)" name="img_title" type="text" class="form-control">
                                            <div id="meta_title_caratteri"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="panel panel-default">
                            <div class="panel-heading"><i style="color: #e80079" class="fa fa-header"></i> <b>Meta Title</b></div>
                            <div class="panel-body">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <div class="col-sm-12">
                                            <input maxlength="80" onkeyup="contaCaratteriTitle(this)" name="meta_title" type="text" class="form-control">
                                            <div id="meta_title_caratteri"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading"><i style="color: #CBE86B" class="fa fa fa-pencil"></i> <b>Meta Description</b></div>
                            <div class="panel-body">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <div class="col-sm-12">
                                            <input maxlength="160" onkeyup="contaCaratteriDescrizione(this)" name="meta_description" type="text" class="form-control">
                                            <div id="meta_descrizione_caratteri"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading"><i style="color: #0A246A" class="fa fa-globe"></i> <b>Canonical Url</b></div>
                            <div class="panel-body">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <div class="col-sm-12">
                                            <input name="canonical" type="url" value="" class="form-control">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading"><i style="color: #8e79e8 class="fa fa-indent"></i> <b>Meta Robots Index</b></div>
                            <div class="panel-body">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <div class="col-sm-12">
                                            <select name="selezione_index" id="selezione_index" class="form-control">
                                                <option value="index">Index</option>
                                                <option value="noindex">NoIndex</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>


                    </div> <!-- pull-right col-md-4 -->
                </div> <!-- col-md-12 -->
            </div>  <!-- row -->
        </form>
    </div> <!-- panel-body -->


<div id="errorNazCat"></div>


</section>



<?php include 'includes/footer.php'; ?>


<script type="text/javascript" xmlns="http://www.w3.org/1999/html">
    tinymce.init({
        //selector: '#desc_carat',
        selector: "textarea",
        setup: function (editor) {
            editor.on('change', function () {
                editor.save();
            });
            editor.addButton('classe', {
                text: 'Classe Personalizzata',
                icon: false,
                onclick: function () {
//                    editor.getNode()({title : 'Classe Personalizzata', selector : 'h1', classes : 'titolo-pagina-catalogo'});
                    tinyMCE.activeEditor.dom.addClass(tinyMCE.activeEditor.selection.getNode(), 'titolo-pagina-catalogo');
                }
            })
        },
        theme: 'modern',
//        width: 1100,
        height: 200,
        language: 'it',
        plugins: [
            "advlist autolink lists link image charmap print preview anchor",
            "searchreplace visualblocks code fullscreen",
            "insertdatetime media table contextmenu paste imagetools wordcount"
        ],
        toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | preview" +
        "code | classe",
        images_upload_url: 'upload.php',
        images_upload_base_path: '/admin/immagini',
        media_live_embeds: true,
        style_formats_merge: true,
        menubar: true,
        image_caption: true,
        /*file_browser_callback: function(field_name, url, type, win) {
         if(type=='image') $('#my_form input').click();
         },*/
        image_prepend_url: "http://localhost/mla-cms/admin/immagini/",
        language_url: 'js/tinymce/js/tinymce/langs/it.js' ,
        //preview_styles: true,
        menu: {
            file: {title: 'File', items: 'newdocument'},
            edit: {title: 'Modifica', items: 'undo redo | cut copy paste pastetext | selectall'},
            insert: {title: 'Inserisci', items: 'link media | template hr'},
            view: {title: 'Vedi', items: 'visualaid preview visualblocks'},
            format: {
                title: 'Formato',
                items: 'bold italic underline strikethrough superscript subscript | formats | removeformat'
            },
            table: {title: 'Tabella', items: 'inserttable tableprops deletetable | cell row column'},
            tools: {title: 'Tools', items: 'spellchecker code'},
            style_formats: [
                {
                    title: 'Classe Personalizzata',
                    items:
                        [
                            {
                                title : 'Classe Personalizzata', selector : 'h1', classes : 'titolo-pagina-catalogo'
                            }
                        ]
                }
            ]
        },
        content_css: [
            'css/style.css'],
        style_formats: [
            {
                title: 'Classe Personalizzata',
                items:
                    [
                        {
                            title : 'Classe Personalizzata', selector : 'h1', classes : 'titolo-pagina-catalogo'
                        }
                    ]
            }
        ]
    });


    var catSel;
    var nazSel;

    var getUrlParameter = function getUrlParameter(sParam) {
        var sPageURL = decodeURIComponent(window.location.search.substring(1)),
            sURLVariables = sPageURL.split('&'),
            sParameterName,
            i;

        for (i = 0; i < sURLVariables.length; i++) {
            sParameterName = sURLVariables[i].split('=');

            if (sParameterName[0] === sParam) {
                return sParameterName[1] === undefined ? true : sParameterName[1];
            }
        }
    };

    catSel = getUrlParameter('cat'); // Verifico se c'è il get nell'url

    function cat_select(val){
        catSel = val;
        console.log(catSel);
        $('#creaNazione').removeAttr('disabled');
    };

    function naz_select(val) {
        nazSel = val;
        console.log(nazSel);
        $('#creaNazione').removeAttr('disabled');
    };


    $(function() {
        $('#catAjax').change(function() {
            $('#creaNazione').removeAttr('disabled');
            $.ajax({
                url: 'verificaNazECat.php',
                method: 'GET',
                data: {nazione:nazSel, categoria:catSel},
                success: function (data) {
//                    console.log(data);
                    console.log(data.length);
                    $('#errorNazCat').html(data);
                    if(data.length== 466){
                        $('#creaNazione').attr('disabled', true);
                    }
                }
            });
        });

        $('#nazAjax').change(function() {
            $('#creaNazione').removeAttr('disabled');
            $.ajax({
                url: 'verificaNazECat.php',
                method: 'GET',
                data: {nazione:nazSel, categoria:catSel},
                success: function (data) {
//                    console.log(data);
                    $('#errorNazCat').html(data);
                    console.log(data.length);
                    if(data.length == 466){
                        $('#creaNazione').attr('disabled', true);
                    }
                }
            });
        });
    });


//    catSel = getUrlParameter('cat');
//
//    if(catSel){
//        $(document).ready(function () {
//
//            var catId = $('#catAjax').val();
//            $('#nazAjax option').remove();
//            $('#nazAjax').append($('<option>', {
//                value: '',
//                text: 'Seleziona Nazione'
//            }));
////        console.log(catId);
//
//            $.ajax({
//                url: "includes/api.php/nazione_page?&filter=id_categoria,eq," + catId +"&transform=1",
//                type: "GET",
//                dataType: 'json',
//                headers: {
//                    'Content-type': 'application/json',
//                    'Access-Control-Allow-Headers': 'Content-Type',
//                    'Access-Control-Allow-Origin': '*',
//                    'X-XSRF-TOKEN': '<?php //echo (isset($_COOKIE['XSRF-TOKEN'])) ? $_COOKIE['XSRF-TOKEN'] : ''?>//'
//                },
//                success: function (data) {
//                    if(data) {
////                    console.log(data);
////                    $('#urlFirendly').val(urlFre);
////                        console.log(data.nazioni);
//                        var nazArr = [];
//
//                        $.each(data.nazione_page, function (i, val) {
////                        console.log(i);
////                        console.log(val);
//                            var nazid;
//                            nazid = val.id_nazione;
////                        console.log(nazid);
//                            nazArr.push(nazid);
//                        });
//                        console.log(nazArr);
//
//
//                        $.ajax({
//                            url: "includes/api.php/nazioni/" + nazArr + "&transform=1",
//                            type: "GET",
//                            dataType: 'json',
//                            headers: {
//                                'Content-type': 'application/json',
//                                'Access-Control-Allow-Headers': 'Content-Type',
//                                'Access-Control-Allow-Origin': '*',
//                                'X-XSRF-TOKEN': '<?php //echo (isset($_COOKIE['XSRF-TOKEN'])) ? $_COOKIE['XSRF-TOKEN'] : ''?>//'
//                            },
//                            success: function (data) {
////                            console.log(data);
//
//                                if (data.length > 1) {
////
//
//                                    $.each(data, function (i, val) {
//                                        console.log(val.nazione);
//                                        $('#nazAjax').append($('<option>', {
//                                            value: val.id,
//                                            text: val.nazione
//                                        }));
//                                    });
//                                } else {
//                                    $('#nazAjax').append($('<option>', {
//                                        value: data.id,
//                                        text: data.nazione
//                                    }));
//                                }
//                            }
//                        });
////                    });
//                    }
//
//                }
//            });
//        });
//    }


//    $('#catAjax').on('change', function () {
//
//        var idCat = $(this).val();
//        console.log(idCat);
//        $('#nazAjax option').remove();
//        $('#nazAjax').append($('<option>', {
//            value: '',
//            text: 'Seleziona Nazione'
//        }));
//        $('#cittAjax option').remove();
//        $('#cittAjax').append($('<option>', {
//            value: '',
//            text: 'Seleziona Città'
//        }));
//
//
//        $.ajax({
//            url: "includes/api.php/nazione_page?&filter=id_categoria,eq," + idCat +"&transform=1",
//            type: "GET",
//            dataType: 'json',
//            headers: {
//                'Content-type': 'application/json',
//                'Access-Control-Allow-Headers': 'Content-Type',
//                'Access-Control-Allow-Origin': '*',
//                'X-XSRF-TOKEN': '<?php //echo (isset($_COOKIE['XSRF-TOKEN'])) ? $_COOKIE['XSRF-TOKEN'] : ''?>//'
//            },
//            success: function (data) {
//                if(data) {
////                    console.log(data);
//
//                    var nazArr = [];
//
//                    $.each(data.nazione_page, function (i, val) {
////                        console.log(i);
////                        console.log(val);
//                        var nazid;
//                        nazid = val.id_nazione;
////                        console.log(nazid);
//                        nazArr.push(nazid);
//                    });
//                    console.log(nazArr);
//
//                    $.ajax({
//                        url: "includes/api.php/nazioni/" + nazArr + "&transform=1",
//                        type: "GET",
//                        dataType: 'json',
//                        headers: {
//                            'Content-type': 'application/json',
//                            'Access-Control-Allow-Headers': 'Content-Type',
//                            'Access-Control-Allow-Origin': '*',
//                            'X-XSRF-TOKEN': '<?php //echo (isset($_COOKIE['XSRF-TOKEN'])) ? $_COOKIE['XSRF-TOKEN'] : ''?>//'
//                        },
//                        success: function (data) {
////                            console.log(data);
//
//                            if (data.length > 1) {
////
//                                $.each(data, function (i, val) {
////                                    console.log(val.nazione);
//                                    $('#nazAjax').append($('<option>', {
//                                        value: val.id,
//                                        text: val.nazione
//                                    }));
//                                });
//                            } else {
//                                $('#nazAjax').append($('<option>', {
//                                    value: data.id,
//                                    text: data.nazione
//                                }));
//                            }
//                        }
//                    });
////                    });
//                }
//
//            }
//        })
//
//    });



</script>
