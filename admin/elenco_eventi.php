<?php
/**
 * Created da Giuseppe Alessandro De Blasio.
 * User: Giuseppe Alessandro De Blasio
 * Date: 16/03/17
 * Time: 09:42
 */
?>
<?php include 'includes/header.php'; ?>
<?php if(!$sessione->loggato()){ redirect('accedi.php');} ?>

<!-- Barra di Navigazione -->
<?php include ("includes/navbar.php") ?>

<?php include ("includes/aside.php") ?>

<?php

$eventi = Eventi::seleziona_tutti();
$cat_eventi = categoriaEventi::seleziona_tutti();
$nuovo_evento = new Eventi();

if (isset($_POST['nuovo_evento'])):

    $nuovo_evento->data_inizio = data_db($_POST['inizio_evento']);
    $nuovo_evento->data_fine = data_db($_POST['fine_evento']);
    $nuovo_evento->localita = $_POST['localita'];
    $nuovo_evento->id_cat_eventi = $_POST['catEvento'];
    $nuovo_evento->breve_descrizione = $_POST['breveDesc'];
    $nuovo_evento->descrizione = $_POST['descrizione'];
//$nuovo_evento->img_evento = $_FILES['img_evento'];
    $nuovo_evento->impostazione_file_eventi($_FILES['img_evento']);
    $nuovo_evento->caricamento_foto_eventi();
    $nuovo_evento->img_alt = $_POST['alt_evento'];
    $nuovo_evento->link_easy = "https://services.mlaworld.com/infodays/form.aspx?sede=".$_POST['url_easy'];

//    $nuovo_evento->url_frendly = rewriteUrl()
    $nuovo_evento->salva();
//var_dump($nuovo_evento);
    redirect('elenco_eventi.php?agg=1');
endif;

if(isset($_GET['agg']) AND $_GET['agg'] == 1):
    successoMessaggio('Evento Aggiunto Correttamente', 'Inserimento Evento Avvenuto con Successo');
endif;


if(isset($_GET['id'])):
    $elimina_evento = Eventi::seleziona_per_id($_GET['id']);
    $elimina_evento->cancella();
    redirect('elenco_eventi.php');
endif;


if(isset($_POST['aggiorna_evento'])):

    $agg_evento = Eventi::seleziona_per_id($_POST['idEventoMod']);
    $agg_evento->data_inizio = data_db($_POST['inizio_evento']);
    $agg_evento->data_fine = data_db($_POST['fine_evento']);
    $agg_evento->localita = $_POST['localita'];
    $agg_evento->id_cat_eventi = $_POST['catEvento'];
    $agg_evento->breve_descrizione = $_POST['breveDesc'];
    $agg_evento->descrizione = $_POST['descrizione'];
    if(!empty($_FILES['img_evento_mod'])):
        $agg_evento->impostazione_file_eventi($_FILES['img_evento_mod']);
        $agg_evento->caricamento_foto_eventi();
    endif;
    $agg_evento->img_alt = $_POST['alt_image'];
    $agg_evento->link_easy = "https://services.mlaworld.com/infodays/form.aspx?sede=".$_POST['url_easy'];
    $agg_evento->salva();

    redirect('elenco_eventi.php?mod=1');
endif;

if(isset($_GET['mod']) AND $_GET['mod'] == 1):
    successoMessaggio('Evento Modificato Correttamente', 'Modifica Evento Avvenuto con Successo');
endif;

?>


<br>
<section class="main-content">
    <button type="button" class="btn btn-labeled btn-primary pull-right" data-toggle="modal" data-target="#aggiungiEvento">
        <span class="btn-label"><i class="fa fa-plus-circle"></i></span>Aggiungi Eventi
    </button>

    <h1 class="page-header">Elenco Eventi</h1>

    <div class="panel panel-default">
        <div class="panel-heading">Elenco Promozioni
            <!--            <a title="" class="pull-right" href="javascript:void(0);" data-original-title="Collapse Panel" data-toggle="tooltip" data-perform="panel-collapse">-->
            <!--                <em class="fa fa-minus"></em>-->
            <!--            </a>-->
        </div> <!-- panel-heading -->



        <!--            <div class="panel-wrapper collapse in" style="height: auto;">-->
        <div class="panel-body">
            <div class="table-responsive">
                <hr>
                <div class="form-group">
                    <label class="control-label">Filtra Stato
                        <div class="col-sm-12">
                            <label class="checkbox-inline c-checkbox">
                                <input onchange="filtroStato()" id="inProgrammazione" name="stato" type="checkbox" value="In Programmazione">
                                <span class="fa fa-check"></span>In Programmazione
                            </label>
                            <label class="checkbox-inline c-checkbox">
                                <input onchange="filtroStato()" id="inCorso" name="stato" type="checkbox" value="In Corso">
                                <span class="fa fa-check"></span>In Corso
                            </label>
                            <label class="checkbox-inline c-checkbox">
                                <input onchange="filtroStato()" id="inScadenza" name="stato" type="checkbox" value="In Scadenza">
                                <span class="fa fa-check"></span>In Scadenza
                            </label>
                            <label class="checkbox-inline c-checkbox">
                                <input  onchange="filtroStato()" id="scadenza" name="stato" type="checkbox" value="Scaduto">
                                <span class="fa fa-check"></span>Scaduto</label>
                        </div>
                    </label>
                </div>
                <hr>
                <br>
            </div>
            <table id="tabEventi" class="table table-bordered table-hover">
                <thead>
                <tr>
                    <th>Primo Piano</th>
                    <th>#id</th>
                    <th>Data Inizio</th>
                    <th>Data Fine</th>
                    <th>Località</th>
                    <th>Categoria Evento</th>
                    <th>Breve Descrizione</th>
                    <th>Immagine</th>
                    <th>Stato</th>
                    <th>Modifica</th>
                    <th>Elimina</th>
                </tr>
                </thead>
                <tbody>

                <?php foreach ($eventi as $evento): ?>
                    <tr>
                        <td class="text-center"><i id="stars-<?php echo $evento->id; ?>" class="fa fa-star" data-stars="<?php echo $evento->stars; ?>" data-id="<?php echo $evento->id; ?>" onclick="starsState(this)" <?php echo ($evento->stars == 1)? 'style="color:#EBD300;font-size:30px"' : 'style="font-size:30px"'; ?>></i></td>
                        <td><?php echo $evento->id; ?></td>
                        <td><?php echo data_it($evento->data_inizio); ?></td>
                        <td><?php echo data_it($evento->data_fine); ?></td>
                        <td><?php echo $evento->localita; ?></td>
                        <td><?php $cat_evento_id = categoriaEventi::seleziona_per_id($evento->id_cat_eventi); echo $cat_evento_id->titolo_categoria; ?></td>
                        <td><?php echo $evento->breve_descrizione; ?></td>
                        <td><img style="width: 80px; height: 80px;" src="<?php echo $evento->percorso_immagine_e_plecholder_back(); ?>" alt="<?php echo $evento->img_alt; ?>"></td>
                        <td>
                        <?php
                        $data_attuale = new DateTime();
                        $dataPresa = $data_attuale->format('Y-m-d');
                            switch ($dataPresa){
                                case $evento->data_inizio > $dataPresa:
                                    echo '<div class="label label-info pull-center">In Programmazione</div>'.
                                        '</div><input type="hidden" name="stato" value="In Programmazione" >';

                                    break;
                                case $evento->data_inizio <=  $dataPresa AND $evento->data_fine > $dataPresa :
                                    echo '<div class="label label-success pull-center" >In Corso</div>'.
                                        '</div><input type="hidden" name="stato" value="In Corso">';
                                    break;
                                case $evento->data_fine == $dataPresa:
                                    echo '<div class="label label-warning pull-center" >In Scadenza</div>'.
                                        '</div><input type="hidden" name="stato" value="In Scadenza">';
                                    break;
                                case  $evento->data_fine < $dataPresa:
                                    echo '<div class="label label-danger pull-center">Scaduto</div>'.
                                        '</div><input type="hidden" name="stato" value="Scaduto">';
                                    break;

                            }
                        ?>
                        </td>
                        <td>
                            <a type="button" name="modEvento" href="#javascript(0)"
                               data-toggle="modal"
                               data-record-id="<?php echo $evento->id; ?>"
                               data-inizio-evento="<?php echo $evento->data_inizio; ?>"
                               data-fine-evento="<?php echo $evento->data_fine; ?>"
                               data-localita="<?php echo $evento->localita; ?>"
                               data-id-cat-evento="<?php echo $evento->id_cat_eventi; ?>"
                               data-breve-descr="<?php echo $evento->breve_descrizione; ?>"
                               data-descrizione="<?php echo $evento->descrizione; ?>"
                               data-link-easy="<?php $url = parse_url($evento->link_easy); echo substr($url['query'],strpos($url['query'], "=") +1); ?>"
                               data-alt-img="<?php echo $evento->img_alt; ?>"
                               data-target="#modEventi">
                                <i class="fa fa-edit"></i>Modifica
                            </a>
                        </td>
                        <td>
                            <a href="#" class="delEvento" data-record-id="<?php echo $evento->id; ?>"  data-record-titolo="<?php // echo $promozione->titolo; ?>"><i style="color: red;" class="fa fa-trash-o"> Elimina</i></a>
                        </td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>
        </div><!-- panel-footer -->
    </div><!-- table-responsive -->

</section>


<?php include 'includes/footer.php'; ?>


<div id="aggiungiEvento" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg">
        <form action="" method="post" enctype="multipart/form-data" data-parsley-validate>
            <div class="modal-content">
                <div class="modal-header"> <!-- modal header -->
                    <button type="button" class="close"
                            data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title">Aggiungi Nuovo Evento</h4>
                </div>
                <div class="modal-body"> <!-- modal body -->
                    <div class="row">
                        <div class="form-group col-md-6">
                            <label class="" for="">Data Inizio Evento</label>
                            <div id="eventoInizio" class="input-append date mb-lg">
                                <input data-format="dd/MM/yyyy" name="inizio_evento" type="text" class="form-control" required>
                                <span class="add-on">
<!--                                                        <span class="fa-calendar fa"></span>-->
                                                    </span>
                            </div>
                        </div>
                        <div class="form-group col-md-6">
                            <label class="" for="">Data Fine Evento</label>
                            <div id="eventoFine" class="input-append date">
                                <input data-format="dd/MM/yyyy" name="fine_evento" type="text" class="form-control" required>
                                <span class="add-on"></span>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-md-6">
                            <label class="" for="">Località</label>
                            <input type="text" name="localita" class="form-control" required>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="">Categoria Eventi</label>
                            <select name="catEvento" id="" class="form-control">
                                <option value="Seleziona Categoria">Seleziona Categoria</option>
                                <?php foreach ($cat_eventi as $cat_evento): ?>
                                    <option value="<?php echo $cat_evento->id; ?>"><?php echo $cat_evento->titolo_categoria; ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-md-6">
                            <label class="" for="">Breve Descrizione</label>
                            <textarea name="breveDesc" id="" cols="30" rows="2" class="form-control" required></textarea>
                        </div>
                        <div class="form-group col-md-6">
                            <label class="" for="">Descrizione</label>
                            <textarea name="descrizione" id="" cols="30" rows="10" class="form-control" required></textarea>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-md-6">
                            <label class="" for="">Immagine Evento</label>
                            <input type="file" name="img_evento" class="form-control">
                        </div>
                        <div class="form-group col-md-6">
                            <label class="" for="">Immagine Alt</label>
                            <input type="text" name="alt_evento" class="form-control">
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-md-6">
                            <label class="" for="">Link Easy</label>
                            <input type="text" name="url_easy" class="form-control" required>
                            <div><i style="color: #FF0000" class="fa fa-info-circle"></i> Inserire solo la località <b>il link verrà generato in automatico</b></div>
                        </div>
                    </div>
                </div><!-- modal-body -->
                <div class="modal-footer"> <!-- modal footer -->
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Cancella</button>
                    <button type="submit" name="nuovo_evento" class="btn btn-primary">Inserisci Nuovo Evento</button>
                </div>
            </div> <!-- / .modal-content -->
        </form>
    </div> <!-- / .modal-dialog -->
</div><!-- / .modal -->

<div id="modEventi" class="modal fade"  role="dialog">
    <div class="modal-dialog modal-lg">
        <form action="" method="post" enctype="multipart/form-data" data-parsley-validate>
            <div class="modal-content">
                <div class="modal-header"> <!-- modal header -->
                    <button type="button" class="close"
                            data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title">Aggiungi Nuovo Evento</h4>
                </div>
                <div class="modal-body"> <!-- modal body -->
                    <input type="hidden" name="idEventoMod" value="">
                    <div class="row">
                        <div class="form-group col-md-6">
                            <label class="" for="">Data Inizio Evento</label>
                            <div id="eventoInizioMod" class="input-append date mb-lg">
                                <input  name="inizio_evento" id="inizio_evento" type="text" value="" class="form-control" required />
                                <span class="add-on">
<!--                                                        <span class="fa-calendar fa"></span> data-format="dd/MM/yyyy"-->
                                </span>
                            </div>
                        </div>
                        <div class="form-group col-md-6">
                            <label class="" for="">Data Fine Evento</label>
                            <div id="eventoFineMod" class="input-append date">
                                <input  name="fine_evento" type="text" value="" class="form-control" required>
                                <span class="add-on"></span>
<!--                                data-format="dd/MM/yyyy"-->
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-md-6">
                            <label class="" for="">Località</label>
                            <input type="text" name="localita" class="form-control" required>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="">Categoria Eventi</label>
                            <select name="catEvento" id="" class="form-control">
                                <option value="Seleziona Categoria">Seleziona Categoria</option>
                                <?php foreach ($cat_eventi as $cat_evento): ?>
                                    <option value="<?php echo $cat_evento->id; ?>"><?php echo $cat_evento->titolo_categoria; ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-md-6">
                            <label class="" for="">Breve Descrizione</label>
                            <textarea name="breveDesc" val="" id="" cols="30" rows="2" class="form-control" required></textarea>
                        </div>
                        <div class="form-group col-md-6">
                            <label class="" for="">Descrizione</label>
                            <textarea name="descrizione" id="" cols="30" rows="10" class="form-control" required></textarea>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-md-6">
                            <label class="" for="">Immagine Evento</label>
                            <input type="file" name="img_evento_mod" class="form-control">
                        </div>
                        <div class="form-group col-md-6">
                            <label class="" for="">Immagine Alt</label>
                            <input type="text" name="alt_image" class="form-control">
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-md-6">
                            <label class="" for="">Link Easy</label>
                            <input type="text" name="url_easy" class="form-control" required>
                            <div><i style="color: #FF0000" class="fa fa-info-circle"></i> Inserire solo la località <b>il link verrà generato in automatico</b></div>
                        </div>
                    </div>
                </div><!-- modal-body -->
                <div class="modal-footer"> <!-- modal footer -->
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Cancella</button>
                    <button type="submit" name="aggiorna_evento" class="btn btn-primary">Aggiorna Evento</button>
                </div>
            </div> <!-- / .modal-content -->
        </form>
    </div> <!-- / .modal-dialog -->
</div><!-- / .modal -->

<script>



    $('#eventoInizio').datetimepicker({
        language: 'it',
        pickTime: false
    });

    $('#eventoFine').datetimepicker({
        language: 'it',
        pickTime: false
    });


    function starsState(data){
        var dataId = $(data).data("id");
        var starsBool = $(data).data("stars");

        if(starsBool === 0){
            starsBool = 1;
        } else if (starsBool === 1){
            starsBool = 0;
        }

//        var starsResult = $(data).data("stars");



//        alert(dataId);
//        alert(starsBool);

        $.ajax({
            url: 'includes/api.php/eventi/' + dataId ,
            type: 'PUT',
            data: {stars : starsBool },
            headers: {
                'Content-type': 'application/json',
                'Access-Control-Allow-Headers': 'Content-Type',
                'Access-Control-Allow-Origin': '*',
                'X-XSRF-TOKEN': '<?php echo (isset($_COOKIE['XSRF-TOKEN']))? $_COOKIE['XSRF-TOKEN'] : ''?>'
            },
            dataType: 'json',
            async: true,
            success: function (data) {
//                var starsResult;
//                console.log('valore stars ' + starsResult);
//                    console.log(data);
                if(starsBool === 1){
                    $('#stars-'+dataId).css({"color":"#EBD300"});
                    $('#stars-'+dataId).data('stars', starsBool);
                } else if (starsBool === 0){
                    $('#stars-'+dataId).css({"color":"#656565"});
                    $('#stars-'+dataId).data('stars', starsBool);
                }

            }
        });
    }


    var TabEventi = $('#tabEventi').dataTable({
            "language": {
                "url": "js/datatables/traduzione/Italian.json"
            },
        columnDefs: [{
            orderable: false,
//            className: 'select-checkbox',
            targets:   0
        }],
        select: {
//            style:    'os',
            selector: 'td:first-child'
        },
        order: [[ 1, 'asc' ]]
    });

    function filtroStato() {
        var stato= $('input:checkbox[name="stato"]:checked').map(function() {
            return '^' + this.value + '\$';
        }).get().join('|');
        // variabile e valore column array
        TabEventi.fnFilter(stato, 8, true, false, false, false);
    }

    $(window).load(function () {
        var input = $('#inCorso');

        input.trigger( "click" );
    });

    $(document).ready(function () {

        $('.delEvento').on('click', function () {

//            var titolo = $(this).attr('data-record-titolo');

            var id = $(this).attr('data-record-id');

            swal({
                title: 'Eliminazione Evento',
                text: ' l\'evento sta per essere eliminato.',
                type: 'warning',
                showCancelButton: true,
                cancelButtonText: 'Annulla!',
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Sei sicuro di voler Cancellare l\'evento?'
            }).then(function () {
                swal(
                    'Cancellato!',
                    'l\'evento è stato cancellato con Successo.',
                    'success'
                );
                $.get("elenco_eventi.php", {id : id});
                $('.swal2-confirm').hide();
                setTimeout(function () {
                    window.location = 'elenco_eventi.php';
                }, 1500);
            });
        });
    });


    $(document).ready(function(){
        $('#modEventi').on('show.bs.modal', function(e) {
            var idEvento = $(e.relatedTarget).data('recordId');
            $(e.currentTarget).find('input[name="idEventoMod"]').val(idEvento);

            var dataInizioEvento = $(e.relatedTarget).data('inizioEvento');
            var d = dataInizioEvento.slice(0, 10).split('-');
            dataFineMod = d[2] +'/'+ d[1] +'/'+ d[0];
            $(e.currentTarget).find('input[name="inizio_evento"]').val(dataFineMod);

            var dataFineEvento = $(e.relatedTarget).data('fineEvento');
            var d = dataFineEvento.slice(0, 10).split('-');
            dataFineMod = d[2] +'/'+ d[1] +'/'+ d[0];
            $(e.currentTarget).find('input[name="fine_evento"]').val(dataFineMod);

            var localita = $(e.relatedTarget).data('localita');
            $(e.currentTarget).find('input[name="localita"]').val(localita);

            var catEventoId = $(e.relatedTarget).data('idCatEvento');
//            $(e.currentTarget).find('input[name="catEvento"]').val(catEventoId);
            $("select option").filter(function() {
                return $(this).val() == catEventoId;
            }).prop('selected', true);

            var breveDescr = $(e.relatedTarget).data('breveDescr');
            $(e.currentTarget).find('textarea[name="breveDesc"]').val(breveDescr);

            var descrizione = $(e.relatedTarget).data('descrizione');
            $(e.currentTarget).find('textarea[name="descrizione"]').val(descrizione);

            var alt_evento = $(e.relatedTarget).data('altImg');
            $(e.currentTarget).find('input[name="alt_image"]').val(alt_evento);

            var linkEasy = $(e.relatedTarget).data('linkEasy');
            $(e.currentTarget).find('input[name="url_easy"]').val(linkEasy);


//            var dateAttuale = new Date();
//            console.log(dateAttuale);

                $('#eventoInizioMod').datetimepicker({
                    language: 'it',
                    pickTime: false,
                    defaultDate: new Date(),
                    changeMonth: true,
                    numberOfMonths: 1,
                    minDate: new Date(),
                    onSelect: function (date) {
                        var date2 = $('#dt1').datepicker('getDate');
                        date2.setDate(date2.getDate() + 1);
                        $('#eventoFineMod').datepicker('setDate', date2);
                        //sets minDate to dt1 date + 1
                        $('#eventoFineMod').datepicker('option', 'minDate', date2);
                    }
//                onSelect: function(selectedDate) {
//                    var option = this.id == "inizio_evento" ? "minDate" : "maxDate",
//                        instance = $(this).data("datepicker"),
//                        date = $.datepicker.parseDate(instance.settings.dateFormat || $.datepicker._defaults.dateFormat, selectedDate, instance.settings);
//                    dataPrima.not(this).datepicker("option", option, date);
//                }
                });
                $('#eventoFineMod').datetimepicker({
                    language: 'it',
                    pickTime: false,
                    defaultDate: new Date(),
                    changeMonth: true,
                    numberOfMonths: 1,
                    minDate: new Date(),
                    onSelect: function (date) {
                        var date2 = $('#dt1').datepicker('getDate');
                        date2.setDate(date2.getDate() + 1);
                        $('#eventoFineMod').datepicker('setDate', date2);
                        //sets minDate to dt1 date + 1
                        $('#eventoFineMod').datepicker('option', 'minDate', date2);
                    }
//                onSelect: function(selectedDate) {
//                    var option = this.id == "inizio_evento" ? "minDate" : "maxDate",
//                        instance = $(this).data("datepicker"),
//                        date = $.datepicker.parseDate(instance.settings.dateFormat || $.datepicker._defaults.dateFormat, selectedDate, instance.settings);
//                    dataPrima.not(this).datepicker("option", option, date);
//                }
                });

                var oggi = $('.today');
                $(oggi).removeClass('disabled');
//                var attivo = $('.day').find('.today');
//                 $(attivo).removeClass('disabled');



//            $('#eventoFineMod').datetimepicker({
//                language: 'it',
//                pickTime: false
//            });




        });
    });
</script>