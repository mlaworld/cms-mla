<?php include 'includes/header.php'; ?>
<?php if(!$sessione->loggato()){ redirect('accedi.php');} ?>

<!-- Barra di Navigazione -->
<?php include ("includes/navbar.php") ?>

<?php include ("includes/aside.php") ?>
<!-- End aside-->

<?php include ("includes/aside_utenti.php") ?>
<?php

/**
 * Created by Giuseppe Alessandro De Blasio.
 * User: Giuseppe Alessandro De Blasio
 * Date: 20/09/2016
 * Time: 17:32
 */
$messaggio = "";

if(isset($_FILES['file'])){

    $immagine = new Image();
    $immagine->impostazione_file_foto($_FILES['file']);
    $immagine->data_immagine = date("Y-m-d");
    if($immagine->salva_immagine_post()){

        $messaggio = "Immagine caricata Correttamente";
    } elseif (isset($_POST['invia']))
    {
        $immagine->titolo = $_POST['titolo'];
        $immagine->didascalia = $_POST['didascalia'];
        $immagine->descrizione = $_POST['descrizione'];
        $immagine->testo_alternativo = $_POST['testo_alternativo'];
        $immagine->impostazione_file_foto($_FILES['file']);
        $immagine->salva_immagine_post();
        if($immagine->errore){
            $messaggio = join("<br>", $immagine->errore);
        } else {
            $messaggio = "Immagine caricata Correttamente";
        }

    }
    else
    {
        $messaggio = join("<br>", $immagine->errore);
    }

}

//if(isset($_POST['dropzone'])){
//    echo "SIIIIII";
//}
//echo $immagine->nomefile;

?>
<!--'http://localhost/mla-cms-new/admin/upload.php'-->
<script type="text/javascript">
    $(document).ready(function () {

//        var nomeImg = <?php //echo json_encode($immagine->nomefile) ?>//;

        Dropzone.options.myAwesomeDropzone = false;
        Dropzone.autoDiscover = false;
        $("#image").dropzone({
//        url: <?php //echo ROOT_SITO.DS.'admin'.DS.'upload.php'?>//,
//        paramName: <?php //echo $immagine->nomefile; ?>//, // The name that will be used to transfer the file
//            url: 'http://localhost/mla-cms-new/admin/upload.php',
//            paramName: ,
            maxFilesize: 15, // MB
            maxFiles: 5,
            parallelUploads: 5,
            addRemoveLinks: true,
            dictMaxFilesExceeded: "You can only upload upto 5 images",
            dictRemoveFile: "Delete",
            dictCancelUploadConfirmation: "Are you sure to cancel upload?",
            accept: function (file, done) {
                console.log(file);
                if ((file.type).toLowerCase() != "image/jpg" &&
                    (file.type).toLowerCase() != "image/gif" &&
                    (file.type).toLowerCase() != "image/jpeg" &&
                    (file.type).toLowerCase() != "image/png"
                ) {
                    done("Invalid file");
                }
                else {
                    done();
                }
            },
//            init: function () {
//            var mockFile = { name: <?php //echo $immagine->nomefile; ?>//, size: <?php //echo $immagine->grandezza; ?>//, type: <?php //echo $immagine->tipo; ?>// };
//                var mockFile = { name: 'test' , size: 10, type: 'jpg' };
//                this.options.addedfile.call(this, mockFile);
//            this.options.thumbnail.call(this, mockFile, <?php //echo ROOT_SITO.DS.'images'; ?>//);
//                mockFile.previewElement.classList.add('dz-success');
//                mockFile.previewElement.classList.add('dz-complete');
//            },
            success: function (file, response) {
                var imgName = response;
                console.log(file);
                file.previewElement.classList.add("dz-success");
//                console.log("Successfully uploaded :" + imgName);
                swal({
                    title: 'Immagine Caricata Correttamente!',
                    type: 'success',
                    timer: 2000
                })
            },
            error: function (file, response) {
                file.previewElement.classList.add("dz-error");
                swal({
                    title: 'Errore Caricamento!',
                    type: 'error',
                    timer: 2000
                })
            }
        });
    });

</script>




<section class="main-content">

<div class="col-lg-12">
    <h1 class="page-header">Media Upload</h1>
</div>


    <?php if($messaggio) :?>
    <div class="row">
        <div class="col-md-12">
            <div class="alert <?php echo ($messaggio == "Immagine caricata Correttamente") ? 'alert-success' : 'alert-danger' ?> alert-dismissable">
                <button type="button" data-dismiss="alert" aria-hidden="true" class="close">×</button>
                <?php  echo "<b class='dz-success'>" . $messaggio . "</b>"; ?>.
            </div>
        </div>
    </div>
    <?php endif; ?>


<div class="row">
    <div class="col-md-6">
        <form action="upload.php" method="post" enctype="multipart/form-data" data-parsley-validate>
            <div class="form-group">
                <label for="titolo">Titolo Immagine</label>
                <input type="text" name="titolo" class="form-control">
            </div>
            <div class="form-group">
                <label for="didascalia">Didascalia</label>
                <input type="text" name="didascalia" class="form-control">
            </div>
            <div class="form-group">
                <label for="descrizione">Descrizione</label>
                <input type="text" name="descrizione" class="form-control">
            </div>
            <div class="form-group">
                <label for="testo_alternativo">Testo Alternativo</label>
                <input type="text" name="testo_alternativo" class="form-control">
            </div>
            <div class="form-group">
                <label for="titolo">Seleziona Immagine</label>
                <input class="form-control inline bootstrap-filestyle" type="file" name="file" required>
            </div>
            <input type="submit" class="btn btn-primary" name="invia" value="Carica Immagine">
        </form>
    </div>

</div>

<div class="row">
    <div class="col-lg-12">
        <form action="upload.php" method="post" class="dropzone" id="image" name="dropzone">

        </form>
    </div>
</div>

</section>










<?php include 'includes/footer.php'; ?>



