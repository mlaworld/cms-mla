<?php
/**
 * Created da Giuseppe Alessandro De Blasio.
 * User: Giuseppe Alessandro De Blasio
 * Date: 13/03/17
 * Time: 18:53
 */
?>
<?php include 'includes/header.php'?>
<?php //include 'includes/init.php';

$nazioni = Nazioni::seleziona_tutti();

if(isset($_POST['promoId'])){

    $idPreso = $_POST['promoId'];

    global $database;
    $sql_promoMod = "SELECT id, Centro, id_nazione, EXISTS (SELECT id_centro FROM promo_centri WHERE centro.id = promo_centri.id_centro AND promo_centri.id_promo = {$idPreso}) AS esiste FROM centro";

    $result = $database->query($sql_promoMod);

    $array_centriMod = array();

    while ($row = mysqli_fetch_assoc($result)) {
        $array_centriMod[] = $row;
    }
}


//if(empty($array_centriMod)){
//    echo "<h3 class='text-center'>Nessuna Caratteristica del centro Inserita</h3>";
//} else
//    {

  echo "
                                            <table id=\"ModTabellaCentri\" class=\"table table-striped table-bordered table-hover\">
                                                <thead>
                                                <tr>
                                                    <th style=\"width: 5%\" class=\"check-all\">
                                                        <div data-toggle=\"tooltip\" data-title=\"Check All\" class=\"checkbox c-checkbox\" data-original-title=\"\" title=\"\">
                                                            <label>
                                                                <input type=\"checkbox\">
                                                                <span class=\"fa fa-check\"></span>
                                                            </label>
                                                        </div>
                                                    </th>
                                                    <th>Centro</th>
                                                    <th>Nazione</th>
                                                </tr>
                                                </thead>
                                                <tbody>";
                                                foreach ($array_centriMod as $centro):
                                              echo "<tr>
                                                    <td>
                                                        <div class=\"checkbox c-checkbox\">
                                                            <label>";
                                                    $esiste = ($centro['esiste'] == 1 )? 'checked' : '';
                                                              echo "<input name=\"arrCentroMod[]\" value=\"{$centro['id']}\" type=\"checkbox\" {$esiste} >";
                                                              echo  "<span class=\"fa fa-check\"></span>
                                                            </label>
                                                        </div>
                                                    </td>
                                                    <td>
                                                    " . $centro['Centro'] . "
                                                    </td>
                                                    <td>";
                                                    $nazione =  Nazioni::seleziona_per_id($centro['id_nazione']); echo $nazione->nazione;
                                              echo   "</td>
                                                </tr>";
                                                endforeach;
                                              echo   "</tbody>
                                            </table>
                                        
";



//}

?>

<?php include 'includes/footer.php'?>


<script>
        var modTabCentri = $('#ModTabellaCentri').dataTable({
            "language": {
                "url": "js/datatables/traduzione/Italian.json"
            },
            columnDefs: [{
                orderable: false,
                targets:   0
            }],
            select: {

                selector: 'td:first-child'
            },
            order: [[ 1, 'asc' ]]
        });

</script>