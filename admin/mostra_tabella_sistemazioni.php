<?php
/**
 * Created da Giuseppe Alessandro De Blasio.
 * User: Giuseppe Alessandro De Blasio
 * Date: 18/04/17
 * Time: 17:27
 */
error_reporting(0);

include 'includes/init.php';

if(isset($_POST['id_sist_centro'])){

    global $database;
    $ultimoId = Centro::ultimo_id();
    $sql = "SELECT * FROM view_sistemazioni_attive ";
    $sql .= "WHERE id_corso_adulti = {$_POST['id_sist_centro']}";

    $result = $database->query($sql);
//    var_dump($result);

    $sqlAttivi = "SELECT * FROM view_conta_stati_att";
    $risAttivi = $database->query($sqlAttivi);
    $row = mysqli_fetch_array($risAttivi);
    $attivi = array_shift($row) + 1;
//    return $attivi;
//    var_dump($attivi);
$td = 10;

?>

    <div class="panel-body">
    <div class="table-responsive">
    <table class="table table-striped">
    <thead>
    <tr>
        <th>N°</th>
        <th>Settimana/e</th>

<?php
$idAlloggio = array();

while ($row = mysqli_fetch_object($result)): ?>

        <th><input type="hidden" value="<?php echo $idAlloggio[]= $row->id_alloggio; ?>"><?php echo $row->nome_alloggio; ?></th>

<?php endwhile; }  ?>

        <?php $arrFix  = array();

        foreach ( $idAlloggio as $key => $val ):
            $arrFix[ $key+2 ] = $val;
        endforeach;

    ?>
    </tr>
    </thead>

        <tbody>
        <?php echo "Numero di elementi attivi: " . $attivi; ?>
        <?php for($i=1; $i<=$td; $i++){ ?>
                <tr>
                    <td><?php echo $i; ?></td>
                    <?php for($a=1; $a<=$attivi; $a++){ ?>
                    <td><?php echo "<input class='idPrezzo'  type='hidden' value='{$arrFix[$a]}'><input type='text' data-value='{$arrFix[$a]}' value='' class='form-control' onclick='clickValore(this);'/>" ?></td>
                    <?php }; ?>
                 </tr>
        <?php }; ?>
        </tbody>
    </table>
    </div>
    </div>

<script>
    function clickValore(data) {
        alert($(data).data("value"));
    }
</script>


