<?php
/**
 * Created da Giuseppe Alessandro De Blasio.
 * User: Giuseppe Alessandro De Blasio
 * Date: 13/10/16
 * Time: 10:36
 */

include 'includes/init.php';

if(isset($_POST['mostro_id_centro'])){
    $sel_car_centr = caratCentro::seleziona_per_campo_custom_carat_centro('id_centro', $_POST['mostro_id_centro']);
    }

$conta_centro = new caratCentro();

(empty($sel_car_centr))? $sel_car_centr="" : $conta = $conta_centro->conta_elementi($sel_car_centr);

if($sel_car_centr == NULL){
    echo "<h3 class='text-center'>Nessuna Caratteristica del centro Inserita</h3>";
} else {
    echo "<thead id=\"sorted_list\">
            <tr>
               <th>Titolo</th>
               <th>Descrizione</th>
               <th>Posizione</th>
               <th>Modifica</th>
               <th>Elimina</th>
            </tr>
        </thead>
        <tbody>";

$i = 0;
    foreach ($sel_car_centr as $dato){

        $i++;
        echo "
    <div id=\"info\">Waiting for update</div> 
    
    <form method=\"post\" action=\"\">
           <tr class=\"trascinabile\" id=\"table-sorted\">
                <td id=\"$dato->id\" value=\"$dato->id\"> $dato->titolo  </td>
                <td id=\"$dato->id\" value=\"$dato->id\"> $dato->descrizione  </td>
                <td id=\"$dato->id\" value=\"$dato->id\"> $dato->posizione_row</td>
                <td id=\"$dato->id\" value=\"$dato->id\"><a  id='modCarat' style=\"color: rgba(83, 86, 93, 0.68)\" href=\"javascript:apriUrlPopUp('modifica_carat_centro.php?id={$dato->id}')\" ><i style=\"color: rgb(5, 25, 93)\"; class=\" fa fa-edit\" ></i> Modifica</a></td>
                <td id=\"$dato->id\" value=\"$dato->id\"><a style=\"color: rgba(83, 86, 93, 0.68)\" id=\"el_carat_cent\" data-record-id=\"$dato->id\" data-record-title=\"$dato->descrizione\" href=\"#\" value=\"$dato->id\" data-toggle=\"modal\" data-target=\"#canc-carat-centro\"><i style=\"color: #e90811;\" class=\"fa fa-trash-o\"></i> Elimina</a></td>
               <!-- <td id=\"sorted_\"  style=\"display: none\"><input name=\"sorted_id\"  id=\"sorted_id\" type=\"hidden\" value=\"\" /></td>-->
            </tr>
    </form>";

            //$i--;
        }
    echo "</tbody> ";
}


//print_r($_POST['ordine']);
if(isset($_POST['ordine'])):
foreach ($_POST['ordine'] as $position => $item) :
    global $database;


    //print_r($_POST['ordine']);
    echo "posizione " . $position ."<br>";
    echo "item " . $item;


    //$sql[$_POST['ordine']] = "UPDATE carat_centri SET sorted = {$item} WHERE id = {$_POST['id_centro_preso']}";
    $sql[] = "UPDATE carat_centri SET sorted = {$item} WHERE id = {$_POST['id_centro_preso']}";
    //print_r($sql);
    //$database->query($sql);

    //return (mysqli_affected_rows($database->conn) == 1) ? TRUE : FALSE;
endforeach;

endif;

if(isset($_GET["sort_order"])) {
    $id_ary = explode(",",$_GET["sort_order"]);
    for($i=0;$i < count($id_ary);$i++) {
        $sql = "UPDATE carat_centri SET sorted='" . $i . "' WHERE id=". $id_ary[$i];
        $database->query($sql);
        //echo $sql;
        //mysqli_query($conn, $sql) or die("database error:". mysqli_error($conn));
    }
}


?>
<!-- <script type="text/javascript">
    $(document).ready(function () {
        $('tbody').sortable({
            axis: 'y',
            //handle : '.trascinabile',
            update: function (event, ui) {
                //var data = $(this).sortable('serialize');
                //console.log(ui.item.index());
                //var ordine_s = $('#sorted_id').val();

                //console.log(ordine_s);


                /*Array.prototype.clean = function(deleteValue) {
                    for (var i = 0; i < this.length; i++) {
                        if (this[i] == deleteValue) {
                            this.splice(i, 1);
                            i--;
                        }
                    }
                    return this;
                };*/


                function cleanArray(actual) {
                    var newArray = new Array();
                    for (var i = 1; i < actual.length; i++) {
                        if (actual[i]) {
                            newArray.push(actual[i]);
                        }
                    }
                    return newArray;
                }


                //var newOrdine = $(this).sortable('disable', 'toArray').toString();
                //var newOrdine = $(this).sortable('toArray').toString();
                var newOrdine;

                var prendoIndex = ui.item.index();
                var arraynew = cleanArray(newOrdine = $(this).sortable('toArray'));
                //console.log(arraynew);
                //alert(newOrdine);
                //var id_centro_preso = $('#el_carat_cent').val();
                var id_centro_preso = $('#el_carat_cent').data('recordId');

                $.post('mostra_carat_cent.php',{ordine:arraynew, prendoIndex:prendoIndex, id_centro_preso:id_centro_preso});
                /*var sorted = $('#sorted_').val();
                for (var i=0; i< sorted+i; i++ ){
                    console.log(sorted);
                }
                console.log(sorted);*/
                /*$.ajax({
                    data: data,
                    type: 'POST'
                    //url: '/your/url/here'
                });*/
            }
        });
    });


    window.onunload = refreshParent;
    function refreshParent() {
        window.opener.location.reload();
    }


</script> -->


  <!--  <script>
        $(document).ready(function() {
            $("tbody").sortable({
                //handle : '.handle',
                update : function () {
                    var order = $('td#table-sorted').sortable('serialize');
                    $("#info").load("mostra_carat_cent.php?listItem="+order);
                }
            });
        });

    </script> -->

<script>
    $(function() {
        $( "tbody" ).sortable({
            placeholder: "ui-state-highlight",
            update: function( event, ui ) {
                updateDisplayOrder();
            }
        });
    });


    function updateDisplayOrder() {
        var order = new Array();
        console.log(order);
        $('tbody tr td').each(function() {
            order.push($(this).attr("id"));
        });
        var dataString = 'sort_order='+order;
        $.ajax({
            type: "GET",
            url: "mostra_carat_cent.php",
            data: dataString,
            cache: false,
            success: function(data){
            }
        });
    }
</script>

<?php
//header('Content-Type: application/json');
//echo json_encode();

?>

