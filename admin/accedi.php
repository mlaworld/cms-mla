<?php include ("includes/header.php") ?>
<?php require_once 'includes/init.php'; ?>
<?php 

$utente = new Utente();

if($sessione->loggato()){
    redirect('index.php');
}

if(isset($_POST['accedi'])){
    $username = trim($_POST['username']);
    $salt = "cms86";
    $password = trim(md5($_POST['password'] . $salt));

    $cerca_utente = Utente::verifica_utente($username, $password);

//    $auth = new PHP_API_AUTH(array(
//        'authenticator'=>function($username,$password){ $_SESSION['user']=($username && $password); }
//    ));

    if($cerca_utente){
    $sessione->accedi($cerca_utente);
//        if ($auth->executeCommand()) exit(0);
//            if (empty($_SESSION['user']) || !$auth->hasValidCsrfToken()) {
//                header('HTTP/1.0 401 Unauthorized');
//                exit(0);
//            }

        include 'auth.php';
        redirect('auth.php');
    } else {
        $messaggio = "La password o la username sono errate";
    }

    if ($sessione->loggato()) {
        $utente_sel = Utente::seleziona_per_id($_SESSION['id_utente']);
        //$utente->ultimo_accesso = $utente->aggiorno_ultimo_accesso_utente();
        //$utente->ip_utente = $utente->aggiorno_ip_cliente();
        $date = DateTime::createFromFormat('d-m-Y', DateTimeZone::EUROPE);
//        $date = $date->format('Y-m-d');
        $utente_sel->ultimo_accesso = $utente->ultimo_accesso_utente();
        $utente_sel->ip_utente = $utente->getClientIP();
        $utente_sel->tempo_scaduto = $utente->inattivita();

        print_r($utente_sel->salva());
   }

} else {
    $messaggio = "";
    $username = "";
    $password = "";
}

    
    
    
    if(isset($_POST['registrazione'])){
        $errori = [];
        
        $utente->username = $utente->pulisci($_POST['username']);
        $utente->password = $utente->pulisci($_POST['password']);
        $utente->conferma_password = $utente->pulisci($_POST['conferma_password']);
        $utente->email = $utente->pulisci($_POST['email']);
        $utente->codice_validazione = md5($utente->username . microtime());
        $utente->ruolo = "sottoscrittore";
        $utente->ip_utente = $utente->getClientIP();
        $utente->ultimo_accesso = $utente->pulisci($utente->ultimo_accesso_utente());
        $utente->data_registrazione = $utente->pulisci($utente->data_registrazione_utente());
        
        
        if ($utente->esiste_email($utente->email)){
            $errori[] = "Mi dispiace ma questa email risulta già registrata";
        }
        
        if ($utente->esiste_username($utente->username)) {
            $errori[] = "Mi dispiace ma questa username risulta già registrata";
        }
        
        if($_POST['checkbox'] != FALSE){
            $errori[] = "Per Piacere Accetta i termini e condizioni!";
        }
        
        if($utente->password !== $utente->conferma_password){
            $errori[] = "Le Password non coincidono";
        } else{
            $utente->password = $utente->password_crypt($_POST['password']);
        }
        
        if(!empty($errori)){
            foreach ($errori as $errore) 
                {
                    echo $utente->errori_campi($errore);
                }
            } else {
                
            $utente->salva();
            echo '<div data-toggle="notify" data-onload data-message="Registrazione Avvenuta con Successo controlla la tua <b>email!</b>" data-options="{&quot;status&quot;:&quot;success&quot;, &quot;pos&quot;:&quot;top-center&quot;}" class="hidden-xs"></div>';
            /*$utente->oggetto = "Attivazione Account";
            $utente->msg = "Per piacere clicca sul link sottostante per verificare il tuo account
            http://localhost/admin/attivazione_utente.php?email=$utente->email&codice=$utente->codice_validazione
                ";
            $utente->headers = "Da: info@giuseppealessandrodeblasio.it";
                
            $email = $utente->email;
            $oggetto = $utente->oggetto;
            $msg= $utente->msg;
            $headers =  $utente->headers;
            invio_email($email, $oggetto, $msg, $headers);
                
            return TRUE;*/
        }
    }
    
   
    


?>

<?php $sessione->mostra_messaggio(); ?>
<?php $utente->verifca_login_utente(); ?>
  <?php //if(isset($_POST['registrazione'])) { echo '<div data-toggle="notify" data-onload data-message="Registrazione Avvenuta con Successo controlla la tua <b>email!</b>" data-options="{&quot;status&quot;:&quot;success&quot;, &quot;pos&quot;:&quot;top-center&quot;}" class="hidden-xs"></div>';} ?>

<div style="height: 100%; padding: 20px 0; /*background-color: #2c3037*/ background-image:url('immagini/mla_accedi.jpg'); background-repeat: no-repeat;background-size: cover;  background-position: center center;" class="row row-table">
      <div class="col-lg-3 col-md-6 col-sm-8 col-xs-12 align-middle">
         <div data-toggle="play-animation" data-play="fadeInDown" data-offset="0" class="panel b0">
            <p class="text-center mb-lg">
               <br>
               <a href="#">
                  <img src="immagini/LOGO_MLA.png" alt="Image" width="187px" height="53.84px" class="block-center img-rounded">
               </a>
            </p>
            <div id="accordion" data-toggle="collapse-autoactive" class="panel-group">
               <!-- START panel-->
               <div class="panel radius-clear b0 shadow-clear">
                  <div class="panel-heading radius-clear panel-heading-active"><a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" class="text-muted btn-block text-center">ACCEDI PER CONTINUARE.</a>
                  </div>
                  <div id="collapseOne" class="panel-collapse collapse in">
                     <div class="panel-body">
                         <form action="" role="form" class="mb-lg" method="POST" data-parsley-validate>
                           <div class="form-group has-feedback">
                              <label for="username">Username</label>
                              <input id="username" name="username" type="text" placeholder="Inserisci Username" class="form-control" data-parsley-id="10" data-parsley-required>
                              <span class="fa fa-user form-control-feedback text-muted"></span>
                           </div>
                           <div class="form-group has-feedback">
                              <label for="password">Password</label>
                              <input id="password" name="password" type="password" placeholder="Password" class="form-control" required="required">
                              <span class="fa fa-lock form-control-feedback text-muted"></span>
                           </div>
                           <div class="clearfix">
                              <div class="checkbox c-checkbox pull-left mt0">
                                 <label>
                                    <input type="checkbox" value="" name="ricrodami">
                                    <span class="fa fa-check"></span>Ricordami</label>
                              </div>
                           </div>
                             <button type="submit" name="accedi" id="accedi" class="btn btn-block btn-primary">Accedi</button>
                        </form>
                     </div>
                  </div>
               </div>
               <!-- END panel-->
               <!-- START panel-->

               
               <?php $sessione->mostra_messaggio(); ?>
                <?php $utente->verifca_registrazione_utente(); ?>
               <div class="panel radius-clear b0 shadow-clear">
                  <div class="panel-heading radius-clear"><a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" class="text-muted btn-block text-center">Iscriviti Adesso?</a>
                  </div>
                  <div id="collapseTwo" class="panel-collapse collapse">
                     <div class="panel-body">
                         <form action="" method="POST" role="form" class="mb-lg" data-parsley-validate>
                           <div class="form-group has-feedback">
                              <label for="username">Username</label>
                              <input id="username" name="username" type="text" placeholder="Inserisci username" class="form-control" required="required">
                              <span class="fa fa-user form-control-feedback text-muted"></span>
                           </div>
                           <div class="form-group has-feedback">
                              <label for="email">Email</label>
                              <input id="email"  name="email" type="email" placeholder="Inserisci email" class="form-control" required="required">
                              <span class="fa fa-envelope form-control-feedback text-muted"></span>
                           </div>
                           <div class="form-group has-feedback">
                              <label for="signupInputPassword1">Password</label>
                              <input id="password" name="password" type="password" placeholder="Password" class="form-control" data-parsley-minlength="6" required="required">
                              <span class="fa fa-lock form-control-feedback text-muted"></span>
                           </div>
                           <div class="form-group has-feedback">
                              <label for="password">Ripeti Password</label>
                              <input id="ripeti_password"  name="conferma_password" type="password" data-parsley-minlength="6" placeholder="Ripeti Password" class="form-control" required="required">
                              <span class="fa fa-lock form-control-feedback text-muted"></span>
                           </div>
                           <div class="clearfix">
                              <div class="checkbox c-checkbox pull-left mt0">
                                 <label>
                                     <input name="checkbox" type="checkbox" value="" required="required" name="termini_condizioni">
                                    <span class="fa fa-check"></span>Sono d'accordo con i <a href="#">termini</a>
                                 </label>
                              </div>
                           </div>
                             <button type="submit" name="registrazione-not-active" class="btn btn-block btn-primary">Registrati</button>
                        </form>
                     </div>
                  </div>
               </div>
               <!-- END panel-->
               <!-- START panel-->
               
               <!--<div class="panel radius-clear b0 shadow-clear">
                  <div class="panel-heading radius-clear"><a data-toggle="collapse" data-parent="#accordion" href="#collapseThree" class="text-muted btn-block text-center">Hai perso la password?</a>
                  </div>
                  <div id="collapseThree" class="panel-collapse collapse">
                     <div class="panel-body">
                        <form role="form" data-parsley-validate>
                           <p class="text-center">Verifica nella tua casella di posta le istruzioni su come reimpostare la password.</p>
                           <div class="form-group has-feedback">
                              <label for="resetta_password">Indirizzo Email</label>
                              <input id="resetta_password" type="email" placeholder="Inserisci email" class="form-control" required="required">
                              <span class="fa fa-envelope form-control-feedback text-muted"></span>
                           </div>
                           <button type="submit" name="recupera_password-not-active" class="btn btn-danger btn-block">Resetta</button>
                           <input type="hidden" class="hide" name="token" id="token" value="<?php // disattivato echo $sessione->generazione_token(); ?>">
                        </form>
                     </div>
                  </div>
               </div>-->
               <!-- END panel-->
            </div>
         </div>
          <p class="copyright" style="color: #FFFFFF; font-weight: bold; text-align: center; background-color: #3498db;">The Golden Globe srl / p.iva 01557500632 – © Copyright <?php echo date("Y") ?>.</p>
      </div>
   </div>

 <?php include ("includes/footer.php") ?>