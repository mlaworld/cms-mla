<?php

/**
 * Created da Giuseppe Alessandro De Blasio.
 * User: Giuseppe Alessandro De Blasio
 * Date: 18/09/2016
 * Time: 09:58
 */


class Utente extends Db_oggetto{
    protected static $tabella_db = "utenti";
    protected static $campi_tabella_db = array(
        'username',
        'password',
        'nome',
        'cognome',
        'email',
        'ruolo',
        'codice_validazione',
        'attivo',
        'ultimo_accesso',
        'immagine_utente',
        'ip_utente',
        'data_registrazione',
        'tempo_scaduto'
    );
    public $id;
    public $username;
    public $password;
    public $conferma_password;
    public $nome;
    public $cognome;
    public $email;
    public $ruolo;
    public $codice_validazione;
    public $attivo;
    public $ultimo_accesso;
    public $ricordami;
    public $immagine_utente;
    public $percorso_immagini = "images/utente";
    public $immagine_plaecholder = "http://placehold.it/400x400text=image";
    public $errors = array();
    public $ip_utente;
    public $data_registrazione;
    public $oggetto;
    public $msg;
    public $headers;
    public $salt = "cms86";
    public $tempo_scaduto;




    public function caricamento_foto() {
        
        if(!empty($this->errors)){
            return FALSE;
        }
        
        if (empty($this->immagine_utente) || empty($this->tmp_path)) {
            $this->errors[] = "Il file non è utilizzabile";
            return FALSE;
        }
        
        $percorso_immagini = ROOT_SITO . DS . 'admin' . DS . $this->percorso_immagini . DS . $this->immagine_utente;
        
        if(file_exists($percorso_immagini)){
            $this->errors[] = "Il file {$this->immagine_utente} già esiste";
            return FALSE;
        }
        
        if(move_uploaded_file($this->tmp_path, $percorso_immagini)){
            unset($this->tmp_path);
            return TRUE;
        } else {
            $this->errors[] = "Molto probabilmente non si ha il permesso di inserire il file nalla directory";
            return FALSE;
        }
    }
    
    public function percorso_immagine_e_plecholder() {
        return empty($this->immagine_utente)? $this->immagine_plaecholder : $this->percorso_immagini.DS.$this->immagine_utente;
    }

    public function verifica_utente($username, $password) {
        
        global $database;
        
        //$nome = $database->escape($nome);
        //$cognome = $database->escape($cognome);
        $username = $database->escape($username);     
        $password = $database->escape($password);
        
        $sql = "SELECT * FROM " . static::$tabella_db . " WHERE ";
        $sql.= " username =  '{$username}' ";
        $sql.= " AND password =  '{$password}' ";
        $sql.= " LIMIT 1 ";
        
        $risultato_array = self::cerca_con_query($sql);
        return !empty($risultato_array)? array_shift($risultato_array) : FALSE;
        
    }
    
    public function registrazione_utente($username, $email, $password) {
        global $database;
        
        //$this->nome = escape($nome);
        //$this->cognome = escape($cognome);
        $this->username = escape($username);
        $this->email = escape($email);
        $salt = "cms86";
        $this->password = escape($password . $salt);
        
        if ($this->esiste_email($email)) 
        {
            return FALSE;
        } elseif ($this->esiste_username($username)) 
            {
                return FALSE;
            } else
            {
                $salt = $this->salt;
                $this->password = md5($password . $salt);
                $this->codice_validazione = $codice_validazione =  md5($username . microtime());
                $sql = "INSERT INTO utenti (username, email, password, codice_validazione, attivo)";
                $sql.= " VALUES('$username', '$email', '$password', 0)";
                $risultato = $database->query($sql);
                
                /*
                $oggetto = "Attivazione Account";
                $msg = "Per piacere clicca sul link sottostante per atticare il tuo account
                http://localhost/login/activate.php?email=$email&code=$codice_validazione
                ";
                $headers = "Da: info@giuseppealessandrodeblasio.it";
                
                send_email($email, $oggetto, $msg, $headers);*/
                
                return TRUE;
            } 
        
        
        
    }
    
    public function verifca_login_utente() {
         
        $errori = [];
         
        if (/*$_SERVER['REQUEST_METHOD'] == "POST"*/isset($_POST['accedi'])){
            $this->username = $this->pulisci($_POST['username']);
            $this->password = $this->pulisci($_POST['password'] . $this->salt);
            $this->ricordami = isset($_POST['ricordami']);
        
        
            if (empty($this->username)){
                $errori[] = "Il campo email non può essere lasciato libero";
            }
            
            if (empty($this->password)){
                $errori[] = "Il campo password non può essere lasciato libero";
            }
        
            if(!empty($errori)){
                foreach ($errori as $errore) 
                {
                    echo $this->errori_campi($errore);
                }
            } else {
                if($this->verifica_utente($this->username, $this->password . $this->salt)){
                    redirect("admin.php");
            } else{
                    echo $this->errori_campi("La username o la password non è corretta");
                }
            }
        
        }
    }
    
    public function verifca_registrazione_utente() {
         
        $errori = [];
         
        if(/*$_SERVER['REQUEST_METHOD'] == "POST"*/isset($_POST['registrazione'])){
            $this->username = $this->pulisci($_POST['username']);
            $this->password = $this->pulisci($_POST['password']);
            $this->confrema_password = $this->pulisci($_POST['conferma_password']);
        
        
            /*if (empty($this->username)){
                $errori[] = "Il campo email non può essere lasciato libero";
            }
            
            if (empty($this->password)){
                $errori[] = "Il campo password non può essere lasciato libero";
            }*/
            
            if($this->password !== $this->confrema_password){
                $errori[] = "Le Password non coincidono";
            }
        
            if(!empty($errori)){
                foreach ($errori as $errore) {
                        echo $this->errori_campi($errore);
                    }
                } else {
                if(registrazione_utente($this->username, $this->email, $this->password)){
                    //Sessione::setta_messaggio("<p class='bg-success text-center'>Per piacere controlla la tua email o la cartella spam per attivare il link</p>");
                    redirect("si.php");

                } else {
                //Sessione::setta_messaggio("<p class='bg-danger text-center'>Ci dispiace ma non è stato possibile registrare l'utente</p>");
                redirect("no.php");
                }
            }
        
        }
    }
    
    public function password_crypt($password) {
        
        
        return $this->password = md5($password . $this->salt);
        
        //return $this->password = password_hash($hash, PASSWORD_DEFAULT);
    }
    
    public function codice_verifica() {
        
        $this->codice_validazione = md5($this->username . microtime());
    }
    
    
    public function esiste_email($email){
        global $database;
        
        $sql = "SELECT id FROM " . static::$tabella_db . " WHERE ";
        $sql.= " email = '{$this->email}' ";
        $risultato = $database->query($sql);
    
        if($database->conta_righe($risultato) == 1){
        return TRUE;
        } else{
            return FALSE; 
        }
    }


    public function ultimoAdmin($ruolo){
        global $database;

            $sql = "SELECT id FROM " . static::$tabella_db . " WHERE ";
            $sql.= " ruolo = '{$ruolo}' ";
            $risultato = $database->query($sql);

            if($database->conta_righe($risultato) == 1){
                return TRUE;
            } else {
                return FALSE;
            }


    }

    public function attivoUser($id, $valore) {

        global $database;

        $sql = "UPDATE " . static::$tabella_db . " SET ";
        $sql.= " attivo = '{$valore}' WHERE id = '{$id}' ";
        $database->query($sql);

    }

    public function esiste_username($username){
        global $database;

        $sql = "SELECT id FROM " . static::$tabella_db . " WHERE ";
        $sql.= " username = '{$this->username}' ";
        $risultato = $database->query($sql);
    
        if($database->conta_righe($risultato) == 1){
        return TRUE;
        } else{
            return FALSE;
        }
    }
    
    public function errori_campi($messaggio_errore)
    {
        $messaggio_errore = <<<DELIMITER
            <div data-toggle="notify" data-onload data-message="<strong>ATTENZIONE! </strong>{$messaggio_errore}" data-options="{&quot;status&quot;:&quot;danger&quot;, &quot;pos&quot;:&quot;top-center&quot;}" class="hidden-xs">
            </div>
                
                
DELIMITER;
        return $messaggio_errore;
        
        
    }
    
    
    public function attivazione_utente($session){
        global $database;


        if($_SERVER['REQUEST_METHOD'] == "GET")
    {
        if(isset($_GET['email']))
        {
            $this->email = clean($_GET['email']);
            
            $this->codice_validazione = clean($_GET['code']);
            
            $sql = "SELECT id FROM utenti WHERE email = '". escape($_GET['email']) ."' AND codice_attivazione = '". escape($_GET['codice']) ."' ";
            $risultato = $database->query($sql);
            
            
            if($database->conta_righe($risultato)== 1)
            {
//                $sql2 = "UPDATE users SET active = 1, codice_attivazione = 0 WHERE email = '". escape($email) ."' AND codice_attivazione = '". escape($codice_validazione) ."' ";
//                $result2 = $database->query($sql2);
//                $database->conferma_query($result2);
//                $session->setta_messaggio("<p class='bg-success'>Il tuo account è stato attivato! Adesso puoi effettuare il Login</p>");
//                redirect("accedi.php");
            } else 
                {
                    $session->setta_messaggio("<p class='bg-danger'>Il tuo account è gia stato attivato o non può essere attivato!</p>");
                    redirect("accedi.php");
                }
        }
    }
    
}

    public function recupero_password($sessione){
        global $database;
        
        if(isset($_POST['recupera_password'])){
            if(isset($_SESSION['token']) && $_POST['token'] === $_SESSION['token'])
            {
                $this->email = clean($_POST['email']);
                
                if($this->esiste_email($this->email)){
                    $this->codice_validazione = md5($this->email . microtime());
                    
                    setcookie('temp_access_code' , $this->codice_validazione , time() + 900);
                    
                    $sql = "UPDATE ". static::$tabella_db ." SET codice_validazione ='". escape($this->codice_validazione) ."' WHERE email = '". escape($this->email)."' ";
                    $result = $database->query($sql);
                    
                    
                    $this->oggetto = "Perpiacere si prega di resettare la password";
                    $this->msg = "Questo è il tuo codice per resettare la password {$this->codice_validazione}
                                
                    Cliccate qui per resettare la password http://localhost/code.php?email=$this->email&code=$this->codice_validazione

                    ";
                    $this->headers = "From info@giuseppealessandrodeblasio.it ";
                    
                    invio_email($this->email, $this->oggetto, $this->msg, $this->headers);
                    
                    
                    $sessione->setta_messaggio("<p class='bg-success text-center'>Perpiacere controllare nella tua email il link per il reset anche nella cartella spam</p>");
                    redirect("index.php");
                   
                }else if($_POST['email'] == NULL || $_POST['email'] == "")
                {
                    echo $this->errori_campi("Il campo email non può essere lasciato libero!!");
                }else 
                    {
                       echo $this->errori_campi("Questa email non esiste nel nostro DB");
                    }    
                    
                
                
                }else
                {
                    redirect("index.php");
                }
    
            if (isset($_POST['cancella_invio'])) 
            {
                redirect("accedi.php");
            }       
        }
    
    }

public function reset_password() {
    global $database;
    
    if(isset($_COOKIE['temp_access_code']))
    {
        if (isset($_GET['email']) && isset($_GET['code'])) {
            
            
            if(isset($_SESSION['token']) && isset($_POST['token']))
            {
            
                if ($_POST['token'] === $_SESSION['token'])
                {
                    if($_POST['password'] === $_POST['conferma_password'])
                    {
                        $update_password = md5($_POST['password'] . $this->salt);
                        $sql = "UPDATE ". static::$tabella_db ." SET password='".  escape($update_password)."', validation_code = 0 WHERE email= '".  escape($_GET['email'])."' ";
                        query($sql);
                        
                        setta_messaggio("<p class='bg-success text-center'>La tua password è stata resettata con successo!!</p>");
                        redirect("login.php");
                    } else 
                    {
                        echo $this->errori_campi("Le password non corrispondono!!");
                    }
                }    
                
            } 
        }
    }else
        {
            setta_messaggio("<p class='bg-danger text-center'>Ci dispiace ma il tempo del codice è scaduto</p>");
            redirect("recover.php");
        }
}

    public function getClientIP() {

    if (isset($_SERVER)) {

        if (isset($_SERVER["HTTP_X_FORWARDED_FOR"]))
            return $_SERVER["HTTP_X_FORWARDED_FOR"];

        if (isset($_SERVER["HTTP_CLIENT_IP"]))
            return $_SERVER["HTTP_CLIENT_IP"];

        return $_SERVER["REMOTE_ADDR"];
    }

        if (getenv('HTTP_X_FORWARDED_FOR'))
            return getenv('HTTP_X_FORWARDED_FOR');

        if (getenv('HTTP_CLIENT_IP'))
            return getenv('HTTP_CLIENT_IP');

        return getenv('REMOTE_ADDR');
    }
    
    public function aggiorno_ip_cliente() {
        global $database;
        
        $proprieta = $this->cancella_proprieta();
        
        $coppia_proprieta = array();
        
        $this->ip_utente = $ip_utente = $this->getClientIP();
        
        foreach ($proprieta as $chiave => $valore) {
            
            $coppia_proprieta[] = "{$chiave}='{$valore}'";
        }
                
        $sql = "UPDATE " . static::$tabella_db . " SET ";
        $sql.= " ip_utente = " . $this->ip_utente ." ";
        $sql.= " WHERE id = " . $database->escape($this->id);
        
        $database->query($sql);
        
        print_r($sql);
        
        return (mysqli_affected_rows($database->conn) == 1) ? TRUE : FALSE;
        
        
    }
    
    public function data_registrazione_utente() {

        $giorni = array("Domenica", "Lunedì", "Martedì", "Mercoledì", "Giovedì", "Venerdì", "Sabato");
        $mesi = array("Gennaio", "Febbraio", "Marzo", "Aprile", "Maggio", "Giugno", "Luglio", "Agosto", "Settembre", "Ottobre","Novembre", "Dicembre");

         // giorno della settimana in italiano
        $numero_giorno_settimana = date("w");
        $nome_giorno = $giorni[$numero_giorno_settimana];

        // giorno del mese
        $numero_giorno_mese = date("j");

        // nome del mese in italiano
        $numero_mese = date("n");
        $nome_mese = $mesi[$numero_mese-1];

        // numero dell'anno
        $anno = date("Y");

        return $nome_giorno . " " . $numero_giorno_mese . " " . $nome_mese . " " . $anno ." ore " . date("H:i:s");

    }
    
        public function ultimo_accesso_utente() {

        //$giorni = array("Domenica", "Lunedì", "Martedì", "Mercoledì", "Giovedì", "Venerdì", "Sabato");
        //$mesi = array("Gennaio", "Febbraio", "Marzo", "Aprile", "Maggio", "Giugno", "Luglio", "Agosto", "Settembre", "Ottobre","Novembre", "Dicembre");

         // giorno della settimana in italiano
        //$numero_giorno_settimana = date("w");
        //$nome_giorno = $giorni[$numero_giorno_settimana];

        // giorno del mese
        //$numero_giorno_mese = date("j");

        // nome del mese in italiano
        //$numero_mese = date("n");
        //$nome_mese = $mesi[$numero_mese-1];

        // numero dell'anno
        //$anno = date("Y");

        //return $nome_giorno . " " . $numero_giorno_mese . " " . $nome_mese . " " . $anno . " ". date("H:i:s");

            //$date = DateTime::createFromFormat('d-m-Y', DateTimeZone::EUROPE);
            //$date = $date->format('Y-m-d');
            //$this->ultimo_accesso = $date;
            //$timestamp =date_default_timezone_set('UTC');
//            return $this->ultimo_accesso = $ultimo_accesso = date("Y-m-d H:i:s"); commentato per provare strtotime
            return $this->ultimo_accesso = $ultimo_accesso = date("Y-m-d H:i:s");


    }
    
    public function aggiorno_ultimo_accesso_utente(){
        global $database;
        
        $proprieta = $this->cancella_proprieta();
        
        $coppia_proprieta = array();
        
        foreach ($proprieta as $chiave => $valore) {
            
            $coppia_proprieta[] = "{$chiave}='{$valore}'";
        }
                
        $sql = "UPDATE " . static::$tabella_db . " SET ";
        $sql.= " ultimo_accesso =  now() ";
        $sql .= " WHERE id = " . $database->escape($this->id);
        
        $database->query($sql);
        
        return (mysqli_affected_rows($database->conn) == 1) ? TRUE : FALSE;
    }
    
    public function ajax_salva_immagine_utente($immagine_utente, $utente_id) {
        global $database;
        
        $immagine_utente = $database->escape($immagine_utente);
        $utente_id = $database->escape($utente_id);
        
        $this->immagine_utente = $immagine_utente;
        $this->id = $utente_id;
        
        $sql = "UPDATE " . self::$tabella_db . " SET immagine_utente = '{$this->immagine_utente}' ";
        $sql.= "WHERE id = {$this->id} ";
//        $aggiorna_immagine = $database->query($sql);
        $database->query($sql);

        echo $this->percorso_immagine_e_plecholder();
        
    }


    public function inattivita(){
       return $this->tempo_scaduto = strtotime('+1 day');
    }
  
    
} // FINE CLASSE UTENTE


