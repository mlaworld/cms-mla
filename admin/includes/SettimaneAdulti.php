<?php

/**
 * Created da Giuseppe Alessandro De Blasio.
 * User: Giuseppe Alessandro De Blasio
 * Date: 13/04/17
 * Time: 11:35
 */
class SettimaneAdulti extends Db_oggetto
{
    protected static $tabella_db = "settimane_adulti";
    protected static $campi_tabella_db = array(
        'id_alloggio_adulti',
        'settimane',
        'prezzo'
    );

    public $id;
    public $id_corso_adulti;
    public $nome_alloggio;

}