<?php

/**
 * Created da Giuseppe Alessandro De Blasio.
 * User: Giuseppe Alessandro De Blasio
 * Date: 01/10/2016
 * Time: 10:02
 */
class Nazioni extends Db_oggetto  {
    protected static $tabella_db = "nazioni";
    protected static $campi_tabella_db = array('nazione', 'stato', 'urlFriendly', 'data_creazione');
    public $id;
    public $nazione;
    public $stato;
    public $data_creazione;
    public $nazione_esiste;
    public $urlFriendly;
    public $messaggio;


    public function esiste_nazione($nazione){
        global $database;

        $nazione = $database->escape($nazione);

        $sql = "SELECT id FROM " . static::$tabella_db . " WHERE ";
        $sql.= " nazione = '{$nazione}' ";
        $risultato = $database->query($sql);

        if($database->conta_righe($risultato) == 1){

            return $this->nazione_esiste ='<div class="alert alert-danger alert-dismissable"><button type="button" data-dismiss="alert" aria-hidden="true" class="close">×</button>Questa Nazione è già presente.</div>';
        } else {
            header('Location: nazioni.php');
             $_SESSION['mess_successo'] = '<div data-toggle="notify" data-onload data-message="Nazione <b>Aggiunta</b> Correttamente" data-options="{&quot;status&quot;:&quot;success&quot;, &quot;pos&quot;:&quot;bottom-right&quot;}" class="hidden-xs"></div>';
            return $this->salva();
            unset($_SESSION['mess_successo']);

        }
    }

        public function seleziona_tutti_per_stato() {

        global $database;
        return static::cerca_con_query("SELECT * FROM ". static::$tabella_db. " WHERE stato = 1 ");
    }

    public function seleziona_per_categoria()
    {
        global $database;
        return static::cerca_con_query("SELECT * FROM ". static::$tabella_db. " WHERE stato = 1 ");
    }


} // FINE CLASSE NAZIONI