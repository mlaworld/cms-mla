<?php

/**
 * Created da Giuseppe Alessandro De Blasio.
 * User: Giuseppe Alessandro De Blasio
 * Date: 10/10/16
 * Time: 10:13
 */
class Centro extends Db_oggetto
{
    protected static $tabella_db = "centro";
    protected static $campi_tabella_db = array(
        'id_catalogo',
        'Centro',
        'Foto',
        'img_alt',
        'img_title',
        'Lingua',
        'id_nazione',
        'Sistemazione',
        'EtaMin',
        'EtaMax',
        'PrezziDa',
        'CentroUrl',
        'BreveDescrizione',
        'DurataMin',
        'College',
        'Famiglia',
        'Hotel',
        'Residence',
        'GoldPlus',
        'FootballAcademy',
        'DanceAcademy',
        'BasketAcademy',
        'TheatreAcademy',
        'BagnoPrivato',
        'MedicoItaliano',
        'Escursioni',
        'Trinity',
        'Studio',
        'Appartamento',
        'Monolocale',
        'id_categoria',
        'id_citta',
        'id_prezzo',
        'metaTitle',
        'metaDescription',
        'canonicalUrl',
        'metaIndex',
        'urlfriendly',
        'stato',
        'lat',
        'lng',
        'NomeStruttura',
        'IframeMaps',
        'IframeAttrazioni'

    );

    public $id;
    public $id_catalogo;
    public $Centro;
    public $Foto;
    public $img_alt;
    public $img_title;
    public $Lingua;
    public $id_nazione;
    public $Sistemazione;
    public $EtaMin;
    public $EtaMax;
    public $PrezziDa;
    public $CentroUrl;
    public $BreveDescrizione;
    public $DurataMin;
    public $College;
    public $Famiglia;
    public $Hotel;
    public $Residence;
    public $GoldPlus;
    public $FootballAcademy;
    public $DanceAcademy;
    public $BasketAcademy;
    public $TheatreAcademy;
    public $BagnoPrivato;
    public $MedicoItaliano;
    public $Escursioni;
    public $Trinity;
    public $Studio;
    public $Appartamento;
    public $Monolocale;
    public $id_categoria;
    public $id_citta;
    public $id_prezzo;
    public $metaTitle;
    public $metaDescription;
    public $canonicalUrl;
    public $metaIndex;
    public $urlfriendly;
    public $stato;
    public $lat;
    public $lng;
    public $NomeStruttura;
    public $IframeMaps;
    public $IframeAttrazioni;
    public $tipo;
    public $grandezza;
    public $tmp_path;
    public $percorso_immagini = "images/centro";
    public $immagine_plaecholder = "http://placehold.it/600x600text=immagine";
    public $errore = array();
    public $errore_caricamento_array = array (

        UPLOAD_ERR_OK                       =>  "Non c'è alcun errore, il file è stato caricato con successo.",
        UPLOAD_ERR_INI_SIZE                 =>  "Il file caricato supera la direttiva upload_max_filesize in php.ini",
        UPLOAD_ERR_FORM_SIZE                =>  "Il file inviato eccede il MAX_FILE_SIZE che è stato specificato nel modulo HTML",
        UPLOAD_ERR_PARTIAL                  =>  "Il file è stato solo parzialmente caricato.",
        UPLOAD_ERR_NO_FILE                  =>  "Nessun file è stato caricato.",
        UPLOAD_ERR_NO_TMP_DIR               =>  "Manca una cartella temporanea. Introdotta in PHP 5.0.3",
        UPLOAD_ERR_CANT_WRITE               =>  "Impossibile scrivere file su disco. Introdotto in PHP 5.1.0.",
        UPLOAD_ERR_EXTENSION                =>  "Una estensione PHP ha interrotto il caricamento del file."
    );


    public function caricamento_foto_centro() {

        if(!empty($this->errore)){
            return FALSE;
        }

        if (empty($this->Foto) || empty($this->tmp_path)) {
            $this->errore[] = "Il file non è utilizzabile";
            return FALSE;
        }

//        $percorso_immagini = ROOT_SITO . DS . 'admin' . DS . $this->percorso_immagini . DS. $this->Foto;
        $percorso_immagini = ROOT_SITO . DS . $this->percorso_immagini . DS. $this->Foto; // carico sul front-end

        if(file_exists($percorso_immagini)){
            $this->errore[] = "Il file {$this->Foto} già esiste";
            return FALSE;
        }

        if(move_uploaded_file($this->tmp_path, $percorso_immagini)){
            unset($this->tmp_path);
            return TRUE;
        } else {
            $this->errore[] = "Molto probabilmente non si ha il permesso di inserire il file nalla directory";
            return FALSE;
        }
    }

    public function percorso_immagine_e_plecholder() {
        return empty($this->Foto)? $this->immagine_plaecholder : $this->percorso_immagini.DS.$this->Foto;
    }

    public function percorso_immagine_e_plecholder_back() {
        return empty($this->Foto)? $this->immagine_plaecholder : "../..".DS.$this->percorso_immagini.DS.$this->Foto;
    }

    public function impostazione_file_centro($file) {

        if(empty($file) || !$file || !is_array($file)){
            $this->errore[] = "Non c'è alcun file da caricare";
            return FALSE;
        } elseif ($file['error'] != 0 ) {

            $this->errore[] = $this->errore_caricamento_array[$file['error']];
            return FALSE;
        } else {
            $this->Foto = basename($file['name']);
            $this->tmp_path = $file['tmp_name'];
            $this->tipo = $file['type'];
            $this->grandezza = $file['size'];
        }
    }

    public function conta_centri_pubblicati() {
        global $database;

        $sql = "SELECT COUNT(*) FROM " . static::$tabella_db;
        $sql.= " WHERE stato LIKE '%Pubblicato%' ";
        $risultato = $database->query($sql);
        $row = mysqli_fetch_array($risultato);

        return array_shift($row);
    }

    public function seleziona_per_citta($citta, $categoria){

        global $database;

        return static::cerca_con_query("SELECT * FROM ". static::$tabella_db . " WHERE id_citta = $citta AND id_categoria = $categoria ");

    }


    public function duplicazioneRow($id){
        global $database;

        $row = array();


        $sql = "SELECT * FROM " . static::$tabella_db . " WHERE id = $id ";
        $risultato = $database->query($sql);
        $row = mysqli_fetch_assoc($risultato);
        unset($row['id']);


        $proprieta = $this->valoriDuplicazione();

        $sql = "INSERT INTO " . static::$tabella_db."(" .implode("," , array_keys($proprieta)) .")";
        $sql .= " SELECT '" .implode("','", array_values($row)). "' FROM " . static::$tabella_db. " WHERE id = $id ";

    }


    public function cercoLingua($eta){
        global $database;
        $risultato = $database->query($sql = "SELECT DISTINCT Lingua FROM " . static::$tabella_db . " WHERE EtaMin >= $eta") ;
        return $risultato;
    }


    public function cercoNazione($lingua, $eta ){
        global $database;

        return $risultato = $database->query($sql = "SELECT DISTINCT nazioni.nazione FROM nazioni INNER JOIN centro ON nazioni.id = centro.id_nazione WHERE centro.Lingua = '{$lingua}' AND centro.EtaMin >= $eta ");
//        $row = mysqli_fetch_assoc($risultato);
//        return $row;
    }

}// FINE CLASSE CENTRO