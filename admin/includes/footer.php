
<!--   <script src="js/jquery/jquery.min.js"></script>-->

   <script src="js/bootstrap/js/bootstrap.min.js"></script>
   
   <!-- Plugins-->
   <script src="js/chosen/chosen.jquery.min.js"></script>
   <script src="js/slider/js/bootstrap-slider.js"></script>
   <script src="js/filestyle/bootstrap-filestyle.min.js"></script>
   
   <!-- Animo-->
   <script src="js/animate/animate.min.js"></script>
   
   <!-- Sparklines-->
   <script src="js/sparklines/jquery.sparkline.min.js"></script>
   
   <!-- Slimscroll-->
   <script src="js/slimscroll/jquery.slimscroll.min.js"></script>
   
   <!-- Parsley-->
   <script src="js/parsley/parsley.min.js"></script>
   
   <!--  Flot Charts-->
   <script src="js/flot/jquery.flot.min.js"></script>
   <script src="js/flot/jquery.flot.tooltip.min.js"></script>
   <script src="js/flot/jquery.flot.resize.min.js"></script>
   <script src="js/flot/jquery.flot.pie.min.js"></script>
   <script src="js/flot/jquery.flot.time.min.js"></script>
   <script src="js/flot/jquery.flot.categories.min.js"></script>
   <!--[if lt IE 8]><script src="js/excanvas.min.js"></script><![endif]-->
   
   <!--  fullcalendar -->
   <!--<script src="js/fullcalendar/fullcalendar.js"></script>-->
   <!--<script src="js/fullcalendar/fullcalendar.min.js"></script>-->
   
   <!--  Flot Charts -->
      <!--<script src="js/datetimepicker/js/bootstrap-datetimepicker.min.js"></script> -->
   
   <!--  Google Maps -->
   <script src="js/google-map/jquery.gmap.min.js"></script>
   
   <!--  Google Maps -->
   <script src="js/inputmask/jquery.inputmask.bundle.min.js"></script>
   
   <!--  JQuery UI -->
<!--   <script src="js/jqueryui/js/jquery-1.10.2.js"></script>-->
<!--   <script src="js/jqueryui/js/jquery-ui-1.10.4.custom.js"></script>-->
   <script src="js/jqueryui/js/jquery-ui-1.10.4.custom.min.js"></script>
   
   <!--  Marked -->
   <script src="js/marked/marked.js"></script>

   <!--  Tags Imput -->
   <script src="js/tagsinput/bootstrap-tagsinput.min.js"></script>

   <!--  Touch Punch -->
   <!--<script src="js/touch-punch/jquery.ui.touch-punch.min.js"></script> -->

   <!--  dropzone -->
   <script src="js/dropzone/dropzone.js"></script>

   <!-- tinymce -->
   <!--<script src="js/tinymce/js/tinymce/jquery.tinymce.min.js"></script>-->



   <!-- Toastr -->
   <script src="js/toastr/toastr.js"></script>
   <!-- END Toastr-->

   <!-- Data Tables -->
   <script type="text/javascript" src="js/datatables/datatables.min.js"></script>
   <!-- /Data Tables -->

   <script type="text/javascript" src="js/sweetalert/sweetalert2.min.js"></script>

   <!-- App Main-->
   <script src="js/app/app.js"></script>
   <!-- END Scripts-->


<!--   <script type="text/javascript" src="js/moment/min/moment-with-langs.min.js"></script>-->
<!--   <script type="text/javascript" src="js/datetimepicker/js/bootstrap-datetimepicker.min.js"></script>-->


</body>

</html>