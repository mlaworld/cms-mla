<?php

/**
 * Created da Giuseppe Alessandro De Blasio.
 * User: Giuseppe Alessandro De Blasio
 * Date: 30/09/2016
 * Time: 15:35
 */
class cataloghi extends Db_oggetto
{
    protected static $tabella_db = "cataloghi";
    protected static $campi_tabella_db = array('nome_catalogo', 'breve_desc' , 'image', 'stato', 'anno', 'link', 'posizione');
    public $id;
    public $nome_catalogo;
    public $breve_desc;
    public $image;
    public $stato;
    public $anno;
    public $link;
    public $posizione;
    public $tipo;
    public $grandezza;
    public $tmp_path;
    public $percorso_immagini = 'images/cataloghi';
    public $immagine_plaecholder = "http://placehold.it/600x600text=immagine";
    public $errore = array();
    public $errore_caricamento_array = array (
        UPLOAD_ERR_OK                       =>  "Non c'è alcun errore, il file è stato caricato con successo.",
        UPLOAD_ERR_INI_SIZE                 =>  "Il file caricato supera la direttiva upload_max_filesize in php.ini",
        UPLOAD_ERR_FORM_SIZE                =>  "Il file inviato eccede il MAX_FILE_SIZE che è stato specificato nel modulo HTML",
        UPLOAD_ERR_PARTIAL                  =>  "Il file è stato solo parzialmente caricato.",
        UPLOAD_ERR_NO_FILE                  =>  "Nessun file è stato caricato.",
        UPLOAD_ERR_NO_TMP_DIR               =>  "Manca una cartella temporanea. Introdotta in PHP 5.0.3",
        UPLOAD_ERR_CANT_WRITE               =>  "Impossibile scrivere file su disco. Introdotto in PHP 5.1.0.",
        UPLOAD_ERR_EXTENSION                =>  "Una estensione PHP ha interrotto il caricamento del file."
    );

    public function impostazione_file_catalogo($file) {

        if(empty($file) || !$file || !is_array($file)){
            $this->errore[] = "Non c'è alcun file da caricare";
            return FALSE;
        } elseif ($file['error'] != 0 ) {
            $this->errore[] = $this->errore_caricamento_array[$file['error']];
            return FALSE;
        } else {
            $this->image = basename($file['name']);
            $this->tmp_path = $file['tmp_name'];
            $this->tipo = $file['type'];
            $this->grandezza = $file['size'];
        }
    }


    public function caricamento_foto_catalogo() {

        if(!empty($this->errore)){
            return FALSE;
        }

        if (empty($this->image) || empty($this->tmp_path)) {
            $this->errore[] = "Il file non è utilizzabile";
            return FALSE;
        }

        $percorso_immagini = ROOT_SITO . DS .  $this->percorso_immagini . DS. $this->image;

        if(file_exists($percorso_immagini)){
            $this->errore[] = "Il file {$this->image} già esiste";
            return FALSE;
        }

        if(move_uploaded_file($this->tmp_path, $percorso_immagini)){
            unset($this->tmp_path);
            return TRUE;
        } else {
            $this->errore[] = "Molto probabilmente non si ha il permesso di inserire il file nalla directory";
            return FALSE;
        }
    }

    public function rimuoviImg($img){
        $this->image = $img;

        $percorso_immagini = ROOT_SITO . DS . $this->percorso_immagini . DS . $this->image;
//        var_dump($percorso_immagini);
        if(file_exists($percorso_immagini)) {
            unlink($percorso_immagini);
            return TRUE;
        } else {
            $this->errore[] = "Il file {$this->image} già esiste";
            return FALSE;
        }
    }

    public function seleziona_tutti_per_pos() {

        return static::cerca_con_query("SELECT * FROM ". static::$tabella_db. " ORDER BY posizione ASC");
    }

    public function selezione_home_page() {
        return static::cerca_con_query("SELECT * FROM ". static::$tabella_db. " WHERE stato = 1 ORDER BY posizione ASC");
    }

    public function percorso_immagine_e_plecholder() {
        return empty($this->image)? $this->immagine_plaecholder : $this->percorso_immagini.DS.$this->image;
    }

    public function percorso_immagine_e_plecholder_back() {
        return empty($this->image)? $this->immagine_plaecholder : "../".DS.$this->percorso_immagini.DS.$this->image;
    }

    public function percorso_immagine_e_plecholder_front() {
        return empty($this->image)? $this->immagine_plaecholder : "../".$this->percorso_immagini.DS.$this->image;
    }


}