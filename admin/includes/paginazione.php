<?php

class Paginazione {
    
    public $pagina_corrente;
    public $elementi_per_pagina;
    public $elementi_totali;
    public $numero_pagine;
    protected $mid_range;


    public function __construct($pagina = 1, $elementi_per_pagina = 4, $elementi_totali = 0, $mid_range=1) {
       
        $this->pagina_corrente = (int)$pagina;
        $this->elementi_per_pagina = (int)$elementi_per_pagina;
        $this->elementi_totali = (int)$elementi_totali;
        $this->mid_range = (int) $mid_range; // midrange must be an odd int >= 1
	if($this->mid_range %2 == 0 Or $this->mid_range < 1) exit("Unable to paginate: Invalid mid_range value (must be an odd integer >= 1)");
        
        if($this->numero_pagine > 10) {
            $this->start_range = $this->pagina_corrente - floor($this->mid_range/2);
			$this->end_range = $this->pagina_corrente + floor($this->mid_range/2);
			if($this->start_range <= 0) {
				$this->end_range += abs($this->start_range)+1;
				$this->start_range = 1;
			}
			if($this->end_range > $this->numero_pagine) {
				$this->start_range -= $this->end_range-$this->numero_pagine;
				$this->end_range = $this->numero_pagine;
			}
        
        }
    }
    
    public function dopo() {
        return $this->pagina_corrente + 1;
    }
    
    public function prima() {
        return $this->pagina_corrente - 1;
    }
    
    public function pagine_totali() {
        return ceil($this->elementi_totali/$this->elementi_per_pagina);
    }
    
    public function e_prima() {
        return $this->prima() >= 1 ? TRUE : FALSE ;
    }
    
    public function e_dopo() {
        return $this->dopo() <= $this->pagine_totali() ? TRUE : FALSE ;
    }
    
    public function offset() {
        return ($this->pagina_corrente -1 ) * $this->elementi_per_pagina;
    }
    
    
}