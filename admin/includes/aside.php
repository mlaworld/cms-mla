      <!-- START aside-->
      <aside class="aside">
         <!-- START Sidebar (left)-->
         <nav class="sidebar">
            <ul class="nav">
               <!-- START user info-->
               <li>
                  <div data-toggle="collapse-next" class="item user-block has-submenu">
                     <!-- User picture-->
                     <div class="user-block-picture">
                        <img src="<?php $utente = Utente::seleziona_per_id($_SESSION['id_utente']); echo $utente->percorso_immagine_e_plecholder(); ?>" alt="<?php echo $utente->percorso_immagine_e_plecholder();?>" width="60" height="60" class="img-thumbnail img-circle">
                        <!-- Status when collapsed-->
                        <div class="user-block-status">
                           <div class="point point-success point-lg"></div>
                        </div>
                     </div>
                     <!-- Name and Role-->
                     <div class="user-block-info">
                         <span class="user-block-name item-text">Benvenuto, <?php $utente = Utente::seleziona_per_id($_SESSION['id_utente']); echo $utente->nome; ?></span>
                        <span class="user-block-role">Designer</span>
                        <!-- START Dropdown to change status-->
                        <div class="btn-group user-block-status">
                           <button type="button" data-toggle="dropdown" data-play="fadeIn" data-duration="0.2" class="btn btn-xs dropdown-toggle">
                              <div class="point point-success"></div>Online</button>
                           <ul class="dropdown-menu text-left pull-right">
                              <li>
                                 <a href="#">
                                    <div class="point point-success"></div>Online</a>
                              </li>
                              <li>
                                 <a href="#">
                                    <div class="point point-warning"></div>Non Online</a>
                              </li>
                              <li>
                                 <a href="#">
                                    <div class="point point-danger"></div>Occupato</a>
                              </li>
                           </ul>
                        </div>
                        <!-- END Dropdown to change status-->
                     </div>
                  </div>
                  <!-- START User links collapse-->
                  <ul class="nav collapse">
                     <li><a href="profilo.php">Profilo</a>
                     </li>
                     <li><a href="#">Impostazioni</a>
                     </li>
                     <li><a href="#">Notifiche<div class="label label-danger pull-right">120</div></a>
                     </li>
                     <li><a href="#">Messaggi<div class="label label-success pull-right">300</div></a>
                     </li>
                     <li class="divider"></li>
                     <li><a href="esci.php">Esci</a>
                     </li>
                  </ul>
                  <!-- END User links collapse-->
               </li>
               <!-- END user info-->
               <!-- START Menu-->
               
                <li <?php echo basename($_SERVER['PHP_SELF']) == "index.php" ? 'class="active"' : ""; ?> >  
                  <a href="index.php" title="Dashboard" data-toggle=".collapse-next" class="has-submenu">
                     <em class="fa fa-dashboard"></em>
                     <!-- <div class="label label-primary pull-right">12</div> -->
                     <span class="item-text">Dashboard</span>
                  </a>
                  <!-- START SubMenu item-->
                  <ul class="nav collapse in">
                     <!--<li class="active">
                        <a href="index.php" title="Default" data-toggle="" class="no-submenu">
                           <span class="item-text">Default</span>
                        </a>
                     </li>-->
                     <!--<li>
                        <a href="dashboard-nosidebar.html" title="No Sidebar" data-toggle="" class="no-submenu">
                           <div class="label label-primary pull-right">new</div>
                           <span class="item-text">Nessuna Sidebar</span>
                        </a>
                     </li>
                     <li>
                        <a href="dashboard-noprofile.html" title="No Profile" data-toggle="" class="no-submenu">
                           <div class="label label-primary pull-right">new</div>
                           <span class="item-text">Nessun Profilo</span>
                        </a>
                     </li>-->
                  </ul>
                  <!-- END SubMenu item-->
               </li>

               <li <?php echo basename($_SERVER['PHP_SELF']) == "menu/index.php?act=menu" ? 'class="active"' : ""; ?> >
                  <a href="menu.php" title="Gestione Menu" data-toggle="" class="no-submenu">
                     <em class="fa fa-desktop"></em>
                     <span class="item-text">Gestione Menu</span>
                  </a>
               </li>

               <li  <?php echo basename($_SERVER['PHP_SELF']) == "#" ? 'class="active"' : ""; ?>>
                  <a href="#" title="Media" data-toggle="collapse-next" class="has-submenu">
                     <em class="fa fa-image"></em>
                     <span class="item-text">Media Upload</span>
                  </a>
                  <!-- START SubMenu item-->
                  <ul class="nav collapse">
                     <li <?php echo basename($_SERVER['PHP_SELF']) == "upload.php" ? 'class="active"' : ""; ?> >
                        <a href="upload.php" title="Carica Immagini" data-toggle="" class="no-submenu">
                           <span class="item-text">Carica Immagini</span>
                        </a>
                     </li>

                     <li <?php echo basename($_SERVER['PHP_SELF']) == "gestione_upload.php" ? 'class="active"' : ""; ?> >
                        <a href="gestione_upload.php" title="Gestisci Immagini" data-toggle="" class="no-submenu">
                           <span class="item-text">Gestisci Immagini</span>
                        </a>
                     </li>
                  </ul>
               </li>

               <!--      PROMOZIONI MLA       -->
               <li  <?php echo basename($_SERVER['PHP_SELF']) == "#" ? 'class="active"' : ""; ?>>
                  <a href="#" title="Promozioni MLA" data-toggle="collapse-next" class="has-submenu">
                     <em class="fa fa-calendar-o"></em>
                     <span class="item-text">Promo</span>
                  </a>
                  <!-- SubMenu PROMO item-->
                  <ul class="nav collapse">
                     <li <?php echo basename($_SERVER['PHP_SELF']) == "elenco_promo.php" ? 'class="active"' : ""; ?> >
                        <a href="elenco_promo.php" title="Elenco Promozioni" data-toggle="" class="no-submenu">
                           <span class="item-text">Elenco Promozioni</span>
                        </a>
                     </li>

<!--                     <li --><?php //echo basename($_SERVER['PHP_SELF']) == "gestione_promo.php" ? 'class="active"' : ""; ?><!-- >-->
<!--                        <a href="gestione_promo.php" title="Gestisci Promo" data-toggle="" class="no-submenu">-->
<!--                           <span class="item-text">Gestione Promo</span>-->
<!--                        </a>-->
<!--                     </li>-->
                  </ul>
                  <!-- /SubMenu PROMO item-->
               </li>
               <!--      /PROMOZIONI MLA       -->

               <!--      EVENTI MLA       -->
               <li  <?php echo basename($_SERVER['PHP_SELF']) == "#" ? 'class="active"' : ""; ?>>
                  <a href="#" title="Eventi MLA" data-toggle="collapse-next" class="has-submenu">
                     <em class="fa fa-calendar"></em>
                     <span class="item-text">Eventi MLA</span>
                  </a>
                  <!-- SubMenu EVENTI item-->
                  <ul class="nav collapse">
                     <li <?php echo basename($_SERVER['PHP_SELF']) == "categoria_eventi.php" ? 'class="active"' : ""; ?> >
                        <a href="categoria_eventi.php" title="Categoria Eventi MLA" data-toggle="" class="no-submenu">
                           <span class="item-text">Categoria Eventi MLA</span>
                        </a>
                     </li>
                  </ul>
                  <ul class="nav collapse">
                     <li <?php echo basename($_SERVER['PHP_SELF']) == "elenco_eventi.php" ? 'class="active"' : ""; ?> >
                        <a href="elenco_eventi.php" title="Elenco Eventi MLA" data-toggle="" class="no-submenu">
                           <span class="item-text">Elenco Eventi MLA</span>
                        </a>
                     </li>
                  </ul>
               </li>
                     <!--      /EVENTI MLA       -->
               
<!--               <li --><?php //echo basename($_SERVER['PHP_SELF']) == "categorie_post.php" ? 'class="active"' : ""; ?><!-- >-->
<!--                   <a href="categorie_post.php" title="Categorie" data-toggle="" class="has-submenu">-->
<!--                     <em class="fa fa-pencil-square-o"></em>-->
<!--                     <span class="item-text">Categorie Pagine</span>-->
<!--                  </a>-->
<!--               </li>-->
               
<!--               <li --><?php //echo basename($_SERVER['PHP_SELF']) == "#" ? 'class="active"' : ""; ?><!-- >-->
<!--                  <a href="#" title="Pagine" data-toggle="collapse-next" class="has-submenu">-->
<!--                     <em class="fa fa-file-text-o"></em>-->
<!--                     <span class="item-text">Pagine</span>-->
<!--                  </a>-->
                  <!-- START SubMenu item-->
<!--                  <ul class="nav collapse ">-->

<!--                     <li --><?php //echo basename($_SERVER['PHP_SELF']) == "elenco_page.php" ? 'class="active"' : ""; ?><!-- >-->
<!--                        <a href="elenco_page.php" title="Elenco Articoli" data-toggle="" class="no-submenu">-->
<!--                           <span class="item-text">Elenco Pagine</span>-->
<!--                        </a>-->
<!--                     </li>-->
<!---->
<!--                     <li --><?php //echo basename($_SERVER['PHP_SELF']) == "aggiungi_page.php" ? 'class="active"' : ""; ?><!-- >-->
<!--                        <a href="aggiungi_page.php" title="Aggiungi Articoli" data-toggle="" class="no-submenu">-->
<!--                           <span class="item-text">Aggiungi Pagine</span>-->
<!--                        </a>-->
<!--                     </li>-->
<!--                  </ul>-->
                  <!-- END SubMenu item-->
<!--               </li>-->

               <li <?php echo basename($_SERVER['PHP_SELF']) == "nazioni.php" ? 'class="active"' : ""; ?> >
                  <a href="nazioni.php" title="Nazioni" data-toggle="" class="no-submenu">
                     <em class="fa fa-tasks"></em>
                     <span class="item-text">Gestione Nazioni</span>
                  </a>
               </li>

               <!-- SubMenu EVENTI item-->
               <li  <?php echo basename($_SERVER['PHP_SELF']) == "#" ? 'class="active"' : ""; ?>>
                  <a href="#" title="Gestione Categorie" data-toggle="collapse-next" class="has-submenu">
                     <em class="fa fa-tasks"></em>
                     <span class="item-text">Gestione Categorie</span>
                  </a>
                  <ul class="nav collapse">
                     <li <?php echo basename($_SERVER['PHP_SELF']) == "categorie_post.php" ? 'class="active"' : ""; ?> >
                        <a href="categorie_post.php" title="Gestione Categorie" data-toggle="" class="no-submenu">
                           <span class="item-text">Gestione Categorie</span>
                        </a>
                     </li>
                  </ul>
               </li>

               <li <?php echo basename($_SERVER['PHP_SELF']) == "cataloghi.php" ? 'class="active"' : ""; ?> >
                  <a href="cataloghi.php" title="Gestione Cataloghi" data-toggle="" class="no-submenu">
                     <em class="fa fa-tasks"></em>
                     <span class="item-text">Gestione Cataloghi</span>
                  </a>
               </li>

               <hr>

               <li <?php echo basename($_SERVER['PHP_SELF']) == "#" ? 'class="active"' : ""; ?> >
                  <a href="#" title="Gestione Pagina Categorie" data-toggle="collapse-next" class="has-submenu">
                     <em class="fa fa-globe"></em>
                     <span class="item-text">Gestione Pag. Categorie</span>
                  </a>
                  <!-- START SubMenu item-->
                  <ul class="nav collapse ">

                     <li <?php echo basename($_SERVER['PHP_SELF']) == "elenco_categorie.php" ? 'class="active"' : ""; ?> >
                        <a href="elenco_categorie.php" title="Elenco Categorie" data-toggle="" class="no-submenu">
                           <span class="item-text">Elenco Pag. Categorie</span>
                        </a>
                     </li>

                     <li <?php echo basename($_SERVER['PHP_SELF']) == "aggiungi_nazioni_page.php" ? 'class="active"' : ""; ?> >
                        <a href="aggiungi_categorie_page.php" title="Aggiungi Categoria" data-toggle="" class="no-submenu">
                           <span class="item-text">Aggiungi Pag. Categoria</span>
                        </a>
                     </li>
                  </ul>
                  <!-- END SubMenu item-->
               </li>

               <li <?php echo basename($_SERVER['PHP_SELF']) == "#" ? 'class="active"' : ""; ?> >
                  <a href="#" title="Gestione Pagina Nazioni" data-toggle="collapse-next" class="has-submenu">
                     <em class="fa fa-globe"></em>
                     <span class="item-text">Gestione Pag. Nazioni</span>
                  </a>
                  <!-- START SubMenu item-->
                  <ul class="nav collapse ">

                     <li <?php echo basename($_SERVER['PHP_SELF']) == "elenco_nazioni.php" ? 'class="active"' : ""; ?> >
                        <a href="elenco_nazioni.php" title="Elenco Nazioni" data-toggle="" class="no-submenu">
                           <span class="item-text">Elenco Pag. Nazioni</span>
                        </a>
                     </li>

                     <?php  $utente = Utente::seleziona_per_id($_SESSION['id_utente']); if($utente->ruolo == "admin"): ?>
                     <li <?php echo basename($_SERVER['PHP_SELF']) == "aggiungi_nazioni_page.php" ? 'class="active"' : ""; ?> >
                        <a href="aggiungi_nazioni_page.php" title="Aggiungi Pagina Nazione" data-toggle="" class="no-submenu">
                           <span class="item-text">Aggiungi Pag. Nazione</span>
                        </a>
                     </li>
                     <?php endif; ?>
                      
                      <?php $catNazioni = CategoriePagine::seleziona_tutti_per_stato(); ?>
                      <?php foreach ($catNazioni as $categorie): ?>
                          <li <?php echo basename($_SERVER['PHP_SELF']) == "aggiungi_nazioni_page.php?cat=$categorie->id" ? 'class="active"' : ""; ?> >
                              <a href="aggiungi_nazioni_page.php?cat=<?php echo $categorie->id ?>" title="Aggiungi Pagina Nazione" data-toggle="" class="no-submenu">
                                  <span class="item-text">Aggiungi per <?php echo $categorie->titolo_categoria; ?></span>
                              </a>
                          </li>
                      <?php endforeach; ?>
                      
                  </ul>
                  <!-- END SubMenu item-->
               </li>

               <li <?php echo basename($_SERVER['PHP_SELF']) == "#" ? 'class="active"' : ""; ?> >
                  <a href="#" title="Gestione Pagina Città" data-toggle="collapse-next" class="has-submenu">
                     <em class="fa fa-building-o"></em>
                     <span class="item-text">Gestione Pag. Città</span>
                  </a>
                  <!-- START SubMenu item-->
                  <ul class="nav collapse ">

                     <li <?php echo basename($_SERVER['PHP_SELF']) == "elenco_citta.php" ? 'class="active"' : ""; ?> >
                        <a href="elenco_citta.php" title="Elenco Città" data-toggle="" class="no-submenu">
                           <span class="item-text">Elenco Pag. Città</span>
                        </a>
                     </li>

                     <li <?php echo basename($_SERVER['PHP_SELF']) == "aggiungi_città_page.php" ? 'class="active"' : ""; ?> >
                        <a href="aggiungi_citta_page.php" title="Aggiungi Pagina Città" data-toggle="" class="no-submenu">
                           <span class="item-text">Aggiungi Pag. Città</span>
                        </a>
                     </li>

                      <?php $catNazioni = CategoriePagine::seleziona_tutti_per_stato(); ?>
                      <?php foreach ($catNazioni as $categorie): ?>
                     <li <?php echo basename($_SERVER['PHP_SELF']) == "aggiungi_città_page.php?cat=$categorie->id" ? 'class="active"' : ""; ?> >
                        <a href="aggiungi_citta_page.php?cat=<?php echo $categorie->id ?>" title="Aggiungi Pagina Città" data-toggle="" class="no-submenu">
                           <span class="item-text">Aggiungi ciità per <?php echo $categorie->titolo_categoria;?></span>
                        </a>
                     </li>
                      <?php endforeach; ?>
                  </ul>
                  <!-- END SubMenu item-->
               </li>

               <li <?php echo basename($_SERVER['PHP_SELF']) == "#" ? 'class="active"' : ""; ?> >
                  <a href="#" title="Gestione Centri" data-toggle="collapse-next" class="has-submenu">
                     <em class="fa fa-dot-circle-o"></em>
                     <span class="item-text">Gestione Pag. Centri</span>
                  </a>
                  <!-- START SubMenu item-->
                  <ul class="nav collapse ">


                     <li <?php echo basename($_SERVER['PHP_SELF']) == "aggiungi_centro.php" ? 'class="active"' : ""; ?> >
                        <a href="aggiungi_centro.php" title="Aggiungi Centro" data-toggle="" class="no-submenu">
                           <span class="item-text">Aggiungi Centro</span>
                        </a>
                     </li>

                     <li <?php echo basename($_SERVER['PHP_SELF']) == "aggiungi_centro_adulti.php" ? 'class="active"' : ""; ?> >
                        <a href="aggiungi_centro_adulti.php" title="Aggiungi Centro Adulti" data-toggle="" class="no-submenu">
                           <span class="item-text">Aggiungi Centro Adulti</span>
                        </a>
                     </li>

                     <li <?php echo basename($_SERVER['PHP_SELF']) == "elenco_centri.php" ? 'class="active"' : ""; ?> >
                        <a href="elenco_centri.php" title="Elenco Centri" data-toggle="" class="no-submenu">
                           <span class="item-text">Elenco Centri</span>
                        </a>
                     </li>

                  </ul>
                  <!-- END SubMenu item-->
               </li>
               
               <!-- <li <?php //echo basename($_SERVER['PHP_SELF']) == "commenti.php" ? 'class="active"' : ""; ?> >
                   <a href="commenti.php" title="Categorie" data-toggle=".collapse-next" class="has-submenu">
                     <em class="fa fa-bar-chart-o"></em>
                     <span class="item-text">Commenti</span>
                  </a>
                </li> -->
               
               <!-- <li>
                  <a href="#" title="Forms" data-toggle="collapse-next" class="has-submenu">
                     <em class="fa fa-edit"></em>
                     <span class="item-text">Forms</span>
                  </a> -->
                  <!-- START SubMenu item-->
                  <!-- <ul class="nav collapse ">
                     <li>
                        <a href="#" title="Standard" data-toggle="" class="no-submenu">
                           <span class="item-text">Standard</span>
                        </a>
                     </li>
                     <li>
                        <a href="#" title="Extended" data-toggle="" class="no-submenu">
                           <div class="label label-primary pull-right">new</div>
                           <span class="item-text">Extended</span>
                        </a>
                     </li>
                     <li>
                        <a href="#" title="Validation" data-toggle="" class="no-submenu">
                           <span class="item-text">Validation</span>
                        </a>
                     </li>
                  </ul> -->
                  <!-- END SubMenu item-->
               <!-- </li> -->
               <!-- <li>
                  <a href="#" title="Elements" data-toggle="collapse-next" class="has-submenu">
                     <em class="fa fa-wrench"></em>
                     <span class="item-text">Elements</span>
                  </a> __>
                  <!-- START SubMenu item-->
                  <!-- <ul class="nav collapse ">
                     <li>
                        <a href="#" title="Panels" data-toggle="" class="no-submenu">
                           <div class="label label-primary pull-right">new</div>
                           <span class="item-text">Panels</span>
                        </a>
                     </li>
                     <li>
                        <a href="#" title="Portlets" data-toggle="" class="no-submenu">
                           <span class="item-text">Portlets</span>
                        </a>
                     </li>
                     <li>
                        <a href="#" title="Buttons" data-toggle="" class="no-submenu">
                           <span class="item-text">Buttons</span>
                        </a>
                     </li>
                     <li>
                        <a href="#" title="Icons" data-toggle="" class="no-submenu">
                           <div class="label label-primary pull-right">+400</div>
                           <span class="item-text">Icons</span>
                        </a>
                     </li>
                     <li>
                        <a href="#" title="Notifications" data-toggle="" class="no-submenu">
                           <span class="item-text">Notifications</span>
                        </a>
                     </li>
                     <li>
                        <a href="#" title="Typography" data-toggle="" class="no-submenu">
                           <span class="item-text">Typography</span>
                        </a>
                     </li>
                     <li>
                        <a href="#" title="Grid" data-toggle="" class="no-submenu">
                           <span class="item-text">Grid</span>
                        </a>
                     </li>
                     <li>
                        <a href="#" title="Grid Masonry" data-toggle="" class="no-submenu">
                           <span class="item-text">Grid Masonry</span>
                        </a>
                     </li>
                     <li>
                        <a href="#" title="Animations" data-toggle="" class="no-submenu">
                           <span class="item-text">Animations</span>
                        </a>
                     </li>
                     <li>
                        <a href="#" title="Dropdown" data-toggle="" class="no-submenu">
                           <span class="item-text">Dropdown</span>
                        </a>
                     </li>
                     <li>
                        <a href="#" title="Widgets" data-toggle="" class="no-submenu">
                           <span class="item-text">Widgets</span>
                        </a>
                     </li>
                     <li>
                        <a href="#" title="Maps" data-toggle="" class="no-submenu">
                           <span class="item-text">Maps</span>
                        </a>
                     </li>
                     <li>
                        <a href="#" title="Calendar" data-toggle="" class="no-submenu">
                           <span class="item-text">Calendar</span>
                        </a>
                     </li>
                     <li>
                        <a href="#" title="Spinners" data-toggle="" class="no-submenu">
                           <div class="label label-primary pull-right">new</div>
                           <span class="item-text">Spinners</span>
                        </a>
                     </li>
                  </ul> -->
                  <!-- END SubMenu item-->
               <!-- </li> -->
               <!-- <li>
                  <a href="#" title="Pages" data-toggle="collapse-next" class="has-submenu">
                     <em class="fa fa-file-text"></em>
                     <div class="label label-primary pull-right">new</div>
                     <span class="item-text">Pages</span>
                  </a> -->
                     <!-- START SubMenu item-->
                     <!-- <ul class="nav collapse ">
                        <li>
                           <a href="#" title="Landing" data-toggle="" class="no-submenu">
                              <span class="item-text">Landing</span>
                           </a>
                        </li>
                        <li>
                           <a href="#" title="Login" data-toggle="" class="no-submenu">
                              <span class="item-text">Login</span>
                           </a>
                        </li>
                        <li>
                           <a href="#" title="Login Multi" data-toggle="" class="no-submenu">
                              <span class="item-text">Login Multi</span>
                           </a>
                        </li>
                        <li>
                           <a href="#" title="Sign up" data-toggle="" class="no-submenu">
                              <span class="item-text">Sign up</span>
                           </a>
                        </li>
                        <li>
                           <a href="#" title="Lock" data-toggle="" class="no-submenu">
                              <span class="item-text">Lock</span>
                           </a>
                        </li>
                        <li>
                           <a href="#" title="Recover Password" data-toggle="" class="no-submenu">
                              <span class="item-text">Recover Password</span>
                           </a>
                        </li>
                        <li>
                           <a href="#" title="Empty Template" data-toggle="" class="no-submenu">
                              <span class="item-text">Empty Template</span>
                           </a>
                        </li>
                        <li>
                           <a href="#" title="Timeline" data-toggle="" class="no-submenu">
                              <span class="item-text">Timeline</span>
                           </a>
                        </li>
                     </ul> -->
                     <!-- END SubMenu item-->
               <!-- </li> __>
               <!-- END Menu-->
               
               <!-- menu footer    -->
              <?php include 'menu_footer.php';?>
               
            </ul>
         </nav>
         <!-- END Sidebar (left)-->
      </aside>