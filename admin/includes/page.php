<?php
/**
 * Created da Giuseppe Alessandro De Blasio.
 * User: Giuseppe Alessandro De Blasio
 * Date: 20/09/2016
 * Time: 16:16
 */

class Page extends Db_oggetto {

    protected static $tabella_db = "page";
    protected static $campi_tabella_db = array(
        'titolo',
        'contenuto' ,
        'url_friendly',
        'meta_title',
        'meta_description',
        'canonical_url',
        'meta_robots',
        'categoria_page',
        'stato',
        'autore',
        'data_modifica',
        'data_creazione'
    );

    public $id;
    public $titolo;
    public $contenuto;
    public $stato;
    public $autore;
    public $url_friendly;
    public $meta_title;
    public $meta_description;
    public $canonical_url;
    public $meta_robots;
    public $categoria_page;
    public $data_modifica;
    public $data_creazione;

} // FINE CLASSE PAGE