<?php
/**
 * Created da Giuseppe Alessandro De Blasio.
 * User: Giuseppe Alessandro De Blasio
 * Date: 20/09/17
 * Time: 11:32
 */

class Mailer extends PHPMailer
{

    private $_host = 'smtp.mlaworld.com';
    private $_user = 'noreply@mlaworld.com';
    private $_password = 'easy.2015.mla';


    public function __construct($exceptions = null)
    {
        parent::__construct($exceptions);
        $this->isSMTP();
        $this->isHTML();
        $this->CharSet = "UTF-8";
//        $this->msgHTML($this->message);
        $this->Host = $this->_host;
        $this->SMTPAuth = true;
        $this->Username = $this->_user;
        $this->Password = $this->_password;
//        $this->SMTPSecure = 'tls';
//        $this->SMTPSecure = false;
//        $this->Port = 587;
        $this->Port = 25;
    }

    public function sendMail($from, $to, $subject, $body)
    {
        $this->setFrom($from);
        $this->addAddress($to);
        $this->Subject = $subject;
        $this->Body = $body;


        return $this->send();
    }

//    public function setFrom($address, $name = '', $auto = true)
//    {
//        return parent::setFrom($address, $name = '', $auto = true);
//    }
//
//    /**
//     * Overrides parent send()
//     *
//     * @return boolean
//     */
//    public function send() {
//        echo 'Hello from my subclass';
//        return parent::send();
//    }
}