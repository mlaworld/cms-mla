<?php


//getcwd(); conoscere il percorso assoluto del sito

// Scrivo la DS del SITO IN LOCALE
//defined('DS') ? null : define('DS', DIRECTORY_SEPARATOR);

// Scrivo la DS del SITO IN LOCALE E REMOTO
($_SERVER['REMOTE_ADDR'] == '::1')? defined('DS') ? null : define('DS', DIRECTORY_SEPARATOR): defined('DS') ? null : define('DS', '/');


// Scrivo la ROOT del SITO IN LOCALE
//define('ROOT_SITO', 'C:'. DS . 'xampp' .DS. 'htdocs'. DS .'mla-cms-new');

//Quando carico su Server
//define('ROOT_SITO', $_SERVER['DOCUMENT_ROOT']. DS. 'mla-cms');

//($_SERVER['REMOTE_ADDR'] == '::1')? define('ROOT_SITO', 'C:'. DS . 'xampp' .DS. 'htdocs'. DS .'mla-cms-new'): define('ROOT_SITO', $_SERVER['DOCUMENT_ROOT']. DS. 'cms-mla');
($_SERVER['REMOTE_ADDR'] == '::1')? define('ROOT_SITO', 'C:'. DS . 'xampp' .DS. 'htdocs'. DS .'mla-cms-new'): define('ROOT_SITO', $_SERVER['DOCUMENT_ROOT']);


// Scrivo la ROOT della cartella includes nella ADMIN AREA
defined('PERCORSO_INCLUDES')? NULL : define('PERCORSO_INCLUDES', ROOT_SITO. DS .  'admin' . DS . 'includes');


// Scrivo la ROOT della cartella ADMIN AREA
//defined('PERCORSO_INCLUDES_ADMIN')? NULL : define('PERCORSO_INCLUDES_ADMIN', ROOT_SITO. DS .  'admin');

// Scrivo la ROOT della cartella includes nel FRONT DEL SITO
defined('PERCORSO_INCLUDES_FRONT')? NULL : define('PERCORSO_INCLUDES_FRONT', ROOT_SITO. DS . 'includes');

// Percorso per le images Admin
define('PERCORSO_IMMAGINE', ROOT_SITO. DS .  'admin' . DS . 'images');

// Percorso per le images Front
define('PERCORSO_IMMAGINE_FRONT', ROOT_SITO. DS. 'img');


ob_start();

require_once (PERCORSO_INCLUDES.DS.'config.php');
require_once (PERCORSO_INCLUDES.DS.'funzioni.php');
//require_once (PERCORSO_INCLUDES.DS.'nazione.php');
require_once (PERCORSO_INCLUDES.DS.'db.php');
require_once (PERCORSO_INCLUDES.DS.'databasePDO.php');
require_once (PERCORSO_INCLUDES.DS.'sessione.php');
require_once (PERCORSO_INCLUDES.DS.'utente.php');
require_once (PERCORSO_INCLUDES.DS.'auth.php');
require_once (PERCORSO_INCLUDES.DS.'apiClass.php');
require_once (PERCORSO_INCLUDES.DS.'image.php');
require_once (PERCORSO_INCLUDES.DS.'categorie_post.php');
//require_once (PERCORSO_INCLUDES.DS.'cerca.php');
//require_once (PERCORSO_INCLUDES.DS.'templates.php');
require_once (PERCORSO_INCLUDES.DS.'paginazione.php');
require_once (PERCORSO_INCLUDES.DS.'db_oggetto.php');
require_once (PERCORSO_INCLUDES.DS.'phpmailer/class.phpmailer.php');
require_once (PERCORSO_INCLUDES.DS.'FlashMessages.php');
//require_once (PERCORSO_INCLUDES.DS.'amp/src/AmpConverter.php');
