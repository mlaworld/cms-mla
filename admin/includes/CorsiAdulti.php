<?php

/**
 * Created da Giuseppe Alessandro De Blasio.
 * User: Giuseppe Alessandro De Blasio
 * Date: 13/04/17
 * Time: 11:16
 */
class CorsiAdulti extends Db_oggetto
{
    protected static $tabella_db = "corsi_adulti";
    protected static $campi_tabella_db = array(
        'id_centro',
        'nome_corso',
        'num_settimane',
        'livello',
        'prezzo_da_corso',
        'eta_min',
        'durata_corso',
        'durata_lezione',
        'orario_lezione',
        'studenti_per_classe',
        'arrivo',
        'partenze',
        'chiusura_scuola',
        'inizio_corso'
    );

    public $id;
    public $id_centro;
    public $nome_corso;
    public $num_settimane;
    public $livello;
    public $prezzo_da_corso;
    public $eta_min;
    public $durata_corso;
    public $durata_lezione;
    public $orario_lezione;
    public $studenti_per_classe;
    public $arrivo;
    public $partenze;
    public $chiusura_scuola;
    public $inizio_corso;


}