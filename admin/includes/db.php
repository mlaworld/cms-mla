<?php

/**
 * Created da Giuseppe Alessandro De Blasio.
 * User: Giuseppe Alessandro De Blasio
 * Date: 17/09/2016
 * Time: 11:56
 */

class Database {

    public $conn;

    // EASY MENU MANAGER
    var $link;
    var $result;
    // FINE VARIABILI EASY MENU MANAGER

    public function __construct() {

        $this->apro_con_db();
    }


    public function apro_con_db() {

        $this->conn = new mysqli(DB_HOST, DB_USER, DB_PASSWORD, DB_NAME);

        if ($this->conn->connect_errno) {
            die("Impossibile stabilire la connessione con il db " . $this->conn->connect_errno);
        }

    }

    public function queryPrepare($sql)
    {
        $stmt = $this->conn->prepare($sql);
        return $stmt;
    }

    public function query($sql) {

        $risultato = $this->conn->query($sql);

        $this->conferma_query($risultato);

        return $risultato;

    }

    public function conferma_query($risultato) {

        if(!$risultato){
//            die("Query Fallita ". $this->conn->error);

            $errore = $this->conn->error;
            echo $result = '
            <script> 
               $(document).ready(function(){
                        swal("' . $errore .'", "Contattare Giuseppe per maggiori info!", "error");
            });
            </script>';

        }
    }

    public function escape($stringa){

        $escape_stringa = $this->conn->real_escape_string($stringa);
        return $escape_stringa;
    }

    public function inserisco_id() {

        return mysqli_insert_id($this->conn);
    }

    public function insert_id(){
        return $this->conn->insert_id;
    }

    public function conta_righe($risultato)
    {
        return mysqli_num_rows($risultato);
    }

    /**
     * @return mixed
     */
    public function chiudoConnessione()
    {
        return $this->conn = mysqli_close($this->conn);
    }

    public function valoreUnico(){
       return ($this->conn = mysqli_errno($this->conn) == 1062)? "<script>alert('Errore')</script>" : "";
    }



} // FINE CLASSE DATABASE

$database = new Database();