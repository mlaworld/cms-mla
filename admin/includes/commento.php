<?php

class Commento extends Db_oggetto {
    
    protected static $tabella_db = "commenti";
    protected static $campi_tabella_db = array('articolo_id', 'autore', 'immagine_utente', 'risp_commento_id', 'email', 'testo_commento', 'stato', 'data');
    public $id;
    public $articolo_id;
    public $autore;
    public $immagine_utente;
    public $risp_commento_id;
    public $email;
    public $testo_commento;
    public $stato;
    public $data;
    
    
    public static function crea_commento($articolo_id, $autore="Guest",  $email="", $testo_commento="", $stato="non approvato", $data = "" ) {
        
        if(!empty($articolo_id) && !empty($autore) && !empty($email) && !empty($testo_commento) && !empty($stato) && !empty($data)){
            $commento = new Commento();
            $commento->articolo_id = (int)$articolo_id;
            $commento->autore  = $autore;
            //$commento->immagine_utente = $immagine_utente;
            //$commento->risp_commento_id = $risp_commento_id;
            $commento->email = $email;
            $commento->testo_commento = $testo_commento;
            $commento->stato = $stato = "non approvato";
            $commento->data = $data = date("Y-m-d H:i:s");
            
            return $commento;
            
        } else {
            return FALSE;
        }
        
    }
    
    public static function cerca_commento($articolo_id = 0){
        global $database;
        
        $sql = "SELECT * FROM " . self::$tabella_db ;
        $sql.= " WHERE articolo_id = " .$database->escape($articolo_id);
        $sql.= " ORDER BY articolo_id DESC";
        
        return self::cerca_con_query($sql);
    }
    
    public static function rispondi_commento($articolo_id, $autore="Guest", $risp_commento_id = 0, $email="", $testo_commento="", $stato="non approvato", $data = ""){
        
        if (!empty($articolo_id) && !empty($autore) && !empty($email) && !empty($risp_commento_id) && !empty($testo_commento)  && !empty($stato) && !empty($data)){
            $risp_commento = new Commento();
            $risp_commento->articolo_id = (int)$articolo_id;
            $risp_commento->autore  = $autore;
            //$risp_commento->immagine_utente = $immagine_utente;
            $risp_commento->risp_commento_id = (int)$risp_commento_id;
            $risp_commento->email = $email;
            $risp_commento->testo_commento = $testo_commento;
            $risp_commento->stato = $stato = "non approvato";
            $risp_commento->data = $data = date("Y-m-d H:i:s");
            
            return $risp_commento;
        } else {
            return FALSE;
        }
        
    }
    
    public static function cerca_risp_commento($articolo_id = 0 ){
        global $database;
        
        $sql = "SELECT * FROM " . self::$tabella_db ;
        $sql.= " WHERE articolo_id = " .$database->escape($articolo_id). " AND risp_commento_id >0";
        $sql.= " ORDER BY risp_commento_id ASC";
        
        return self::cerca_con_query($sql);

    }
    
    public static function conta_commenti($articolo_id = 0) {
        global $database;
        
        $sql = "SELECT COUNT(articolo_id) FROM " . self::$tabella_db;
        $sql.= " WHERE articolo_id = ". $database->escape($articolo_id);
        
        $risultato = $database->query($sql);
        $row = mysqli_fetch_array($risultato);
        
        return array_shift($row);
    }

    public static function conta_commenti_approvati($articolo_id = 0) {
        global $database;
        
        $sql = "SELECT COUNT(*) FROM " . static::$tabella_db;
        $sql.= " WHERE stato = 'approvato' AND articolo_id = ". $database->escape($articolo_id);
        $risultato = $database->query($sql);
        $row = mysqli_fetch_array($risultato);
        
        return array_shift($row);
    }        
    
    public static function seleziona_per_articolo($articolo_id) {
        global $database;
        
        $risultato_array = static::cerca_con_query("SELECT * FROM ". static::$tabella_db . " WHERE articolo_id = " . $articolo_id); 
        
        return !empty($risultato_array)? array_shift($risultato_array): FALSE;
    
    }    
   
    
    
}