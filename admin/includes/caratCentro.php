<?php

/**
 * Created da Giuseppe Alessandro De Blasio.
 * User: Giuseppe Alessandro De Blasio
 * Date: 10/10/16
 * Time: 12:20
 */
class caratCentro extends Db_oggetto
{
    protected static $tabella_db = "carat_centri";
    protected static $campi_tabella_db = array(
        'titolo',
        'descrizione',
        'posizione_row',
        'id_centro',
        'sorted'
    );

    public $id;
    public $titolo;
    public $descrizione;
    public $posizione_row;
    public $id_centro;
    public $sorted;



    public function getTabellaDb()
    {
        return self::$tabella_db;
    }
    /**
     * @return array
     */
    public function seleziona_centri_nulli(){
        global $database;

        //return static::cerca_con_query("SELECT * FROM ". static::$tabella_db . " WHERE id_centro = 0 ");
        //SELECT * FROM `carat_centri` WHERE `id_centro` = 0
        $sql= "SELECT * FROM ". static::$tabella_db . " WHERE id_centro = 0";
        //$result = $database->query($sql);

        $risultato = $database->query($sql);
        $row = mysqli_fetch_array($risultato);

        return array_keys($row);

    }

    public function seleziona_per_id_centro($id){

        global $database;

        $risultato_array = static::cerca_con_query("SELECT * FROM " . static::$tabella_db . " WHERE id_centro =$id " );

        var_dump($risultato_array);
        return !empty($risultato_array)? $risultato_array: FALSE;

        //$risultato = $database->query($sql);

        //return $row = mysqli_fetch_array($risultato);

    }

    public function cancella_carat_centro($id){

        global $database;

        $sql = "DELETE FROM " . static::$tabella_db. " ";
        $sql .= " WHERE id_centro= " . $database->escape($id). " ";
        //$sql .= " LIMIT 1";

        $database->query($sql);

        return (mysqli_affected_rows($database->conn) == 1)? true : false ;
    }

    public function conta_per_centro($id_centro){
        global $database;

        $sql = "SELECT COUNT(*) FROM carat_centri WHERE id_centro = $id_centro";
        $result = $database->query($sql);
        $row = mysqli_fetch_array($result);
        return array_shift($row) +1;



    }

    public function seleziona_per_campo_custom_carat_centro($campo, $valore = null) {
        global $database;

        $risultato_array = static::cerca_con_query("SELECT * FROM ". static::$tabella_db . " WHERE $campo= $valore ORDER BY carat_centri.sorted ASC ");

        return !empty($risultato_array)? $risultato_array : FALSE;
    }

    public function duplicazioneRow($id)
    {
        global $database;

        $row_carat = array();

        $sql_carat = "SELECT * FROM " . static::$tabella_db . " WHERE id_centro = $id ";
        $risultato_carat = $database->query($sql_carat);
        $row_carat = mysqli_fetch_assoc($risultato_carat);
        unset($row_carat['id_centro']);

        $proprieta_carat = $this->valoriDuplicazioneCarat();

        $sql_carat = "INSERT INTO " . static::$tabella_db . "(" . implode(",", array_keys($proprieta_carat)) . ")";
        $sql_carat .= " SELECT '" . implode("','", array_values($row_carat)) . "' FROM " . static::$tabella_db . " WHERE id_centro = $id ";

        /*if ($database->query($sql) AND $database->query($sql_carat) ) {
            $this->id = $database->inserisco_id();
            return TRUE;
        } else {
            return FALSE;
        }*/

        echo $sql_carat;
    }




    } // FINE CLASSE CARATTERISTICHE CENTRO

