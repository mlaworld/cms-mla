<?php

$fotografie = Foto::seleziona_tutti();

?>


<div class="modal fade" id="libreria-foto">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Immagini Utente</h4>
            </div>
            <div class="modal-body">
                <div class="col-md-9">
                    <div class="thumbnails row">
                        <?php foreach ($fotografie as $foto): ?>
                        <div class="col-xs-6">
                            <a role="checkbox" aria-checked="false" tabindex="0" id="" href="#" class="thumbnail">
                                <img name="immagine_nome" class="img-thumbnail img-responsive" src="<?php echo $foto->percorso_immagine_utente(); ?>" data-immagine="<?php echo $foto->id; ?>">
                            </a> 
                            <div class="foto-id hidden">
                            </div>
                        </div>
                            <?php endforeach;?>
                        
                    </div>
                    <div class="col-md-3">
                        <div id="modal_sidebar"></div>
                    </div>
                </div>
                <div class="modal-footer">
                    <div class="row">
                        <button id="imposta_immagine_utente" type="button" class="btn btn-primary" disabled="true" data-dismiss="modal">Applica selezione</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
