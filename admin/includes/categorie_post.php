<?php

class CategoriePagine extends Db_oggetto {
    
    protected static $tabella_db = "categorie_post";
    protected static $campi_tabella_db = array('titolo_categoria', 'urlFriendly', 'stato');
    public $id;
    public $titolo_categoria;
    public $urlFriendly;
    public $stato;


    public function returnUrlFriendly($urlFriendly){

        global $database;
        $risultato = $database->query($sql = "SELECT * FROM " . static::$tabella_db . " WHERE urlFriendly = {$database->escape("{$urlFriendly}")} ") ;
        return $risultato;

    }

//    public function seleziona_tutti_per_stato() {
//    global $database;
////        return static::cerca_con_query("SELECT * FROM ". static::$tabella_db. " WHERE stato = 1 ");
//        return static::cerca_con_query("SELECT * FROM ". static::$tabella_db. " WHERE stato = '{$database->escape(1)}' ");
//    }

    public function seleziona_tutti_per_stato()
    {
        global $databasePDO;
        $databasePDO->query("SELECT * FROM ". static::$tabella_db . " WHERE stato = :stato ");
        $databasePDO->bind(':stato', 1);
        return $databasePDO->resultClass('CategoriePagine');
    }


    public function valoreDuplicato(){
        global $database;
        return (mysqli_errno($database->conn) == 1062)? "<script>alert('Errore')</script>" : "";
    }

    public function catNonPresente()
    {
        global $database;
        return static::cerca_con_query("SELECT * FROM categorie_post WHERE categorie_post.id NOT IN (SELECT categorie_page.id_cat FROM categorie_page)");
    }

}