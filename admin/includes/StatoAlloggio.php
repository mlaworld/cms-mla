<?php

/**
 * Created da Giuseppe Alessandro De Blasio.
 * User: Giuseppe Alessandro De Blasio
 * Date: 13/04/17
 * Time: 12:01
 */
class StatoAlloggio extends Db_oggetto
{
    protected static $tabella_db = "stato_alloggio";
    protected static $campi_tabella_db = array(
        'id_corso_adulti',
        'stato'
    );

    public $id;
    public $id_corso_adulti;
    public $stato;


}