<?php

/**
 * Created da Giuseppe Alessandro De Blasio.
 * User: Giuseppe Alessandro De Blasio
 * Date: 15/03/17
 * Time: 18:50
 */
class categoriaEventi extends Db_oggetto
{
    protected static $tabella_db = "categorie_eventi";
    protected static $campi_tabella_db = array('titolo_categoria');
    public $id;
    public $titolo_categoria;

}