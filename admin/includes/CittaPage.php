<?php

/**
 * Created da Giuseppe Alessandro De Blasio.
 * User: Giuseppe Alessandro De Blasio
 * Date: 19/10/16
 * Time: 16:32
 */
class CittaPage extends Db_oggetto {
    protected static $tabella_db = "citta_page";
    protected static $campi_tabella_db = array(
        'image',
        'img_alt',
        'img_title',
        'nome_citta',
        'desc_titolo',
        'urlFriendly',
        'descrizione',
        'titBox1',
        'box1',
        'titBox2',
        'box2',
        'titolo_desc',
        'cittaUrl',
        'metaTitle',
        'metaDescription',
        'canonicalUrl',
        'metaIndex',
        'stato',
        'id_nazione',
        'id_centro',
        'id_categoria',
        'nessun_testo',
        'urlRedirect'
    );
    public $id;
    public $image;
    public $img_alt;
    public $img_title;
    public $nome_citta;
    public $desc_titolo;
    public $descrizione;
    public $titBox1;
    public $box1;
    public $titBox2;
    public $box2;
    public $titolo_desc;
    public $urlFriendly;
    public $cittaUrl;
    public $metaTitle;
    public $metaDescription;
    public $canonicalUrl;
    public $metaIndex;
    public $stato;
    public $id_nazione;
    public $id_centro;
    public $id_categoria;
    public $nessun_testo;
    public $urlRedirect;
    public $tipo;
    public $grandezza;
    public $tmp_path;
    public $percorso_immagini = "images/citta";
    //public $immagine_plaecholder = "https://placeholdit.imgix.net/~text?txtsize=66&txt=MLA%20NO%20IMAGE&w=400%20&h=267";
    public $immagine_plaecholder = "http://localhost/mla-cms/admin/images/image-not-found.png";
    public $errore = array();
    public $errore_caricamento_array = array (

        UPLOAD_ERR_OK                       =>  "Non c'è alcun errore, il file è stato caricato con successo.",
        UPLOAD_ERR_INI_SIZE                 =>  "Il file caricato supera la direttiva upload_max_filesize in php.ini",
        UPLOAD_ERR_FORM_SIZE                =>  "Il file inviato eccede il MAX_FILE_SIZE che è stato specificato nel modulo HTML",
        UPLOAD_ERR_PARTIAL                  =>  "Il file è stato solo parzialmente caricato.",
        UPLOAD_ERR_NO_FILE                  =>  "Nessun file è stato caricato.",
        UPLOAD_ERR_NO_TMP_DIR               =>  "Manca una cartella temporanea. Introdotta in PHP 5.0.3",
        UPLOAD_ERR_CANT_WRITE               =>  "Impossibile scrivere file su disco. Introdotto in PHP 5.1.0.",
        UPLOAD_ERR_EXTENSION                =>  "Una estensione PHP ha interrotto il caricamento del file."
    );


    public function caricamento_foto_citta() {

        if(!empty($this->errore)){
            return FALSE;
        }

        if (empty($this->image) || empty($this->tmp_path)) {
            $this->errore[] = "Il file non è utilizzabile";
            return FALSE;
        }

        $percorso_immagini = ROOT_SITO . DS . $this->percorso_immagini . DS. $this->image;

        if(file_exists($percorso_immagini)){
            $this->errore[] = "Il file {$this->image} già esiste";
            return FALSE;
        }

        if(move_uploaded_file($this->tmp_path, $percorso_immagini)){
            unset($this->tmp_path);
            return TRUE;
        } else {
            $this->errore[] = "Molto probabilmente non si ha il permesso di inserire il file nalla directory";
            return FALSE;
        }
    }

    public function percorso_immagine_e_plecholder() {
        return empty($this->image)? $this->immagine_plaecholder : "..".DS.$this->percorso_immagini.DS.$this->image;
    }

    public function percorso_immagine_e_plecholder_front() {
        return empty($this->image)? $this->immagine_plaecholder : $this->percorso_immagini.DS.$this->image;
    }

//    public function percorso_immagine_e_plecholder_front_nazione() {
//        return empty($this->image)? $this->immagine_plaecholder : "../".DS.$this->percorso_immagini.DS.$this->image;
//    }

    public function impostazione_file_citta($file) {

        if(empty($file) || !$file || !is_array($file)){
            $this->errore[] = "Non c'è alcun file da caricare";
            return FALSE;
        } elseif ($file['error'] != 0 ) {

            $this->errore[] = $this->errore_caricamento_array[$file['error']];
            return FALSE;
        } else {
            $this->image = basename($file['name']);
            $this->tmp_path = $file['tmp_name'];
            $this->tipo = $file['type'];
            $this->grandezza = $file['size'];
        }
    }


    public function seleziona_per_nazione($nazione, $categoria){
        return static::cerca_con_query("SELECT * FROM ". static::$tabella_db. " WHERE id_nazione = '{$nazione}' AND id_categoria = '{$categoria}'  ");

    }


    public function esiste_nazione_e_categoria($categoria, $nazione)
    {
        global $database;

        $nazione = $database->escape($nazione);
        $categoria = $database->escape($categoria);

        $sql = "SELECT id_categoria, id_nazione FROM " . static::$tabella_db . " WHERE ";
        $sql .= " id_categoria = $categoria  AND id_nazione = $nazione ";
        $risultato = $database->query($sql);


        if ($database->conta_righe($risultato) == 1) {

            echo $doppio = "
                        <script>
                            $(document).ready(function(){
                                swal(
                                  'Oops...',
                                  'Questa nazione e la categoria già esiste!',
                                  'error'
                                );                                
                            });                            
                        </script>";
            return $doppio;
        } else{
            return $this->salva();
        }

        global $msg_instant;
        $msg_instant->success('Città <b>modificata</b> correttamente');
        $msg_instant->display();
    }

    public function esiste_nazioni_categoria_modifica($categoria, $nazione, $id)
    {
        global $database;

        $nazione = $database->escape($nazione);
        $categoria = $database->escape($categoria);

        $sql = "SELECT id_categoria, id_nazione FROM " . static::$tabella_db . " WHERE ";
        $sql .= " id_categoria = $categoria  AND id_nazione = $nazione ";
        $risultato = $database->query($sql);


        if ($database->conta_righe($risultato) > 1 AND $this->id !== $id) {

            echo $doppio = "
                        <script>
                            $(document).ready(function(){
                                swal(
                                  'Oops...',
                                  'Questa nazione e la categoria già esiste!',
                                  'error'
                                );                                
                            });                            
                        </script>";
            return $doppio;
        } else {
            $this->salva();
        }

        global $msg_instant;
        $msg_instant->success('Nazione <b>Modificata</b> correttamente');
        $msg_instant->display();
    }


    /* public function seleziona_per_citta($citta){
         global $database;

         return static::cerca_con_query("SELECT * FROM ". static::$tabella_db. " WHERE id = $citta ");

     }*/




}