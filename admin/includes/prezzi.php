<?php

/**
 * Created da Giuseppe Alessandro De Blasio.
 * User: Giuseppe Alessandro De Blasio
 * Date: 09/01/17
 * Time: 15:55
 */
class Prezzi extends Db_oggetto
{
    protected static $tabella_db = "prezzi";
    protected static $campi_tabella_db = array(
        'stagionalita',
        'prezzo',
        'data_inizio',
        'iscrizione_entro',
        'id_catalogo',
        'stato',
        'id_centro'
);

    public $id;
    public $stagionalita;
    public $prezzo;
    public $data_inizio;
    public $iscrizione_entro;
    public $id_catalogo;
    public $stato;
    public $id_centro;
//    public $sorted;


    public function seleziona_per_campo_custom_prezzi_centro($campo, $valore = null) {
        global $database;

        $risultato_array = static::cerca_con_query("SELECT * FROM ". static::$tabella_db . " WHERE $campo = $valore");

        return !empty($risultato_array)? $risultato_array : FALSE;
    }

}// FINE CLASSE PREZZI