<?php

function caricamentoAutomaticoClasse($classe){

    $classe = strtolower($classe);

    $percorso = PERCORSO_INCLUDES.DS."{$classe}.php";

    if(is_file($percorso)&& !class_exists($classe)){
        include $percorso;
    }
}

spl_autoload_register('caricamentoAutomaticoClasse');

function redirect($percorso){
    header("LOCATION: {$percorso}");
}

function redirectTime($time = 2, $url){
    header("refresh:$time;url=$url");
}

function invio_email($email, $oggetto, $msg, $headers){
    return mail($email, $oggetto, $msg, $headers);
}

function relativeTime($time, $short = false){
    $SECOND = 1;
    $MINUTE = 60 * $SECOND;
    $HOUR = 60 * $MINUTE;
    $DAY = 24 * $HOUR;
    $MONTH = 30 * $DAY;
    $before = time() - $time;

    if ($before < 0)
    {
        return "not yet";
    }

    if ($short){
        if ($before < 1 * $MINUTE)
        {
            return ($before <2) ? "just now" : $before . " ago";
        }

        if ($before < 2 * $MINUTE)
        {
            return "1m ago";
        }

        if ($before < 45 * $MINUTE)
        {
            return floor($before / 60) . "m ago";
        }

        if ($before < 90 * $MINUTE)
        {
            return "1h ago";
        }

        if ($before < 24 * $HOUR)
        {

            return floor($before / 60 / 60). "h ago";
        }

        if ($before < 48 * $HOUR)
        {
            return "1d ago";
        }

        if ($before < 30 * $DAY)
        {
            return floor($before / 60 / 60 / 24) . "d ago";
        }


        if ($before < 12 * $MONTH)
        {
            $months = floor($before / 60 / 60 / 24 / 30);
            return $months <= 1 ? "1mo ago" : $months . "mo ago";
        }
        else
        {
            $years = floor  ($before / 60 / 60 / 24 / 30 / 12);
            return $years <= 1 ? "1y ago" : $years."y ago";
        }
    }

    if ($before < 1 * $MINUTE)
    {
        return ($before <= 1) ? "just now" : $before . " seconds ago";
    }

    if ($before < 2 * $MINUTE)
    {
        return "a minute ago";
    }

    if ($before < 45 * $MINUTE)
    {
        return floor($before / 60) . " minutes ago";
    }

    if ($before < 90 * $MINUTE)
    {
        return "an hour ago";
    }

    if ($before < 24 * $HOUR)
    {

        return (floor($before / 60 / 60) == 1 ? 'about an hour' : floor($before / 60 / 60).' hours'). " ago";
    }

    if ($before < 48 * $HOUR)
    {
        return "yesterday";
    }

    if ($before < 30 * $DAY)
    {
        return floor($before / 60 / 60 / 24) . " days ago";
    }

    if ($before < 12 * $MONTH)
    {

        $months = floor($before / 60 / 60 / 24 / 30);
        return $months <= 1 ? "one month ago" : $months . " months ago";
    }
    else
    {
        $years = floor  ($before / 60 / 60 / 24 / 30 / 12);
        return $years <= 1 ? "one year ago" : $years." years ago";
    }

    return "$time";
}

function data_it($data)
{
  // Creo una array dividendo la data YYYY-MM-DD sulla base del trattino
  $array = explode("-", $data); 

  // Riorganizzo gli elementi in stile DD/MM/YYYY
  $data_it = $array[2]."/".$array[1]."/".$array[0]; 

  // Restituisco il valore della data in formato italiano
  return $data_it; 
}

function data_db($data){
    $array = explode("/", $data);

    $data_db = $array[2]."-".$array[1]."-".$array[0];

    return $data_db;
}

function ritorna_giorno($data){
    $array = explode("-", $data);

    $giorno_db = $array[2];

    return $giorno_db;
}

function datatime_it($data)
{

    date_default_timezone_set("Europe/Rome");
    $old_date = date($data);
    $old_date_timestamp = strtotime($old_date);
    return $new_date = date('d/m/Y H:i:s', $old_date_timestamp);
}

function data_eventi_home($data){

    $arrDate = explode(" ", $data);

    $giornoEsteso = "";
    switch ($arrDate){
        case $arrDate[0] == 'Monday':
            $giornoEsteso = 'Lunedì';
            break;
        case $arrDate[0] == 'Tuesday':
            $giornoEsteso = 'Martedì';
            break;
        case $arrDate[0] == 'Wednesday':
            $giornoEsteso = 'Mercoledì';
            break;
        case $arrDate[0] == 'Thursday':
            $giornoEsteso = 'Giovedì';
            break;
        case $arrDate[0] == 'Friday':
            $giornoEsteso = 'Venerdì';
            break;
        case $arrDate[0] == 'Saturday':
            $giornoEsteso = 'Sabato';
            break;
        case $arrDate[0] == 'Sunday':
            $giornoEsteso = 'Domenica';
            break;
    }

    $giorno = $arrDate[1];


    $mese = "";
    switch ($arrDate){
        case $arrDate[2] == 'January':
            $mese = 'Gennaio';
            break;
        case $arrDate[2] == 'February':
            $mese = 'Febbraio';
            break;
        case $arrDate[2] == 'March':
            $mese = 'Marzo';
            break;
        case $arrDate[2] == 'April':
            $mese = 'Aprile';
            break;
        case $arrDate[2] == 'May':
            $mese = 'Maggio';
            break;
        case $arrDate[2] == 'June':
            $mese = 'Giugno';
            break;
        case $arrDate[2] == 'July':
            $mese = 'Luglio';
            break;
        case $arrDate[2] == 'August':
            $mese = 'Agosto';
            break;
        case $arrDate[2] == 'September':
            $mese = 'Settembre';
            break;
        case $arrDate[2] == 'October':
            $mese = 'Ottobre';
            break;
        case $arrDate[2] == 'November':
            $mese = 'Novembre';
            break;
        case $arrDate[2] == 'December':
            $mese = 'Dicembre';
            break;
    }


    return $giornoEsteso . " " . $giorno . " " . $mese;
}

    
function leggi_tutto($miotesto, $link, $var, $id, $titolo) {
$caratteri = 800;
    
    $miotesto = substr($miotesto, 0, $caratteri);
    $miotesto = substr($miotesto, 0, strrpos($miotesto,' '));
    $miotesto = $miotesto."[...]"."</p>". "<a href='$link?$var=$id&titolo={$titolo} ' style='color: #E77A37;'>Leggi di più</a>";
    return $miotesto;

}

function leggi_di_piu_feed($miotesto, $caratteri, $link = '#') {
    $nr_caratteri = $caratteri;

    $miotesto = substr($miotesto, 0, $nr_caratteri);
    $miotesto = substr($miotesto, 0, strrpos($miotesto,' '));
    $miotesto = $miotesto."...<br><a href=".$link."> Leggi di più »"."</a></p>";
    return $miotesto;

}


function bloccoFeed($stringa, $max_char){
    if(strlen($stringa)>$max_char){
        $stringa_tagliata=substr($stringa, 0,$max_char);
        $last_space=strrpos($stringa_tagliata," ");
        $stringa_ok=substr($stringa_tagliata, 0,$last_space);
        return $stringa_ok."...";
    }else{
        return $stringa;
    }
}

function cercaTuttoNelDB($search){
    global $mysqli;

    $out = "";

    $sql = "show tables";
    $rs = $mysqli->query($sql);
    if($rs->num_rows > 0){
        while($r = $rs->fetch_array()){
            $table = $r[0];
            $out .= $table.";";
            $sql_search = "select * from ".$table." where ";
            $sql_search_fields = Array();
            $sql2 = "SHOW COLUMNS FROM ".$table;
            $rs2 = $mysqli->query($sql2);
            if($rs2->num_rows > 0){
                while($r2 = $rs2->fetch_array()){
                    $colum = $r2[0];
                    $sql_search_fields[] = $colum." like('%".$search."%')";
                }
                $rs2->close();
            }
            $sql_search .= implode(" OR ", $sql_search_fields);
            $rs3 = $mysqli->query($sql_search);
            $out .= $rs3->num_rows."\n";
            if($rs3->num_rows > 0){
                $rs3->close();
            }
        }
        $rs->close();
    }

    return $out;
}


function traduci($testo,$src,$dest) {
 $tag='JHIKE';
 $testo=urlencode($tag.$testo.$tag);
 $params="langpair=$src|$dest&text=".$testo;
 $data= postURL('http://translate.google.com/translate_t', $params);
 preg_match_all("/result_box.*".$tag."(.*)".$tag."\s*\<\/div\>/mi", $data, $result, PREG_SET_ORDER);
 return  $result[0][1];
}


function rewriteUrl($stringa){
    $strResult = str_ireplace("à", "a", $stringa);
    $strResult  = str_ireplace("á", "a", $strResult);
    $strResult =  str_ireplace("è", "e", $strResult);
    $strResult =  str_ireplace("é", "e", $strResult);
    $strResult =  str_ireplace("ì", "i", $strResult);
    $strResult =  str_ireplace("í", "i", $strResult);
    $strResult =  str_ireplace("ò", "o", $strResult);
    $strResult =  str_ireplace("ó", "o", $strResult);
    $strResult =  str_ireplace("ù", "u", $strResult);
    $strResult =  str_ireplace("ú", "u", $strResult);
    $strResult =  str_ireplace("ç", "c", $strResult);
    $strResult =  str_ireplace("ö", "o", $strResult);
    $strResult =  str_ireplace("û", "u", $strResult);
    $strResult =  str_ireplace("ê", "e", $strResult);
    $strResult =  str_ireplace("ü", "u", $strResult);
    $strResult =  str_ireplace("ë", "e", $strResult);
    $strResult =  str_ireplace("ä", "a", $strResult);
    $strResult =  str_ireplace("'", " ", $strResult);

    $strResult = preg_replace('/[^A-Za-z0-9 ]/', "", $strResult);
    $strResult = trim($strResult);
    $strResult =  preg_replace('/[ ]{2,}/', " ", $strResult);

    $strResult = str_replace(" ", "-", $strResult);

    return $strResult;

}

function TrgAmp($html){
    //Crea tag di apertura amp-img e sostituisci il corrispondente tag NON AMP
    $html = str_ireplace(
        ['<img','<video','/video>','<audio','/audio>'],
        ['<amp-img','<amp-video','/amp-video>','<amp-audio','/amp-audio>'],
        $html
    );

    # Crea il tag di chiusura amp-img
    $html = preg_replace('/<amp-img(.*?)>/', '<amp-img layout="responsive" width="300" height="250" $1></amp-img>', $html);

    # Whitelist tag - filtro iframe, embed script etc
    $html = strip_tags($html,'<h1><h2><h3><h4><h5><h6><a><p><ul><ol><li><blockquote><q><cite><ins><del><strong><em><code><pre><svg><table><thead><tbody><tfoot><th><tr><td><dl><dt><dd><article><section><header><footer><aside><figure><time><abbr><div><span><hr><small><br><amp-img><amp-audio><amp-video><amp-ad><amp-anim><amp-carousel><amp-fit-rext><amp-image-lightbox><amp-instagram><amp-lightbox><amp-twitter><amp-youtube>');

    //Elimino stile inline a paragrafi, intestazioni e images
    return preg_replace('/(<[^>]+) style=".*?"/i', '$1', $html);
}

function formattazioneTesto($testo){
    $nuovoTetso = ucwords(ucfirst(strtolower($testo)));
    return $nuovoTetso;
}


function confrontoData($dataInserita, $testoVero, $testoFalso){

    $dataCorrente = date('Y/m/d');

    return ($dataInserita<$dataCorrente)? "$testoVero" : "$testoFalso";

}

function successoMessaggio($messaggio, $titolo){

    echo '
    <script>
        $(document).ready(function () {
    
            toastr.options = {
                "closeButton": true,
                "debug": false,
                "newestOnTop": false,
                "progressBar": false,
                "positionClass": "toast-top-full-width",
                "preventDuplicates": false,
                "onclick": null,
                "showDuration": "900",
                "hideDuration": "3000",
                "timeOut": "5000",
                "extendedTimeOut": "1000",
                "showEasing": "swing",
                "hideEasing": "linear",
                "showMethod": "fadeIn",
                "hideMethod": "fadeOut"
            };
    
            return toastr.success("'.$messaggio.'", "'.$titolo.'");
        });
    </script>';
}


function erroreMessaggio($messaggio, $titolo){

    echo '
    <script>
        $(document).ready(function () {
    
            toastr.options = {
                "closeButton": true,
                "debug": false,
                "newestOnTop": false,
                "progressBar": false,
                "positionClass": "toast-top-full-width",
                "preventDuplicates": false,
                "onclick": null,
                "showDuration": "900",
                "hideDuration": "3000",
                "timeOut": "5000",
                "extendedTimeOut": "1000",
                "showEasing": "swing",
                "hideEasing": "linear",
                "showMethod": "fadeIn",
                "hideMethod": "fadeOut"
            };
    
            return toastr.error("'.$messaggio.'", "'.$titolo.'");
        });
    </script>';
}

function urlSrcIframe($content = null){
    preg_match('/src="([^"]+)"/', $content, $matches);
    $url = $matches[1];
    return $url;
}



function encrypt_url($string) {
    $key = "MLA_101"; //key di encrypt e decrypts.
    $result = '';
    $test = "";
    for($i=0; $i<strlen($string); $i++) {
        $char = substr($string, $i, 1);
        $keychar = substr($key, ($i % strlen($key))-1, 1);
        $char = chr(ord($char)+ord($keychar));

        $test[$char]= ord($char)+ord($keychar);
        $result.=$char;
    }

    return urlencode(base64_encode($result));
}


function decrypt_url($string) {
    $key = "MLA_101"; //key di encrypt e decrypts.
    $result = '';
    $string = base64_decode(urldecode($string));
    for($i=0; $i<strlen($string); $i++) {
        $char = substr($string, $i, 1);
        $keychar = substr($key, ($i % strlen($key))-1, 1);
        $char = chr(ord($char)-ord($keychar));
        $result.=$char;
    }
    return $result;
}

function verifica_email($email){

    return filter_var($email, FILTER_VALIDATE_EMAIL);

}

function verifica_campi_string($stringa){

    $ptn = "/^[-a-zA-Z ]+$/";
    $str = $stringa;
    preg_match($ptn, $str, $matches);

    if(!empty($matches)){
        return $matches = true;
    } else {
        return $matches =  false;
    }
}

function verifica_campi_numero($numero){

    if(is_numeric($numero)){
        return true;
    } else {
        return false;
    }

}

function verifica_data($data, $format = 'Y-m-d H:i:s'){

    $d = DateTime::createFromFormat($format, $data);
    return $d && $d->format($format) == $data;
}


?>
