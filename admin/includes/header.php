<?php require_once 'init.php'; ?>

    <?php ob_start(); ?>

<!DOCTYPE html>
<!--[if lt IE 7]> <html class="ie ie6 lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="ie ie7 lt-ie9 lt-ie8"        lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="ie ie8 lt-ie9"               lang="en"> <![endif]-->
<!--[if IE 9]>    <html class="ie ie9"                      lang="en"> <![endif]-->
<!--[if !IE]><!-->
<html lang="it" class="no-ie">
<!--<![endif]-->

<head>
   <!-- Meta-->
   <meta charset="utf-8">
   <meta http-equiv="X-UA-Compatible" content="IE=edge">
   <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0">
   <meta name="description" content="CMS di proprietà di Giuseppe Alessandro De Blasio">
   <meta name="keywords" content="Giuseppe Alessandro De Blasio, Addmin">
   <meta name="author" content="Giuseppe Alessandro De Blasio">
   <title>Admin Area - MLA CMS</title>
   <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
   <!--[if lt IE 9]>
   <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
   <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
   <![endif]-->
   <!-- Bootstrap CSS-->
   <link rel="stylesheet" href="css/bootstrap.css">
   <!-- CSS Esterni-->
   <link rel="stylesheet" href="css/fontawesome/css/font-awesome.min.css">
   <link rel="stylesheet" href="css/animate/animate.css">
   <link rel="stylesheet" href="css/csspinner/csspinner.min.css">
   
   <!--  datetimepicker -->
   <link rel="stylesheet" href="css/datetimepicker/css/bootstrap-datetimepicker.min.css">
   
   <!--  fullcalendar -->
   <link rel="stylesheet" href="css/fullcalendar/fullcalendar.css">
   <link rel="stylesheet" href="css/fullcalendar/fullcalendar.print.css">

   <!--  dropzone -->
   <link rel="stylesheet" href="css/dropzone/dropzone.css">

   <!--  toastr -->
   <link rel="stylesheet" href="css/toastr/toastr.min.css">
   
   <!--  JQuery UI -->
   <!--<link rel="stylesheet" href="../css/jqueryui/css/no-theme/jquery-ui-1.10.4.custom.css">
   <link rel="stylesheet" href="../css/jqueryui/css/no-theme/jquery-ui-1.10.4.custom.min.css">
   <link rel="stylesheet" href="../css/jqueryui/css/ui-lightness/jquery-ui-1.10.4.custom.css">
   <link rel="stylesheet" href="../css/jqueryui/css/ui-lightness/jquery-ui-1.10.4.custom.min.css">-->
   
   <!-- CSS Applicazione -->
   <link rel="stylesheet" href="css/style.css">

   <!--  Tags Imput -->
   <link rel="stylesheet" href="css/tagsinput/bootstrap-tagsinput.css">
   
   <!-- CSS Slider -->
   <link rel="stylesheet" href="css/slider/css/slider.css">

   <!-- Data Tabeles -->
   <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs/dt-1.10.13/r-2.1.1/datatables.min.css"/>

   <link rel="stylesheet" type="text/css" href="css/sweetalert/sweetalert2.min.css">

   <!-- Modernizr JS Script-->
   <script src="js/modernizr/modernizr.js" type="application/javascript"></script>

   <!-- FastClick for mobiles-->
   <script src="js/fastclick/fastclick.js" type="application/javascript"></script>

   <!--  TinyMCE -->
   <script src="js/tinymce/js/tinymce/tinymce.min.js"></script>

   <!--  jQuery  -->
   <script src="js/jquery-2.1.4.min.js"></script>


   <!--  Datetimepicker and Moment -->
   <script type="text/javascript" src="js/moment/min/moment-with-langs.min.js"></script>
   <script type="text/javascript" src="js/datetimepicker/js/bootstrap-datetimepicker.min.js"></script>

   </head>
   <body>
      <section class="wrapper">




