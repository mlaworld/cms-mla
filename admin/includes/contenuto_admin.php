<!-- INIZIO CONTENTUO ADMIN ->
      <section>
         <!-- START Page content-->
         <br>
         <section class="main-content">
             <a href="../admin/aggiungi_page.php"><button type="button" class="btn btn-labeled btn-primary pull-right">
               <span class="btn-label"><i class="fa fa-plus-circle"></i>
               </span>Aggiungi Pagina</button></a>
            <h3>Dashboard
               <br>
               <small>Benvenuto utente</small>
            </h3>
           <!-- <div data-toggle="notify" data-onload data-message="&lt;b&gt;New Updates Available!&lt;/b&gt; Don't forget to check them!" data-options="{&quot;status&quot;:&quot;danger&quot;, &quot;pos&quot;:&quot;top-right&quot;}" class="hidden-xs"></div> -->
            <div class="row">
               <!-- START dashboard main content-->
               <div class="col-md-12">
                  <!-- START summary widgets-->
                  <div class="row">
                     <div class="col-lg-3 col-sm-6">
                        <!-- START widget-->
                        <div data-toggle="play-animation" data-play="fadeInDown" data-offset="0" data-delay="100" class="panel widget">
                           <div class="panel-body bg-primary">
                              <div class="row row-table row-flush">
                                 <div class="col-xs-8">
                                    <p class="mb0">Nuovi visitatori</p>
                                    <h3 class="m0">1.5k</h3>
                                 </div>
                                 <div class="col-xs-4 text-center">
                                    <em class="fa fa-user fa-2x"><sup class="fa fa-plus"></sup>
                                    </em>
                                 </div>
                              </div>
                           </div>
                           <div class="panel-body">
                              <!-- Bar chart-->
                              <div class="text-center">
                                 <div data-bar-color="primary" data-height="30" data-bar-width="6" data-bar-spacing="6" class="inlinesparkline inline">5,3,4,6,5,9,4,4,10,5,9,6,4</div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="col-lg-3 col-sm-6">
                        <!-- START widget-->
                        <div data-toggle="play-animation" data-play="fadeInDown" data-offset="0" data-delay="500" class="panel widget">
                           <div class="panel-body bg-warning">
                              <div class="row row-table row-flush">
                                 <div class="col-xs-8">
                                    <p class="mb0">Bounce Rate</p>
                                    <h3 class="m0">50%</h3>
                                 </div>
                                 <div class="col-xs-4 text-center">
                                    <em class="fa fa-users fa-2x"></em>
                                 </div>
                              </div>
                           </div>
                           <div class="panel-body">
                              <!-- Bar chart-->
                              <div class="text-center">
                                 <div data-bar-color="warning" data-height="30" data-bar-width="6" data-bar-spacing="6" class="inlinesparkline inline">10,30,40,70,50,90,70,50,90,40,40,60,40</div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="col-lg-3 col-sm-6">
                        <!-- START widget-->
                        <div data-toggle="play-animation" data-play="fadeInDown" data-offset="0" data-delay="1000" class="panel widget">
                           <div class="panel-body bg-danger">
                              <div class="row row-table row-flush">
                                 <div class="col-xs-8">
                                    <p class="mb0">Searchs</p>
                                    <h3 class="m0">28%</h3>
                                 </div>
                                 <div class="col-xs-4 text-center">
                                    <em class="fa fa-search fa-2x"></em>
                                 </div>
                              </div>
                           </div>
                           <div class="panel-body">
                              <!-- Bar chart-->
                              <div class="text-center">
                                 <div data-bar-color="danger" data-height="30" data-bar-width="6" data-bar-spacing="6" class="inlinesparkline inline">2,7,5,9,4,2,7,5,7,5,9,6,4</div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="col-lg-3 col-sm-6">
                        <!-- START widget-->
                        <div data-toggle="play-animation" data-play="fadeInDown" data-offset="0" data-delay="1500" class="panel widget">
                           <div class="panel-body bg-success">
                              <div class="row row-table row-flush">
                                 <div class="col-xs-8">
                                    <p class="mb0">Traffico</p>
                                    <h3 class="m0">140.5 kb</h3>
                                 </div>
                                 <div class="col-xs-4 text-center">
                                    <em class="fa fa-globe fa-2x"></em>
                                 </div>
                              </div>
                           </div>
                           <div class="panel-body">
                              <!-- Bar chart-->
                              <div class="text-center">
                                 <div data-bar-color="success" data-height="30" data-bar-width="6" data-bar-spacing="6" class="inlinesparkline inline">4,7,5,9,6,4,8,6,3,4,7,5,9</div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
                  <!-- END summary widgets-->
                  <!-- START chart-->
                  <div class="row">
                     <div class="col-lg-12">
                        <div class="panel panel-default">
                           <div class="panel-collapse">
                              <div class="panel-body">

                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
                  <!-- END chart-->
                  <!-- START Secondary Widgets-->
                  <div class="row">
                     <div class="col-md-4">
                        <!-- START widget-->
                        <div data-toggle="play-animation" data-play="fadeInLeft" data-offset="0" data-delay="1400" class="panel widget">
                           <div class="panel-body">
                              <div class="text-right text-muted">
                                 <em class="fa fa-users fa-2x"></em>
                              </div>
                               <h3 class="mt0"><?php echo Utente::conta_elementi(); ?></h3>
                              <p class="text-muted">Utenti iscritti in MLA - CMS </p>
                              <div class="progress progress-striped progress-xs">
                                 <div role="progressbar" aria-valuenow="10" aria-valuemin="0" aria-valuemax="100" style="width: 05%;" class="progress-bar progress-bar-success">
                                    <span class="sr-only">80% Complete</span>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <!-- END widget-->
                     </div>
                     <div class="col-md-4">
                        <!-- START widget-->
                        <div data-toggle="play-animation" data-play="fadeInLeft" data-offset="0" data-delay="1400" class="panel widget">
                           <div class="panel-body">
                              <div class="text-right text-muted">
                                 <em class="fa fa-bar-chart-o fa-2x"></em>
                              </div>
                              <h3 class="mt0">€ 1250</h3>
                              <p class="text-muted">Reddito Medio Mensile</p>
                              <div class="progress progress-striped progress-xs">
                                 <div role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: 40%;" class="progress-bar progress-bar-info">
                                    <span class="sr-only">40% Complete</span>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <!-- END widget-->
                     </div>
                     <div class="col-md-4">
                        <!-- START widget-->
                        <div data-toggle="play-animation" data-play="fadeInLeft" data-offset="0" data-delay="1400" class="panel widget">
                           <div class="panel-body">
                              <div class="text-right text-muted">
                                 <em class="fa fa-trophy fa-2x"></em>
                              </div>
                              <h3 class="mt0">€ 13865</h3>
                              <p class="text-muted">Obiettivo Reddito annuale</p>
                              <div class="progress progress-striped progress-xs">
                                 <div role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 60%;" class="progress-bar progress-bar-warning">
                                    <span class="sr-only">60% Complete</span>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <!-- END widget-->
                     </div>
                  </div>
                  <!-- END Secondary Widgets-->
                  <!-- START table-->
<!--                  <div class="row">-->
<!--                     <div class="col-lg-12">-->
                        <!-- START panel-->
<!--                        <div class="panel panel-default">-->
<!--                           <div class="panel-heading">Attività in sospeso-->
<!--                              <a href="#" data-perform="panel-dismiss" data-toggle="tooltip" title="Close Panel" class="pull-right">-->
<!--                                 <em class="fa fa-times"></em>-->
<!--                              </a>-->
<!--                              <a href="#" data-perform="panel-collapse" data-toggle="tooltip" title="Collapse Panel" class="pull-right">-->
<!--                                 <em class="fa fa-minus"></em>-->
<!--                              </a>-->
<!--                           </div>-->
                           <!-- START table-responsive-->
<!--                           <div class="table-responsive">-->
<!--                              <table class="table table-striped table-bordered table-hover">-->
<!--                                 <thead>-->
<!--                                    <tr>-->
<!--                                       <th>Nome Attività</th>-->
<!--                                       <th>Progresso</th>-->
<!--                                       <th>Scadenza</th>-->
<!--                                       <th>Azione</th>-->
<!--                                    </tr>-->
<!--                                 </thead>-->
<!--                                 <tbody>-->
<!--                                    <tr>-->
<!--                                       <td>Nunc luctus, quam non condimentum ornare</td>-->
<!--                                       <td>-->
<!--                                          <div class="progress progress-striped progress-xs">-->
<!--                                             <div role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: 80%;" class="progress-bar progress-bar-success">-->
<!--                                                <span class="sr-only">80% Complete</span>-->
<!--                                             </div>-->
<!--                                          </div>-->
<!--                                       </td>-->
<!--                                       <td>-->
<!--                                          <em class="fa fa-calendar fa-fw text-muted"></em>05/05/2014</td>-->
<!--                                       <td class="text-center">-->
<!--                                          <div class="btn-group"><a href="#" data-toggle="dropdown" class="dropdown-toggle"><i class="fa fa-cog"></i></a>-->
<!--                                             <ul class="dropdown-menu pull-right text-left">-->
<!--                                                <li><a href="#">Azione</a>-->
<!--                                                </li>-->
<!--                                                <li><a href="#">Altra Azione</a>-->
<!--                                                </li>-->
<!--                                                <li><a href="#">Qualcos'altro qui</a>-->
<!--                                                </li>-->
<!--                                                <li class="divider"></li>-->
<!--                                                <li><a href="#">link Seprarato</a>-->
<!--                                                </li>-->
<!--                                             </ul>-->
<!--                                          </div>-->
<!--                                       </td>-->
<!--                                    </tr>-->
<!--                                    <tr>-->
<!--                                       <td>Integer in convallis felis.</td>-->
<!--                                       <td>-->
<!--                                          <div class="progress progress-striped progress-xs">-->
<!--                                             <div role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: 20%;" class="progress-bar progress-bar-danger">-->
<!--                                                <span class="sr-only">20% Complete</span>-->
<!--                                             </div>-->
<!--                                          </div>-->
<!--                                       </td>-->
<!--                                       <td>-->
<!--                                          <em class="fa fa-calendar fa-fw text-muted"></em>15/05/2014</td>-->
<!--                                       <td class="text-center">-->
<!--                                          <div class="btn-group"><a href="#" data-toggle="dropdown" class="dropdown-toggle"><i class="fa fa-cog"></i></a>-->
<!--                                             <ul class="dropdown-menu pull-right text-left">-->
<!--                                                <li><a href="#">Azione</a>-->
<!--                                                </li>-->
<!--                                                <li><a href="#">Altra Azione</a>-->
<!--                                                </li>-->
<!--                                                <li><a href="#">Qualcos'altro qui</a>-->
<!--                                                </li>-->
<!--                                                <li class="divider"></li>-->
<!--                                                <li><a href="#">link Seprarato</a>-->
<!--                                                </li>-->
<!--                                             </ul>-->
<!--                                          </div>-->
<!--                                       </td>-->
<!--                                    </tr>-->
<!--                                    <tr>-->
<!--                                       <td>Nullam sit amet magna vestibulum libero dapibus iaculis.</td>-->
<!--                                       <td>-->
<!--                                          <div class="progress progress-striped progress-xs">-->
<!--                                             <div role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100" style="width: 50%;" class="progress-bar progress-bar-info">-->
<!--                                                <span class="sr-only">50% Complete</span>-->
<!--                                             </div>-->
<!--                                          </div>-->
<!--                                       </td>-->
<!--                                       <td>-->
<!--                                          <em class="fa fa-calendar fa-fw text-muted"></em>05/10/2014</td>-->
<!--                                       <td class="text-center">-->
<!--                                          <div class="btn-group"><a href="#" data-toggle="dropdown" class="dropdown-toggle"><i class="fa fa-cog"></i></a>-->
<!--                                             <ul class="dropdown-menu pull-right text-left">-->
<!--                                                <li><a href="#">Azione</a>-->
<!--                                                </li>-->
<!--                                                <li><a href="#">Altra Azione</a>-->
<!--                                                </li>-->
<!--                                                <li><a href="#">Qualcos'altro qui</a>-->
<!--                                                </li>-->
<!--                                                <li class="divider"></li>-->
<!--                                                <li><a href="#">link Seprarato</a>-->
<!--                                                </li>-->
<!--                                             </ul>-->
<!--                                          </div>-->
<!--                                       </td>-->
<!--                                    </tr>-->
<!--                                 </tbody>-->
<!--                              </table>-->
<!--                           </div>-->
                           <!-- END table-responsive-->
<!--                           <div class="panel-footer text-right">-->
<!--                              <a href="#">-->
<!--                                 <small>Vedi tutti</small>-->
<!--                              </a>-->
<!--                           </div>-->
<!--                        </div>-->
                        <!-- END panel-->
<!--                     </div>-->
<!--                  </div>-->
                  <!-- END table-->
<!--               </div>-->
               <!-- END dashboard main content-->
               <!-- START dashboard sidebar-->
               <div class="col-md-3">
                  <!-- START messages-->
                  <div class="panel panel-default none">
                     <div class="panel-heading">
                        <div class="pull-right label label-info">33</div>
                        <div class="panel-title">Messaggi non letti</div>
                     </div>
                     <!-- START list group-->
                     <div class="list-group">
                        <!-- START list group item-->
                        <a href="#" class="list-group-item">
                           <div class="media">
                              <div class="pull-left">
                                 <img style="width: 48px; height: 48px;" src="https://placeholdit.imgix.net/~text?txtsize=9&txt=50%C3%9750&w=50&h=50" alt="Image" class="media-object img-rounded">
                              </div>
                              <div class="media-body clearfix">
                                 <small class="pull-right">2h</small>
                                 <strong class="media-heading text-primary">
                                    <div class="point point-success point-lg text-left"></div>Sheila Carter</strong>
                                 <p class="mb-sm">
                                    <small>Cras sit amet nibh libero, in gravida nulla. Nulla...</small>
                                 </p>
                              </div>
                           </div>
                        </a>
                        <!-- END list group item-->
                        <!-- START list group item-->
                        <a href="#" class="list-group-item">
                           <div class="media">
                              <div class="pull-left">
                                 <img style="width: 48px; height: 48px;" src="https://placeholdit.imgix.net/~text?txtsize=9&txt=50%C3%9750&w=50&h=50" alt="Image" class="media-object img-rounded">
                              </div>
                              <div class="media-body clearfix">
                                 <small class="pull-right">3h</small>
                                 <strong class="media-heading text-primary">
                                    <div class="point point-success point-lg text-left"></div>Rich Reynolds</strong>
                                 <p class="mb-sm">
                                    <small>Cras sit amet nibh libero, in gravida nulla. Nulla...</small>
                                 </p>
                              </div>
                           </div>
                        </a>
                        <!-- END list group item-->
                        <!-- START list group item-->
                        <a href="#" class="list-group-item">
                           <div class="media">
                              <div class="pull-left">
                                 <img style="width: 48px; height: 48px;" src="https://placeholdit.imgix.net/~text?txtsize=9&txt=50%C3%9750&w=50&h=50" alt="Image" class="media-object img-rounded">
                              </div>
                              <div class="media-body clearfix">
                                 <small class="pull-right">4h</small>
                                 <strong class="media-heading text-primary">
                                    <div class="point point-danger point-lg text-left"></div>Beverley Pierce</strong>
                                 <p class="mb-sm">
                                    <small>Cras sit amet nibh libero, in gravida nulla. Nulla...</small>
                                 </p>
                              </div>
                           </div>
                        </a>
                        <!-- END list group item-->
                        <!-- START list group item-->
                        <a href="#" class="list-group-item">
                           <div class="media">
                              <div class="pull-left">
                                 <img style="width: 48px; height: 48px;" src="https://placeholdit.imgix.net/~text?txtsize=9&txt=50%C3%9750&w=50&h=50" alt="Image" class="media-object img-rounded">
                              </div>
                              <div class="media-body clearfix">
                                 <small class="pull-right">4h</small>
                                 <strong class="media-heading text-primary">
                                    <div class="point point-danger point-lg text-left"></div>Alex Somar</strong>
                                 <p class="mb-sm">
                                    <small>Vestibulum pretium aliquam scelerisque.</small>
                                 </p>
                              </div>
                           </div>
                        </a>
                        <!-- END list group item-->
                     </div>
                     <!-- END list group-->
                     <!-- START panel footer-->
                     <div class="panel-footer clearfix">
                        <a href="#" class="pull-left">
                           <small>Leggi Tutti</small>
                        </a>
                        <a href="#" class="pull-right">
                           <small>Respingi Tutti</small>
                        </a>
                     </div>
                     <!-- END panel-footer-->
                  </div>
                  <!-- END messages-->
                  <!-- START activity-->
<!--                  <div class="panel panel-default">-->
<!--                     <div class="panel-heading">-->
<!--                        <div class="panel-title">Attività Recenti</div>-->
<!--                     </div>-->
<!--                     <!-- START list group-->
<!--                     <div class="list-group">-->
<!--                        <!-- START list group item-->
<!--                        <div class="list-group-item">-->
<!--                           <div class="media">-->
<!--                              <div class="pull-left">-->
<!--                                 <span class="fa-stack fa-lg">-->
<!--                                    <em class="fa fa-circle fa-stack-2x text-green"></em>-->
<!--                                    <em class="fa fa-cloud-upload fa-stack-1x fa-inverse text-white"></em>-->
<!--                                 </span>-->
<!--                              </div>-->
<!--                              <div class="media-body clearfix">-->
<!--                                 <div class="media-heading text-green m0">NUOVI CARICAMENTI</div>-->
<!--                                 <p class="m0">-->
<!--                                    <small>Nuovo file <a href="#">avatar.jpg </a>caricato nel nostro server</small>-->
<!--                                 </p>-->
<!--                                 <small>5 minuti fa</small>-->
<!--                              </div>-->
<!--                           </div>-->
<!--                        </div>-->
<!--                        <!-- END list group item-->
<!--                        <!-- START list group item-->
<!--                        <div class="list-group-item">-->
<!--                           <div class="media">-->
<!--                              <div class="pull-left">-->
<!--                                 <span class="fa-stack fa-lg">-->
<!--                                    <em class="fa fa-circle fa-stack-2x text-info"></em>-->
<!--                                    <em class="fa fa-file-text-o fa-stack-1x fa-inverse text-white"></em>-->
<!--                                 </span>-->
<!--                              </div>-->
<!--                              <div class="media-body clearfix">-->
<!--                                 <div class="media-heading text-info m0">NUOVO DOCUMENTO</div>-->
<!--                                 <p class="m0">-->
<!--                                    <small>Nuovo Documento <a href="#">Lorem ipsum </a>è stato creato</small>-->
<!--                                 </p>-->
<!--                                 <small>1 ora fa</small>-->
<!--                              </div>-->
<!--                           </div>-->
<!--                        </div>-->
<!--                        <!-- END list group item-->
<!--                        <!-- START list group item-->
<!--                        <div class="list-group-item">-->
<!--                           <div class="media">-->
<!--                              <div class="pull-left">-->
<!--                                 <span class="fa-stack fa-lg">-->
<!--                                    <em class="fa fa-circle fa-stack-2x text-danger"></em>-->
<!--                                    <em class="fa fa-exclamation fa-stack-1x fa-inverse text-white"></em>-->
<!--                                 </span>-->
<!--                              </div>-->
<!--                              <div class="media-body clearfix">-->
<!--                                 <div class="media-heading text-danger m0">MESSAGGIO IMPORTANTE</div>-->
<!--                                 <p class="m0">-->
<!--                                    <small>Sammy Sam ti ha inviato un messaggio importante. <a href="#">Leggi Ora</a>-->
<!--                                    </small>-->
<!--                                 </p>-->
<!--                                 <small>3 ore fa</small>-->
<!--                              </div>-->
<!--                           </div>-->
<!--                        </div>-->
<!--                        <!-- END list group item-->
<!--                        <!-- START list group item-->
<!--                        <div class="list-group-item">-->
<!--                           <div class="media">-->
<!--                              <div class="pull-left">-->
<!--                                 <span class="fa-stack fa-lg">-->
<!--                                    <em class="fa fa-circle fa-stack-2x text-warning"></em>-->
<!--                                    <em class="fa fa-clock-o fa-stack-1x fa-inverse text-white"></em>-->
<!--                                 </span>-->
<!--                              </div>-->
<!--                              <div class="media-body clearfix">-->
<!--                                 <div class="media-heading text-warning m0">MEETING</div>-->
<!--                                 <p class="m0">-->
<!--                                    <small>Rich Reynods ha aggiunto un nuovo meeting. <a href="#" class="label label-info">ADERISCI</a>-->
<!--                                    </small>-->
<!--                                 </p>-->
<!--                                 <small>ieri</small>-->
<!--                              </div>-->
<!--                           </div>-->
<!--                        </div>-->
<!--                        <!-- END list group item-->
<!--                     </div>-->
<!--                     <!-- END list group-->
<!--                     <!-- START panel footer-->
<!--                     <div class="panel-footer clearfix">-->
<!--                        <a href="#" class="pull-left">-->
<!--                           <small>Carica di più</small>-->
<!--                        </a>-->
<!--                     </div>-->
<!--                     <!-- END panel-footer-->
<!--                  </div>-->
               </div>
               <!-- END dashboard sidebar-->
            </div>
               <?php
//               var_dump( $_SESSION['id_utente']);
//               var_dump( $_SESSION['username']);
//               var_dump( $_SESSION['tempo_scaduto']);
//               var_dump( $_SESSION['token']);

//               var_dump($_SESSION);
//
//               var_dump($_SESSION['ultimo_accesso']);
//
//               echo date('d M Y H:i:s',$_SESSION['ultimo_accesso']);
//               echo $dataNew = date("d M Y H:i:s",strtotime('+1 minutes',$_SESSION['ultimo_accesso']));
//
//               echo "<hr>";
//
//               echo $ultimo_accesso = $_SESSION['ultimo_accesso'] . " ultimo_accesso<br>";
//               echo $aggiungo = strtotime('+1 minutes'). " aggiungo_tempo<br>";
//
//               echo $ora = strtotime("now") . " attuale <br>";
//               echo date('d M Y H:i:s',$ora). " attuale data <br>";
//
//               echo "<hr>";
//               if($ora > $_SESSION['tempo_scaduto'] ){echo "true"; } else{ echo "falso";}
//               successoMessaggio('Bravo', 'testiofvioif');
//               erroreMessaggio('Brav strunz', 'testiofvioif');


//               $data = '2017-02-01';
//
//               $old_date_timestamp = strtotime($data);
//               var_dump($old_date_timestamp);
//               $nuovaData = date('l d F', $old_date_timestamp);
//               $arrDate = explode(" ", $nuovaData);
//
//               var_dump($arrDate);
//               echo $nuovaData;


//               echo "COOKIE: <br>" . var_dump($_COOKIE);
//
//               echo "<hr>";
//
//               echo "SESSION: <br>" . var_dump($_SESSION);

               ?>
         </section>
         <!-- END Page content-->
<script type="text/javascript">


</script>
