               <li class="nav-footer">
                  <div class="nav-footer-divider"></div>
                  <!-- START button group-->
                  <div class="btn-group text-center">
                     <?php if($utente->ruolo == "admin"){
                        echo '<button type="button" data-toggle="tooltip" data-title="Gestione Utenti" class="btn btn-link" onclick="window.location.href=\'utenti.php\'">
                        <em class="fa fa-user text-muted">
                        </em>
                     </button>
                      <button type="button" data-toggle="tooltip" data-title="Aggiungi Utenti" class="btn btn-link" onclick="window.location.href=\'aggiungi_utente.php\'">
                       
                        <em class="fa fa-user text-muted"><sup class="fa fa-plus"></sup>
                        </em>
                     </button>';} ?>
                                
                     <button type="button" data-toggle="tooltip" data-title="Impostazioni" class="btn btn-link">
                        <em class="fa fa-cog text-muted"></em>
                     </button>
                      <button type="button" data-toggle="tooltip" data-title="Esci" class="btn btn-link" onclick="window.location.href='esci.php'">
                        <em class="fa fa-sign-out text-muted"></em>
                     </button>
                  </div>
                  <!-- END button group-->
               </li>
               
               <section><!-- fine wrapper -->
    
    