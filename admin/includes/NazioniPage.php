<?php

/**
 * Created da Giuseppe Alessandro De Blasio.
 * User: Giuseppe Alessandro De Blasio
 * Date: 19/10/16
 * Time: 16:32
 */
class NazioniPage extends Db_oggetto {
    protected static $tabella_db = "nazione_page";
    protected static $campi_tabella_db = array(
        'image',
        'img_alt',
        'img_title',
        'desc_titolo',
        'titBox1',
        'box1',
        'titBox2',
        'box2',
        'descrizione',
        'titolo_desc',
        'nazioneUrl',
        'metaTitle',
        'metaDescription',
        'canonicalUrl',
        'metaIndex',
        'stato',
        'id_nazione',
        'id_citta',
        'id_categoria'
    );
    public $id;
    public $image;
    public $img_alt;
    public $img_title;
    public $desc_titolo;
    public $titBox1;
    public $box1;
    public $titBox2;
    public $box2;
    public $descrizione;
    public $titolo_desc;
    public $nazioneUrl;
    public $metaTitle;
    public $metaDescription;
    public $canonicalUrl;
    public $metaIndex;
    public $stato;
    public $id_nazione;
    public $id_citta;
    public $id_categoria;
    public $tipo;
    public $categoriaNazioniEsistono;
    public $grandezza;
    public $tmp_path;
    public $percorso_immagini = "images/nazioni";
    public $immagine_plaecholder = "https://placeholdit.imgix.net/~text?txtsize=56&txt=350%C3%97350&w=444&h=297";
    public $errore = array();
    public $errore_caricamento_array = array (

        UPLOAD_ERR_OK                       =>  "Non c'è alcun errore, il file è stato caricato con successo.",
        UPLOAD_ERR_INI_SIZE                 =>  "Il file caricato supera la direttiva upload_max_filesize in php.ini",
        UPLOAD_ERR_FORM_SIZE                =>  "Il file inviato eccede il MAX_FILE_SIZE che è stato specificato nel modulo HTML",
        UPLOAD_ERR_PARTIAL                  =>  "Il file è stato solo parzialmente caricato.",
        UPLOAD_ERR_NO_FILE                  =>  "Nessun file è stato caricato.",
        UPLOAD_ERR_NO_TMP_DIR               =>  "Manca una cartella temporanea. Introdotta in PHP 5.0.3",
        UPLOAD_ERR_CANT_WRITE               =>  "Impossibile scrivere file su disco. Introdotto in PHP 5.1.0.",
        UPLOAD_ERR_EXTENSION                =>  "Una estensione PHP ha interrotto il caricamento del file."
    );


    public function caricamento_foto_nazioni() {

        if(!empty($this->errore)){
            return FALSE;
        }

        if (empty($this->image) || empty($this->tmp_path)) {
            $this->errore[] = "Il file non è utilizzabile";
            return FALSE;
        }

        $percorso_immagini = ROOT_SITO . DS . $this->percorso_immagini . DS. $this->image;

        if(file_exists($percorso_immagini)){
            $this->errore[] = "Il file {$this->image} già esiste";
            return FALSE;
        }

        if(move_uploaded_file($this->tmp_path, $percorso_immagini)){
            unset($this->tmp_path);
            return TRUE;
        } else {
            $this->errore[] = "Molto probabilmente non si ha il permesso di inserire il file nalla directory";
            return FALSE;
        }
    }

    public function percorso_immagine_e_plecholder() {
        return empty($this->image)? $this->immagine_plaecholder : '../'.$this->percorso_immagini.'/'.$this->image;
    }

    public function percorso_immagine_e_plecholder_front() {
        return empty($this->image)? $this->immagine_plaecholder : $this->percorso_immagini.DS.$this->image;
    }

    public function impostazione_file_nazione($file) {

        if(empty($file) || !$file || !is_array($file)){
            $this->errore[] = "Non c'è alcun file da caricare";
            return FALSE;
        } elseif ($file['error'] != 0 ) {

            $this->errore[] = $this->errore_caricamento_array[$file['error']];
            return FALSE;
        } else {
            $this->image = basename($file['name']);
            $this->tmp_path = $file['tmp_name'];
            $this->tipo = $file['type'];
            $this->grandezza = $file['size'];
        }
    }

    public function seleziona_per_citta($citta){
        global $database;

        return static::cerca_con_query("SELECT * FROM ". static::$tabella_db. " WHERE id_citta = $citta ");

    }

    public function seleziona_per_nazione($nazione){
        global $database;

        return static::cerca_con_query("SELECT * FROM ". static::$tabella_db. " WHERE id_nazione = $nazione ");

    }

    public function seleziona_per_categoria($categoria){
        global $database;

        return static::cerca_con_query("SELECT * FROM ". static::$tabella_db. " WHERE id_categoria = $categoria ");

    }

//    public function esiste_nazione_e_categoria($categoria, $nazione){
//        global $database;
//
//        $nazione = $database->escape($nazione);
//        $categoria = $database->escape($categoria);
//
//        $sql = "SELECT id_categoria, id_nazione FROM " . static::$tabella_db . " WHERE ";
//        $sql.= " id_categoria = $categoria  AND id_nazione = $nazione ";
////        echo $sql;
//        $risultato = $database->query($sql);
//
////        echo $database->conta_righe($risultato);
//
//        if($database->conta_righe($risultato) == 1){
//
////            return $this->categoriaNazioniEsistono ='<div class="alert alert-danger alert-dismissable"><button type="button" data-dismiss="alert" aria-hidden="true" class="close">×</button>Per questa Nazione già è presente la seguente categoria "'. $categoria .'"</div>';
//                echo $doppio = "
//                        <script>
//                            $(document).ready(function(){
//                                swal(
//                                  'Oops...',
//                                  'Questa nazione e la categoria già esiste!',
//                                  'error'
//                                );
//                            });
//                        </script>";
//            return $doppio;
//            die();
////            return $this->categoriaNazioniEsistono ='<div class="alert alert-danger alert-dismissable"><button type="button" data-dismiss="alert" aria-hidden="true" class="close">×</button>Per questa Nazione già è presente la seguente categoria</div>';
//        }
//        else {
//            if(basename($_SERVER['PHP_SELF']) == "modifica_nazione.php"):
//
//                $_SESSION['mess_successo'] = '<div data-toggle="notify" data-onload data-message="Pagina Nazione <b>Aggiunta</b> Correttamente" data-options="{&quot;status&quot;:&quot;success&quot;, &quot;pos&quot;:&quot;bottom-right&quot;}" class="hidden-xs"></div>';
//                return $this->salva();
//                unset($_SESSION['mess_successo']);
//
//
//            elseif (basename($_SERVER['PHP_SELF']) == "aggiungi_nazioni_page.php"):
//
////                    header('Location: modifica_nazione.php');
//
//
//            $_SESSION['mess_successo'] = '<div data-toggle="notify" data-onload data-message="Pagina Nazione <b>Aggiunta</b> Correttamente" data-options="{&quot;status&quot;:&quot;success&quot;, &quot;pos&quot;:&quot;bottom-right&quot;}" class="hidden-xs"></div>';
//            return $this->salva();
//            unset($_SESSION['mess_successo']);
//
//
//            endif;
//
//        }
//    }

    public function esiste_nazione_e_categoria($categoria, $nazione)
    {
        global $database;

        $nazione = $database->escape($nazione);
        $categoria = $database->escape($categoria);

        $sql = "SELECT id_categoria, id_nazione FROM " . static::$tabella_db . " WHERE ";
        $sql .= " id_categoria = $categoria  AND id_nazione = $nazione ";
        $risultato = $database->query($sql);


        if ($database->conta_righe($risultato) == 1) {

            echo $doppio = "
                        <script>
                            $(document).ready(function(){
                                swal(
                                  'Oops...',
                                  'Questa nazione e la categoria già esiste!',
                                  'error'
                                );                                
                            });                            
                        </script>";
            return $doppio;
        } else {
            $this->salva();
        }

        global $msg_instant;
        $msg_instant->success('Nazione <b>Aggiunta</b> correttamente');
        $msg_instant->display();

    }

    public function esiste_nazioni_categoria_modifica($categoria, $nazione, $id)
    {
        global $database;

        $nazione = $database->escape($nazione);
        $categoria = $database->escape($categoria);

        $sql = "SELECT id, id_categoria, id_nazione FROM " . static::$tabella_db . " WHERE ";
        $sql .= " id_categoria = $categoria  AND id_nazione = $nazione ";
        $risultato = $database->query($sql);

        if ($database->conta_righe($risultato) == 1 AND $this->id  !== $id) {

            echo $doppio = "
                        <script>
                            $(document).ready(function(){
                                swal(
                                  'Oops...',
                                  'Questa nazione e la categoria già esiste!',
                                  'error'
                                );                                
                            });                            
                        </script>";
            return $doppio;
        } else {
            $this->salva();
        }

        global $msg_instant;
        $msg_instant->success('Nazione <b>Modificata</b> correttamente');
        $msg_instant->display();
    }

}