<?php

/**
 * Created da Giuseppe Alessandro De Blasio.
 * User: Giuseppe Alessandro De Blasio
 * Date: 17/09/2016
 * Time: 11:10
 */

class Sessione {
    private $loggato = false;
    public $id_utente;
    public $messaggio;
    public $conta;
    public $username;
    public $vedi_utente;
    public $token;
    public $ultima_attivita;
    public $tempo_scaduto;
    public $inactive = 60;


    public function __construct() {
        
        session_start();
        $this->conta_visitatori();
        $this->verifica_login();
        $this->verifica_messaggio();
        $this->mostra_messaggio();
        $this->fine_sessione();
    }
    
    public function setta_messaggio($messaggio)
    {   
        if (!empty($messaggio)){
        $_SESSION['messaggio'] = $messaggio;
        } else{
            $messaggio = "";
        }
    }

    public function mostra_messaggio(){
        if(isset($_SESSION['messaggio'])){
            
            $this->messaggio = $_SESSION['messaggio'];
            unset($_SESSION['messaggio']);
        } else {
            $this->messaggio = " ";
        }
    }    
    
    
    
    public function messaggio($msg = "") {
        
        if(!empty($msg))
        {
            $_SESSION['messaggio'] = $msg;
        } else {
            echo $this->messaggio;
        }
    }
    
    private function verifica_messaggio(){
        if(isset($_SESSION['messaggio']))
        {
            $this->messaggio = $_SESSION['messaggio'];
            unset($_SESSION['messaggio']);
        } else {
            $this->messaggio = " ";
        }
    }
    
    public function conta_visitatori() {
        if(isset($_SESSION['conta'])){
            return $this->conta = $_SESSION['conta']++;
        } else {
            return $_SESSION['conta'] = 1;
        }
    }
    
    public function loggato() {
        return $this->loggato;
    }
    
    public function accedi($utente) {
        if($utente){
            $this->id_utente = $_SESSION['id_utente'] = $utente->id;
            $this->username = $_SESSION['username'] = $utente->username;
            $this->tempo_scaduto = $_SESSION['tempo_scaduto'] = $utente->inattivita();
            if ($_SESSION['tempo_scaduto'] > $utente->tempo_scaduto){
                redirect('../accedi.php');
                $this->esci();
            } else {
                redirect('../index.php');
            }
            $this->loggato = TRUE;
            $this->ultima_attivita = $_SESSION['ultimo_accesso'] = strtotime(date("d-m-Y h:i:s"));
        }
    }
    
    public function esci() {
        unset($_SESSION['id_utente']);
        unset($this->id_utente);
        unset($_SESSION['ultimo_accesso']);
        unset($_SESSION['username']);
        unset($_SESSION['user']);
        unset($_SESSION['tempo_scaduto']);
        setcookie ("XSRF-TOKEN", "", time() - 3600);
        unset($_SESSION['csrf']);
        $this->loggato = FALSE;
    }
    
    private function verifica_login() {
        if(isset($_SESSION['id_utente'])){
            $this->id_utente = $_SESSION['id_utente'];
            $this->loggato = TRUE;
        } else {
            unset($this->id_utente);
            $this->loggato = FALSE;
        }
    }
    
    public function vedi_utente() {
        if($this->loggato = TRUE){
            if ($utente = $_SESSION['id_utente']) {
                echo $this->username = $_SESSION['username'];
            } else {
                echo " ";
            }
        }
    }
    
    public function generazione_token() {
        $this->token = $_SESSION['token'] = md5(uniqid(mt_rand(), TRUE));
        return $this->token;
    }

    public function fine_sessione()
    {

        if (isset($_SESSION['ultimo_accesso']) > (60 * 30)) {
            $this->esci();
        }
    }

    function isLoginSessionExpired() {
        $login_session_duration = 10;
        $current_time = time();
        if(isset($_SESSION['loggedin_time']) and isset($_SESSION["user_id"])){
            if(((time() - $_SESSION['loggedin_time']) > $login_session_duration)){
                return true;
            }
        }
        return false;
    }
    
    
} // FINE CLASSE SESSIONE

$sessione = new Sessione();

$messaggio = $sessione->messaggio();

