<?php

class Articoli extends Db_oggetto {
    
    protected static $tabella_db = "articoli";
    protected static $campi_tabella_db = array('categoria_id', 'titolo', 'autore', 'utente', 'data', 'immagine_articolo', 'contenuto', 'tags', 'numero_commenti', 'stato', 'visualizzazioni');
    public $id;
    public $categoria_id;
    public $titolo;
    public $autore;
    public $utente;
    public $data;
    public $immagine_articolo;
    public $contenuto;
    public $tags;
    public $numero_commenti;
    public $stato;
    public $visualizzazioni;
    public $tipo;
    public $grandezza;
    public $tmp_path;
    public $percorso_immagini = "images";
    public $immagine_plaecholder = "http://placehold.it/600x600text=immagine";
    public $errore = array();
    public $errore_caricamento_array = array (
        
        UPLOAD_ERR_OK                       =>  "Non c'è alcun errore, il file è stato caricato con successo.",
        UPLOAD_ERR_INI_SIZE                 =>  "Il file caricato supera la direttiva upload_max_filesize in php.ini",
        UPLOAD_ERR_FORM_SIZE                =>  "Il file inviato eccede il MAX_FILE_SIZE che è stato specificato nel modulo HTML",
        UPLOAD_ERR_PARTIAL                  =>  "Il file è stato solo parzialmente caricato.",
        UPLOAD_ERR_NO_FILE                  =>  "Nessun file è stato caricato.",
        UPLOAD_ERR_NO_TMP_DIR               =>  "Manca una cartella temporanea. Introdotta in PHP 5.0.3",
        UPLOAD_ERR_CANT_WRITE               =>  "Impossibile scrivere file su disco. Introdotto in PHP 5.1.0.",
        UPLOAD_ERR_EXTENSION                =>  "Una estensione PHP ha interrotto il caricamento del file."
    );
    
    
    public function caricamento_foto_articolo() {
        
        if(!empty($this->errore)){
            return FALSE;
        }
        
        if (empty($this->immagine_articolo) || empty($this->tmp_path)) {
            $this->errore[] = "Il file non è utilizzabile";
            return FALSE;
        }
        
        $percorso_immagini = ROOT_SITO . DS . 'admin' . DS . $this->percorso_immagini . DS. $this->immagine_articolo;
        
        if(file_exists($percorso_immagini)){
            $this->errore[] = "Il file {$this->immagine_articolo} già esiste";
            return FALSE;
        }
        
        if(move_uploaded_file($this->tmp_path, $percorso_immagini)){
            unset($this->tmp_path);
            return TRUE;
        } else {
            $this->errore[] = "Molto probabilmente non si ha il permesso di inserire il file nalla directory";
            return FALSE;
        }
    }
    
    public function percorso_immagine_e_plecholder() {
        return empty($this->immagine_articolo)? $this->immagine_plaecholder : $this->percorso_immagini.DS.$this->immagine_articolo;
    }
    
    public function percorso_immagine_e_plecholder_front() {
        return empty($this->immagine_articolo)? $this->immagine_plaecholder : "admin".DS.$this->percorso_immagini.DS.$this->immagine_articolo;
    }
    
    public function impostazione_file_articolo($file) {
        
        if(empty($file) || !$file || !is_array($file)){
            $this->errore[] = "Non c'è alcun file da caricare";
            return FALSE;
        } elseif ($file['error'] != 0 ) {
        
            $this->errore[] = $this->errore_caricamento_array[$file['error']];
            return FALSE;
        } else {
            $this->immagine_articolo = basename($file['name']);
            $this->tmp_path = $file['tmp_name'];
            $this->tipo = $file['type'];
            $this->grandezza = $file['size'];
        }
    }
    
    public function conta_articoli_pubblicati() {
        global $database;
        
        $sql = "SELECT COUNT(*) FROM " . static::$tabella_db;
        $sql.= " WHERE stato LIKE '%Pubblicato%' ";
        $risultato = $database->query($sql);
        $row = mysqli_fetch_array($risultato);
        
        return array_shift($row);
    }
    
    public static function ultimi_cinque_articoli() {
        global $database;
         
        //$sql = "SELECT * FROM " . static::$tabella_db; 
        //$sql.= " WHERE id > (SELECT MAX(id) - 5 FROM " . static::$tabella_db .")";
        /*$sql = "SELECT * FROM " . static::$tabella_db . " WHERE id > (SELECT MAX(id) - 5 FROM " . static::$tabella_db . " )" ;
        $risultato = $database->query($sql);
        $row = mysqli_fetch_array($risultato);
        
        if ($risultato->num_rows > 0) {
   
        while($row = $risultato->fetch_array()) {
        echo '<li><a href="articolo.php?id=' . $articolo->id .'">' .$articolo->titolo . '</a></li>';
        }
        
        //return array_shift($row);
        
        //$risultato_array = static::cerca_con_query("SELECT * FROM " . static::$tabella_db . " WHERE id > (SELECT MAX(id) - 5 FROM " . static::$tabella_db . ") ");
        //return !empty($risultato_array)? array_shift($risultato_array): FALSE;
        
        //return static::cerca_con_query("SELECT * FROM (SELECT * FROM articoli ORDER BY id DESC LIMIT 5) sub ORDER BY id ASC");
        //$risultato = $database->query($sql);
        //$row = mysqli_fetch_array($risultato);
         
        
        //return array_shift($row);
    }*/   
        $sql = "SELECT * FROM (SELECT * FROM articoli ORDER BY id DESC LIMIT 5) sub ORDER BY id ASC";
        return $risultato = mysqli_query($database->conn, $sql);
    }
    
}