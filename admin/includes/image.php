<?php

/**
 * Created da Giuseppe Alessandro De Blasio.
 * User: Giuseppe Alessandro De Blasio
 * Date: 20/09/2016
 * Time: 10:16
 */

class Image extends Db_oggetto {
    
    protected static $tabella_db = "image";
    protected static $campi_tabella_db = array('titolo','didascalia','descrizione','nomefile','testo_alternativo','tipo','grandezza','data_immagine');
    public $id;
    public $titolo;
    public $didascalia;
    public $descrizione;
    public $nomefile;
    public $testo_alternativo;
    public $tipo;
    public $grandezza;
    public $tmp_path;
    public $data_immagine;
    public $username;
    public $percorso_upload = "images" ;//&& "../../immagini/nazioni"
    public $percorso_backend = "../images" ;//&& "../../immagini/nazioni"
    public $errore = array();
    public $errore_caricamento_array = array (
        
        UPLOAD_ERR_OK                       =>  "Non c'è alcun errore, il file è stato caricato con successo.",
        UPLOAD_ERR_INI_SIZE                 =>  "Il file caricato supera la direttiva upload_max_filesize in php.ini",
        UPLOAD_ERR_FORM_SIZE                =>  "Il file inviato eccede il MAX_FILE_SIZE che è stato specificato nel modulo HTML",
        UPLOAD_ERR_PARTIAL                  =>  "Il file è stato solo parzialmente caricato.",
        UPLOAD_ERR_NO_FILE                  =>  "Nessun file è stato caricato.",
        UPLOAD_ERR_NO_TMP_DIR               =>  "Manca una cartella temporanea. Introdotta in PHP 5.0.3",
        UPLOAD_ERR_CANT_WRITE               =>  "Impossibile scrivere file su disco. Introdotto in PHP 5.1.0.",
        UPLOAD_ERR_EXTENSION                =>  "Una estensione PHP ha interrotto il caricamento del file."
    );
    
    public function percorso_immagine() {
        return $this->percorso_upload.DS.$this->nomefile;
    }

    public function percorso_immagine_backend() {
        return $this->percorso_backend.DS.$this->nomefile;
    }
    
    public function percorso_immagine_utente() {
        return $this->percorso_upload.DS."utente".DS.$this->percorso_username().DS.$this->nomefile;
    }

    public function percorso_immagine_post() {
        return $this->percorso_upload.DS."post".DS.$this->percorso_username().DS.$this->nomefile;
    }
    
    /*public function salva() {
        if($this->id){
            $this->aggiorna();
        } else {
            if(!empty($this->errore)){
                return FALSE;
            }
            if(empty($this->nomefile) || empty($this->tmp_path)){
                $this->errore[] = "Il file non è utilizzabile";
                return FALSE;
            }
            $percorso_destinazione = ROOT_SITO . DS . 'admin' . DS . $this->percorso_upload . DS . $this->nomefile;
            
            if(file_exists($percorso_destinazione)){
                $this->errore[] = "Il file {$this->nomefile} già esiste";
                return FALSE;
            }
            
            if(move_uploaded_file($this->tmp_pathp, $percorso_destinazione)){
                if($this->crea()){
                    unset($this->tmp_path);
                    return TRUE;
                } else {
                    $this->errore[] = "Molto probabilmente non si ha il permesso per inserire il file nella directory" ;
                    return FALSE;
                }
            }
        }
    }*/
    
    public function percorso_username() {
        $this->username = $_SESSION['id_utente'];
        return $this->username;
    }
    
    
    public function salva_immagine_utente() {
        if($this->id){
            $this->aggiorna();
        } else {
            if(!empty($this->errore)){
                return FALSE;
            }
            if(empty($this->nomefile) || empty($this->tmp_path)){
                $this->errore[] = "Il file non è utilizzabile";
                return FALSE;
            }
            $percorso_destinazione = ROOT_SITO . DS . $this->percorso_upload .DS. "utente". DS . $this->percorso_username(). DS. $this->nomefile;
            
            if(file_exists($percorso_destinazione)){
                $this->errore[] = "Il file {$this->nomefile} già esiste";
                return FALSE;
            }
            
            if(move_uploaded_file($this->tmp_path, $percorso_destinazione)){
                if($this->crea()){
                    unset($this->tmp_path);
                    return TRUE;
                } else {
                    $this->errore[] = "Molto probabilmente non si ha il permesso per inserire il file nella directory" ;
                    return FALSE;
                }
            }
        }
    }


    public function salva_immagine_post() {
        if($this->id){
            $this->aggiorna();
        } else {
            if(!empty($this->errore)){
                return FALSE;
            }
            if(empty($this->nomefile) || empty($this->tmp_path)){
                $this->errore[] = "Il file non è utilizzabile";
                return FALSE;
            }
            $percorso_destinazione = ROOT_SITO . DS . $this->percorso_upload . DS. $this->nomefile;

            if(file_exists($percorso_destinazione)){
                $this->errore[] = "Il file {$this->nomefile} già esiste";
                return FALSE;
            }

            if(move_uploaded_file($this->tmp_path, $percorso_destinazione)){
                if($this->crea()){
                    unset($this->tmp_path);
                    return TRUE;
                } else {
                    $this->errore[] = "Molto probabilmente non si ha il permesso per inserire il file nella directory" ;
                    return FALSE;
                }
            }
        }
    }

    public function cancella_foto_post() {
        if($this->cancella()){
//            $percorso_destinazione = ROOT_SITO . DS . 'admin' . DS . $this->percorso_upload .DS. $this->nomefile;
            $percorso_destinazione = ROOT_SITO . DS . $this->percorso_upload .DS. $this->nomefile;
            return unlink($percorso_destinazione)? TRUE : FALSE;
        } else {
            return FALSE;
        }
    }


    public function cancella_foto($id, $nomefile) {
        $this->id = $id;
        $this->nomefile = $nomefile;
        if($this->cancella()){
            //$percorso_destinazione = ROOT_SITO . DS . 'admin' . DS . $this->percorso_upload . DS . $this->percorso_username(). DS. $this->immagine_utente;
            $percorso_destinazione = ROOT_SITO . DS . $this->percorso_upload .DS. $nomefile;
            return unlink($percorso_destinazione)? TRUE : FALSE;
        } else {
            return FALSE;
        }
    }

    public function cancella_foto_utente() {
        if($this->cancella()){
            $percorso_destinazione = ROOT_SITO . DS . 'admin' . DS . $this->percorso_upload .DS. "utente" . DS . $this->percorso_username(). DS. $this->immagine_utente;
            return unlink($percorso_destinazione)? TRUE : FALSE;
        } else {
            return FALSE;
        }
    }    
    
    public function impostazione_file_foto($file) {
        
        if(empty($file) || !$file || !is_array($file)){
            $this->errors[] = "Non c'è alcun file da caricare";
            return FALSE;
        } elseif ($file['error'] != 0 ) {
        
            $this->errors[] = $this->errore_caricamento_array[$file['error']];
            return FALSE;
        } else {
            $this->nomefile = basename($file['name']);
            $this->tmp_path = $file['tmp_name'];
            $this->tipo = $file['type'];
            $this->grandezza = $file['size'];
        }
    }      
    
    
    
} // FINE CLASSE FOTO