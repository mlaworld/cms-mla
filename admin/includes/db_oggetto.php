<?php

/**
 * Created da Giuseppe Alessandro De Blasio.
 * User: Giuseppe Alessandro De Blasio
 * Date: 17/09/2016
 * Time: 10:11
 */

class Db_oggetto{

    protected static $tabella_db = "utenti";
    /*public $errore_caricamento_array = array (

        UPLOAD_ERR_OK                       =>  "Non c'è alcun errore, il file è stato caricato con successo.",
        UPLOAD_ERR_INI_SIZE                 =>  "Il file caricato supera la direttiva upload_max_filesize in php.ini",
        UPLOAD_ERR_FORM_SIZE                =>  "Il file inviato eccede il MAX_FILE_SIZE che è stato specificato nel modulo HTML",
        UPLOAD_ERR_PARTIAL                  =>  "Il file è stato solo parzialmente caricato.",
        UPLOAD_ERR_NO_FILE                  =>  "Nessun file è stato caricato.",
        UPLOAD_ERR_NO_TMP_DIR               =>  "Manca una cartella temporanea. Introdotta in PHP 5.0.3",
        UPLOAD_ERR_CANT_WRITE               =>  "Impossibile scrivere file su disco. Introdotto in PHP 5.1.0.",
        UPLOAD_ERR_EXTENSION                =>  "Una estensione PHP ha interrotto il caricamento del file."
    );
*/
    public function cerca_con_query($sql) {

        global $database;

        $risultato = $database->query($sql);

        $oggetto_array = array();

        while ($row = mysqli_fetch_array($risultato)) {
            $oggetto_array[] = static::instanza($row);
        }

        return $oggetto_array;
    }

    public function instanza($record) {

        $chiamo_classe = get_called_class();

        $oggetto = new $chiamo_classe;

        foreach ($record as $attributo => $valore) {

            if($oggetto->tiene_attributo($attributo)){
                $oggetto->$attributo = $valore;
            }
        }
        return $oggetto;
    }

    public function tiene_attributo($attributo) {

        $proprieta_oggetto = get_object_vars($this);

        return array_key_exists($attributo, $proprieta_oggetto);
    }



    public function seleziona_tutti() {

        return static::cerca_con_query("SELECT * FROM ". static::$tabella_db. " ");
    }


    public function seleziona_per_id($id) {
        global $database;

        $risultato_array = static::cerca_con_query("SELECT * FROM ". static::$tabella_db . " WHERE id= $id LIMIT 1");

        return !empty($risultato_array)? array_shift($risultato_array): FALSE;

    }

    public function seleziona_per_campo_custom($campo, $valore = null) {
        global $database;

         $risultato_array = static::cerca_con_query("SELECT * FROM ". static::$tabella_db . " WHERE $campo = $database->escape('$valore') ");

        return !empty($risultato_array)? array_shift($risultato_array) : FALSE;
    }


    public function seleziona_titolo($titolo) {
        global $database;

        $risultato_array = static::cerca_con_query("SELECT * FROM ". static::$tabella_db . " WHERE titolo= $database->escape('{$titolo}') LIMIT 1");

        return !empty($risultato_array)? array_shift($risultato_array): FALSE;

    }

    public function proprieta() {
        $proprieta = array();

        foreach (static::$campi_tabella_db as $campi_db) {

            if(property_exists($this, $campi_db)){
                $proprieta[$campi_db] = $this->$campi_db;
            }
        }
        return $proprieta;
    }


    public function cancella_proprieta() {
        global $database;

        $cancella_proprieta = array();

        foreach ($this->proprieta() as $chiave => $valore) {
            $cancella_proprieta[$chiave] = $database->escape($valore);
        }
        return $cancella_proprieta;
    }


    public function salva() {

        return isset($this->id)? $this->aggiorna() : $this->crea();
    }

    public function crea(){
        global $database;

        $proprieta = $this->cancella_proprieta();

        $sql = "INSERT INTO " . static::$tabella_db."(" .implode("," , array_keys($proprieta)) .")";
        $sql .= " VALUES ('" .implode("','", array_values($proprieta)). "')";

        if ($database->query($sql)) {
            $this->id = $database->inserisco_id();
            return TRUE;
        } else {
            return FALSE;
        }

    }

    public function valoriDuplicazione()
    {
        $proprieta = array();


        foreach (static::$campi_tabella_db as $campi_db) {

            if (property_exists($this, $campi_db)) {
                $proprieta[$campi_db] = $this->$campi_db;
                unset($proprieta['id']);

            }
        }

        return $proprieta;
    }


    public function valoriDuplicazioneCarat()
    {
        $proprieta_carat = array();


        foreach (static::$campi_tabella_db as $campi_db) {

            if (property_exists($this, $campi_db)) {
                $proprieta_carat[$campi_db] = $this->$campi_db;
                unset($proprieta_carat['id']);

            }
        }

        return $proprieta_carat;
    }







    public function duplicazioneRow($id){
        global $database;

        $row = array();


        $sql = "SELECT * FROM " . static::$tabella_db . " WHERE id = $id ";
        $risultato = $database->query($sql);
        $row = mysqli_fetch_assoc($risultato);
        unset($row['id']);


        //print_r($row);

        $proprieta = $this->valoriDuplicazione();

        $sql = "INSERT INTO " . static::$tabella_db."(" .implode("," , array_keys($proprieta)) .")";
        $sql .= " SELECT '" .implode("','", array_values($row)). "' FROM " . static::$tabella_db. " WHERE id = $id ";

        if ($database->query($sql)) {
            $this->id = $database->inserisco_id();
            return TRUE;
        } else {
            return FALSE;
        }

        //echo $sql;

    }


    public function aggiorna() {
        global $database;

        $proprieta = $this->cancella_proprieta();

        $coppia_proprieta = array();

        foreach ($proprieta as $chiave => $valore) {

            $coppia_proprieta[] = "{$chiave}='{$valore}'";
        }

        $sql = "UPDATE " . static::$tabella_db . " SET ";
        $sql .= implode(",", $coppia_proprieta);
        $sql .= " WHERE id = " . $database->escape($this->id);

        $database->query($sql);

        return (mysqli_affected_rows($database->conn) == 1) ? TRUE : FALSE;

    }

    public function cancella() {

        global $database;

        $sql = "DELETE FROM " . static::$tabella_db. " ";
        $sql .= " WHERE id= " . $database->escape($this->id);
        $sql .= " LIMIT 1";

        $database->query($sql);

        return (mysqli_affected_rows($database->conn) == 1)? true : false ;
    }


    public function cancella_custom($campo, $valore=null){

        global $database;



        $sql = "DELETE FROM " . static::$tabella_db. " ";
        $sql .= " WHERE $campo= " . $database->escape($valore);
        //$sql .= " LIMIT 1";

        $database->query($sql);

        return (mysqli_affected_rows($database->conn) == 1)? true : false ;
    }

    public function conta_elementi() {
        global $database;

        $sql = "SELECT COUNT(*) FROM " . static::$tabella_db;
        $risultato = $database->query($sql);
        $row = mysqli_fetch_array($risultato);

        return array_shift($row);
    }

    public function conta_elementi_filtro($campo, $valore) {
        global $database;

        $sql = "SELECT COUNT(*) FROM " . static::$tabella_db . " WHERE $campo = '{$valore}' ";
        $risultato = $database->query($sql);
        $row = mysqli_fetch_array($risultato);

        return array_shift($row);
    }

    public function pulisci($stringa){

        return htmlentities($stringa);
    }


    public function view_cataloghi($cerca){
        global $database;

        $sql = "SELECT * FROM view_cataloghi";
        $sql .= " WHERE Centro LIKE '%$cerca%' OR nome_catalogo LIKE '%$cerca%' ";

        $risultato = $database->query($sql);
        return $row = mysqli_fetch_array($risultato);

//        return var_dump(array_shift($row));
//        return (mysqli_affected_rows($database->conn) == 1)? true : false ;
//        return $centri = Centro::cerca_con_query($sql);

    }

    public function ultimo_id(){
        global $database;

//        $sql = "SELECT * FROM " . static::$tabella_db;
//        $sql = "SELECT LAST_INSERT_ID() "  . static::$tabella_db;
        $sql= "SELECT id FROM " . static::$tabella_db . " ORDER BY ID DESC LIMIT 1";
        $risultato = $database->query($sql);

        while ($row = mysqli_fetch_assoc($risultato) ):

            $idResult = $row['id'];

            endwhile;

        return $idResult;


    }

    public function seleziona_per_nazione($nazione, $categoria){
        global $database;

        return static::cerca_con_query("SELECT * FROM ". static::$tabella_db. " WHERE id_nazione = '{$nazione}' AND id_categoria = '{$categoria}'  ");

    }

    ####################### GESTIONE FILE ######################################


    public function impostazione_file($file) {

        if(empty($file) || !$file || !is_array($file)){
            $this->errors[] = "Non c'è alcun file da caricare";
            return FALSE;
        } elseif ($file['error'] != 0 ) {

            $this->errors[] = $this->errore_caricamento_array[$file['error']];
            return FALSE;
        } else {
            $this->immagine_utente = basename($file['name']);
            $this->tmp_path = $file['tmp_name'];
            $this->tipo = $file['type'];
            $this->grandezza = $file['size'];
        }
    }

}