<?php

/**
 * Created da Giuseppe Alessandro De Blasio.
 * User: Giuseppe Alessandro De Blasio
 * Date: 05/06/17
 * Time: 15:24
 */
class DatabasePDO extends Db_oggetto
{

    private $dbh;
    private $error;
    private $stmt;

    public function __construct()
    {
        $this->conn_db_pdo();
    }


    public function conn_db_pdo()
    {
        $dns = 'mysql:host=' . DB_HOST .';'. 'dbname=' . DB_NAME.';';
        // Set options
        $options = array(
            PDO::ATTR_PERSISTENT    => true,
            PDO::ATTR_ERRMODE       => PDO::ERRMODE_EXCEPTION
        );
        //Creo nuova istanza PDO
        try{
            $this->dbh = new PDO($dns, DB_USER, DB_PASSWORD, $options);
        }
        // Se ci sono errori
        catch (PDOException $e){
            $this->error = $e->getMessage();
        }

    }

    public function query($query){
        $this->stmt = $this->dbh->prepare($query);
    }

    public function bind($param, $value, $type = null){
        if (is_null($type)) {
            switch (true) {
                case is_int($value):
                    $type = PDO::PARAM_INT;
                    break;
                case is_bool($value):
                    $type = PDO::PARAM_BOOL;
                    break;
                case is_null($value):
                    $type = PDO::PARAM_NULL;
                    break;
                default:
                    $type = PDO::PARAM_STR;
            }
        }
        $this->stmt->bindValue($param, $value, $type);
    }

    public function execute(){
        return $this->stmt->execute();
    }

    public function resultsetArr(){
        $this->execute();
        return $this->stmt->fetchAll(PDO::FETCH_ASSOC);
    }

    public function resulsetObj()
    {
        $this->execute();
        $row = $this->stmt->fetchAll(PDO::FETCH_OBJ);

        foreach ($row as $objClass){
            $obj[] =  $objClass;
        }
            return $obj;
    }

    public function resultClass($classe)
    {
        $this->execute();
        $row = $this->stmt->fetchAll(PDO::FETCH_CLASS, $classe);
//        foreach ($row as $objClass){
//           $obj = $objClass;
//        }
        return $row;
    }

//    public function fetchAll()
//    {
//        $this->execute();
//        return $row = $this->stmt->fetchAll(PDO::FETCH_COLUMN);
//    }

    public function single(){
        $this->execute();
        return $this->stmt->fetch(PDO::FETCH_ASSOC);
    }

    public function rowCount(){
        return $this->stmt->rowCount();
    }

    public function lastInsertId(){
        return $this->dbh->lastInsertId();
    }

    public function beginTransaction(){
        return $this->dbh->beginTransaction();
    }

    public function endTransaction(){
        return $this->dbh->commit();
    }

    public function cancelTransaction(){
        return $this->dbh->rollBack();
    }

    public function debugDumpParams(){
        return $this->stmt->debugDumpParams();
    }

}
//FINE CLASSE DATABASE PDO

$databasePDO  = new DatabasePDO();