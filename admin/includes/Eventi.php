<?php

/**
 * Created da Giuseppe Alessandro De Blasio.
 * User: Giuseppe Alessandro De Blasio
 * Date: 15/03/17
 * Time: 18:00
 */
class Eventi extends Db_oggetto
{
    protected static $tabella_db = "eventi";
    protected static $campi_tabella_db = array(
        'data_inizio',
        'data_fine',
        'localita',
        'id_cat_eventi',
        'link_evento',
        'breve_descrizione',
        'descrizione',
        'img_evento',
        'img_alt',
        'url_frendly',
        'link_easy',
        'lat',
        'lng',
        'stars'
    );

    public $id;
    public $data_inizio;
    public $data_fine;
    public $localita;
    public $id_cat_eventi;
    public $link_evento;
    public $breve_descrizione;
    public $descrizione;
    public $img_evento;
    public $img_alt;
    public $url_frendly;
    public $link_easy;
    public $lat;
    public $stars;
    public $lng;
    public $tipo;
    public $grandezza;
    public $tmp_path;
    public $percorso_immagini = 'images/eventi';
    public $immagine_plaecholder = "http://placehold.it/600x600text=immagine";
    public $errore = array();
    public $errore_caricamento_array = array (
        UPLOAD_ERR_OK                       =>  "Non c'è alcun errore, il file è stato caricato con successo.",
        UPLOAD_ERR_INI_SIZE                 =>  "Il file caricato supera la direttiva upload_max_filesize in php.ini",
        UPLOAD_ERR_FORM_SIZE                =>  "Il file inviato eccede il MAX_FILE_SIZE che è stato specificato nel modulo HTML",
        UPLOAD_ERR_PARTIAL                  =>  "Il file è stato solo parzialmente caricato.",
        UPLOAD_ERR_NO_FILE                  =>  "Nessun file è stato caricato.",
        UPLOAD_ERR_NO_TMP_DIR               =>  "Manca una cartella temporanea. Introdotta in PHP 5.0.3",
        UPLOAD_ERR_CANT_WRITE               =>  "Impossibile scrivere file su disco. Introdotto in PHP 5.1.0.",
        UPLOAD_ERR_EXTENSION                =>  "Una estensione PHP ha interrotto il caricamento del file."
    );


    public function view_front_end()
    {

        return static::cerca_con_query("SELECT * FROM " . static::$tabella_db . " WHERE DATE(NOW()) >= eventi.data_inizio  AND DATE(NOW()) <= eventi.data_fine AND stars = 1 ORDER BY eventi.data_inizio ASC ");
    }
    public function view_front_end_ajax()
    {

//        return static::cerca_con_query("SELECT * FROM " . static::$tabella_db . " WHERE DATE(NOW()) >= eventi.data_inizio  AND DATE(NOW()) <= eventi.data_fine ORDER BY eventi.data_inizio ASC ");
        return static::cerca_con_query("SELECT * FROM " . static::$tabella_db . " WHERE DATE(NOW()) >= eventi.data_inizio  AND DATE(NOW()) <= eventi.data_fine AND stars = 0 ORDER BY eventi.data_inizio ASC ");
    }

    public function percorso_immagine_e_plecholder_front() {
        return empty($this->img_evento)? $this->immagine_plaecholder : $this->percorso_immagini.DS.$this->img_evento;
    }

    public function percorso_immagine_e_plecholder_back() {
        return empty($this->img_evento)? $this->immagine_plaecholder : "../".DS.$this->percorso_immagini.DS.$this->img_evento;
    }

    public function impostazione_file_eventi($file) {

        if(empty($file) || !$file || !is_array($file)){
            $this->errore[] = "Non c'è alcun file da caricare";
            return FALSE;
        } elseif ($file['error'] != 0 ) {
            $this->errore[] = $this->errore_caricamento_array[$file['error']];
            return FALSE;
        } else {
            $this->img_evento = basename($file['name']);
            $this->tmp_path = $file['tmp_name'];
            $this->tipo = $file['type'];
            $this->grandezza = $file['size'];
        }
    }

    public function caricamento_foto_eventi() {

        if(!empty($this->errore)){
            return FALSE;
        }

        if (empty($this->img_evento) || empty($this->tmp_path)) {
            $this->errore[] = "Il file non è utilizzabile";
            return FALSE;
        }

        $percorso_immagini = ROOT_SITO . DS . $this->percorso_immagini . DS. $this->img_evento;

        if(file_exists($percorso_immagini)){
            $this->errore[] = "Il file {$this->img_evento} già esiste";
            return FALSE;
        }

        if(move_uploaded_file($this->tmp_path, $percorso_immagini)){
            unset($this->tmp_path);
            return TRUE;
        } else {
            $this->errore[] = "Molto probabilmente non si ha il permesso di inserire il file nalla directory";
            return FALSE;
        }
    }

    public function conta_eventi() {
        global $database;

        $sql = "SELECT COUNT(*) FROM " . static::$tabella_db . " WHERE stars = 0 ";
        $risultato = $database->query($sql);
        $row = mysqli_fetch_array($risultato);

        return array_shift($row);
    }



} // FINE EVENTI