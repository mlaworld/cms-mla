<?php

/**
 * Created by PhpStorm.
 * User: giuseppe
 * Date: 10/03/17
 * Time: 11:20
 */
class PromoCentri  extends Db_oggetto
{
    protected static $tabella_db = "promo_centri";
    protected static $campi_tabella_db = array(
        'id_promo',
        'id_centro'
    );

    public $id;
    public $id_promo;
    public $id_centro;


    public function delete_per_id_promo($id_promo){

        global $database;

        $sql = "DELETE FROM " . static::$tabella_db. " ";
        $sql .= " WHERE id_promo = " . $database->escape($id_promo);

        $database->query($sql);

        return (mysqli_affected_rows($database->conn) == 1)? true : false ;

    }

}