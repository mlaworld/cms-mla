<?php

/**
 * Created da Giuseppe Alessandro De Blasio.
 * User: Giuseppe Alessandro De Blasio
 * Date: 10/05/17
 * Time: 10:26
 */
class CategoriePage extends Db_oggetto
{
    protected static $tabella_db = "categorie_page";
    protected static $campi_tabella_db = array
    (
        'titolo',
        'titBox1',
        'box1',
        'titBox2',
        'box2',
        'titBox3',
        'box3',
        'id_cat',
        'urlFriCat',
        'metaTitle',
        'metaDescription',
        'canonicalUrl',
        'metaRobots',
        'stato'
    );
    public $id;
    public $titolo;
    public $titBox1;
    public $box1;
    public $titBox2;
    public $box2;
    public $titBox3;
    public $box3;
    public $id_cat;
    public $urlFriCat;
    public $metaTitle;
    public $metaDescription;
    public $canonicalUrl;
    public $metaRobots;
    public $stato;



    public function seleziona_per_campo_categorie_front($valore = null)
    {
        global $databasePDO;
        $databasePDO->query("SELECT * FROM ". static::$tabella_db . " WHERE urlFriCat = :urlFriCat ");
        $databasePDO->bind(':urlFriCat', $valore, PDO::PARAM_STR);
//        $databasePDO->execute();
        return $databasePDO->resulsetObj();

    }



}