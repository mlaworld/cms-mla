<?php

/**
 * Created da Giuseppe Alessandro De Blasio.
 * User: Giuseppe Alessandro De Blasio
 * Date: 19/09/2016
 * Time: 11:26
 */

/*class templates {
    protected $dir;
    protected $vars;

  public function __construct($dir = '') {
    $this->dir = (strlen($dir) == 0 || substr($dir, -1) == '/' ? $dir : $dir . '/');
    $this->vars = array();
  }

  protected function __set($var, $value) {
    $this->vars[$var] = $value;
  }
 
  public function set_var() {
    $args = func_get_args();
    if (func_num_args() == 2) {
      $this->__set($args[0], $args[1]);
    }else{
      foreach ($args[0] as $var => $value) {
        $this->__set($var, $value);
      }
    }
  }

  public function get_var($var) {
    return $this->vars[$var];
  }

  public function is_set($var) {
    return isset($this->vars[$var]);
  }
  
  public function out($templates, $stringa = false) {
    ob_start();
    require $this->dir . $templates . '.php';
    $content = ob_get_clean();
    if ($stringa) return $content; else echo $content;
  }  

  
}// FINE CLASSE TEMPLATE */

class Template {
   

   //IMPOSTIAMO DELLE PROPRIETA'
   var $output; 
   var $template;
   //var $variabile = array();  
      
   //IL METODO CARICA TEMPLATE USA LA GET CONTENTS LEGGE I CONTENUTI 
   function carica($filepath) {

      $this->template = file_get_contents($filepath);

   }
   //SETTIAMO LE VARIBILI SONO UN ARRAY DI VALORI QUINDI FAREMO IL CICLO 
   function settaVariabili($array) {
         
     //$this->variabile = $array; 
     $nuovo = $this->template;   
         
     foreach($array as $chiave => $valore)
     {
        $nuovo = str_replace("{".$chiave."}", $valore, $nuovo);
     }  
     //QUESTO NON FA ALTRO CHE costruire il templates
     if (isset($this->output))
     {
              $this->output .= $nuovo;
     } else {
              $this->output  = $nuovo;
     }

   }
   //LO STAMPA E RIPULISCE L'OUTPUT RESETTA TUTTO ALLA FINE DELLA STAMPA
   function procedi() {
        print("$this->output\n");
        $this->output = "";
   }

}  // FINE CLASSE TEMPLATE