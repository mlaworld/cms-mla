<?php include 'includes/header.php'; ?>
<?php if(!$sessione->loggato()){ redirect('accedi.php');} ?>

<!-- Barra di Navigazione -->
<?php include ("includes/navbar.php") ?>

<?php include ("includes/aside.php") ?>
<?php
/**
 * Created by Giuseppe Alessandro De Blasio.
 * User: Giuseppe Alessandro De Blasio
 * Date: 06/03/17
 * Time: 17:02
 */
 ?>
<?php

$promo = Promo::seleziona_tutti();
$promoMod = PromoCentri::seleziona_tutti();


$nazioni = Nazioni::seleziona_tutti();
$centri = Centro::seleziona_tutti();
$ins_promo = new Promo();
$ins_ass_centri = new PromoCentri();


if (isset($_POST['creaPromo'])){
    global $database;
    $ins_promo->data_inserimento =  date('Y/m/d h:i:s');
    $ins_promo->link =  $_POST['href_url'];
    $ins_promo->titolo =  $_POST['titolo_promo'];
    $ins_promo->breve_desc =  $_POST['desc_promo'];
    $ins_promo->inizio_promo =  data_db($_POST['inizio_promo']);
    $ins_promo->scad_promo =  data_db($_POST['fine_promo']);
    $ins_promo->stato =  $_POST['stato_promo'];
    $ins_promo->provenienza = 'CMS MLA';
    $ins_promo->impostazione_file_promo($_FILES['img_promo']);
    $ins_promo->caricamento_foto_promo();
    $ins_promo->img_alt =  $_POST['img_alt_promo'];
    $ins_promo->salva();
    if(isset($_POST['arrCentro'])){
//        var_dump($_POST['arrCentro']);
        $ultimo_id = $ins_promo->id  = $database->insert_id();
            for($i = 0; $i < count($_POST['arrCentro']); $i++){
                $sql = "INSERT INTO promo_centri (id_promo, id_centro) ";
                $sql .= " VALUES ('{$ultimo_id}', '{$_POST['arrCentro'][$i]}' ) ";
                $database->query($sql);
            }
    }
    redirect('elenco_promo.php?agg=1');
}

if(isset($_GET['agg']) AND $_GET['agg'] == 1){
    successoMessaggio('Promozione Aggiunta Correttamente', 'Inserimento Promo Avvenuta con Successo');
}


if (isset($_POST['modPromo'])){
    global $database;

    $mod_promo = Promo::seleziona_per_id($_POST['idPromoMod']);
    $mod_promo->link = $_POST['href_url_mod'];
    $mod_promo->titolo =  $_POST['titolo_mod'];
    $mod_promo->breve_desc =  $_POST['desc_promo_mod'];
    $mod_promo->inizio_promo =  data_db($_POST['inizio_promo_mod']);
    $mod_promo->scad_promo =  data_db($_POST['fine_promo_mod']);
    $mod_promo->stato =  $_POST['stato_promo_mod'];
    $mod_promo->provenienza = 'CMS MLA';
    (!empty($_FILES['img_promo_mod']))? $mod_promo->impostazione_file_promo($_FILES['img_promo_mod']): NULL;
    $mod_promo->caricamento_foto_promo();
    $mod_promo->img_alt =  $_POST['img_alt_promo_mod'];
    $mod_promo->salva();
        if(isset($_POST['arrCentroMod'])){
            $mod_promo_centro = new PromoCentri();
            $mod_promo_centro->delete_per_id_promo($_POST['idPromoMod']);
            for($i = 0; $i < count($_POST['arrCentroMod']); $i++){
                $sql = "INSERT INTO promo_centri (id_promo, id_centro) ";
                $sql .= " VALUES ('{$_POST['idPromoMod']}', '{$_POST['arrCentroMod'][$i]}' ) ";
                $database->query($sql);
            }

        }

    redirect('elenco_promo.php?mod=1');
}

if(isset($_GET['mod']) AND $_GET['mod'] == 1){
    successoMessaggio('Promozione Modificata Correttamente', 'Modifica Promo Avvenuta con Successo');
}


?>

<section class="main-content">
    <br>
    <button type="button" class="btn btn-labeled btn-primary pull-right" data-toggle="modal" data-target="#offertaModal">
               <span class="btn-label"><i class="fa fa-plus-circle"></i></span>Aggiungi Promo
    </button>

    <h1 class="page-header">Elenco Promozioni</h1>

    <div class="panel panel-default">
        <div class="panel-heading">Elenco Promozioni
<!--            <a title="" class="pull-right" href="javascript:void(0);" data-original-title="Collapse Panel" data-toggle="tooltip" data-perform="panel-collapse">-->
<!--                <em class="fa fa-minus"></em>-->
<!--            </a>-->
        </div> <!-- panel-heading -->


<!--            <div class="panel-wrapper collapse in" style="height: auto;">-->
        <div class="panel-body">
                <div class="table-responsive">
                    <hr>
                        <div class="form-group">
                            <label class="control-label">Filtra Stato
                            <div class="col-sm-10">
                                <label class="checkbox-inline c-checkbox">
                                    <input onchange="filtroStato()" name="stato" type="checkbox" value="Attivo">
                                    <span class="fa fa-check"></span>Attivo</label>
                                <label class="checkbox-inline c-checkbox">
                                    <input onchange="filtroStato()" name="stato" type="checkbox" value="Da Lavorare">
                                    <span class="fa fa-check"></span>Da Lavorare</label>
                                <label class="checkbox-inline c-checkbox">
                                    <input  onchange="filtroStato()" name="stato" type="checkbox" value="Chiuso">
                                    <span class="fa fa-check"></span>Chiuso</label>
                            </div>
                            </label>
                        </div>
                    <hr>
                    <br>
                    </div>
                    <table id="tabPromo" class="table table-bordered table-hover">
                        <thead>
                            <tr>
                                <th>#id</th>
                                <th>Data Inserimento</th>
                                <th>Titolo</th>
                                <th>Breve Descrizione</th>
                                <th>Inizio Promo</th>
                                <th>Scadenza Promo</th>
                                <th>Stato</th>
                                <th>Provenienza</th>
                                <th>Modifica</th>
                                <th>Elimina</th>
                            </tr>
                        </thead>
                        <tbody>

                        <?php foreach ($promo as $promozione): ?>
                        <tr>
                            <td><?php echo $promozione->id; ?></td>
                            <td><?php echo datatime_it($promozione->data_inserimento); ?></td>
                            <td><?php echo $promozione->titolo; ?></td>
                            <td><?php echo $promozione->breve_desc; ?></td>
                            <td><?php echo data_it($promozione->inizio_promo); ?></td>
                            <td><?php echo data_it($promozione->scad_promo); ?></td>
                            <td>
                                <?php
                                switch ( $promozione->stato){
                                    case "Attivo":
                                        echo '<div class="label label-success pull-center" value="'.$promozione->stato.'">'. $promozione->stato .'</div><input type="hidden" name="stato" value="'.$promozione->stato.'" >';
                                        break;
                                    case "Da Lavorare":
                                        echo '<div class="label label-warning pull-center" value="'.$promozione->stato.'">'. $promozione->stato .'</div><input type="hidden" name="stato" value="'.$promozione->stato.'" >';
                                        break;
                                    case "Chiuso":
                                        echo '<div class="label label-danger pull-center" value="'.$promozione->stato.'">'. $promozione->stato .'</div><input type="hidden" name="stato" value="'.$promozione->stato.'" >';
                                        break;
                                }
                                ?>
                            </td>
                            <td><?php echo $promozione->provenienza; ?></td>

                            <td>
                                <a type="button" class="modPromo" href="#"
                                   data-toggle="modal"
                                   data-record-id="<?php echo $promozione->id; ?>"
                                   data-url-mod="<?php echo $promozione->link; ?>"
                                   data-titolo="<?php echo $promozione->titolo; ?>"
                                   data-breve-desc="<?php echo $promozione->breve_desc; ?>"
                                   data-inizio-promo="<?php echo $promozione->inizio_promo; ?>"
                                   data-fine-promo="<?php echo $promozione->scad_promo; ?>"
                                   data-stato="<?php echo $promozione->stato; ?>"
                                   data-alt-image="<?php echo $promozione->img_alt; ?>"
                                   data-centri=""
                                   data-url-ajax="includes/api.php/promo/<?php echo $promozione->id; ?>"
                                   data-target="#modOffertaModal">
                                    <i class="fa fa-edit"></i>Modifica
                                </a>
                            </td>
                            <td>
                                <a href="#" class="delPromoId" data-record-id="<?php echo $promozione->id; ?>"  data-record-titolo="<?php echo $promozione->titolo; ?>"><i style="color: red;" class="fa fa-trash-o"> Elimina</i></a>
                            </td>
                        </tr>
                        <?php endforeach; ?>
                        </tbody>
                    </table>
<!--                    <div class="panel-footer">-->
<!--                        <div class="row">-->
<!---->
<!--                        </div>-->
                    </div><!-- panel-footer -->
                </div><!-- table-responsive -->
            <!-- </div><!-- panel-wrapper collapse in -->
<!--    </div> <!-- panel panel-default -->



    <?php




    ?>
</section>
<!-- Modal Inserimento Offerta -->
<div class="modal fade" id="offertaModal" tabindex="-1" role="dialog" aria-labelledby="offertaModalLabel">
    <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Inserisci Nuova Promo</h4>
                </div>
                <form action="" method="post" enctype="multipart/form-data" data-parsley-validate>
                <div class="modal-body">
                    <div class="tabbable">
                        <ul class="nav nav-tabs">
                            <li class="active"><a href="#tab1" data-toggle="tab">Dati Generali</a></li>
                            <li><a href="#tab2" data-toggle="tab">Seleziona Centri</a></li>
                        </ul>
                            <div class="tab-content">
                                <div class="tab-pane active" id="tab1">
                                    <div class="row">
                                        <div class="form-group col-md-6">
                                            <label class="" for="">Data Attuale</label>
                                            <?php $dataAttuale = date ("d/m/Y");?>
                                            <div data-pick-time="false" class="input-group date mb-lg">
                                                <input type="text" name="promo_data_attuale" value="<?php echo $dataAttuale; ?>" disabled class="form-control" required>
                                                <span class="input-group-addon"><span class="fa-calendar fa"></span></span>
                                            </div>
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label class="" for="">URL</label>
                                            <input class="col-md-6 form-control" value="http://www.mlaworld.com/" type="text" name="href_url" required>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="form-group col-md-6">
                                            <label class="" for="">Titolo</label>
                                            <input class="col-md-6 form-control" type="text" name="titolo_promo" required>
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label class="" for="">Breve Descrizione</label>
                                            <textarea class="col-md-6 form-control" name="desc_promo" cols="4" required></textarea>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="form-group col-md-6">
                                            <label class="" for="">Data Inizio Promo</label>
                                                <div id="promoInizio" class="input-append date mb-lg">
                                                    <input data-format="dd/MM/yyyy" name="inizio_promo" type="text" class="form-control" required>
                                                    <span class="add-on">
<!--                                                        <span class="fa-calendar fa"></span>-->
                                                    </span>
                                                </div>
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label class="" for="">Data Fine Promo</label>
                                            <div id="promoFine" class="input-append date">
                                                <input data-format="dd/MM/yyyy" name="fine_promo" type="text" class="form-control" required>
                                                <span class="add-on"></span>
                                            </div>
                                        </div>
                                    </div>
                                        <div class="row">
                                            <div class="form-group col-md-4">
                                                <label class="" for="">Seleziona Stato</label>
                                                <select name="stato_promo" id="" class="form-control" required>
                                                    <option value="Da Lavorare">Da Lavorare</option>
                                                    <option value="Attivo">Attivo</option>
                                                </select>
                                            </div>
                                            <div class="form-group col-md-4">
                                                <label class="" for="">Seleziona Immagine</label>
                                                <input type="file" name="img_promo" class="form-control" required>
                                            </div>
                                            <div class="form-group col-md-4">
                                                <label class="" for="">Alt Immagine</label>
                                                <input type="text" name="img_alt_promo" class="form-control" required>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="form-group col-md-12">
<!--                                            <button class="btn btn-primary pull-right" name="caratPromo" type="submit">Invia Promo</button>-->
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane" id="tab2">
                                    <div class="panel panel-default">
                                        <div class="panel-heading">Elenco Centri
<!--                                            <a href="javascript:void(0);" data-perform="panel-dismiss" data-toggle="tooltip" title="" class="pull-right" data-original-title="Close Panel">-->
<!--                                                <em class="fa fa-times"></em>-->
<!--                                            </a>-->
<!--                                            <a href="javascript:void(0);" data-perform="panel-collapse" data-toggle="tooltip" title="" class="pull-right" data-original-title="Collapse Panel">-->
<!--                                                <em class="fa fa-minus"></em>-->
<!--                                            </a>-->
                                        </div>
                                        <!-- START table-responsive-->
                                        <div class="panel-wrapper collapse in" aria-expanded="true"><div class="table-responsive">
                                                <table id="tabellaCentri" class="table table-striped table-bordered table-hover">
                                                    <thead>
                                                    <tr>
                                                        <th style="width: 5%" class="check-all">
                                                            <div data-toggle="tooltip" data-title="Check All" class="checkbox c-checkbox" data-original-title="" title="">
                                                                <label>
                                                                    <input type="checkbox">
                                                                    <span class="fa fa-check"></span>
                                                                </label>
                                                            </div>
                                                        </th>
                                                        <th>Centro</th>
                                                        <th>Nazione</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    <?php foreach ($centri as $centro): ?>
                                                    <tr>
                                                        <td>
                                                            <div class="checkbox c-checkbox">
                                                                <label>
                                                                    <input name="arrCentro[]" value="<?php echo $centro->id; ?>" type="checkbox">
                                                                    <span class="fa fa-check"></span>
                                                                </label>
                                                            </div>
                                                        </td>
                                                        <td>
                                                            <?php echo $centro->Centro; ?>
                                                        </td>
                                                        <td>
                                                            <?php $nazione = Nazioni::seleziona_per_id($centro->id_nazione); echo $nazione->nazione;   ?>
                                                        </td>
                                                    </tr>
                                                    <?php endforeach; ?>
                                                    </tbody>
                                                </table>
                                            </div>
                                            <div class="panel-footer">
                                                <div class="row">
                                                    <div class="form-group">
                                                        <div class="col-md-9"></div>
                                                        <div class="col-md-3">
<!--                                                            <button class="btn btn-primary pull-right" name="caratPromo" type="submit">Invia Selezionati</button>-->
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- END table-responsive-->

                                    </div>
                            </div>
                    </div>

                </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Chiudi</button>
                        <button type="submit" name="creaPromo" class="btn btn-primary">Inserisci Nuova Promo</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>


<!-- Modale Modifica Offerta -->
<div class="modal fade" id="modOffertaModal"  tabindex="-1" role="dialog" aria-labelledby="modOffertaModalLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Modifica Promo</h4>
            </div>
            <form action="" method="post" enctype="multipart/form-data" data-parsley-validate>
                <div class="modal-body">
                    <div class="fetched-data"></div>
                    <div class="tabbable">
                        <ul class="nav nav-tabs">
                            <li class="active"><a href="#tab3" data-toggle="tab">Dati Generali</a></li>
                            <li><a href="#tab4" data-toggle="tab">Seleziona Centri</a></li>
                        </ul>
                        <div class="tab-content">
                            <div class="tab-pane active" id="tab3">
                                <div class="row">
                                    <div class="form-group col-md-6">
<!--                                        <label class="" for="">Data Attuale</label-->
<!--                                        --><?php ////$dataAttuale = date ("d/m/Y");?>
<!--                                        <div data-pick-time="false" class="input-group date mb-lg">-->
<!--                                            <input type="text" name="promo_data_attuale" value="--><?php ////echo $dataAttuale; ?><!--" disabled class="form-control" required>-->
<!--                                            <span class="input-group-addon"><span class="fa-calendar fa"></span></span>-->
<!--                                        </div>-->
                                        <input class="col-md-6 form-control" type="hidden" name="idPromoMod" id="idPromoMod" value="">
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label class="" for="">URL</label>
                                        <input class="col-md-6 form-control href_url_mod"  id="href_url_mod" value="" type="text" name="href_url_mod" required>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-md-6">
                                        <label class="" for="">Titolo</label>
                                        <input class="col-md-6 form-control" type="text" id="titolo_mod" name="titolo_mod" required>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label class="" for="">Breve Descrizione</label>
                                        <textarea class="col-md-6 form-control" id="desc_promo_mod" name="desc_promo_mod" cols="4" required></textarea>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-md-6">
                                        <label class="" for="">Data Inizio Promo</label>
                                        <div id="promoInizioMod" class="input-append date mb-lg">
                                            <input data-format="dd/MM/yyyy" name="inizio_promo_mod" id="inizio_promo_mod" type="text" class="form-control" required>
                                            <span class="add-on">
                                                    </span>
                                        </div>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label class="" for="">Data Fine Promo</label>
                                        <div id="promoFineMod" class="input-append date">
                                            <input data-format="dd/MM/yyyy" name="fine_promo_mod" id="fine_promo_mod" type="text" class="form-control" required>
                                            <span class="add-on"></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-md-4">
                                        <label class="" for="">Seleziona Stato</label>
                                        <select name="stato_promo_mod" id="stato_promo_mod" class="form-control" required>
                                            <option value="Da Lavorare">Da Lavorare</option>
                                            <option value="Attivo">Attivo</option>
                                            <option value="Chiuso">Chiuso</option>
                                        </select>
                                    </div>
                                    <div class="form-group col-md-4">
                                        <label class="" for="">Seleziona Immagine</label>
                                        <input type="file" name="img_promo_mod" id="img_promo_mod" class="form-control">
                                    </div>
                                    <div class="form-group col-md-4">
                                        <label class="" for="">Alt Immagine</label>
                                        <input type="text" name="img_alt_promo_mod" id="img_alt_promo_mod" class="form-control" required>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-md-12">
                                        <!--                                            <button class="btn btn-primary pull-right" name="caratPromo" type="submit">Invia Promo</button>-->
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane" id="tab4">
                                <div class="panel panel-default">
                                    <div class="panel-heading">Elenco Centri
                                    </div>
                                    <!-- START table-responsive-->
                                    <div class="panel-wrapper collapse in" aria-expanded="true"><div class="table-responsive">
                                            <div class="tebExample">

                                            </div>
                                        <div class="panel-footer">
                                            <div class="row">
                                                <div class="form-group">
                                                    <div class="col-md-9"></div>
                                                    <div class="col-md-3">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- END table-responsive-->

                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Chiudi</button>
                        <button type="submit" name="modPromo" class="btn btn-primary">Aggiorna Promo</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>






<?php include 'includes/footer.php'; ?>


<script>

    $(document).ready(function(){
//        $('#modOffertaModal').on('show.bs.modal', function (e) {

//            e.preventDefault();



//            var promoId = $(e.relatedTarget).data('recordId');
//            $(e.currentTarget).find('input[name="idPromoMod"]').val(promoId) ;
//
//            var urlMod =  $(e.relatedTarget).data('urlMod');
//            $(e.currentTarget).find('input[name="href_url_mod"]').val(urlMod);
//
//            var titolo = $(e.relatedTarget).data('titolo');
//            $(e.currentTarget).find('input[name="titolo_mod"]').val(titolo);
//
//            var descPromoMod = $(e.relatedTarget).data('breveDesc');
//            $(e.currentTarget).find('textarea[name="desc_promo_mod"]').val(descPromoMod);
//
//            var dataInizio = $(e.relatedTarget).data('inizioPromo');
//            var d = dataInizio.slice(0, 10).split('-');
//            dataInizioMod = d[2] +'/'+ d[1] +'/'+ d[0];
//            $(e.currentTarget).find('input[name="inizio_promo_mod"]').val(dataInizioMod);
//
//            var dataFine = $(e.relatedTarget).data('finePromo');
//            var d = dataFine.slice(0, 10).split('-');
//            dataFineMod = d[2] +'/'+ d[1] +'/'+ d[0];
//            $(e.currentTarget).find('input[name="fine_promo_mod"]').val(dataFineMod);
//
//            var stato = $(e.relatedTarget).data('stato');
//            $(e.currentTarget).find('input[name="stato_promo_mod"]').val(stato);
//            $("select option").filter(function() {
//                return $(this).text() == stato;
//            }).prop('selected', true);
//
//            var imgAlt = $(e.relatedTarget).data('altImage');
//            $(e.currentTarget).find('input[name="img_alt_promo_mod"]').val(imgAlt);



            $('.modPromo').on('click', function (e) {
                e.preventDefault();
                $('.main-content').addClass('csspinner');

                 var promoId  = $(this).data('recordId');
                 var href =  $(this).data('urlAjax');

                $.ajax({
                   type: 'GET',
                    url: href,
                headers: {
                    'Content-type': 'application/json',
                    'Access-Control-Allow-Headers': 'Content-Type',
                    'Access-Control-Allow-Origin': '*',
                    'X-XSRF-TOKEN': '<?php echo (isset($_COOKIE['XSRF-TOKEN']))? $_COOKIE['XSRF-TOKEN'] : ''?>'
                   },
               dataType: 'json',
                  async: true,
                    success: function (data) {
                        $('.main-content').removeClass('csspinner');
                        $('#idPromoMod').val(data.id);
                        $('#href_url_mod').val(data.link);
                        $('#titolo_mod').val(data.titolo);
                        $('#desc_promo_mod').val(data.breve_desc);
                        var d = data.inizio_promo.slice(0, 10).split('-');
                        dataInizioMod = d[2] +'/'+ d[1] +'/'+ d[0];
                        $('#inizio_promo_mod').val(dataInizioMod);
                        var d = data.scad_promo.slice(0, 10).split('-');
                        dataFineMod = d[2] +'/'+ d[1] +'/'+ d[0];
                        $('#fine_promo_mod').val(dataFineMod);
                        var stato = $('#stato_promo_mod').val(data.stato);
                        $("select option").filter(function() {
                             return $(this).text() == stato;
                         }).prop('selected', true);
                        $('#img_alt_promo_mod').val(data.img_alt);
                    }
                });
//            });


            tabella_centri_mod(promoId);

            function tabella_centri_mod(promoId) {

                $.ajax({
                    type: 'POST',
                    url: 'modifica_promo.php',
                    data: {promoId: promoId},
                    async: true,
                    dataType: 'HTML',
                    success: function (mostra_tabella_offerta) {

                        $('.main-content').removeClass('csspinner');
                        if (!mostra_tabella_offerta.error) {
                            $(".tebExample").html(mostra_tabella_offerta);
                        }
                    }
                });
            }
            $('#promoInizioMod').datetimepicker({
                language: 'it',
                pickTime: false
            });

            $('#promoFineMod').datetimepicker({
                language: 'it',
                pickTime: false
            });

        });
    });




    var TabPromo =  $('#tabPromo').dataTable({
            "language": {
            "url": "js/datatables/traduzione/Italian.json"
        }
    });

    function filtroStato() {
        var stato= $('input:checkbox[name="stato"]:checked').map(function() {
            return '^' + this.value + '\$';
        }).get().join('|');
        // variabile e valore column array
        TabPromo.fnFilter(stato, 6, true, false, false, false);
    }

    var TabCentri = $('#tabellaCentri').dataTable({
        "language": {
            "url": "js/datatables/traduzione/Italian.json"
        },
        columnDefs: [{
            orderable: false,
//            className: 'select-checkbox',
            targets:   0
        }],
        select: {
//            style:    'os',
            selector: 'td:first-child'
        },
        order: [[ 1, 'asc' ]]
    });


    $('#promoInizio').datetimepicker({
        language: 'it',
        pickTime: false
    });

    $('#promoFine').datetimepicker({
        language: 'it',
        pickTime: false
    });


 $(document).ready(function () {

    $('.delPromoId').on('click', function () {

        var titolo = $(this).attr('data-record-titolo');

        var id = $(this).attr('data-record-id');

            swal({
                title: 'Eliminazione ' + titolo,
                text: titolo + ' sta per essere eliminato.',
                type: 'warning',
                showCancelButton: true,
                cancelButtonText: 'Annulla!',
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Sei sicuro di voler Cancellare ' + titolo + '?!'
            }).then(function () {
                swal(
                    'Cancellato!',
                    titolo + ' è stato cancellato con Successo.',
                    'success'
                );
                $.get("elimina_promo.php", {id : id});
                $('.swal2-confirm').hide();
                setTimeout(function () {
                    window.location = 'elenco_promo.php';
                }, 2000);
            });
        });
    });

</script>

