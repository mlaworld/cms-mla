<?php include 'includes/header.php'; ?>
<?php include 'includes/modale_foto_utenti.php'; ?>
<?php include 'includes/footer.php'; ?>

<?php if(!$sessione->loggato()){ redirect('accedi.php');} ?>

        <!-- Barra di Navigazione -->
        <?php include ("includes/navbar.php") ?>
        
        <?php include ("includes/aside.php") ?>
        <!-- End aside-->
      
 <?php include ("includes/aside_utenti.php") ?> 
        
    <?php
    
    $articoli = Articoli::seleziona_tutti();
    
    $pagina = !empty($_GET['pagina']) ? (int)$_GET['pagina'] : 1;
    
    $elementi_per_pagina = 10;
    
    $elementi_totali = Articoli::conta_elementi();
    
    $paginazione = new Paginazione($pagina, $elementi_per_pagina, $elementi_totali);
    
    $sql = "SELECT * FROM articoli ";
    $sql.= "LIMIT {$elementi_per_pagina} ";
    $sql.= "OFFSET {$paginazione->offset()}";
    $articoli = Articoli::cerca_con_query($sql)
    
    
    

?>    
        
    <?php

 
    
    if(isset($_POST['cerca_frase'])){
        $cerca = $_POST['cerca'];
        if(!empty($cerca)){
            $sql = "SELECT * FROM articoli ";
            $sql.= " WHERE titolo LIKE '%$cerca%' ";
            $sql.=" OR utente LIKE '%$cerca%' OR contenuto LIKE '%$cerca%' ";
            $articoli = Articoli::cerca_con_query($sql);
        } else {
            redirect('elenco_articoli.php');
        }
    }
    
    /*if(isset($_POST['Azione'])){
        redirect('modifica_articolo.php?id={'.$articolo->id.'}');
    }
    
    if(isset($_POST['applica'])){ 
        if($_POST['azione'] == "Modifica" && $_POST['checkbox_array[4]'] == "Modifica"){
            redirect("modifica_articolo.php?id={$_POST['checkbox_array[]']}");
        }
    }*/
    
    
if(isset($_POST['checkbox_array'])) {

    
foreach($_POST['checkbox_array'] as $articolo->id ){
        
  $bulk_opzione = $_POST['azione'];
        
 switch($bulk_opzione) {
 
 case 'Pubblicato':
 
global $database;
     
$query = "UPDATE articoli SET stato = '{$bulk_opzione}' WHERE id = {$articolo->id}  ";
        
$pubblicato = mysqli_query($database->conn, $query);
            
$database->conferma_query( $pubblicato);

redirect('elenco_articoli.php');

$sessione->messaggio('<div data-toggle="notify" data-onload data-message="L\'articolo è stato <b>Pubblicato</b> Correttamente" data-options="{&quot;status&quot;:&quot;success&quot;, &quot;pos&quot;:&quot;bottom-right&quot;}" class="hidden-xs"></div>');
            
         break;
            
            
case 'Bozza':
        
global $database;
    
$query = "UPDATE articoli SET stato = '{$bulk_opzione}' WHERE id = {$articolo->id}  ";
        
$bozza = mysqli_query($database->conn, $query);
            
$database->conferma_query($bozza);

redirect('elenco_articoli.php');

$sessione->messaggio('<div data-toggle="notify" data-onload data-message="L\'articolo è stato posto in <b>Bozza</b>" data-options="{&quot;status&quot;:&quot;success&quot;, &quot;pos&quot;:&quot;bottom-right&quot;}" class="hidden-xs"></div>');
            
         break;
     
case 'Modifica':     
     redirect("modifica_articolo.php?id=$articolo->id");
     
    break;


case 'Cancella':
  
        redirect("cancella_articolo.php?id=$articolo->id");
      
    break;
  
        }
    
    
    } 

}
    
    
    ?>    
        
        
     
     <br>
     <section class="main-content">    
        <div class="panel panel-default">
               <div class="panel-heading">Elenco Articoli
                  <!--<a title="" class="pull-right" href="javascript:void(0);" data-original-title="Close Panel" data-toggle="tooltip" data-perform="panel-dismiss">
                     <em class="fa fa-times"></em>
                  </a>-->
                  <a title="" class="pull-right" href="javascript:void(0);" data-original-title="Collapse Panel" data-toggle="tooltip" data-perform="panel-collapse">
                     <em class="fa fa-minus"></em>
                  </a>
               </div>
               <!-- START table-responsive-->
               <form action="" method="POST"> 
               <div class="panel-wrapper collapse in" style="height: auto;"><div class="table-responsive">
                    <table class="table table-bordered table-hover">
                     <thead>
                        <tr>
                            <th>#</th>
                            <th>Utente</th>
                            <th>Titolo</th>
                            <th>Categoria</th>
                            <th>Stato</th>
                            <th>Immagine</th>
                            <th>Tags</th>
                            <th>Commenti</th>
                            <th>Data</th>
                            <th>Vedi Articolo</th>
                            <th>Visualizzazioni</th>
                           <th class="check-all" style="width: 5%">
                              <div title=""  class="checkbox c-checkbox" data-original-title="" data-toggle="tooltip" data-title="Check All">
                                 <label>
                                    <input type="checkbox">
                                    <span class="fa fa-check"></span>
                                 </label>
                              </div>
                           </th>
                        </tr>
                     </thead>
                     <tbody>
                         
                         <?php foreach ($articoli as $articolo): ?>
                        <tr>
                            <td><?php echo $articolo->id; ?></td>
                            <td><?php echo $articolo->utente; ?></td>
                            <td><?php echo $articolo->titolo; ?></td>
                            <td><?php echo $articolo->categoria_id; ?></td>
                            <td><?php echo $articolo->stato; ?></td>
                            <td><img class="text-center img-responsive" style="width: 300px; height: 150px; " src="<?php echo $articolo->percorso_immagine_e_plecholder(); ?>" title="<?php $articolo->immagine_articolo ?>"></td>
                            <td><?php echo $articolo->tags; ?></td>
                            <td><a class="text-center" href="commenti.php?id=<?php echo $articolo->id;?>"><?php echo Commento::conta_commenti($articolo->id);?></a></td>
                            <td><?php echo data_it($articolo->data); ?></td>
                            <td><?php echo '<a href="../articolo.php?id='.$articolo->id.'&titolo='.rewriteUrl($articolo->titolo).'">'. $articolo->titolo.'</a>' ?></td>
                            <td><?php echo $articolo->visualizzazioni; ?></td>
                           <td>
                              <div class="checkbox c-checkbox">
                                 <label>
                                     <input name="checkbox_array[]" type="checkbox" value="<?php echo $articolo->id; ?>">
                                    <span class="fa fa-check"></span>
                                 </label>
                              </div>
                           </td>
                        </tr>
                        <?php endforeach; ?>
                        

                     </tbody>
                  </table>
               </div>
                   
                 <div class="panel-footer">
                  <div class="row">
                     <div class="col-lg-2">
                        <div class="input-group">
                            <script>
                               /* $(document).ready(function(){
                                    $('#cerca').keyup(function(){
                                                     console.log('sto cercando');
                                       var cerca = $('#cerca').val();
                                        
                                        $.ajax({
                                            url: 'elenco_articoli.php',
                                            data: {cerca : cerca},
                                            type: 'POST',
                                            success: function(data){
                                                if(!data.error){
                                                    $('#risultato').html(data);
                                                }
                                            }
                                        });
                                    });
                                });*/
                            
                            </script>
                           <input class="input-sm form-control" type="text" name="cerca" id="cerca" placeholder="Cerca">
                           <span class="input-group-btn">
                               <button class="btn btn-sm btn-default" type="submit" name="cerca_frase" id="cerca_frase">Cerca</button>
                           </span>
                        </div>
                     </div>
                      <div class="col-lg-8">
                              <ul class="pagination pull-right">
                                  <?php
                                  if($paginazione->pagine_totali() > 1){
                                      if($paginazione->e_prima()){
                                          echo "<li><a href='elenco_articoli.php?pagina={$paginazione->prima()}'>Prima</a></li>";
                                      }
                                      for ($i = 1; $i <= $paginazione->pagine_totali(); $i++){
                                          if($i == $paginazione->pagina_corrente){
                                              echo "<li class='active'><a href='elenco_articoli.php?pagina={$i}'>{$i}</a></li>";
                                          } else {
                                              echo "<li><a href='elenco_articoli.php?pagina={$i}'>{$i}</a></li>";
                                          }
                                      }

                                      if($paginazione->e_dopo()){ 
                                          echo "<li><a href='elenco_articoli.php?pagina={$paginazione->dopo()}'>Dopo</a></li>";
                                      }                                      
                                  }
                                  
                                  ?>
                              </ul>
                      </div>
                     <div class="col-lg-2">
                        <div class="input-group pull-right">
                            <?php $azioni = array(
                                0 => "Azione",
                                1 => "Pubblicato",
                                2 => "Bozza",
                                3 => "Cancella",
                                4 => "Modifica"
                            ); ?> 
                           <select name="azione" class="input-sm form-control">
                               <?php foreach ($azioni as $key => $azione): ?>
                               <option <?php if(isset($azione) && $azione == "Cancella") echo 'class="cancella_messaggio"'; ?> value="<?php echo $azione ?>"><?php echo $azione ?></option>
                               <?php endforeach; ?>
                           </select>
                           <span class="input-group-btn">
                              <button name="applica" class="btn btn-sm btn-default">Applica</button>
                           </span>
                        </div>
                     </div>
                  </div>
               </div>
             </div>
            </form>  
               <!-- END table-responsive-->
            </div>
     </section>
                            
<?php include 'includes/footer.php'; ?>