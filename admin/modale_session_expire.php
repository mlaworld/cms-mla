<?php
/**
 * Created by PhpStorm.
 * User: Massimo Tardi
 * Date: 29/09/2016
 * Time: 17:06
 */


?>


<div class="modal fade" id="logout_popup" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <div style="width:100%;height:100%;margin: 0px; padding:0px">
                    <div style="width:25%;margin: 0px; padding:0px;float:left;">
                        <i class="fa fa-warning" style="font-size: 140px;color:#da4f49"></i>
                    </div>
                    <div style="width:70%;margin: 0px; padding:0px;float:right;padding-top: 10px;padding-left: 3%;">
                        <h4>La sessione sta per scadere!</h4>
                        <p style="font-size: 15px;">Verrà effetuata in  <span id="timer" style="display: inline;font-size: 30px;font-style: bold">10</span> secondi.</p>
                        <p style="font-size: 15px;">Vuoi rimanere ancora loggato?</p>
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>
            <div style="margin-left: 30%;margin-bottom: 20px;margin-top: 20px;">
                <a href="javascript:;" onclick="resetTimer()" class="btn btn-primary" aria-hidden="true">Si, Voglio Accedere nuovamente</a>
                <a href="<?php echo ROOT_SITO.'esci.php';?>" class="btn btn-danger" aria-hidden="true">No, Voglio Rimanere Disconnesso</a>
            </div>
        </div>
    </div>
</div>
