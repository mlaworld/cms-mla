<?php
/**
 * Created da Giuseppe Alessandro De Blasio.
 * User: Giuseppe Alessandro De Blasio
 * Date: 30/09/2016
 * Time: 15:33
 */
?>
<?php include 'includes/header.php'; ?>

<?php if (!$sessione->loggato()) {
    redirect('accedi.php');
} ?>

<!-- Barra di Navigazione -->
<?php include("includes/navbar.php") ?>

<?php include("includes/aside.php") ?>
<!-- End aside-->

<?php $cataloghi = Cataloghi::seleziona_tutti_per_pos(); ?>

<?php
$catalogo = new Cataloghi();

if (isset($_POST['inserisci_catalogo'])) {
    if ($catalogo) {
        $catalogo->nome_catalogo = $_POST['catalogo'];
        $catalogo->anno = $_POST['anno'];
        $catalogo->breve_desc = $_POST['desc_cat'];
        $catalogo->link = $_POST['url_cat'];
        if(isset($_POST['stato'])){
            $catalogo->stato = $_POST['stato'] = 1;
        }
        $catalogo->posizione = $_POST['posizione'];
        $catalogo->impostazione_file_catalogo($_FILES['file']);
        $catalogo->caricamento_foto_catalogo();
        $catalogo->salva();
//        var_dump($catalogo);
//        echo '<div data-toggle="notify" data-onload data-message="Catalogo <b>Aggiunto</b> Correttamente" data-options="{&quot;status&quot;:&quot;success&quot;, &quot;pos&quot;:&quot;bottom-right&quot;}" class="hidden-xs"></div>';
        redirect('cataloghi.php?inserimento=1');

    }
}

if ( isset($_GET['inserimento']) && $_GET['inserimento'] == 1 ) {
    $msg_instant->success('Città <b>Aggiunto</b> correttamente');
    $msg_instant->display();
}

/* ELIMINO CATALOGHI */
if (isset($_POST['elimina'])) {
    redirect("elimina_cataloghi.php?id={$_POST['elimina']}");

}

?>

<br>

<section class="main-content">

    <h1 class="page-header">Cataloghi</h1>

    <div class="col-md-4">
        <div class="panel panel-default">
            <div class="panel-heading">Aggiungi Nuovo Catalogo</div>
            <div class="panel-body">
                <form role="form" method="POST" action="" enctype="multipart/form-data" data-parsley-validate>
                    <div class="form-group">
                        <label>Inserisci Nome Catalogo</label>
                        <input type="text" name="catalogo" placeholder="Inserisci Nome Catalogo"  class="form-control" required>
                    </div>
                    <div class="form-group">
                        <label>Inserisci Breve Descrizione Catalogo</label>
                        <textarea name="desc_cat" cols="3" placeholder="Inserisci breve descrizione Catalogo"  class="form-control" required></textarea>
                    </div>
                    <div class="form-group">
                        <label>Link URL</label>
                        <input type="text" name="url_cat" placeholder="Inserisci es. /vacanze-studio" value="" class="form-control" required>
                    </div>
                    <div class="form-group">
                        <label>Immagine</label>
                        <input type="file" name="file" placeholder="Inserisci breve descrizione Catalogo"  class="form-control" required>
                    </div>
                    <div class="form-group">
                        <label>Posizione Catalogo Front-End</label>
                        <select name="posizione" class="form-control" required>
                            <?php for($i=1; $i<=100; $i++): ?>
                            <option value="<?php echo $i ?>" required><?php echo $i ?></option>
                            <?php endfor; ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="anno">Anno</label>
                        <input type="text" name="anno" placeholder="Inserisci Anno Catalogo" class="form-control" required/>
                    </div>
                    <div class="form-group">
                        <label for="anno">Stato</label>
                        <br>
                        <td>
                            <label class='switch text-center'>
                                    <input type='checkbox' name='stato' checked='' value='1' >
                                    <span></span>
                             </label>
                        </td>
                    </div>
                    <button type="submit" name="inserisci_catalogo" class="btn btn-sm btn-primary pull-right">Inserisci</button>
                </form>
            </div>
        </div>
    </div>

<!--    <div class="col-md-2"></div>-->

    <div class="col-md-8">
        <div class="panel panel-default">
            <div class="panel-heading">Cataloghi Presenti</div>
            <div class="panel-body">
                <!-- START table-responsive-->
                <div class="table-responsive">
                    <table id="tabCataloghi" class="table table-striped table-bordered table-hover">
                        <thead>
                        <tr>
                            <th>Posizione</th>
<!--                            <th>#id</th>-->
                            <th>Immagine</th>
                            <th>Nome Catalogo</th>
                            <th>Stato</th>
                            <th>Anno</th>
                            <th>Modifica</th>
                            <th>Elimina</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php foreach ($cataloghi as $catalogo): ?>
                            <tr>
                                <td><?php echo $catalogo->posizione; ?></td>
                                <td class="text-center"><img src="<?php echo  $catalogo->percorso_immagine_e_plecholder_front(); ?>" width="50px" alt=""></td>
                                <td><?php echo $catalogo->nome_catalogo; ?></td>
                                <td><?php echo ($catalogo->stato == 1)? "<i style='color:green;' class='fa fa-check-circle-o'></i><b style='color:green;'> Attivo</b>" : "<i style='color:red;' class='fa fa-exclamation-triangle'></i><b style='color:red;'> Sospeso</b>" ; ?></td>
                                <td><?php echo $catalogo->anno; ?></td>
                                <td><form action="" method="POST">
                                        <a style="color: blue;" href="#modale_cataloghi" name="modifica_cat"
                                           data-record-id="<?php echo $catalogo->id; ?>"
                                           data-record-anno="<?php echo $catalogo->anno; ?>"
                                           data-record-stato="<?php echo $catalogo->stato; ?>"
                                           data-record-catalogo="<?php echo $catalogo->nome_catalogo; ?>"
                                           data-record-descrizione="<?php echo $catalogo->breve_desc;  ?>"
                                           data-record-img="<?php echo $catalogo->image;  ?>"
                                           data-record-link="<?php echo $catalogo->link;  ?>"
                                           data-record-posizione="<?php echo $catalogo->posizione  ?>"
                                           data-toggle="modal"
                                           data-target="#modale_cataloghi">
                                            <i class="fa fa-edit"></i> Modifica
                                        </a>
                                        <input name="idcatalogo" type="hidden" value="<?php echo $catalogo->id;  ?>" />
                                        <input name="nomecatalogo" type="hidden" value="<?php echo $catalogo->nome_catalogo;  ?>" />
                                        <input name="statocatalogo" type="hidden" value="<?php echo $catalogo->stato;  ?>" />
                                        <input name="annocatalogo" type="hidden" value="<?php echo $catalogo->anno;  ?>" />
                                        <input name="desCat" type="hidden" value="<?php echo $catalogo->breve_desc;  ?>"  />
                                        <input name="imgCat" type="hidden" value="<?php echo $catalogo->image;  ?>" />
                                        <input name="linkCat" type="hidden" value="<?php echo $catalogo->link;  ?>"  />
                                        <input name="posCat" type="hidden" value="<?php echo $catalogo->posizione;  ?>"  />
                                    </form>
                                </td>
                                <td>
                                    <a href="#" data-record-id="<?php echo $catalogo->id; ?>" data-record-title="<?php echo $catalogo->nome_catalogo; ?>" data-toggle="modal" data-target="#cancella-catalogo"><i style="color: red;" class="fa fa-trash-o"> Elimina</i></a>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
                <!-- END table-responsive-->
            </div>
        </div>
    </div>





    <?php
    /* MODIFICO CATEGORIA */
    if (isset($_GET['modifica'])) {
        include "aggiorna_catalogo.php";
    }

    if (isset($_POST['aggiorna_catalogo'])){
        $agg_catalogo = cataloghi::seleziona_per_id($_POST['catalogo_id']);
        $agg_catalogo->nome_catalogo = $_POST['catalogo_nome'];
        $agg_catalogo->stato = isset($_POST['stato_modale']);
        $agg_catalogo->anno = $_POST['anno_catalogo'];
        $agg_catalogo->breve_desc = $_POST['desc_cat'];
        $agg_catalogo->link = $_POST['url_cat'];
        $agg_catalogo->posizione = $_POST['posizione'];

        echo (!empty($_FILES['file_agg']))? "true" : "false";


        (!empty($agg_catalogo->image AND $agg_catalogo->image == $_FILES['file_agg']))? $agg_catalogo->rimuoviImg($agg_catalogo->image) : NULL;
        if (!empty($_FILES['file_agg'])){
            $agg_catalogo->impostazione_file_catalogo($_FILES['file_agg']);
            $agg_catalogo->caricamento_foto_catalogo();
        }

        $agg_catalogo->salva();

        redirect("cataloghi.php?aggiornamento=1");


    }
    if ( isset($_GET['aggiornamento']) && $_GET['aggiornamento'] == 1 ) {
        $msg_instant->success('Catalogo <b>Aggiornato</b> Correttamente');
        $msg_instant->display();
    }



    ?>

</section>


<div id="modale_cataloghi" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Aggiorna Catalogo</h4>
            </div>
            <form action="" method="post" enctype="multipart/form-data">
                <div class="modal-body">
                    <input type="hidden" value="" name="catalogo_id" class="catalogo_id" />
                    <div class="form-group">
                        <label>Aggiorna Catalogo</label>
                        <input id="catalogo_nome" type="text" name="catalogo_nome" placeholder="Inserisci Catalogo" class="form-control catalogo_nome">
                    </div>
                    <div class="form-group">
                        <label>Inserisci Breve Descrizione Catalogo</label>
                        <textarea name="desc_cat" cols="3" placeholder="Inserisci breve descrizione Catalogo"  class="form-control desc_catalogo" required></textarea>
                    </div>
                    <div class="form-group">
                        <label>Link URL</label>
                        <input type="text" name="url_cat" placeholder="Inserisci es. /vacanze-studio" value="" class="form-control urlCat" required>
                    </div>
                    <div class="form-group">
                        <label for="anno_catalogo">Anno Catalogo</label>
                        <br>
                        <td>
                                <input type='text' name='anno_catalogo' id="anno_catalogo" class="form-control anno_catalogo" value=""  />
                                <span></span>
                        </td>
                    </div>
                    <div class="form-group">
                        <label for="imgview">Immagine Attuale</label><br>
                        <img class="imgview img-thumbnail" src="" alt="" width="150px">
                    </div>
                    <div class="form-group">
                        <label>Scegli Nuova Immagine</label>
                        <input type="file" name="file_agg" placeholder="Inserisci breve descrizione Catalogo"  class="form-control" >
                    </div>
                    <div class="form-group">
                        <label>Posizione Catalogo Front-End</label>
                        <select name="posizione" class="form-control" required>
                            <?php for($i=1; $i<=100; $i++): ?>
                                <option class="posCat" value="<?php echo $i ?>" required><?php echo $i ?></option>
                            <?php endfor; ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="stato">Stato</label>
                        <br>
                        <td>
                            <label class='switch text-center'>
                                <input type='checkbox' name='stato_modale' id="stato_modale" class="stato_catalogo" value=""  />
                                <span></span>
                            </label>
                        </td>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" name="annulla" class="btn btn-default" data-dismiss="modal">Cancella</button>
                    <button type="submit" name="aggiorna_catalogo" class="btn btn-primary agg_catalogo">Salva Catalogo</button>
                </div>
            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<?php include 'modale_canc_catalogo.php'; ?>

<?php include 'includes/footer.php'; ?>

<script>
    $(document).ready(function () {
        $('#modale_cataloghi').on('show.bs.modal', function (e) {
            var data = $(e.relatedTarget).data();

            String.prototype.hexEncode = function(){
                var hex, i;

                var result = "";
                for (i=0; i<this.length; i++) {
                    hex = this.charCodeAt(i).toString(16);
                    result += ("000"+hex).slice(-4);
                }

                return result
            };

            String.prototype.hexDecode = function(){
                var j;
                var hexes = this.match(/.{1,4}/g) || [];
                var back = "";
                for(j = 0; j<hexes.length; j++) {
                    back += String.fromCharCode(parseInt(hexes[j], 16));
                }

                return back;
            };

            var path = 'http://' + document.location.hostname + '/images/cataloghi/';
            path.toString();
            path.hexEncode().hexDecode();
            var imgCat = $(e.relatedTarget).data('recordImg');
            $(e.currentTarget).find('input[name="imgCat"]').val(imgCat);
            (imgCat == "")? path = "http://placehold.it/350x350text=immagine" : path;
            $('.imgview').attr('src', path + imgCat);

        });
    });

    $('#tabCataloghi').dataTable({
        "language": {
            "url": "js/datatables/traduzione/Italian.json"
        }
    });

</script>

