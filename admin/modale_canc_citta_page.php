<?php
/**
 * Created by Giuseppe Alessandro De Blasio.
 * User: Giuseppe Alessandro De Blasio
 * Date: 06/10/2016
 * Time: 11:01
 */
?>

    <div class="modal fade" id="cancella-citta-page" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title" id="myModalLabel">Conferma Eliminazione</h4>
                </div>
                <div class="modal-body">
                    <p>Stai per eliminare la nazione <b><i class="title"></i></b>, questa procedura è irreversibile.</p>
                    <p>Sei sicuro di vuoler procedere?</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Annulla</button>
                    <button type="button" name="elimina" value="<?php echo $nazioni_page->id; ?>" class="btn btn-danger btn-ok">Cancella</button>
                </div>
            </div>
        </div>
    </div>

