<?php include 'includes/header.php'; ?>
<?php include 'includes/modale_foto_utenti.php'; ?>

<?php if(!$sessione->loggato()){ redirect('accedi.php');} ?>

        <!-- Barra di Navigazione -->
        <?php include ("includes/navbar.php") ?>
        
        <?php include ("includes/aside.php") ?>
        <!-- End aside-->
      
        <?php include ("includes/aside_utenti.php") ?>

    <?php
    
    $pagina = !empty($_GET['immagine']) ? (int)$_GET['immagine'] : 1;
    
    $elementi_per_pagina = 2;
    
    $elementi_totali = Foto::conta_elementi();
    
    $paginazione = new Paginazione($pagina, $elementi_per_pagina, $elementi_totali);
    
    $sql = "SELECT * FROM foto ";
    $sql.= "LIMIT {$elementi_per_pagina} ";
    $sql.= "OFFSET {$paginazione->offset()}";
    $fotografie = Foto::cerca_con_query($sql);
    
    
    
    
    if(isset($_POST['salva'])){
        if($utente){
            $utente->nome = $utente->pulisci($_POST['nome']);
            $utente->cognome = $utente->pulisci($_POST['cognome']);
            $utente->email = $utente->pulisci($_POST['email']);

            if(empty($_POST['password'])){
                NULL;
            }  else {
                $utente->password = trim($utente->password_crypt($_POST['password']));
            }
            if(empty($_FILES['immagine_utente'])){
                $utente->salva();
                echo '<div data-toggle="notify" data-onload data-message="Il profilo è stato <b>aggiornato</b> correttamente!!" data-options="{&quot;status&quot;:&quot;success&quot;, &quot;pos&quot;:&quot;bottom-right&quot;}" class="hidden-xs"></div>';
            } else{
                if(isset($_FILES['immagine_utente']) && !empty($_POST['titolo'])){
                    $foto = new Foto();
                    $foto->titolo = $utente->pulisci($_POST['titolo']);
                    $foto->data_immagine = date('d-m-Y H:i:s');
                    $foto->impostazione_file_foto($_FILES['immagine_utente']);
                    $foto->salva();
                    redirect("profilo.php");
                }
                
                $utente->impostazione_file($_FILES['immagine_utente']);
                $utente->caricamento_foto();
                $utente->salva();
                echo '<div data-toggle="notify" data-onload data-message="Il profilo è stato <b>aggiornato</b> correttamente!!" data-options="{&quot;status&quot;:&quot;success&quot;, &quot;pos&quot;:&quot;bottom-right&quot;}" class="hidden-xs"></div>';
                
            }
            if($utente->salva() == TRUE)
            {
                echo '<div data-toggle="notify" data-onload data-message="Il profilo è stato <b>aggiornato</b> correttamente!!" data-options="{&quot;status&quot;:&quot;success&quot;, &quot;pos&quot;:&quot;bottom-right&quot;}" class="hidden-xs"></div>';
            }
        }
    }
    
  /*  if(isset($_POST['cancella_foto'])){
        $foto->cancella_foto_utente();
        redirect('profilo.php');
    }*/
    
    ?>

        <br>
        <section class="main-content">
            <div class="row">
            <div class="col-sm-3">
             <div class="row">
               <div data-toggle="portlet" class="col-lg-10 ui-sortable">
                  <!-- START widget-->
                  <div class="panel widget" style="opacity: 1; z-index: 0;">
                     <div class="portlet-handler">
                        <div class="row row-table row-flush">
                           <div class="col-xs-4 bg-info text-center">
                              <em class="fa fa-users fa-2x"></em>
                           </div>
                           <div class="col-xs-8">
                              <div class="panel-body text-center">
                                 <h4 class="mt0">10k</h4>
                                 <p class="mb0 text-muted">VISITORS</p>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
                  <!-- END widget-->
               </div>
               <div data-toggle="portlet" class="col-lg-10 ui-sortable">
                  <!-- START widget-->
                  
                  <!-- END widget-->
               <div class="panel widget" style="opacity: 1; z-index: 0;">
                     <div class="portlet-handler">
                        <div class="row row-table row-flush">
                           <div class="col-xs-4 bg-danger text-center">
                              <em class="fa fa-music fa-2x"></em>
                           </div>
                           <div class="col-xs-8">
                              <div class="panel-body text-center">
                                 <h4 class="mt0">100%</h4>
                                 <p class="mb0 text-muted">VOLUME</p>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div></div>
               <div data-toggle="portlet" class="col-lg-10 ui-sortable">
                  <!-- START widget-->
                  <div class="panel widget">
                     <div class="portlet-handler">
                        <div class="row row-table row-flush">
                           <div class="col-xs-4 bg-inverse text-center">
                              <em class="fa fa-code-fork fa-2x"></em>
                           </div>
                           <div class="col-xs-8">
                              <div class="panel-body text-center">
                                 <h4 class="mt0">150</h4>
                                 <p class="mb0 text-muted">FORKS</p>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
                  <!-- END widget-->
               </div>
               <div data-toggle="portlet" class="col-lg-10 ui-sortable">
                  <!-- START widget-->
                  <div class="panel widget">
                     <div class="portlet-handler">
                        <div class="row row-table row-flush">
                           <div class="col-xs-4 bg-green text-center">
                              <em class="fa fa-inbox fa-2x"></em>
                           </div>
                           <div class="col-xs-8">
                              <div class="panel-body text-center">
                                 <h4 class="mt0">10</h4>
                                 <p class="mb0 text-muted">NEW MESSAGES</p>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
                  <!-- END widget-->
               </div>
            </div>
            </div>
            <div class="col-xs-5">
                <div class="panel widget">
                     <div class="half-float">
                         <img src="immagini/bg3.jpg" class="img-responsive">
                         <div class="half-float-bottom" id="panel-anim-swing">
                            <img src="<?php $utente = Utente::seleziona_per_id($_SESSION['id_utente']); echo $utente->percorso_immagine_e_plecholder(); ?>" style="width: 120px; height: 120px;" alt="Image" class="img-thumbnail img-circle"  data-toggle="play-animation" data-target="#panel-anim-swing" data-play="rotateIn">
                        </div>
                     </div>
                     <div class="panel-body text-center">
                        <h3 class="m0"><?php echo $utente->nome ." ". $utente->cognome;?></h3>
                        <p class="text-muted">Creative Director</p>
                        <p>Proin metus justo, commodo in ultrices at, lobortis sit amet dui. Fusce dolor purus, adipiscing a tempus at, gravida vel purus.</p>
                     </div>
                     <div class="panel-body text-center bg-gray-lighter">
                        <div class="row row-table">
                           <div class="col-xs-4">
                              <h3 class="m0">700</h3>
                              <p class="m0">Following</p>
                           </div>
                           <div class="col-xs-4">
                              <h3 class="m0">1500</h3>
                              <p class="m0">Likes</p>
                           </div>
                           <div class="col-xs-4">
                              <h3 class="m0">300</h3>
                              <p class="m0">Photos</p>
                           </div>
                        </div>
                     </div>
                  </div>
                </div>
        <div class="col-xs-4">
            <div class="panel widget">
                <div class="row row-table row-flush">
                    <div class="col-xs-8">
                            <img src="immagini/bg1.jpg" class="img-responsive">
                        </div>
                        <div class="col-xs-4 align-middle bg-warning">
                           <div class="text-center">
                              <div class="text-lg mt0">24°</div>
                              <p>Sunny</p>
                              <em class="fa fa-sun-o fa-2x"></em>
                           </div>
                        </div>
                    </div>
                 </div>
                 
                <div class="panel widget">
                     <div class="row row-table row-flush">
                        <div class="col-xs-5">
                            <img src="immagini/bg2.jpg" class="img-responsive">
                        </div>
                        <div class="col-xs-7 align-middle p-lg">
                           <p>
                              <span class="text-lg">12</span>May</p>
                           <p>
                              <strong>NEW EVENT HERE</strong>
                           </p>
                           <p>Donec posuere neque in elit luctus tempor consequat enim egestas. Nulla dictum egestas leo at lobortis.</p>
                        </div>
                     </div>
                  </div>
        </div>
            <div class="col-xs-12">
                <div class="row">
                    <div class="col-xs-3">
                <div class="panel panel-warning">
                     <div class="panel-heading">Ultime News
                        <a title="" class="pull-right" href="javascript:void(0);" data-original-title="Collapse Panel" data-toggle="tooltip" data-perform="panel-collapse">
                           <em class="fa fa-minus"></em>
                        </a>
                        <a title="" class="pull-right" href="javascript:void(0);" data-original-title="Refresh Panel" data-toggle="tooltip" data-perform="panel-refresh">
                           <em class="fa fa-refresh"></em>
                        </a>
                     </div>
                     <div class="panel-wrapper collapse in" style="height: auto;">
                        <div class="panel-body">
                            <p>Click on the refresh icon to see how it triggers a refresh-event while it shows a spinner</p>
                        </div>
                     </div>
                  </div>
                  </div>
                <div class="col-xs-5">
                <div class="panel panel-info">
                     <div class="panel-heading">Pannello Gestione Profilo</div>
                     <div class="panel-body">
                        <div class="container col-xs-12">
                            <div class="col-xs-4"></div>
                            
                            <h4 class="page-header">Gestione Profilo</h4>
                                <div class="text-center utente_immagine_box">
                                    <a href="#" data-toggle="modal" data-target="#libreria-foto"><img class="img-thumbnail img-responsive" style="width: 240px; height: 240px;" src="<?php echo $utente->percorso_immagine_e_plecholder(); ?>"></a>
                                </div>
                            <br>
                            <form class="form-horizontal" role="form" action="" method="POST" enctype="multipart/form-data" data-parsley-validate>
                              
                                <input name="utente-id" type="hidden" disabled="disabled" value="<?php echo $utente->id; ?>" href="<?php echo $utente->percorso_immagine_e_plecholder(); ?>" />
                            
                            <h5 class="page-header">Modifica Immagine Profilo</h5>
                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="titolo">Nome Immagine</label>
                                <div class="col-sm-10">
                                    <input  class="form-control" id="nome" name="titolo" type="text" <?php if(isset($_FILES['immagine_utente'])){ echo "required='required'";}?> />
                                </div>
                            </div>
                                
                             <div class="form-group">
                                <label class="col-sm-2 control-label" for="immagine_utente">Carica Immagine</label>
                                <div class="col-sm-10">
                                    <input type="file" name="immagine_utente" class="form-control">
                                </div>
                             </div>
                              
                             <h5 class="page-header">Modifica Dati Personali</h5>
                              <div class="form-group">
                                <label class="col-sm-2 control-label" for="username">Username</label>
                                <div class="col-sm-10">
                                    <input class="form-control" id="username" name="username" type="text" disabled="" value="<?php echo $utente->username; ?>">
                                </div>
                              </div>
                              <div class="form-group">
                                <label class="col-sm-2 control-label" for="nome">Nome</label>
                                <div class="col-sm-10">
                                    <input class="form-control" id="nome" name="nome" type="text" value="<?php echo $utente->nome; ?>">
                                </div>
                              </div>
                              <div class="form-group">
                                <label class="col-sm-2 control-label" for="cognome">Cognome</label>
                                <div class="col-sm-10">
                                    <input class="form-control" id="cognome" name="cognome" type="text" value="<?php echo $utente->cognome; ?>">
                                </div>
                              </div>
                              <div class="form-group">
                                <label class="col-sm-2 control-label" for="email">Email</label>
                                <div class="col-sm-10">
                                    <input class="form-control" id="email" name="email" type="email" value="<?php echo $utente->email; ?>">
                                </div>
                              </div>
                              <div class="form-group">
                                <label class="col-sm-2 control-label" for="password">Password</label>
                                <div class="col-sm-10">
                                    <input class="form-control" id="password" name="password" type="password">
                                </div>
                              </div>
                                <div class="col-xs-10 pull-right">
                                    <div class="alert alert-info alert-dismissable">
                                        <button class="close" aria-hidden="true" type="button" data-dismiss="alert">×</button>Se lasciato libero il campo password <b>NON cambia</b>.
                                    </div>
                                </div>
                                  <button type="submit"  name="salva" class="btn btn-labeled btn-info pull-right">
                                <span class="btn-label"><i class="fa fa-save"></i></span>Salva</button>
                              </form>                                
                        </div>
                        </div>
                     </div>
                </div>
                    
                    
            <div class="col-xs-4">
                <h4 class="page-header">Le tue ultime immagini</h4>
                    <?php //$fotografie = Foto::seleziona_tutti(); ?>
                
                <?php foreach ($fotografie as $foto): ?>
                <div class="row-masonry row-masonry-md-1 row-masonry-sm-2">
                    <div class="col-masonry">
                        <img class="img-thumbnail img-responsive" src="<?php echo $foto->percorso_immagine_utente(); ?>">
                        <form action="" method="GET">
                            <a class="cancella_messaggio btn btn-danger" href="elimina_foto.php?id=<?php echo $foto->id; ?>">Elimina Foto <i class="fa fa-trash-o"></i> </a>
                        </form>
                    </div>
                </div>
                <?php endforeach;?>
                    <ul class="pagination pull-right">
                        <?php
                            if($paginazione->pagine_totali() > 1){
                                if($paginazione->e_prima()){
                                        echo "<li><a href='profilo.php?immagine={$paginazione->prima()}'>Prima</a></li>";
                                    }
                                    for ($i = 1; $i <= $paginazione->pagine_totali(); $i++){
                                        if($i == $paginazione->pagina_corrente){
                                            echo "<li class='active'><a href='profilo.php?immagine={$i}'>{$i}</a></li>";
                                        } else {
                                            echo "<li><a href='profilo.php?immagine={$i}'>{$i}</a></li>";
                                        }
                                    }

                                    if($paginazione->e_dopo()){ 
                                        echo "<li><a href='profilo.php?immagine={$paginazione->dopo()}'>Dopo</a></li>";
                                    }                                      
                                }
                                  
                        ?>
                    </ul>                    
            </div>
        </div>
            </div>
            </section>
        
        
<?php include 'includes/footer.php'; ?>