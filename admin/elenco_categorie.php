<?php
/**
 * Created da Giuseppe Alessandro De Blasio.
 * User: Giuseppe Alessandro De Blasio
 * Date: 09/05/17
 * Time: 18:16
 */

include 'includes/header.php'; ?>
<?php if(!$sessione->loggato()){ redirect('accedi.php');} ?>

    <!-- Barra di Navigazione -->
<?php include ("includes/navbar.php") ?>

<?php include ("includes/aside.php") ?>

<?php $categorie = CategoriePage::seleziona_tutti(); ?>

<br>
    <section class="main-content">
        <h1 class="page-header">Elenco Pagine Categorie</h1>
        <div class="panel panel-default">
            <div class="panel-heading">Elenco Pagine
                <a title="" class="pull-right" href="javascript:void(0);" data-original-title="Collapse Panel" data-toggle="tooltip" data-perform="panel-collapse">
                    <em class="fa fa-minus"></em>
                </a>
            </div> <!-- panel-heading -->

            <form action="" method="POST">
                <div class="panel-wrapper collapse in" style="height: auto;"><div class="table-responsive">
                        <table class="table table-bordered table-hover">
                            <thead>
                            <tr>
                                <th>Id</th>
                                <th>Titolo</th>
                                <th>Categoria</th>
                                <th>Modifica</th>
                                <th>Elimina</th>
                                <th>Vedi Pagina</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php foreach ($categorie as $categoria): ?>
                                <tr>
                                    <td><?php echo $categoria->id; ?></td>
                                    <td><?php echo strip_tags($categoria->titolo); ?></td>
                                    <td><?php $selCat = CategoriePagine::seleziona_per_id($categoria->id_cat); echo $selCat->titolo_categoria; ?></td>
                                    <td><a style="color: rgba(83, 86, 93, 0.68)" href="modifica_categoria.php?id=<?php echo $categoria->id; ?>"><i style="color: rgb(5, 25, 93)"; class=" fa fa-edit"></i> Modifica</a></td>
                                    <td><a class="" href="#" data-record-id="<?php echo $categoria->id; ?>"  data-toggle="modal" data-target="#cancella-pagine"><i style="color: #e90811;" class="fa fa-trash-o"></i> Elimina</a></td>
                                    <td><a href="<?php echo "/".$categoria->urlFriCat;  ?>" target="_blank">Vedi Pagina</a></td>
                                </tr>
                            <?php endforeach; ?>
                            </tbody>
                        </table>
                        <div class="panel-footer">
                            <div class="row">
                        
                            </div>
                        </div><!-- panel-footer -->
                    </div><!-- table-responsive -->
                </div><!-- panel-wrapper collapse in -->
            </form>
        </div> <!-- panel panel-default -->


    </section>




<?php include 'includes/footer.php'; ?>
