<?php
/**
 * Created da Giuseppe Alessandro De Blasio.
 * User: Giuseppe Alessandro De Blasio
 * Date: 17/10/16
 * Time: 11:36
 */
?>

<?php include 'includes/init.php'; ?>

<?php if(!$sessione->loggato()){ redirect('accedi.php');} ?>

<?php

if(empty($_GET['id'])){
    redirect('elenco_centri.php');
}

$carat_centro = caratCentro::seleziona_per_id($_GET['id']);

if($carat_centro){
    $sessione->messaggio('<div data-toggle="notify" data-onload data-message="Caratteristica <b>Eliminata</b> Correttamente" data-options="{&quot;status&quot;:&quot;success&quot;, &quot;pos&quot;:&quot;bottom-right&quot;}" class="hidden-xs"></div>');
    $carat_centro->cancella();
    //redirect("categorie_post.php");
}
