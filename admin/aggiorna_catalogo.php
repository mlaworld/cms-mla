<?php
    
    if(isset($_POST['annulla'])){
        redirect('cataloghi.php');
    }
    
    $catalogo = Cataloghi::seleziona_per_id($_GET['modifica']);
    if(isset($_POST['aggiorna_catalogo'])){
        $catalogo->nome_catalogo = $_POST['catalogo_aggiornato'];
        $catalogo->anno = $_POST['anno'];
        if(isset($_POST['stato'])){
            $catalogo->stato = $_POST['stato'] = 1;
        } else {
            $catalogo->stato = $_POST['stato'] = 0;
        }
        $catalogo->salva();
        echo '<div data-toggle="notify" data-onload data-message="Catalogo <b>Aggiornato</b> Correttamente" data-options="{&quot;status&quot;:&quot;success&quot;, &quot;pos&quot;:&quot;bottom-right&quot;}" class="hidden-xs"></div>';
        redirect('cataloghi.php');
    }
?>
        <div class="col-md-4 modale" id="modal-transparent" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="panel panel-default" style="background-color: #e2e2e2">
                <div class="panel-heading">Aggiorna il Catalogo</div>
                     <div class="panel-body">
                         <form role="form" method="POST" action="">
                           <div class="form-group">
                              <label>Aggiorna il Catalogo</label>
                              <input type="text" name="catalogo_aggiornato" placeholder="Inserisci Categoria" class="form-control" value="<?php echo $catalogo->nome_catalogo; ?>">
                           </div>
                             <div class="form-group">
                                 <label for="anno">Anno</label>
                                 <input type="text" name="anno" value="<?php echo $catalogo->anno; ?>" placeholder="Inserisci Anno Catalogo" class="form-control" required/>
                             </div>
                             <div class="form-group">
                                 <label for="statoß">Stato</label>
                                 <br>
                                 <td>
                                     <label class='switch text-center'>
                                         <input type='checkbox' name='stato' <?php echo ($catalogo->stato == "1")? "checked=''" : "" ; ?> value='<?php echo $catalogo->stato; ?>' >
                                         <span></span>
                                     </label>
                                 </td>
                             </div>
                                <button type="submit" name="aggiorna_catalogo" class="btn btn-sm btn-primary pull-right">Aggiorna</button>
                                <button type="submit" name="annulla" class="btn btn-sm btn-danger">Annulla</button>
                        </form>
                     </div>
            </div>
        </div>

