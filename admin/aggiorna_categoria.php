<?php
    
    if(isset($_POST['annulla'])){
        redirect('categorie_post.php');
    }
    
    $categorie = CategoriePagine::seleziona_per_id($_GET['modifica']);
    if(isset($_POST['aggiorna_categoria'])){
        $categorie->titolo_categoria = $_POST['categoria_aggiornata'];
        $urlRewrite = rewriteUrl(trim(strtolower($categorie->titolo_categoria)));
        $categorie->urlFriendly = $urlRewrite;
        if($categorie->salva()){
            $sessione->messaggio('<div data-toggle="notify" data-onload data-message="Categoria <b>Aggiornata</b> Correttamente" data-options="{&quot;status&quot;:&quot;success&quot;, &quot;pos&quot;:&quot;bottom-right&quot;}" class="hidden-xs"></div>');
            redirect('categorie_post.php');
        }
    }
?>
        <div class="col-md-4">       
                <div class="panel panel-default">
                     <div class="panel-heading">Aggiorna la Categorie per le Pagine</div>
                     <div class="panel-body">
                         <form role="form" method="POST" action="">
                           <div class="form-group">
                              <label>Aggiorna Categoria</label>
                              <input type="text" name="categoria_aggiornata" placeholder="Inserisci Categoria" class="form-control" value="<?php echo $categorie->titolo_categoria; ?>">
                           </div>
                                <button type="submit" name="aggiorna_categoria" class="btn btn-sm btn-default pull-right">Aggiorna</button>
                                <button type="submit" name="annulla" class="btn btn-sm btn-danger">Annulla</button>
                        </form>
                     </div>
                  </div>
        </div>

