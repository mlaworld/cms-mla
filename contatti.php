<?php
/**
 * Created da Giuseppe Alessandro De Blasio.
 * User: Giuseppe Alessandro De Blasio
 * Date: 20/09/17
 * Time: 10:07
 */

include 'includes/header.php';


//echo $message = file_get_contents('http://cms.mlaworld.com/admin/templates/contatti.html');




//$email->Debugoutput = function($str, $level) {echo "debug level $level; message: $str";};
//var_dump($email);

?>
    </head>

    <body>

    <header>

        <?php include 'includes/menu.php'; ?>

    </header>
    <br><br><br>
    <section>
        <div class="contact-us">
            <div class="container">
                <div class="contact-form">
                    <div class="row">
                        <div class="col-sm-7">
                            <div id="alert"></div>
                            <?php if (!empty($errori))
                            {
                                echo '<div class="alert alert-danger" role="alert">'. '<ul>';
                                foreach ($errori as $errore){
                                    echo "<li>" . $errore . "</li>";
                                }
                                echo "</ul></div>";
                            }
                            ?>
                            <form id="contatti" onsubmit="return false">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="form_nome">Nome *</label>
                                                <input data-validetta="required,minLength[1],maxLength[14]" id="form_nome" type="text" name="nome" class="form-control" placeholder="Inserire il proprio nome *" data-vd-message-required="Per piacere inserire il proprio nome!" value="<?php echo (isset($_POST['nome']))? $_POST['nome']:null;  ?>"/>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="form_cognome">Cognome *</label>
                                                <input data-validetta="required,minLength[1],maxLength[14]" id="form_cognome" type="text" name="cognome"
                                                       class="form-control" placeholder="Inserire il proprio cognome *" value="<?php echo (isset($_POST['cognome']))? $_POST['cognome']:null;  ?>"
                                                       data-vd-message-required="Per piacere inserire il proprio cognome!" />
                                            </div>
                                        </div>
                                    </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="form_dataNascita">Data di Nascita *</label>
                                                    <input data-date-inline-picker="true" data-validetta="required" id="form_dataNascita" type="date" name="dataNascita"
                                                           class="form-control" placeholder="Inserire la propria data di nascita *"
                                                           data-vd-message-required="Per piacere inserire la propria data di nascita!"
                                                           value="<?php echo (isset($_POST['dataNascita']))? $_POST['dataNascita']:null;  ?>"/>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="form_indirizzo">Indirizzo *</label>
                                                    <input data-validetta="required,minLength[1],maxLength[100]" id="form_indirizzo" type="text" name="indirizzo"
                                                           class="form-control" placeholder="Inserire il proprio indirizzo *"
                                                           data-vd-message-required="Per piacere inserire il proprio indirizzo!"
                                                           value="<?php echo (isset($_POST['indirizzo']))? $_POST['indirizzo']:null;  ?>" />
                                                </div>
                                            </div>
                                        </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="form_citta">Città *</label>
                                                <input data-validetta="required,minLength[1],maxLength[10]" id="form_citta" type="text" name="citta" class="form-control"
                                                       placeholder="Inserire la propia città *" data-vd-message-required="Per piacere inserire la propria città!"
                                                       value="<?php echo (isset($_POST['citta']))? $_POST['citta']:null;  ?>" />
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="form_email">Email *</label>
                                                <input data-validetta="required,email" id="form_email" type="email" name="email" class="form-control"
                                                       placeholder="Inserire la propria email *" data-vd-message-required="Per piacere inserire la propria email!"
                                                       value="<?php echo (isset($_POST['email']))? $_POST['email']:null;  ?>"/>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="form_telefono">Telefono </label>
                                                <input data-validetta="number" id="form_telefono" type="tel" name="telefono" class="form-control"
                                                       placeholder="Per piacere inserire il proprio numero di telefono " data-vd-message-required="Per piacere inserire il proprio numero di telefono!"
                                                       value="<?php echo (isset($_POST['telefono']))? $_POST['telefono']:null;  ?>" />
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="form_phone">Cellulare *</label>
                                                <input data-validetta="required,number" id="form_cellulare" type="tel" name="cellulare" class="form-control"
                                                       placeholder="Per piacere inserire il proprio numero di cellulare *" data-vd-message-required="Per piacere inserire il proprio numero di cellulare!"
                                                       value="<?php echo (isset($_POST['cellulare']))? $_POST['cellulare']:null;  ?>" />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label for="form_messaggio">Messaggio *</label>
                                                <textarea data-validetta="required,minLength[1]" id="form_messaggio" name="messaggio" class="form-control"
                                                          placeholder="Message *" rows="4" data-vd-message-required="Per piacere inserire il contenuto del messaggio!" >
                                                     <?php echo (isset($_POST['messaggio']))? $_POST['messaggio']:null;  ?>
                                                </textarea>
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <div class="checkbox">
                                                    <label>
                                                        <input type="checkbox" id="terminiCondizioni" data-validetta="required" data-vd-message-required="Accettare i termini e le condizioni!" name="termini_condizioni" <?php echo (isset($_POST['termini_condizioni']))? "checked='checked' ": ''; ?> >Accetta l'informativa sulla <a target="_blank" href="http://www.mlaworld.com/privacy/">privacy</a>
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <input type="submit" id="invioContatti" name="invioContatti" class="btn btn-black" value="Contattaci">
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <br>
                                            <small class="text-muted"><strong>*</strong> I campi contrassegnati con * sono obbligatori.
                                            </small>
                                        </div>
                                    </div>
                            </form>

                        </div>
                        <div class="col-sm-5">
                            <h2>Contatta i nostri uffici</h2>

                            <h4>Milano</h4>

                            <p>Via Vincenzo Monti, 34 – 20123 (MI)</p>
                            <p>tel: (+39) 024695479</p>
                            <p>fax: (+39) 024695489</p>

                            <h4>Napoli</h4>

                            <p>Corso Vittorio Emanuele, 114 – 80121 (NA)</p>
                            <p>tel: (+39) 0817614900</p>
                            <p>fax: (+39) 0817614165</p>

                            <h4>Orari di ufficio</h4>

                            <p>Dal Lunedì al Venerdi dalle ore 9:30 alle ore 13:00 e</p>
                            <p>dalle ore 15:00 alle ore 18:30</p>
                            <p>il Sabato dalle ore 9:30 alle ore 12:30</p>
                            <p>Domenica – Chiuso</p>

                            <h4>Studenti INPS</h4>

                            <p>Esclusivamente dal Martedì al Giovedì</p>
                            <p>dalle ore 10:00 alle ore 12:00</p>
                            <p>Sabato, Domenica – Chiuso</p>
                            <p>Per informazioni è preferibile scrivere a inpsieme@mlaworld.com</p>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </section>
    <br><br><br>


<?php include 'includes/test-inglese-footer.php'; ?>
<?php include 'includes/cataloghi-footer.php'; ?>
<?php include 'includes/40anni-footer.php'; ?>
<?php include 'includes/blog-footer.php'; ?>
<?php include 'includes/footer.php'; ?>