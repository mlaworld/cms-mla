## MLA CMS

Progetto nuovo sito per MLAWORLD.COM

## Codice Utilizzato

**HTML5**, **CSS**, **PHP**, **JavaScript**

## Installazione

Al momento **non** prevista

## Contributors

**Fabiana Turco** - Graphic Design
**Baldi Andrea** - Web Designer (HTML - CSS)
**Giuseppe A. De Blasio** - Programmer(PHP - Javascript - jQuery)

## License

The Golden Globe Srl
 